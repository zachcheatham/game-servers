Game Servers for Android
===
A complete solution to administrating or keeping tabs on your favorite multiplayer game servers.

Screenshots
---
<div style="flex-direction: row; flex-wrap: wrap; justify-content: space-between; align-items: flex-start">
<img src="screenshots/server_list.png" width="20%">
<img src="screenshots/server_details.png" width="20%">
<img src="screenshots/players.png" width="20%">
<img src="screenshots/player_commands.png" width="20%">
<img src="screenshots/command_dialog.png" width="20%">
<img src="screenshots/console.png" width="20%">
</div>

Supported Games
---
- All Source Engine Games
  - Team Fortress 2
  - Counter-Strike: Global Offensive
  - Counter-Strike: Source
  - Day of Defeat: Source
  - Left4Dead 2
  - Garry's Mod
  - Half-Life 2 Deathmatch
  - *Third-Party Source Engine games that I haven't tested.*
- All GoldSrc Games
  - Team Fortress Classic
  - Counter-Strike 1.6
  - Day of Defeat 1.6
  - Half-Life Deathmatch
- Minecraft

Planned Support
---
- 7 Days to Die
- ARMA
- ARK
- Battlefield
- Call of Duty
- DayZ
- Minecraft: Pocket Edition
- Killing Floor
- Rust
- Squad
- The Forest
- Unturned

Features
---
- Listing of all of your saved servers with quick data such as the player count and current map.
- Detailed server information retrieved via query protocol and RCON retrival.
- Import / Export servers using the [HLSW](http://www.hlsw.net/) .sslf format.
- Import servers from `steam://connect` links
- RCON Support for most games including log_address support in Source games.
- Automatic parsing of servers' avalible RCON commands in order to create graphical interfaces for all commands.

## Planned Features
- Better RCON navigation
- Homescreen Widgets
- Background notifications such as player connects, disconnects, chat messages, player count thresholds, etc
- WearOS *Maybe*
