package me.zachcheatham.gameservers.server.admin;

import net.lachlanmckee.timberjunit.TimberTestRule;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import me.zachcheatham.gameservers.server.MockServer;

public class ULXCommandParsingTest
{
    private final MockServer server = new MockServer();
    private final ULXServerAdministration serverAdministration = new ULXServerAdministration(server);

    @Rule
    public TimberTestRule logRule = TimberTestRule.logAllAlways();

    @Before
    public void setRCONValuesAndParse()
    {
        server.setRCONResponse("00:09:57 ULX Help:\n" +
                               "         If a command can take multiple targets, it will usually " +
                               "let you use the keywords '*' for target\n" +
                               "         all, '^' to target yourself, '@' for target your picker," +
                               " '$<userid>' to target by ID (steamid,\n" +
                               "         uniqueid, userid, ip), '#<group>' to target users in a " +
                               "specific group, and '%<group>' to target\n" +
                               "         users with access to the group (inheritance counts). IE," +
                               " ulx slap #user slaps all players who are\n" +
                               "         in the default guest access group. Any of these keywords" +
                               " can be preceded by '!' to negate it.\n" +
                               "         EG, ulx slap !^ slaps everyone but you.\n" +
                               "         You can also separate multiple targets by commas. IE, " +
                               "ulx slap bob,jeff,henry.\n" +
                               "         All commands must be preceded by \"ulx \", ie \"ulx " +
                               "slap\"\n" +
                               "         \n" +
                               "         Command Help:\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Rcon\n" +
                               "         <TAB>o ulx cexec <players> {command} - Run a command on " +
                               "console of target(s). (say: !cexec)\n" +
                               "         <TAB>o ulx ent <classname> [{<flag>:<value>}] - Spawn an" +
                               " ent, separate flag and value with ':'.\n" +
                               "         <TAB>o ulx exec <file> - Execute a file from the cfg " +
                               "directory on the server.\n" +
                               "         <TAB>o ulx luarun {command} - Executes lua in server " +
                               "console. (Use '=' for output)\n" +
                               "         <TAB>o ulx rcon {command} - Execute command on server " +
                               "console. (say: !rcon)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Utility\n" +
                               "         <TAB>o ulx ban <player> [<minutes, 0 for perma: 0<=x, " +
                               "default 0>] [{reason}] - Bans target. (say: !ban)\n" +
                               "         <TAB>o ulx banid <steamid> [<minutes, 0 for perma: 0<=x," +
                               " default 0>] [{reason}] - Bans steamid.\n" +
                               "         <TAB>o ulx debuginfo - Dump some debug information.\n" +
                               "         <TAB>o ulx forcemotd <player> - Opens the MOTD on the " +
                               "target. (say: !forcemotd)\n" +
                               "         <TAB>o ulx freezeprops - Freezes all physics props on " +
                               "the server. (say: !freezeprops)\n" +
                               "         <TAB>o ulx gban <player> [<minutes, 0 for perma: 0<=x, " +
                               "default 0>] [{reason}] - Bans target from all servers. (say: " +
                               "!gban)\n" +
                               "         <TAB>o ulx gbanid <steamid> [<minutes, 0 for perma: " +
                               "0<=x, default 0>] [{reason}] - Bans steamid from all servers.\n" +
                               "         <TAB>o ulx help - Shows this help.\n" +
                               "         <TAB>o ulx kick <player> [{reason}] - Kicks target. " +
                               "(say: !kick)\n" +
                               "         <TAB>o ulx map <map> [<gamemode>] - Changes map and " +
                               "gamemode. (say: !map)\n" +
                               "         <TAB>o ulx noclip [<players, defaults to self>] - " +
                               "Toggles noclip on target(s). (say: !noclip)\n" +
                               "         <TAB>o ulx removeprop - Removes physics prop at " +
                               "crosshair. (say: !removeprop)\n" +
                               "         <TAB>o ulx resettodefaults [<string>] - Resets ALL ULX " +
                               "and ULib configuration!\n" +
                               "         <TAB>o ulx screengrab <player> - Takes a screenshot of a" +
                               " client's screen and opens it in a window. (say: !screengrab)\n" +
                               "         <TAB>o ulx spectate <player> - Spectate target. (say: " +
                               "!spectate)\n" +
                               "         <TAB>o ulx unban <steamid> - Unbans steamid.\n" +
                               "         <TAB>o ulx who - See information about currently online " +
                               "users.\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: TTT\n" +
                               "         <TAB>o ulx addimpair <player> [<Damage: 1<=x<=100, " +
                               "default 25>] [{Reason}] - Damages a player at the beginning of " +
                               "the next round. (say: !addimpair) (opposite: ulx rimpair)\n" +
                               "         <TAB>o ulx addslay <player> [{Reason}] - Slays target " +
                               "and the beginning of the next round. (say: !addslay) (opposite: " +
                               "ulx rslay)\n" +
                               "         <TAB>o ulx addslayid <SteamID> [{Reason}] - Slays target" +
                               " and the beginning of the next round. (say: !addslayid) " +
                               "(opposite: ulx rslayid)\n" +
                               "         <TAB>o ulx spec <player> - Forces a player to/from " +
                               "spectator. (say: !spec) (opposite: ulx unspec)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Voting\n" +
                               "         <TAB>o ulx stopvote - Stops a vote in progress. (say: " +
                               "!stopvote)\n" +
                               "         <TAB>o ulx veto - Veto a successful votemap. (say: " +
                               "!veto)\n" +
                               "         <TAB>o ulx vote <title> {options} - Starts a public vote" +
                               ". (say: !vote)\n" +
                               "         <TAB>o ulx voteaddslay <player> - Starts a vote to add a" +
                               " target to the slay list. (say: !voteaddslay)\n" +
                               "         <TAB>o ulx voteban <player> [<minutes: 0<=x, default " +
                               "1440>] [{reason}] - Starts a public ban vote against target. " +
                               "(say: !voteban)\n" +
                               "         <TAB>o ulx votekick <player> [{reason}] - Starts a " +
                               "public kick vote against target. (say: !votekick)\n" +
                               "         <TAB>o ulx votemap [{map}] - Vote for a map, no args " +
                               "lists available maps. (say: !votemap)\n" +
                               "         <TAB>o ulx votemap2 {map} - Starts a public map vote. " +
                               "(say: !votemap2)\n" +
                               "         <TAB>o ulx votesilentround - Starts a vote to gag " +
                               "everyone in the next round. (say: !votesilentround)\n" +
                               "         <TAB>o ulx votespec <player> - Starts a vote to move a " +
                               "target to spectator. (say: !votespec)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: User Management\n" +
                               "         <TAB>o ulx addgroup <group> [<inherits from>] - Create a" +
                               " new group with optional inheritance.\n" +
                               "         <TAB>o ulx adduser <player> <group> - Add a user to " +
                               "specified group.\n" +
                               "         <TAB>o ulx adduserid <SteamID, IP, or UniqueID> <group> " +
                               "- Add a user by ID to specified group.\n" +
                               "         <TAB>o ulx groupallow <group> <command> [<access tag>] -" +
                               " Add to a group's access.\n" +
                               "         <TAB>o ulx groupdeny <group> <command> - Remove from a " +
                               "group's access.\n" +
                               "         <TAB>o ulx removegroup <group> - Removes a group. USE " +
                               "WITH CAUTION.\n" +
                               "         <TAB>o ulx removeuser <player> - Permanently removes a " +
                               "user's access.\n" +
                               "         <TAB>o ulx removeuserid <SteamID, IP, or UniqueID> - " +
                               "Permanently removes a user's access by ID.\n" +
                               "         <TAB>o ulx renamegroup <current group> <new group> - " +
                               "Renames a group.\n" +
                               "         <TAB>o ulx setgroupcantarget <group> [<target string>] -" +
                               " Sets what a group is allowed to target\n" +
                               "         <TAB>o ulx userallow <player> <command> [<access tag>] -" +
                               " Add to a user's access.\n" +
                               "         <TAB>o ulx userallowid <SteamID, IP, or UniqueID> " +
                               "<command> [<access tag>] - Add to a user's access.\n" +
                               "         <TAB>o ulx userdeny <player> <command> [<remove explicit" +
                               " allow or deny instead of outright denying: 0/1>] - Remove from a" +
                               " user's access.\n" +
                               "         <TAB>o ulx userdenyid <SteamID, IP, or UniqueID> " +
                               "<command> [<remove explicit allow or deny instead of outright " +
                               "denying: 0/1>] - Remove from a user's access.\n" +
                               "         <TAB>o ulx usermanagementhelp - See the user management " +
                               "help.\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Chat\n" +
                               "         <TAB>o ulx asay {message} - Send a message to currently " +
                               "connected admins. (say: @)\n" +
                               "         <TAB>o ulx csay {message} - Send a message to everyone " +
                               "in the middle of their screen. (say: @@@)\n" +
                               "         <TAB>o ulx gag <players> - Gag target(s), disables " +
                               "microphone. (say: !gag) (opposite: ulx ungag)\n" +
                               "         <TAB>o ulx gimp <players> - Gimps target(s) so they are " +
                               "unable to chat normally. (say: !gimp) (opposite: ulx ungimp)\n" +
                               "         <TAB>o ulx mute <players> - Mutes target(s) so they are " +
                               "unable to chat. (say: !mute) (opposite: ulx unmute)\n" +
                               "         <TAB>o ulx pgag <player> - Permanently gags a target. " +
                               "(say: !pgag) (opposite: ulx unpgag)\n" +
                               "         <TAB>o ulx pmute <player> - Permanently mutes a target. " +
                               "(say: !pmute) (opposite: ulx unpmute)\n" +
                               "         <TAB>o ulx psay <player> {message} - Send a private " +
                               "message to target. (say: !p)\n" +
                               "         <TAB>o ulx rename <player> {Name} - Changes the name of " +
                               "a target until they reconnect. (say: !rename)\n" +
                               "         <TAB>o ulx thetime - Shows you the server time. (say: " +
                               "!thetime)\n" +
                               "         <TAB>o ulx tsay {message} - Send a message to everyone " +
                               "in the chat box. (say: @@)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Menus\n" +
                               "         <TAB>o ulx motd - Show the message of the day. (say: " +
                               "!motd)\n" +
                               "         <TAB>o xgui <show, hide, toggle> - Opens and/or closes " +
                               "XGUI. (say: !xgui, !menu) (alias: ulx menu)\n" +
                               "         <TAB>o xgui fban <player> - Opens the add ban window, " +
                               "freezes the specified player, and fills out the Name/SteamID " +
                               "automatically. (say: !fban)\n" +
                               "         <TAB>o xgui xban <player> - Opens the add ban window and" +
                               " fills out Name/SteamID automatically if a player was specified. " +
                               "(say: !xban)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Name Tags\n" +
                               "         <TAB>o ulx addtagrequest <SteamID> <Content> <red: " +
                               "0<=x<=255> <green: 0<=x<=255> <blue: 0<=x<=255> - Add a name tag " +
                               "request by Steam ID. (say: !addtagrequest)\n" +
                               "         <TAB>o ulx approvetag <Steam ID> - Approves a player's " +
                               "name tag request. (say: !approvetag)\n" +
                               "         <TAB>o ulx denytag <Steam ID> - Denies a player's name " +
                               "tag request. (say: !denytag)\n" +
                               "         <TAB>o ulx grouptag <group> <content> <red: 0<=x<=255> " +
                               "<green: 0<=x<=255> <blue: 0<=x<=255> - Sets a group's name tag. " +
                               "(say: !grouptag)\n" +
                               "         <TAB>o ulx rainbowgrouptag <group> <content> - Sets a " +
                               "group's name tag. (say: !rainbowgrouptag)\n" +
                               "         <TAB>o ulx rainbowtag <player> <content> - Sets the " +
                               "target's name tag with a rainbow animation. (say: !rainbowtag)\n" +
                               "         <TAB>o ulx rainbowtagid <Steam ID> <content> - Sets a " +
                               "name tag by Steam ID with a rainbow animation. (say: " +
                               "!rainbowtagid)\n" +
                               "         <TAB>o ulx removegrouptag <group> - Removes a name tag " +
                               "from a group. (say: !removegrouptag)\n" +
                               "         <TAB>o ulx removetag <player> - Removes the target's " +
                               "name tag. (say: !removetag)\n" +
                               "         <TAB>o ulx removetagid <SteamID> - Removes a name tag by" +
                               " Steam ID. (say: !removetagid)\n" +
                               "         <TAB>o ulx requesttag <Content> <red: 0<=x<=255> <green:" +
                               " 0<=x<=255> <blue: 0<=x<=255> - Request a name tag. (say: " +
                               "!requesttag)\n" +
                               "         <TAB>o ulx tag <player> <content> <red: 0<=x<=255, " +
                               "default 255> <green: 0<=x<=255, default 255> <blue: 0<=x<=255, " +
                               "default 255> - Sets the target's name tag. (say: !tag)\n" +
                               "         <TAB>o ulx tagid <Steam ID> <content> <red: 0<=x<=255> " +
                               "<green: 0<=x<=255> <blue: 0<=x<=255> - Sets a name tag by Steam " +
                               "ID. (say: !tagid)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Teleport\n" +
                               "         <TAB>o ulx bring <player> - Brings target to you. (say: " +
                               "!bring)\n" +
                               "         <TAB>o ulx goto <player> - Goto target. (say: !goto)\n" +
                               "         <TAB>o ulx return [<player, defaults to self>] - Returns" +
                               " target to last position before a teleport. (say: !return)\n" +
                               "         <TAB>o ulx send <player> <player> - Goto target. (say: " +
                               "!send)\n" +
                               "         <TAB>o ulx teleport [<player, defaults to self>] - " +
                               "Teleports target. (say: !tp)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Fun\n" +
                               "         <TAB>o ulx armor <players> <armor: 0<=x<=255> - Sets the" +
                               " armor for target(s). (say: !armor)\n" +
                               "         <TAB>o ulx blind <players> [<amount: 0<=x<=255, default " +
                               "255>] - Blinds target(s). (say: !blind) (opposite: ulx unblind)\n" +
                               "         <TAB>o ulx cloak [<players, defaults to self>] [<amount:" +
                               " 0<=x<=255, default 255>] - Cloaks target(s). (say: !cloak) " +
                               "(opposite: ulx uncloak)\n" +
                               "         <TAB>o ulx freeze <players> - Freezes target(s). (say: " +
                               "!freeze) (opposite: ulx unfreeze)\n" +
                               "         <TAB>o ulx god [<players, defaults to self>] - Grants " +
                               "god mode to target(s). (say: !god) (opposite: ulx ungod)\n" +
                               "         <TAB>o ulx hp <players> <hp: 1<=x<=2147483647> - Sets " +
                               "the hp for target(s). (say: !hp)\n" +
                               "         <TAB>o ulx ignite <players> [<seconds: 1<=x<=300, " +
                               "default 300>] - Ignites target(s). (say: !ignite) (opposite: ulx " +
                               "unignite)\n" +
                               "         <TAB>o ulx jail <players> [<seconds, 0 is forever: 0<=x," +
                               " default 0>] - Jails target(s). (say: !jail) (opposite: ulx " +
                               "unjail)\n" +
                               "         <TAB>o ulx jailtp <player> [<seconds, 0 is forever: " +
                               "0<=x, default 0>] - Teleports, then jails target(s). (say: " +
                               "!jailtp)\n" +
                               "         <TAB>o ulx maul <players> - Maul target(s). (say: !maul)" +
                               "\n" +
                               "         <TAB>o ulx playsound <sound> - Plays a sound (relative " +
                               "to sound dir).\n" +
                               "         <TAB>o ulx ragdoll <players> - ragdolls target(s). (say:" +
                               " !ragdoll) (opposite: ulx unragdoll)\n" +
                               "         <TAB>o ulx slap <players> [<damage: 0<=x, default 0>] - " +
                               "Slaps target(s) with given damage. (say: !slap)\n" +
                               "         <TAB>o ulx slay <players> - Slays target(s). (say: " +
                               "!slay)\n" +
                               "         <TAB>o ulx sslay <players> - Silently slays target(s). " +
                               "(say: !sslay)\n" +
                               "         <TAB>o ulx strip <players> - Strip weapons from target" +
                               "(s). (say: !strip)\n" +
                               "         <TAB>o ulx unigniteall - Extinguishes all players and " +
                               "all entities. (say: !unigniteall)\n" +
                               "         <TAB>o ulx whip <players> [<times: 2<=x<=100, default " +
                               "10>] [<damage: 0<=x, default 0>] - Slaps target(s) x times with " +
                               "given damage each time. (say: !whip)\n" +
                               "         \n" +
                               "         \n" +
                               "         -End of help\n" +
                               "         ULX version: <SVN> unknown revision\n" +
                               "         \n" +
                               "00:15:15 ulx debuginfo\n" +
                               "00:15:15 Debug information written to " +
                               "garrysmod/data/ulx/debugdump.txt on server.\n" +
                               "00:16:51 HLSW NOTE: Disconnected\n" +
                               "10:17:33 HLSW NOTE: Connecting to 74.91.124.26:27015 ...\n" +
                               "10:17:33 ulx help\n" +
                               "10:17:33 ULX Help:\n" +
                               "         If a command can take multiple targets, it will usually " +
                               "let you use the keywords '*' for target\n" +
                               "         all, '^' to target yourself, '@' for target your picker," +
                               " '$<userid>' to target by ID (steamid,\n" +
                               "         uniqueid, userid, ip), '#<group>' to target users in a " +
                               "specific group, and '%<group>' to target\n" +
                               "         users with access to the group (inheritance counts). IE," +
                               " ulx slap #user slaps all players who are\n" +
                               "         in the default guest access group. Any of these keywords" +
                               " can be preceded by '!' to negate it.\n" +
                               "         EG, ulx slap !^ slaps everyone but you.\n" +
                               "         You can also separate multiple targets by commas. IE, " +
                               "ulx slap bob,jeff,henry.\n" +
                               "         All commands must be preceded by \"ulx \", ie \"ulx " +
                               "slap\"\n" +
                               "         \n" +
                               "         Command Help:\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Rcon\n" +
                               "         <TAB>o ulx cexec <players> {command} - Run a command on " +
                               "console of target(s). (say: !cexec)\n" +
                               "         <TAB>o ulx ent <classname> [{<flag>:<value>}] - Spawn an" +
                               " ent, separate flag and value with ':'.\n" +
                               "         <TAB>o ulx exec <file> - Execute a file from the cfg " +
                               "directory on the server.\n" +
                               "         <TAB>o ulx luarun {command} - Executes lua in server " +
                               "console. (Use '=' for output)\n" +
                               "         <TAB>o ulx rcon {command} - Execute command on server " +
                               "console. (say: !rcon)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Utility\n" +
                               "         <TAB>o ulx ban <player> [<minutes, 0 for perma: 0<=x, " +
                               "default 0>] [{reason}] - Bans target. (say: !ban)\n" +
                               "         <TAB>o ulx banid <steamid> [<minutes, 0 for perma: 0<=x," +
                               " default 0>] [{reason}] - Bans steamid.\n" +
                               "         <TAB>o ulx debuginfo - Dump some debug information.\n" +
                               "         <TAB>o ulx forcemotd <player> - Opens the MOTD on the " +
                               "target. (say: !forcemotd)\n" +
                               "         <TAB>o ulx freezeprops - Freezes all physics props on " +
                               "the server. (say: !freezeprops)\n" +
                               "         <TAB>o ulx gban <player> [<minutes, 0 for perma: 0<=x, " +
                               "default 0>] [{reason}] - Bans target from all servers. (say: " +
                               "!gban)\n" +
                               "         <TAB>o ulx gbanid <steamid> [<minutes, 0 for perma: " +
                               "0<=x, default 0>] [{reason}] - Bans steamid from all servers.\n" +
                               "         <TAB>o ulx help - Shows this help.\n" +
                               "         <TAB>o ulx kick <player> [{reason}] - Kicks target. " +
                               "(say: !kick)\n" +
                               "         <TAB>o ulx map <map> [<gamemode>] - Changes map and " +
                               "gamemode. (say: !map)\n" +
                               "         <TAB>o ulx noclip [<players, defaults to self>] - " +
                               "Toggles noclip on target(s). (say: !noclip)\n" +
                               "         <TAB>o ulx removeprop - Removes physics prop at " +
                               "crosshair. (say: !removeprop)\n" +
                               "         <TAB>o ulx resettodefaults [<string>] - Resets ALL ULX " +
                               "and ULib configuration!\n" +
                               "         <TAB>o ulx screengrab <player> - Takes a screenshot of a" +
                               " client's screen and opens it in a window. (say: !screengrab)\n" +
                               "         <TAB>o ulx spectate <player> - Spectate target. (say: " +
                               "!spectate)\n" +
                               "         <TAB>o ulx unban <steamid> - Unbans steamid.\n" +
                               "         <TAB>o ulx who - See information about currently online " +
                               "users.\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: TTT\n" +
                               "         <TAB>o ulx addimpair <player> [<Damage: 1<=x<=100, " +
                               "default 25>] [{Reason}] - Damages a player at the beginning of " +
                               "the next round. (say: !addimpair) (opposite: ulx rimpair)\n" +
                               "         <TAB>o ulx addslay <player> [{Reason}] - Slays target " +
                               "and the beginning of the next round. (say: !addslay) (opposite: " +
                               "ulx rslay)\n" +
                               "         <TAB>o ulx addslayid <SteamID> [{Reason}] - Slays target" +
                               " and the beginning of the next round. (say: !addslayid) " +
                               "(opposite: ulx rslayid)\n" +
                               "         <TAB>o ulx spec <player> - Forces a player to/from " +
                               "spectator. (say: !spec) (opposite: ulx unspec)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Voting\n" +
                               "         <TAB>o ulx stopvote - Stops a vote in progress. (say: " +
                               "!stopvote)\n" +
                               "         <TAB>o ulx veto - Veto a successful votemap. (say: " +
                               "!veto)\n" +
                               "         <TAB>o ulx vote <title> {options} - Starts a public vote" +
                               ". (say: !vote)\n" +
                               "         <TAB>o ulx voteaddslay <player> - Starts a vote to add a" +
                               " target to the slay list. (say: !voteaddslay)\n" +
                               "         <TAB>o ulx voteban <player> [<minutes: 0<=x, default " +
                               "1440>] [{reason}] - Starts a public ban vote against target. " +
                               "(say: !voteban)\n" +
                               "         <TAB>o ulx votekick <player> [{reason}] - Starts a " +
                               "public kick vote against target. (say: !votekick)\n" +
                               "         <TAB>o ulx votemap [{map}] - Vote for a map, no args " +
                               "lists available maps. (say: !votemap)\n" +
                               "         <TAB>o ulx votemap2 {map} - Starts a public map vote. " +
                               "(say: !votemap2)\n" +
                               "         <TAB>o ulx votesilentround - Starts a vote to gag " +
                               "everyone in the next round. (say: !votesilentround)\n" +
                               "         <TAB>o ulx votespec <player> - Starts a vote to move a " +
                               "target to spectator. (say: !votespec)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: User Management\n" +
                               "         <TAB>o ulx addgroup <group> [<inherits from>] - Create a" +
                               " new group with optional inheritance.\n" +
                               "         <TAB>o ulx adduser <player> <group> - Add a user to " +
                               "specified group.\n" +
                               "         <TAB>o ulx adduserid <SteamID, IP, or UniqueID> <group> " +
                               "- Add a user by ID to specified group.\n" +
                               "         <TAB>o ulx groupallow <group> <command> [<access tag>] -" +
                               " Add to a group's access.\n" +
                               "         <TAB>o ulx groupdeny <group> <command> - Remove from a " +
                               "group's access.\n" +
                               "         <TAB>o ulx removegroup <group> - Removes a group. USE " +
                               "WITH CAUTION.\n" +
                               "         <TAB>o ulx removeuser <player> - Permanently removes a " +
                               "user's access.\n" +
                               "         <TAB>o ulx removeuserid <SteamID, IP, or UniqueID> - " +
                               "Permanently removes a user's access by ID.\n" +
                               "         <TAB>o ulx renamegroup <current group> <new group> - " +
                               "Renames a group.\n" +
                               "         <TAB>o ulx setgroupcantarget <group> [<target string>] -" +
                               " Sets what a group is allowed to target\n" +
                               "         <TAB>o ulx userallow <player> <command> [<access tag>] -" +
                               " Add to a user's access.\n" +
                               "         <TAB>o ulx userallowid <SteamID, IP, or UniqueID> " +
                               "<command> [<access tag>] - Add to a user's access.\n" +
                               "         <TAB>o ulx userdeny <player> <command> [<remove explicit" +
                               " allow or deny instead of outright denying: 0/1>] - Remove from a" +
                               " user's access.\n" +
                               "         <TAB>o ulx userdenyid <SteamID, IP, or UniqueID> " +
                               "<command> [<remove explicit allow or deny instead of outright " +
                               "denying: 0/1>] - Remove from a user's access.\n" +
                               "         <TAB>o ulx usermanagementhelp - See the user management " +
                               "help.\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Chat\n" +
                               "         <TAB>o ulx asay {message} - Send a message to currently " +
                               "connected admins. (say: @)\n" +
                               "         <TAB>o ulx csay {message} - Send a message to everyone " +
                               "in the middle of their screen. (say: @@@)\n" +
                               "         <TAB>o ulx gag <players> - Gag target(s), disables " +
                               "microphone. (say: !gag) (opposite: ulx ungag)\n" +
                               "         <TAB>o ulx gimp <players> - Gimps target(s) so they are " +
                               "unable to chat normally. (say: !gimp) (opposite: ulx ungimp)\n" +
                               "         <TAB>o ulx mute <players> - Mutes target(s) so they are " +
                               "unable to chat. (say: !mute) (opposite: ulx unmute)\n" +
                               "         <TAB>o ulx pgag <player> - Permanently gags a target. " +
                               "(say: !pgag) (opposite: ulx unpgag)\n" +
                               "         <TAB>o ulx pmute <player> - Permanently mutes a target. " +
                               "(say: !pmute) (opposite: ulx unpmute)\n" +
                               "         <TAB>o ulx psay <player> {message} - Send a private " +
                               "message to target. (say: !p)\n" +
                               "         <TAB>o ulx rename <player> {Name} - Changes the name of " +
                               "a target until they reconnect. (say: !rename)\n" +
                               "         <TAB>o ulx thetime - Shows you the server time. (say: " +
                               "!thetime)\n" +
                               "         <TAB>o ulx tsay {message} - Send a message to everyone " +
                               "in the chat box. (say: @@)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Menus\n" +
                               "         <TAB>o ulx motd - Show the message of the day. (say: " +
                               "!motd)\n" +
                               "         <TAB>o xgui <show, hide, toggle> - Opens and/or closes " +
                               "XGUI. (say: !xgui, !menu) (alias: ulx menu)\n" +
                               "         <TAB>o xgui fban <player> - Opens the add ban window, " +
                               "freezes the specified player, and fills out the Name/SteamID " +
                               "automatically. (say: !fban)\n" +
                               "         <TAB>o xgui xban <player> - Opens the add ban window and" +
                               " fills out Name/SteamID automatically if a player was specified. " +
                               "(say: !xban)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Name Tags\n" +
                               "         <TAB>o ulx addtagrequest <SteamID> <Content> <red: " +
                               "0<=x<=255> <green: 0<=x<=255> <blue: 0<=x<=255> - Add a name tag " +
                               "request by Steam ID. (say: !addtagrequest)\n" +
                               "         <TAB>o ulx approvetag <Steam ID> - Approves a player's " +
                               "name tag request. (say: !approvetag)\n" +
                               "         <TAB>o ulx denytag <Steam ID> - Denies a player's name " +
                               "tag request. (say: !denytag)\n" +
                               "         <TAB>o ulx grouptag <group> <content> <red: 0<=x<=255> " +
                               "<green: 0<=x<=255> <blue: 0<=x<=255> - Sets a group's name tag. " +
                               "(say: !grouptag)\n" +
                               "         <TAB>o ulx rainbowgrouptag <group> <content> - Sets a " +
                               "group's name tag. (say: !rainbowgrouptag)\n" +
                               "         <TAB>o ulx rainbowtag <player> <content> - Sets the " +
                               "target's name tag with a rainbow animation. (say: !rainbowtag)\n" +
                               "         <TAB>o ulx rainbowtagid <Steam ID> <content> - Sets a " +
                               "name tag by Steam ID with a rainbow animation. (say: " +
                               "!rainbowtagid)\n" +
                               "         <TAB>o ulx removegrouptag <group> - Removes a name tag " +
                               "from a group. (say: !removegrouptag)\n" +
                               "         <TAB>o ulx removetag <player> - Removes the target's " +
                               "name tag. (say: !removetag)\n" +
                               "         <TAB>o ulx removetagid <SteamID> - Removes a name tag by" +
                               " Steam ID. (say: !removetagid)\n" +
                               "         <TAB>o ulx requesttag <Content> <red: 0<=x<=255> <green:" +
                               " 0<=x<=255> <blue: 0<=x<=255> - Request a name tag. (say: " +
                               "!requesttag)\n" +
                               "         <TAB>o ulx tag <player> <content> <red: 0<=x<=255, " +
                               "default 255> <green: 0<=x<=255, default 255> <blue: 0<=x<=255, " +
                               "default 255> - Sets the target's name tag. (say: !tag)\n" +
                               "         <TAB>o ulx tagid <Steam ID> <content> <red: 0<=x<=255> " +
                               "<green: 0<=x<=255> <blue: 0<=x<=255> - Sets a name tag by Steam " +
                               "ID. (say: !tagid)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Teleport\n" +
                               "         <TAB>o ulx bring <player> - Brings target to you. (say: " +
                               "!bring)\n" +
                               "         <TAB>o ulx goto <player> - Goto target. (say: !goto)\n" +
                               "         <TAB>o ulx return [<player, defaults to self>] - Returns" +
                               " target to last position before a teleport. (say: !return)\n" +
                               "         <TAB>o ulx send <player> <player> - Goto target. (say: " +
                               "!send)\n" +
                               "         <TAB>o ulx teleport [<player, defaults to self>] - " +
                               "Teleports target. (say: !tp)\n" +
                               "         \n" +
                               "         \n" +
                               "         Category: Fun\n" +
                               "         <TAB>o ulx armor <players> <armor: 0<=x<=255> - Sets the" +
                               " armor for target(s). (say: !armor)\n" +
                               "         <TAB>o ulx blind <players> [<amount: 0<=x<=255, default " +
                               "255>] - Blinds target(s). (say: !blind) (opposite: ulx unblind)\n" +
                               "         <TAB>o ulx cloak [<players, defaults to self>] [<amount:" +
                               " 0<=x<=255, default 255>] - Cloaks target(s). (say: !cloak) " +
                               "(opposite: ulx uncloak)\n" +
                               "         <TAB>o ulx freeze <players> - Freezes target(s). (say: " +
                               "!freeze) (opposite: ulx unfreeze)\n" +
                               "         <TAB>o ulx god [<players, defaults to self>] - Grants " +
                               "god mode to target(s). (say: !god) (opposite: ulx ungod)\n" +
                               "         <TAB>o ulx hp <players> <hp: 1<=x<=2147483647> - Sets " +
                               "the hp for target(s). (say: !hp)\n" +
                               "         <TAB>o ulx ignite <players> [<seconds: 1<=x<=300, " +
                               "default 300>] - Ignites target(s). (say: !ignite) (opposite: ulx " +
                               "unignite)\n" +
                               "         <TAB>o ulx jail <players> [<seconds, 0 is forever: 0<=x," +
                               " default 0>] - Jails target(s). (say: !jail) (opposite: ulx " +
                               "unjail)\n" +
                               "         <TAB>o ulx jailtp <player> [<seconds, 0 is forever: " +
                               "0<=x, default 0>] - Teleports, then jails target(s). (say: " +
                               "!jailtp)\n" +
                               "         <TAB>o ulx maul <players> - Maul target(s). (say: !maul)" +
                               "\n" +
                               "         <TAB>o ulx playsound <sound> - Plays a sound (relative " +
                               "to sound dir).\n" +
                               "         <TAB>o ulx ragdoll <players> - ragdolls target(s). (say:" +
                               " !ragdoll) (opposite: ulx unragdoll)\n" +
                               "         <TAB>o ulx slap <players> [<damage: 0<=x, default 0>] - " +
                               "Slaps target(s) with given damage. (say: !slap)\n" +
                               "         <TAB>o ulx slay <players> - Slays target(s). (say: " +
                               "!slay)\n" +
                               "         <TAB>o ulx sslay <players> - Silently slays target(s). " +
                               "(say: !sslay)\n" +
                               "         <TAB>o ulx strip <players> - Strip weapons from target" +
                               "(s). (say: !strip)\n" +
                               "         <TAB>o ulx unigniteall - Extinguishes all players and " +
                               "all entities. (say: !unigniteall)\n" +
                               "         <TAB>o ulx whip <players> [<times: 2<=x<=100, default " +
                               "10>] [<damage: 0<=x, default 0>] - Slaps target(s) x times with " +
                               "given damage each time. (say: !whip)\n" +
                               "         \n" +
                               "         \n" +
                               "         -End of help\n" +
                               "         ULX version: <SVN> unknown revision");

        serverAdministration.populateCommands();
    }

    @Test
    public void basicSingleArgumentCommand()
    {
        Command knownCommand = new Command("ulx sslay", "Sslay",
                "Silently slays target(s).",
                new CommandArgument[]{
                    new CommandArgument(
                            "Players",
                            true,
                            false,
                            CommandArgument.TYPE_STRING,
                            0, 0)
                });

        Command command = serverAdministration.getCommand("ulx sslay");

        compareCommands(knownCommand, command);
    }

    @Test
    public void noArgumentCommand()
    {
        Command knownCommand = new Command("ulx thetime", "Thetime",
                "Shows you the server time.",
                new CommandArgument[0]);

        Command command = serverAdministration.getCommand("ulx thetime");

        compareCommands(knownCommand, command);
    }

    @Test
    public void singleOptionalArgumentCommand()
    {
        Command knownCommand = new Command("ulx resettodefaults", "Resettodefaults",
                "Resets ALL ULX and ULib configuration!",
                new CommandArgument[]{
                        new CommandArgument(
                                "String",
                                false,
                                true,
                                CommandArgument.TYPE_STRING,
                                0, 0)
                });

        Command command = serverAdministration.getCommand("ulx resettodefaults");

        compareCommands(knownCommand, command);
    }

    @Test
    public void requiredAndOptionalArgumentsCommand()
    {
        Command knownCommand = new Command("ulx votekick", "Votekick",
                "Starts a public kick vote against target.",
                new CommandArgument[]{
                        new CommandArgument(
                                "Player",
                                true,
                                false,
                                CommandArgument.TYPE_STRING,
                                0, 0),
                        new CommandArgument(
                                "Reason",
                                false,
                                true,
                                CommandArgument.TYPE_STRING,
                                0, 0)
                });

        Command command = serverAdministration.getCommand("ulx votekick");

        compareCommands(knownCommand, command);
    }

    @Test
    public void rangeArgumentCommand()
    {
        Command knownCommand = new Command("ulx tag", "Tag",
                "Sets the target's name tag.",
                new CommandArgument[]{
                        new CommandArgument(
                                "Player",
                                true,
                                false,
                                CommandArgument.TYPE_STRING,
                                0, 0),
                        new CommandArgument(
                                "Content",
                                false,
                                false,
                                CommandArgument.TYPE_STRING,
                                0, 0),
                        new CommandArgument(
                                "Red, Default 255",
                                false,
                                false,
                                CommandArgument.TYPE_INT,
                                0, 255),
                        new CommandArgument(
                                "Green, Default 255",
                                false,
                                false,
                                CommandArgument.TYPE_INT,
                                0, 255),
                        new CommandArgument(
                                "Blue, Default 255",
                                false,
                                false,
                                CommandArgument.TYPE_INT,
                                0, 255)
                });

        Command command = serverAdministration.getCommand("ulx tag");

        compareCommands(knownCommand, command);
    }

    @Test
    public void noMaxRangeArgumentCommand()
    {
        Command knownCommand = new Command("ulx banid", "Banid",
                "Bans steamid.",
                new CommandArgument[]{
                        new CommandArgument(
                                "Steamid",
                                false,
                                false,
                                CommandArgument.TYPE_STRING,
                                0, 0),
                        new CommandArgument(
                                "Minutes, 0 For Perma, Default 0",
                                false,
                                true,
                                CommandArgument.TYPE_TIME_MINUTES,
                                0, -1),
                        new CommandArgument(
                                "Reason",
                                false,
                                true,
                                CommandArgument.TYPE_STRING,
                                0, 0)});

        Command command = serverAdministration.getCommand("ulx banid");

        compareCommands(knownCommand, command);
    }

    @Test
    public void oppositeCommand()
    {
        Command knownCommand = new Command("ulx rslay", "Rslay",
                null,
                new CommandArgument[]{
                        new CommandArgument(
                                "Player",
                                true,
                                false,
                                CommandArgument.TYPE_STRING,
                                0, 0)});

        Command command = serverAdministration.getCommand("ulx rslay");

        compareCommands(knownCommand, command);
    }

    private static void compareCommands(Command a, Command b)
    {
        Assert.assertEquals("Command's commands", a.command, b.command);
        Assert.assertEquals("Command's names", a.name, b.name);
        Assert.assertEquals("Command's description", a.description, b.description);
        Assert.assertEquals("Command's argument count",
                a.arguments == null ? 0 : a.arguments.length,
                b.arguments == null ? 0 : b.arguments.length);

        for (int i = 0; i < a.arguments.length; i++)
        {
            CommandArgument aArg = a.arguments[i];
            CommandArgument bArg = b.arguments[i];

            Assert.assertEquals("Command's argument name #" + i, aArg.name, bArg.name);
            Assert.assertEquals("Command's argument type #" + i, aArg.type, bArg.type);
            Assert.assertEquals("Command's argument required #" + i, aArg.optional, bArg.optional);
            Assert.assertEquals("Command's argument isPlayer #" + i, aArg.playerArgument, bArg.playerArgument);
            Assert.assertEquals("Command's argument min #" + i, aArg.min, bArg.min, 0);
            Assert.assertEquals("Command's argument max #" + i, aArg.max, bArg.max, 0);
        }
    }
}
