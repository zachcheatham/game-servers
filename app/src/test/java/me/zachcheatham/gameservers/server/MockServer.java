package me.zachcheatham.gameservers.server;

public class MockServer extends Server
{
    private String rconResponse = "";

    public MockServer()
    {
        super("", 0);
    }

    public void setRCONResponse(String response)
    {
        rconResponse = response;
    }

    @Override
    protected void query(boolean extended) {}

    @Override
    public ServerType getType()
    {
        return null;
    }

    @Override
    public void setExtraOption(String key, Object value) {}

    @Override
    public Object getExtraOption(String key)
    {
        return null;
    }

    @Override
    protected void setTimeout(int timeout) {}

    @Override
    public int getRealNumPlayers()
    {
        return 0;
    }

    @Override
    public boolean hasBadAdminPassword()
    {
        return false;
    }

    @Override
    public boolean isRconConnected()
    {
        return true;
    }

    @Override
    void requestPrivatePlayerInfo()
    {

    }

    @Override
    public boolean supportsLogging()
    {
        return false;
    }

    @Override
    public void sendUserAdminCommand(String s) {}

    @Override
    public void sendInternalAdminCommand(String s, RCONResponseCallback callback)
    {
        callback.onResponse(rconResponse);
    }

    @Override
    void doSendUserAdminCommand(String s)
    {

    }

    @Override
    public void doSendInternalAdminCommand(int requestId, String command) {}

    @Override
    public void closeQueryConnection() {}

    @Override
    public void closeRconConnection() {}
}
