package me.zachcheatham.gameservers;

import org.junit.Test;

import me.zachcheatham.gameservers.server.player.SourcePlayer;

import static org.junit.Assert.assertEquals;

public class SteamIDConverterTest
{
    @Test
    public void steamID3Detect()
    {
        assertEquals(SourcePlayer.SteamIDType.SteamID3, SourcePlayer.detectSteamIDType("[U:1:62849034]"));
    }

    @Test
    public void steamIDDetect()
    {
        assertEquals(SourcePlayer.SteamIDType.SteamID, SourcePlayer.detectSteamIDType("STEAM_0:0:31424517"));
    }

    @Test
    public void steamID64Detect()
    {
        assertEquals(SourcePlayer.SteamIDType.SteamID64, SourcePlayer.detectSteamIDType("76561198023114762"));
    }

    @Test
    public void bogusSteamIDDetect()
    {
        assertEquals(SourcePlayer.SteamIDType.Unknown, SourcePlayer.detectSteamIDType("NotEnoughPrisms"));
    }

    @Test
    public void bogusSteamID64Detect()
    {
        assertEquals(SourcePlayer.SteamIDType.Unknown, SourcePlayer.detectSteamIDType("432432432"));
    }

    @Test
    public void convertSteamID3ToSteamID64()
    {
        assertEquals("76561198023114762", SourcePlayer.convertSteamIDToSteamID64("[U:1:62849034]", SourcePlayer.SteamIDType.SteamID3));
    }

    @Test
    public void convertSteamID3ToSteamID()
    {
        assertEquals("STEAM_0:0:31424517", SourcePlayer.convertSteamIDToSteamID("[U:1:62849034]", SourcePlayer.SteamIDType.SteamID3));
    }

    @Test
    public void convertSteamID64ToSteamID3()
    {
        assertEquals("[U:1:62849034]", SourcePlayer.convertSteamIDToSteamID3("76561198023114762", SourcePlayer.SteamIDType.SteamID64));
    }

    @Test
    public void convertSteamID64ToSteamID()
    {
        assertEquals( "STEAM_0:0:31424517", SourcePlayer.convertSteamIDToSteamID("76561198023114762", SourcePlayer.SteamIDType.SteamID64));
    }

    @Test
    public void convertSteamIDToSteamID3()
    {
        assertEquals("[U:1:62849034]", SourcePlayer.convertSteamIDToSteamID3("STEAM_0:0:31424517", SourcePlayer.SteamIDType.SteamID));
    }

    @Test
    public void convertSteamIDToSteamID64()
    {
        assertEquals("76561198023114762", SourcePlayer.convertSteamIDToSteamID64("STEAM_0:0:31424517", SourcePlayer.SteamIDType.SteamID));
    }
}
