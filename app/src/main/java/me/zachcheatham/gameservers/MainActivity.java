package me.zachcheatham.gameservers;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.snackbar.Snackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.net.InetAddress;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zachcheatham.gameservers.adapter.ServerListAdapter;
import me.zachcheatham.gameservers.events.AutoRefreshChangeEvent;
import me.zachcheatham.gameservers.events.ServersCompletedUpdatingEvent;
import me.zachcheatham.gameservers.fragment.AddServerDialogFragment;
import me.zachcheatham.gameservers.fragment.ImportServersFragment;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.ServerManagerService;

public class MainActivity extends AppCompatActivity
        implements AddServerDialogFragment.AddServerListener,
        SwipeRefreshLayout.OnRefreshListener
{
    private static final int REQUEST_CODE_OPEN_FILE = 1;

    private final Runnable refreshRunnable = new ServerRefreshRunnable();
    @BindView(R.id.refresh_swipe)
    SwipeRefreshLayout refreshLayout;
    private int refreshInterval = 1000;
    private boolean refreshing = false;
    private boolean paused = false;
    private ServerManagerService managerService;
    private ServerManager manager;
    private Uri pendingImport;
    private ServerListAdapter serverListAdapter;
    private final ServiceConnection serviceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder)
        {
            ServerManagerService.LocalBinder binder = (ServerManagerService.LocalBinder) iBinder;
            managerService = binder.getService();
            manager = managerService.getManager();
            manager.setResourceLevel(ServerManager.ResourceLevel.APP);

            serverListAdapter.setManager(manager);

            if (!refreshing && manager.shouldAutoRefresh())
            {
                refreshing = true;
                new Thread(refreshRunnable).start();
                refreshLayout.setEnabled(false);
            }

            manager.updateAll();

            if (pendingImport != null)
            {
                importServers(pendingImport);
                pendingImport = null;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            refreshing = false;
            refreshLayout.setEnabled(true);
            serverListAdapter.setManager(null);

            managerService = null;
            manager = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(this, R.xml.settings, false);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        serverListAdapter = new ServerListAdapter(this);

        RecyclerView serverList = findViewById(R.id.server_list);
        serverList.setHasFixedSize(true);
        ((SimpleItemAnimator) serverList.getItemAnimator()).setSupportsChangeAnimations(false);
        serverList.setAdapter(serverListAdapter);

        ItemTouchHelper.SimpleCallback itemTouchCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
                {
                    @Override
                    public boolean onMove(RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder,
                            RecyclerView.ViewHolder target)
                    {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction)
                    {
                        int removePosition = viewHolder.getAdapterPosition();
                        int removeIndex = serverListAdapter.getSortedIndex(removePosition);
                        final Server removedServer = manager.getServer(removeIndex);

                        manager.removeServer(removeIndex);
                        manager.saveServers();

                        Snackbar.make(findViewById(R.id.main_content), "Removed server",
                                Snackbar.LENGTH_LONG)
                                .setAction("UNDO", v ->
                                {
                                    manager.addServer(removedServer);
                                    manager.saveServers();
                                })
                                .show();
                    }
                };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(serverList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        serverList.setLayoutManager(layoutManager);
        DividerItemDecoration decoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());
        serverList.addItemDecoration(decoration);

        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        refreshLayout.setOnRefreshListener(this);

        SharedPreferences preferences = getSharedPreferences(Constants.PREFS_KEY,
                Context.MODE_PRIVATE);
        serverListAdapter.setSorting(
                preferences.getInt(Constants.PREFS_SERVER_SORT_MODE,
                        ServerListAdapter.SORTMODE_NUM_PLAYERS));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_activity, menu);

        MenuItem sortingItem;
        switch (serverListAdapter.getSortMode())
        {
        default:
        case ServerListAdapter.SORTMODE_NUM_PLAYERS:
            sortingItem = menu.findItem(R.id.sort_num_players);
            break;
        case ServerListAdapter.SORTMODE_MAX_PLAYERS:
            sortingItem = menu.findItem(R.id.sort_max_players);
            break;
        case ServerListAdapter.SORTMODE_NAME:
            sortingItem = menu.findItem(R.id.sort_name);
            break;
        case ServerListAdapter.SORTMODE_MAP:
            sortingItem = menu.findItem(R.id.sort_map);
            break;
        }
        sortingItem.setChecked(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getGroupId() == R.id.sort_group)
        {
            switch (item.getItemId())
            {
            case R.id.sort_name:
                serverListAdapter.setSorting(ServerListAdapter.SORTMODE_NAME);
                break;
            case R.id.sort_max_players:
                serverListAdapter.setSorting(ServerListAdapter.SORTMODE_MAX_PLAYERS);
                break;
            case R.id.sort_num_players:
                serverListAdapter.setSorting(ServerListAdapter.SORTMODE_NUM_PLAYERS);
                break;
            case R.id.sort_map:
                serverListAdapter.setSorting(ServerListAdapter.SORTMODE_MAP);
                break;
            }

            item.setChecked(true);

            SharedPreferences.Editor preferenceEditor =
                    getSharedPreferences(Constants.PREFS_KEY, Context.MODE_PRIVATE).edit();
            preferenceEditor
                    .putInt(Constants.PREFS_SERVER_SORT_MODE, serverListAdapter.getSortMode());
            preferenceEditor.apply();
            return true;
        }
        else if (item.getItemId() == R.id.import_servers)
        {
            Intent intent = new Intent();
            intent.setType("application/octet-stream");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.title_file_chooser)),
                    REQUEST_CODE_OPEN_FILE);
        }
        else if (item.getItemId() == R.id.settings)
        {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        paused = false;
        if (!refreshing && manager != null && manager.shouldAutoRefresh())
        {
            refreshing = true;
            new Thread(refreshRunnable).start();
            refreshLayout.setEnabled(false);
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        EventBus.getDefault().register(this);

        Intent serviceIntent = new Intent(this, ServerManagerService.class);
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);

        SharedPreferences settings = getSharedPreferences(Constants.PREFS_KEY, 0);
        refreshInterval = Integer
                .valueOf(settings.getString(Constants.PREFS_SERVER_UPDATE_FREQUENCY, "1000"));

        if (getIntent() != null)
        {
            Intent intent = getIntent();
            if (intent.getScheme() != null)
            {
                Uri uri = intent.getData();
                if (intent.getScheme().equals("steam"))
                {
                    if (uri.getHost().equals("connect"))
                    {
                        Bundle arguments = new Bundle();
                        arguments.putString("preAddress", uri.getPathSegments().get(0));

                        AddServerDialogFragment addServerDialog = new AddServerDialogFragment();
                        addServerDialog.setArguments(arguments);
                        addServerDialog
                                .show(getSupportFragmentManager(), AddServerDialogFragment.TAG);
                    }
                }
                else if (intent.getScheme().equals("file"))
                {
                    importServers(uri);
                }
            }

            setIntent(null);
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        paused = true;
        refreshing = false;
    }

    @Override
    protected void onStop()
    {
        super.onStop();

        EventBus.getDefault().unregister(this);

        refreshing = false;
        refreshLayout.setEnabled(true);
        unbindService(serviceConnection);
        managerService = null;
        manager = null;
        serverListAdapter.setManager(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_OPEN_FILE)
            pendingImport = data.getData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults)
    {
        if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED)
        {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.error_import_permission)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
        }
        else if (pendingImport != null)
        {
            //ServerImporter.read(this, pendingImport, ServerImporter.FileType.SSLF, this);
            pendingImport = null;
        }
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        setIntent(intent);
    }

    private void importServers(Uri uri)
    {
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && ContextCompat
        .checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            pendingImport = uri;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
        else
        {
            //ServerImporter.read(this, uri, ServerImporter.FileType.SSLF, this);
        }*/

        Bundle b = new Bundle();
        b.putParcelable("uri", uri);
        ImportServersFragment addServerDialog = new ImportServersFragment();
        addServerDialog.setArguments(b);
        addServerDialog.setServerManager(manager);
        addServerDialog.show(getSupportFragmentManager(), "ImportServersFragment");
    }

    @OnClick(R.id.add_button)
    public void onClick()
    {
        AddServerDialogFragment addServerDialog = new AddServerDialogFragment();
        addServerDialog.show(getSupportFragmentManager(), "AddServerDialogFragment");
    }

    @Override
    public void onRefresh()
    {
        manager.updateAll();
    }

    @Override
    public void addServer(Server server)
    {
        manager.addServer(server);
        manager.saveServers();
    }

    @Override
    public boolean serverExists(InetAddress address, int port)
    {
        return manager.getServer(address, port) != null;
    }

    @Subscribe
    public void onServersDoneUpdating(ServersCompletedUpdatingEvent event)
    {
        refreshLayout.setRefreshing(false);
    }

    @Subscribe
    public void onAutoRefreshChange(AutoRefreshChangeEvent event)
    {
        if (!paused)
        {
            if (event.autoRefreshEnabled)
            {
                refreshing = true;
                new Thread(refreshRunnable).start();
                refreshLayout.setEnabled(false);
            }
            else
            {
                refreshing = false;
                refreshLayout.setEnabled(true);
            }
        }
    }

    private class ServerRefreshRunnable implements Runnable
    {
        @Override
        public void run()
        {
            while (refreshing)
            {
                if (manager != null)
                    manager.updateAll();

                try
                {
                    Thread.sleep(refreshInterval);
                }
                catch (InterruptedException ignored)
                {
                }
            }
        }
    }
}
