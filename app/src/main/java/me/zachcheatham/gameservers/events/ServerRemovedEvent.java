package me.zachcheatham.gameservers.events;

public class ServerRemovedEvent
{
    public final int serverIndex;

    public ServerRemovedEvent(int index)
    {
        this.serverIndex = index;
    }
}
