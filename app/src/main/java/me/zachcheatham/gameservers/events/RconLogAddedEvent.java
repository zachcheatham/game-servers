package me.zachcheatham.gameservers.events;

import me.zachcheatham.gameservers.server.LogHolder;
import me.zachcheatham.gameservers.server.Server;

public class RconLogAddedEvent
{
    public final Server server;
    public final LogHolder logHolder;

    public RconLogAddedEvent(Server server, LogHolder logHolder)
    {
        this.server = server;
        this.logHolder = logHolder;
    }
}
