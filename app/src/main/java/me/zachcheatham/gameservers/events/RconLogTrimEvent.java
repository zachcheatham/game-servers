package me.zachcheatham.gameservers.events;

import java.lang.ref.WeakReference;

import me.zachcheatham.gameservers.server.Server;

public class RconLogTrimEvent
{
    private final WeakReference<Server> server;
    private final int count;

    public RconLogTrimEvent(Server server, int count)
    {
        this.server = new WeakReference<>(server);
        this.count = count;
    }

    public Server getServer()
    {
        return server.get();
    }

    public int getCount()
    {
        return count;
    }
}
