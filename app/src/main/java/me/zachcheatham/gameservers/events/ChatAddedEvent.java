package me.zachcheatham.gameservers.events;

import me.zachcheatham.gameservers.server.ChatMessage;
import me.zachcheatham.gameservers.server.Server;

public class ChatAddedEvent
{
    private final Server server;
    private final ChatMessage message;

    public ChatAddedEvent(Server server, ChatMessage message)
    {
        this.server = server;
        this.message = message;
    }

    public Server getServer()
    {
        return server;
    }

    public ChatMessage getMessage()
    {
        return message;
    }
}
