package me.zachcheatham.gameservers.events;

public class AutoRefreshChangeEvent
{
    public final boolean autoRefreshEnabled;

    public AutoRefreshChangeEvent(boolean autoRefreshEnabled)
    {
        this.autoRefreshEnabled = autoRefreshEnabled;
    }
}
