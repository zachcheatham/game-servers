package me.zachcheatham.gameservers.events;

public class ServerAddedEvent
{
    private final int serverIndex;

    public ServerAddedEvent(int index)
    {
        this.serverIndex = index;
    }
}
