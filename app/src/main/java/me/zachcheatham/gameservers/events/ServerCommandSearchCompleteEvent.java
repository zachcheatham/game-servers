package me.zachcheatham.gameservers.events;

import java.lang.ref.WeakReference;

import me.zachcheatham.gameservers.server.Server;

public class ServerCommandSearchCompleteEvent
{
    private final WeakReference<Server> server;

    public ServerCommandSearchCompleteEvent(Server server)
    {
        this.server = new WeakReference<>(server);
    }

    public Server getServer()
    {
        return server.get();
    }
}
