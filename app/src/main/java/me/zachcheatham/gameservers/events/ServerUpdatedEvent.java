package me.zachcheatham.gameservers.events;

import android.util.SparseBooleanArray;

import java.lang.ref.WeakReference;

import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.server.Server;

public class ServerUpdatedEvent
{
    private final WeakReference<Server> server;
    private final SparseBooleanArray playerListChanges = new SparseBooleanArray();
    private final int prePlayerCount;
    private final int preMaxPlayerCount;
    private final String preHostname;
    private final String preMapName;
    private final boolean preOffline;
    private final int preOfflineCount;
    private boolean playerListChanged = false;
    private boolean playerPrivateInfoUpdated = false;
    private boolean numPlayersChanged = false;
    private boolean maxPlayersChanged = false;
    private boolean nameChanged = false;
    private boolean mapChanged = false;
    private boolean offlineChanged = false;
    private boolean offlineSortThresholdChanged = false;

    public ServerUpdatedEvent(Server server)
    {
        this.server = new WeakReference<>(server);

        prePlayerCount = server.getNumPlayers();
        preMaxPlayerCount = server.getMaxPlayers();
        preHostname = server.getName();
        preMapName = server.getMap();
        preOffline = server.isOffline();
        preOfflineCount = server.getOfflineCount();
    }

    public Server getServer()
    {
        return server.get();
    }

    public void detectChanges()
    {
        Server s = server.get();

        if (s != null)
        {
            if (prePlayerCount != s.getNumPlayers())
                numPlayersChanged = true;
            if (preMaxPlayerCount != s.getMaxPlayers())
                maxPlayersChanged = true;
            if (!preHostname.equals(s.getName()))
                nameChanged = true;
            if (!preMapName.equals(s.getMap()))
                mapChanged = true;
            if (preOfflineCount < Constants.OFFLINE_COUNT_THRESHOLD && s.getOfflineCount() >= Constants.OFFLINE_COUNT_THRESHOLD)
                offlineSortThresholdChanged = true;
            if (preOffline != s.isOffline())
                offlineChanged = true;
        }
    }

    public boolean didNumPlayersChange()
    {
        return numPlayersChanged;
    }

    public int getPreviousPlayerCount()
    {
        return prePlayerCount;
    }

    public boolean didMaxPlayersChange()
    {
        return maxPlayersChanged;
    }

    public int getPreviousMaxPlayerCount()
    {
        return preMaxPlayerCount;
    }

    public boolean didNameChange()
    {
        return nameChanged;
    }

    public boolean didMapChange()
    {
        return mapChanged;
    }

    public boolean didOfflineSortThresholdChange()
    {
        return offlineSortThresholdChanged;
    }

    public boolean didOfflineChange()
    {
        return offlineChanged;
    }

    public boolean didChange()
    {
        return (numPlayersChanged | maxPlayersChanged | nameChanged | mapChanged | offlineChanged);
    }

    public void addPlayerChange(int type)
    {
        playerListChanges.put(type, true);
    }

    public void setPlayerListChanged(boolean d)
    {
        playerListChanged = d;
    }

    public boolean getPlayerChange(int type)
    {
        return playerListChanges.get(type, false);
    }

    public boolean didPlayerListChange()
    {
        return playerListChanged;
    }

    public void setPlayerPrivateInfoUpdated(boolean v)
    {
        playerPrivateInfoUpdated = v;
    }

    public boolean didPlayerPrivateInfoUpdate()
    {
        return playerPrivateInfoUpdated;
    }
}
