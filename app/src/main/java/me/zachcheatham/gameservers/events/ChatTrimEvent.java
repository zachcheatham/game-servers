package me.zachcheatham.gameservers.events;

import java.lang.ref.WeakReference;

import me.zachcheatham.gameservers.server.Server;

public class ChatTrimEvent
{
    public final Server server;
    public final int count;

    public ChatTrimEvent(Server server, int count)
    {
        this.server = server;
        this.count = count;
    }
}
