package me.zachcheatham.gameservers;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

import androidx.core.content.ContextCompat;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import me.zachcheatham.gameservers.service.PersistRCONService;
import me.zachcheatham.gameservers.worker.BackgroundUpdateWorker;
import timber.log.Timber;

public class GameServersApplication extends Application
        implements Application.ActivityLifecycleCallbacks
{
    private static final String TAG_BACKGROUND_WORKER = "me.zachcheatham.gameservers.background";

    private int activitiesRunning = 0;
    private boolean startedBackgroundService = false;

    @Override
    public void onCreate()
    {
        super.onCreate();

        registerActivityLifecycleCallbacks(this);

        if (BuildConfig.DEBUG)
        {
            Timber.plant(new Timber.DebugTree());
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {}

    @Override
    public void onActivityStarted(Activity activity)
    {
        if (activitiesRunning == 0)
        {
            WorkManager.getInstance(this).cancelUniqueWork(TAG_BACKGROUND_WORKER);
            if (startedBackgroundService)
            {
                Intent intent = new Intent(this, PersistRCONService.class);
                intent.setAction(PersistRCONService.ACTION_STOP_SERVICE);
                startService(intent);
            }
        }
        activitiesRunning++;
    }

    @Override
    public void onActivityResumed(Activity activity) {}

    @Override
    public void onActivityPaused(Activity activity) {}

    @Override
    public void onActivityStopped(Activity activity)
    {
        activitiesRunning--;
        if (activitiesRunning == 0)
        {
            ContextCompat.startForegroundService(this, new Intent(this, PersistRCONService.class));
            startedBackgroundService = true;

            SharedPreferences sharedPreferences = getSharedPreferences(Constants.PREFS_KEY, 0);

            if (sharedPreferences.getBoolean("background_update", false))
            {
                int minutes = Integer
                        .parseInt(sharedPreferences.getString("background_update_frequency", "15"));

                Constraints constraints = new Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build();

                PeriodicWorkRequest updateRequest = new PeriodicWorkRequest.Builder(
                        BackgroundUpdateWorker.class, minutes, TimeUnit.MINUTES)
                        .setConstraints(constraints)
                        .build();

                WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                        TAG_BACKGROUND_WORKER,
                        ExistingPeriodicWorkPolicy.KEEP,
                        updateRequest);
            }
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

    @Override
    public void onActivityDestroyed(Activity activity) {}
}
