package me.zachcheatham.gameservers;

public class TimeHelper
{
    public static int getUnixTimestamp()
    {
        return (int) Math.floor(System.currentTimeMillis() / 1000L);
    }
}
