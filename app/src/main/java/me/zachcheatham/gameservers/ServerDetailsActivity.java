package me.zachcheatham.gameservers;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.gw.swipeback.SwipeBackLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.zachcheatham.gameservers.events.AutoRefreshChangeEvent;
import me.zachcheatham.gameservers.events.ServerCommandSearchCompleteEvent;
import me.zachcheatham.gameservers.events.ServerSettingsUpdatedEvent;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.fragment.ChatFragment;
import me.zachcheatham.gameservers.fragment.ConsoleFragment;
import me.zachcheatham.gameservers.fragment.PlayerListFragment;
import me.zachcheatham.gameservers.fragment.ServerExtraInfoListFragment;
import me.zachcheatham.gameservers.fragment.ServerInfoFragment;
import me.zachcheatham.gameservers.fragment.ServerSettingsDialogFragment;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.ServerManagerService;
import me.zachcheatham.gameservers.server.admin.Command;
import me.zachcheatham.gameservers.ui.CommandDialogBuilder;
import timber.log.Timber;

public class ServerDetailsActivity extends AppCompatActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener,
        SwipeBackLayout.OnSwipeBackListener
{
    public static final String ACTION_OPEN_CHAT = "c";
    public static final String EXTRA_KEY_SERVER_INDEX = "s";

    private final Runnable refreshRunnable = new ServerRefreshRunnable();

    @BindView(R.id.navigation) BottomNavigationView bottomNavigation;
    @BindView(R.id.dismiss_swipe) SwipeBackLayout swipeBackLayout;
    @BindView(R.id.toolbar_title) TextView toolbarTitle;

    private boolean refreshing;
    private boolean animateFinish = true;
    private boolean toolbarTitleVisible = true;
    private Integer refreshInterval;
    private int currentFragment = -1;
    private Class currentFragmentClass = null;
    private int serverIndex;
    private ServerManagerService managerService;
    private ServerManager manager;
    private Server server;
    private final ServiceConnection serviceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder)
        {
            ServerManagerService.LocalBinder binder = (ServerManagerService.LocalBinder) iBinder;
            managerService = binder.getService();
            manager = managerService.getManager();
            manager.setResourceLevel(ServerManager.ResourceLevel.APP);
            server = manager.getServer(serverIndex);

            if (server == null)
            {
                finish();
                return;
            }

            applyServer();

            for (Fragment fragment : getSupportFragmentManager().getFragments())
            {
                ((ServiceConnectionListener) fragment).serviceConnected(server);
            }
            invalidateOptionsMenu();

            server.update(true);

            // Hide the RCON tab if we cannot get to RCON
            if (!server.hasAdminPassword())
            {
                bottomNavigation.getMenu().findItem(R.id.navigation_rcon).setVisible(false);
                bottomNavigation.invalidate();
            }

            // Hide the Chat tab if we cannot get to Chat
            if (!server.isChatPossible())
            {
                bottomNavigation.getMenu().findItem(R.id.navigation_chat).setVisible(false);
                bottomNavigation.invalidate();
            }

            if (!refreshing && manager.shouldAutoRefresh())
            {
                refreshing = true;
                new Thread(refreshRunnable).start();
            }

            // Disable notifications as needed
            if (currentFragment == R.id.navigation_chat)
                manager.getNotifier().setDisabledServerChat(serverIndex);
            else if (currentFragment == R.id.navigation_info)
                manager.getNotifier().setDisabledPlayerUpdates(serverIndex);

            // Remove chat messages if we have the chat fragment open
            if (currentFragment == R.id.navigation_chat)
                manager.getNotifier().removeChatNotifications(serverIndex);
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            managerService = null;
            manager = null;
            server = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_server_details);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("");
        }

        serverIndex = getIntent().getIntExtra(EXTRA_KEY_SERVER_INDEX, -1);

        // Make window transparent programmatically so that fragments still have the theme's window background
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        bottomNavigation.setOnNavigationItemSelectedListener(this);
        if (savedInstanceState != null)
        {
            bottomNavigation.setSelectedItemId(savedInstanceState.getInt("nav-selected-item"));
            currentFragment = savedInstanceState.getInt("current-fragment");
        }
        else if (getIntent().getAction() != null && getIntent().getAction().equals(ACTION_OPEN_CHAT))
        {
            showFragment(R.id.navigation_chat);
            bottomNavigation.setSelectedItemId(R.id.navigation_chat);
        }
        else
            bottomNavigation.setSelectedItemId(R.id.navigation_info);

        swipeBackLayout.setSwipeBackListener(this);
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        EventBus.getDefault().register(this);

        SharedPreferences settings = getSharedPreferences(Constants.PREFS_KEY, 0);
        refreshInterval = Integer
                .valueOf(settings.getString(Constants.PREFS_SERVER_UPDATE_FREQUENCY, "1000"));

        Intent intent = new Intent(this, ServerManagerService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop()
    {
        EventBus.getDefault().unregister(this);

        manager.getNotifier().clearDisabledPlayerUpdates();
        manager.getNotifier().clearDisabledServerChat();

        unbindService();
        super.onStop();
    }

    @Override
    public void finish()
    {
        if (currentFragment == 0)
        {
            showFragment(R.id.navigation_info);
        }
        else
        {
            super.finish();
        }

        if (animateFinish)
            overridePendingTransition(0, R.anim.slide_right);
        else
            overridePendingTransition(0,0);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putInt("nav-selected-item", bottomNavigation.getSelectedItemId());
        outState.putInt("current-fragment", currentFragment);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.server_details, menu);
        if (server.getAdminMod() != null)
        {
            server.getAdminMod().populateServerMenu(this, getMenuInflater(), menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.server_settings)
        {
            ServerSettingsDialogFragment addServerDialog = new ServerSettingsDialogFragment();
            addServerDialog.show(getSupportFragmentManager(), ServerSettingsDialogFragment.TAG);

            return true;
        }
        else if (server.getAdminMod() != null)
        {
            Command command = server.getAdminMod().getMenuItemCommand(item);
            if (command != null)
            {
                AlertDialog dialog = CommandDialogBuilder
                        .createDialog(this, command, server, null);
                dialog.show();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item)
    {
        showFragment(item.getItemId());
        return true;
    }

    public void showFragment(int fragmentId)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        if (manager != null)
        {
            // Re-Enable disabled notifications
            if (currentFragment == R.id.navigation_chat)
                manager.getNotifier().clearDisabledServerChat();
            else if (currentFragment == R.id.navigation_info)
                manager.getNotifier().clearDisabledPlayerUpdates();

            // Disable notifications as needed
            if (fragmentId == R.id.navigation_chat)
                manager.getNotifier().setDisabledServerChat(serverIndex);
            else if (fragmentId == R.id.navigation_info)
                manager.getNotifier().setDisabledPlayerUpdates(serverIndex);

            // Clear existing notifications for chat
            if (fragmentId == R.id.navigation_chat)
                manager.getNotifier().removeChatNotifications(serverIndex);
        }

        switch (fragmentId)
        {
        case R.id.navigation_info:
            exchangeFragment(fragmentManager, transaction, fragmentId, ServerInfoFragment.class);
            break;
        case R.id.navigation_players:
            exchangeFragment(fragmentManager, transaction, fragmentId, PlayerListFragment.class);
            break;
        case R.id.navigation_rcon:
            exchangeFragment(fragmentManager, transaction, fragmentId, ConsoleFragment.class);
            break;
        case R.id.navigation_chat:
            exchangeFragment(fragmentManager, transaction, fragmentId, ChatFragment.class);
            break;
        case 0:
        case 1:
            exchangeFragment(fragmentManager, transaction, fragmentId, ServerExtraInfoListFragment.class);
            currentFragment = 0; //Override so all extra fragments are 0
            break;
        }

        transaction.commit();
    }

    private void exchangeFragment(FragmentManager fragmentManager, FragmentTransaction transaction, int itemId, Class<?> c)
    {
        // Animate fragments without navigation items
        if (itemId < 3)
            transaction.setCustomAnimations(R.anim.slide_left, R.anim.hold);
        else if (currentFragment == 0)
            transaction.setCustomAnimations(0, R.anim.slide_right);

        // Hide existing fragment
        if (currentFragment != -1)
        {
            Fragment fragment = fragmentManager.findFragmentByTag(currentFragmentClass.getSimpleName());

            if (fragment != null)
                transaction.hide(fragment);
        }

        // Show next one
        String fragmentTag = c.getSimpleName();
        Fragment fragment = fragmentManager.findFragmentByTag(fragmentTag);
        if (fragment == null)
        {
            try
            {
                Constructor<?> constructor = c.getConstructor();
                fragment = (Fragment) constructor.newInstance();

                if (itemId < 3)
                {
                    // Add mode argument
                    Bundle arguments = new Bundle();
                    arguments.putInt("mode", itemId);
                    fragment.setArguments(arguments);
                }

                transaction.add(R.id.content, fragment, fragmentTag);

            }
            catch (NoSuchMethodException e)
            {
                Timber.e(e, "Problem creating fragment %s", fragmentTag);
            }
            catch (IllegalAccessException e)
            {
                Timber.e(e, "Problem creating fragment %s", fragmentTag);
            }
            catch (InstantiationException e)
            {
                Timber.e(e, "Problem creating fragment %s", fragmentTag);
            }
            catch (InvocationTargetException e)
            {
                Timber.e(e, "Problem creating fragment %s", fragmentTag);
            }
        }
        else
        {
            transaction.show(fragment);
        }

        currentFragment = itemId;
        currentFragmentClass = c;
    }

    private void unbindService()
    {
        server = null;
        manager = null;

        if (managerService != null)
        {
            unbindService(serviceConnection);
            managerService = null;
        }
    }

    private void applyServer()
    {
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;

        if (server.getName() != null && server.getName().length() > 0)
            toolbarTitle.setText(server.getName());
        else
            toolbarTitle.setText(server.getAddressString());
    }

    public void setBottomNavigationVisible(boolean visible)
    {
        if (visible)
            bottomNavigation.setVisibility(View.VISIBLE);
        else
            bottomNavigation.setVisibility(View.GONE);
    }

    public void setActionBarTitleVisible(boolean visible, boolean animate)
    {
        if (toolbarTitleVisible != visible)
        {
            toolbarTitleVisible = visible;
            toolbarTitle.clearAnimation();
            if (animate)
                toolbarTitle.animate().alpha(visible ? 1.0f : 0.0f).start();
            else
                toolbarTitle.setAlpha(visible ? 1.0f : 0.0f);
        }
    }

    public ServerManager getServerManager()
    {
        return manager;
    }

    public Server getServer()
    {
        return server;
    }

    @Override
    public void onRefresh()
    {
        server.update(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.getServer() == server)
            applyServer();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAutoRefreshChange(AutoRefreshChangeEvent event)
    {
        if (event.autoRefreshEnabled)
        {
            refreshing = true;
            new Thread(refreshRunnable).start();
        }
        else
        {
            refreshing = false;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerCompletedFindingCommands(ServerCommandSearchCompleteEvent event)
    {
        if (event.getServer().equals(server))
            invalidateOptionsMenu();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerSettingsChanged(ServerSettingsUpdatedEvent event)
    {
        if (event.getServer() == server)
        {
            server.update(true);
            bottomNavigation.getMenu().findItem(R.id.navigation_rcon)
                            .setVisible(server.hasAdminPassword());
            bottomNavigation.getMenu().findItem(R.id.navigation_chat)
                            .setVisible(server.isChatPossible());
            bottomNavigation.invalidate();
            if (!server.hasAdminPassword())
            {
                if (server.isRconConnected())
                    server.closeRconConnection();

                if (currentFragment == R.id.navigation_rcon)
                    showFragment(R.id.navigation_info);
            }

            if (!server.isChatPossible())
            {
                if (currentFragment == R.id.navigation_chat)
                    showFragment(R.id.navigation_info);
            }
        }
    }

    @Override
    public void onViewPositionChanged(View mView, float swipeBackFraction, float swipeBackFactor) {}

    @Override
    public void onViewSwipeFinished(View mView, boolean isEnd)
    {
        if (isEnd)
        {
            animateFinish = false;
            finish();
        }
    }

    public interface ServiceConnectionListener
    {
        void serviceConnected(Server server);
    }

    private class ServerRefreshRunnable implements Runnable
    {
        @Override
        public void run()
        {
            while (refreshing)
            {
                if (server != null)
                    server.update(true);

                try
                {
                    Thread.sleep(refreshInterval);
                }
                catch (InterruptedException ignored)
                {
                }
            }
        }
    }
}
