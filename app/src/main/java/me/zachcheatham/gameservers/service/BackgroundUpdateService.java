package me.zachcheatham.gameservers.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ServersCompletedUpdatingEvent;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.ServerManagerService;

public class BackgroundUpdateService extends Service
{
    private static final int NOTIFICATION_ID = 12;

    private ServerManager manager = null;
    private ServerManager.ResourceLevel restoreLevel = null;

    private final ServiceConnection serviceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder)
        {
            ServerManagerService.LocalBinder binder = (ServerManagerService.LocalBinder) iBinder;
            manager = binder.getService().getManager();
            if (manager.getResourceLevel().getValue() < ServerManager.ResourceLevel.BG_SYNC.getValue())
            {
                restoreLevel = manager.getResourceLevel();
                manager.setResourceLevel(ServerManager.ResourceLevel.BG_SYNC);
            }
            updateServers();
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            stopForeground(true);
            stopSelf();
        }
    };

    private static Notification createNotification(Context context)
    {
        return new NotificationCompat.Builder(context, Constants.NOTIFICATION_CHANNEL_BACKGROUND)
                .setContentTitle(context.getString(R.string.notification_background_updating_running))
                .setSmallIcon(R.drawable.ic_chat_notification)
                .setOngoing(true).build();
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        EventBus.getDefault().register(this);

        Intent serviceIntent = new Intent(this, ServerManagerService.class);
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        EventBus.getDefault().unregister(this);

        if (restoreLevel != null)
            manager.setResourceLevel(restoreLevel);
        manager = null;
        unbindService(serviceConnection);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String name = getString(R.string.notification_background_worker);
            NotificationChannel channel = new NotificationChannel(
                    Constants.NOTIFICATION_CHANNEL_BACKGROUND, name,
                    NotificationManager.IMPORTANCE_NONE);
            channel.setDescription(getString(R.string.notification_background_worker_desc));
            notificationManager.createNotificationChannel(channel);
        }

        startForeground(NOTIFICATION_ID, createNotification(this));

        return START_STICKY;
    }

    private void updateServers()
    {
        boolean noServers = true;
        for (int i = 0; i < manager.getCount(); i++)
        {
            Server s = manager.getServer(i);
            if (s.hasFlag(Server.FLAG_BACKGROUND_UPDATE))
            {
                noServers = false;
                s.update(false);
            }
        }

        if (noServers)
        {
            stopForeground(true);
            stopSelf();
        }
    }

    @Subscribe
    public void onServersCompletedUpdating(ServersCompletedUpdatingEvent e)
    {
        stopForeground(true);
        stopSelf();
    }
}
