package me.zachcheatham.gameservers.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.ServerManagerService;

public class PersistRCONService extends Service
{
    public static final String ACTION_STOP_SERVICE = "stop";
    private static final int NOTIFICATION_ID = 11;
    private ServerManagerService managerService;
    private ServerManager manager;
    private final ServiceConnection serviceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder)
        {
            ServerManagerService.LocalBinder binder = (ServerManagerService.LocalBinder) iBinder;
            managerService = binder.getService();
            manager = managerService.getManager();
            manager.setResourceLevel(ServerManager.ResourceLevel.PERSIST_RCON);

            int reasonsToLive = 0;
            for (int i = 0; i < manager.getCount(); i++)
            {
                Server server = manager.getServer(i);
                if (!server.isRconConnected() || !server.supportsLogging())
                    server.closeAllConnections();
                else
                    reasonsToLive++;
            }

            if (reasonsToLive == 0)
            {
                stopForeground(true);
                stopSelf();
            }
            else
            {
                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager
                        .notify(NOTIFICATION_ID, createNotification(PersistRCONService.this,
                                getString(R.string.notification_background_worker_running,
                                        reasonsToLive,
                                        getString(reasonsToLive >
                                                  1 ? R.string.sessions : R.string.session))));
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            managerService = null;
            manager = null;
        }
    };

    private static Notification createNotification(Context context, CharSequence contentTitle)
    {
        Intent intent = new Intent(context, PersistRCONService.class);
        intent.setAction(PersistRCONService.ACTION_STOP_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, 0);

        return new NotificationCompat.Builder(context, Constants.NOTIFICATION_CHANNEL_BACKGROUND)
                .setContentTitle(contentTitle)
                .setContentText(context.getString(R.string.notification_background_worker_stop))
                .setSmallIcon(R.drawable.ic_console_line_white)
                .addAction(R.drawable.ic_close_white, context.getString(R.string.action_disconnect),
                        pendingIntent)
                .setOngoing(true).build();
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        Intent serviceIntent = new Intent(this, ServerManagerService.class);
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        unbindService(serviceConnection);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent != null && intent.getAction() != null &&
               intent.getAction().equals(ACTION_STOP_SERVICE))
        {
            stopForeground(true);
            stopSelf();
        }
        else
        {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                String name = getString(R.string.notification_background_worker);
                NotificationChannel channel = new NotificationChannel(
                        Constants.NOTIFICATION_CHANNEL_BACKGROUND, name,
                        NotificationManager.IMPORTANCE_NONE);
                channel.setDescription(getString(R.string.notification_background_worker_desc));
                notificationManager.createNotificationChannel(channel);
            }

            startForeground(NOTIFICATION_ID,
                    createNotification(this,
                            getString(R.string.notification_background_worker_starting)));
        }

        return START_STICKY;
    }
}
