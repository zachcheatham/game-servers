package me.zachcheatham.gameservers.service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;
import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.RconLogAddedEvent;
import me.zachcheatham.gameservers.server.ChatMessage;
import me.zachcheatham.gameservers.server.ChatTarget;
import me.zachcheatham.gameservers.server.LogHolder;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.ServerManagerService;
import me.zachcheatham.gameservers.server.notifications.MessageMarshaller;
import me.zachcheatham.gameservers.server.notifications.Notifier;

public class NotificationHelperService extends Service
{
    public static final String ACTION_DELETE = "d";
    public static final String ACTION_REPLY = "r";
    public static final String EXTRA_KEY_SERVER_INDEX = "s";
    public static final String EXTRA_KEY_NOTIFICATION_TYPE = "t";
    public static final String BUNDLE_KEY_REPLY_MESSAGE = "m";

    private ServerManager manager;
    private Server server;
    private String message;
    private int notificationType;
    private int serverIndex;
    private boolean handled = false;

    private final ServiceConnection serviceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            ServerManagerService.LocalBinder binder = (ServerManagerService.LocalBinder) service;
            manager = binder.getService().getManager();
            server = manager.getServer(serverIndex);

            EventBus.getDefault().register(NotificationHelperService.this);

            if (!server.isRconConnected())
            {
                if (server.hasAdminPassword() && !server.hasBadAdminPassword())
                    server.openRCONConnection();
                else
                    notifyFailedMessage();
            }
            else
                sendChatMessage();
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            manager = null;
            server = null;
            EventBus.getDefault().unregister(this);
            stopSelf();
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent.getAction() != null)
        {
            if (intent.getAction().equals(ACTION_DELETE))
            {
                int type = intent.getIntExtra(EXTRA_KEY_NOTIFICATION_TYPE, -1);
                int server = intent.getIntExtra(EXTRA_KEY_SERVER_INDEX, -1);

                MessageMarshaller.removeChatHistory(
                        getSharedPreferences(Constants.PREFS_KEY_NOTIFICATIONS, 0),
                        type, server);
            }
            else if (intent.getAction().equals(ACTION_REPLY))
            {
                notificationType = intent.getIntExtra(EXTRA_KEY_NOTIFICATION_TYPE, 0);
                serverIndex = intent.getIntExtra(EXTRA_KEY_SERVER_INDEX, -1);

                Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
                message = remoteInput.getCharSequence(BUNDLE_KEY_REPLY_MESSAGE).toString();

                Intent serviceIntent = new Intent(this, ServerManagerService.class);
                bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
            }
        }

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if (manager != null)
        {
            EventBus.getDefault().unregister(this);
            manager = null;
            server = null;
            unbindService(serviceConnection);
        }
    }

    private void sendChatMessage()
    {
        handled = true;

        ChatMessage.MessageType type =
                notificationType == Notifier.NOTIFICATION_TYPE_ADMIN_CHAT ?
                        ChatMessage.MessageType.ADMIN :
                        ChatMessage.MessageType.NORMAL;
        server.sendChatMessage(message, new ChatTarget(type));

        stopSelf();
    }

    private void notifyFailedMessage()
    {
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this, notificationType ==
                                                        Notifier.NOTIFICATION_TYPE_ADMIN_CHAT ?
                        Constants.NOTIFICATION_CHANNEL_ADMIN_CHAT :
                        Constants.NOTIFICATION_CHANNEL_CHAT)
                        .setContentText(getString(R.string.error_sending_chat))
                        .setSmallIcon(R.drawable.ic_message_text_white);

        int notificationId = serverIndex * 2;
        if (notificationType == Notifier.NOTIFICATION_TYPE_ADMIN_CHAT)
            notificationId++;

        notificationManager.notify(notificationId, builder.build());

        handled = true;
        stopSelf();
    }

    @Subscribe
    public void onRconLog(RconLogAddedEvent event)
    {
        if (!handled && event.server == server)
        {
            if (event.logHolder.type == LogHolder.LogType.AUTHENTICATED)
                sendChatMessage();
            else if (event.logHolder.type == LogHolder.LogType.BANNED ||
                     event.logHolder.type == LogHolder.LogType.DISCONNECTED ||
                     event.logHolder.type == LogHolder.LogType.INVALID_PASSWORD)
            {
                notifyFailedMessage();
            }
        }
    }
}
