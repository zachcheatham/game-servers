package me.zachcheatham.gameservers.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.MinecraftServer;

public class MinecraftModsListAdapter
        extends RecyclerView.Adapter<MinecraftModsListAdapter.ViewHolder>
{
    private final MinecraftServer server;

    public MinecraftModsListAdapter(Activity a, MinecraftServer s)
    {
        server = s;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        EventBus.getDefault().unregister(this);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.list_item_server_rule, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position)
    {
        String[] modInfo = server.getModList()[position];

        holder.variableName.setText(modInfo[0]);
        holder.variableValue.setText(modInfo[1]);
    }

    @Override
    public int getItemCount()
    {
        if (server == null || !server.hasMods())
            return 0;
        else
            return server.getModList().length;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.getServer() == server)
            notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.variable_name)
        TextView variableName;
        @BindView(R.id.variable_value)
        TextView variableValue;

        ViewHolder(View view)
        {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
