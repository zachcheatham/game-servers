package me.zachcheatham.gameservers.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;

public class SelectServersListAdapter extends RecyclerView.Adapter<SelectServersListAdapter.ViewHolder>
{
    private final ServerManager manager;

    private ServerClickedListener listener = null;

    public SelectServersListAdapter(ServerManager manager)
    {
        this.manager = manager;
    }

    public void setServerClickedListener(ServerClickedListener listener)
    {
        this.listener = listener;
        notifyItemRangeChanged(0, getItemCount());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        EventBus.getDefault().unregister(this);
    }

    @NonNull
    @Override
    public SelectServersListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {

        View view = LayoutInflater.from(parent.getContext())
              .inflate(R.layout.list_item_server_selectable, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectServersListAdapter.ViewHolder vh, int position)
    {
        Server server = manager.getServer(position);

        vh.name.setText(server.getName());
        vh.address.setText(server.getAddressString());

        int gameLogo = server.getGameLogoResource();
        if (gameLogo == 0)
            vh.gameLogo.setImageBitmap(server.getGameLogoBitmap());
        else
            vh.gameLogo.setImageResource(gameLogo);

        if (listener != null)
            vh.checkBox.setChecked(listener.shouldServerBeChecked(server));
    }

    @Override
    public int getItemCount()
    {
        return manager.getCount();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        notifyItemChanged(manager.getServerIndex(event.getServer()));
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        @BindView(R.id.game_logo) ImageView gameLogo;
        @BindView(R.id.server_players) TextView address;
        @BindView(R.id.server_hostname) TextView name;
        @BindView(R.id.checkbox) CheckBox checkBox;

        ViewHolder(View itemView)
        {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View view)
        {
            if (listener != null)
            {
                Server server = manager.getServer(getAdapterPosition());
                checkBox.setChecked(!checkBox.isChecked());
                listener.onServerClicked(server, checkBox.isChecked());
            }
        }
    }

    public interface ServerClickedListener
    {
        void onServerClicked(Server server, boolean checked);
        boolean shouldServerBeChecked(Server server);
    }
}
