package me.zachcheatham.gameservers.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.Server;

public class ServerImportListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private static final int VIEW_TYPE_WARNING = 0;
    private static final int VIEW_TYPE_SERVER = 1;

    private final List<Server> servers;
    private final SparseBooleanArray selectedServers = new SparseBooleanArray();
    private final int serversExisted;
    private final int serversUnsupported;
    private Context context;

    public ServerImportListAdapter(List<Server> servers, int serversExisted, int serversUnsupported)
    {
        this.servers = servers;
        this.serversExisted = serversExisted;
        this.serversUnsupported = serversUnsupported;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        context = null;
        EventBus.getDefault().unregister(this);
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position >= servers.size())
            return VIEW_TYPE_WARNING;
        else
            return VIEW_TYPE_SERVER;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if (viewType == VIEW_TYPE_WARNING)
        {
            View view = LayoutInflater.from(parent.getContext())
                                      .inflate(R.layout.list_item_warning, parent, false);

            return new WarningViewHolder(view);
        }
        else
        {
            View view = LayoutInflater.from(parent.getContext())
                                      .inflate(R.layout.list_item_server_selectable, parent, false);

            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder vh, int position)
    {
        if (vh.getItemViewType() == VIEW_TYPE_SERVER)
        {
            ViewHolder viewHolder = (ViewHolder) vh;
            Server server = servers.get(position);

            if (server.getName().length() > 0)
                viewHolder.hostname.setText(server.getName());
            else
                viewHolder.hostname.setText(server.getAddressString());

            if (server.isOffline())
            {
                viewHolder.players.setText(R.string.offline);
                viewHolder.players
                        .setTextColor(context.getResources().getColor(R.color.colorTextError));
                viewHolder.map.setText("");
            }
            else
            {
                if (server.getMap().length() > 0)
                    viewHolder.map.setText(context.getString(R.string.label_map, server.getMap()));
                else
                    viewHolder.map.setText("");

                if (server.getRealNumPlayers() < server.getNumPlayers())
                    viewHolder.players
                            .setText(context.getString(R.string.label_players_bots,
                                    server.getRealNumPlayers(),
                                    server.getNumPlayers(), server.getMaxPlayers()));
                else
                    viewHolder.players
                            .setText(context.getString(R.string.label_players,
                                    server.getNumPlayers(),
                                    server.getMaxPlayers()));

                if (server.getNumPlayers() == 0)
                    viewHolder.players.setTextColor(
                            context.getResources().getColor(R.color.colorTextPlayersEmpty));
                else if (server.getMaxPlayers() <= server.getNumPlayers())
                    viewHolder.players
                            .setTextColor(context.getResources().getColor(R.color.colorTextError));
                else
                    viewHolder.players.setTextColor(
                            context.getResources().getColor(R.color.colorTextPlayers));
            }

            if (server.getGameLogoResource() != 0)
                viewHolder.gameLogo.setImageResource(server.getGameLogoResource());
            else
            {
                Bitmap logo = server.getGameLogoBitmap();
                if (logo != null)
                    viewHolder.gameLogo.setImageBitmap(logo);
            }
        }
        else if (vh.getItemViewType() == VIEW_TYPE_WARNING)
        {
            WarningViewHolder viewHolder = (WarningViewHolder) vh;

            int wn = position - servers.size();
            if (wn == 0)
            {
                if (serversExisted == 0)
                    viewHolder.text.setText(String.format(
                            context.getString(serversUnsupported >
                                              1 ? R.string.import_servers_unsupported_plural :
                                    R.string.import_servers_unsupported),
                            serversUnsupported));
                else
                    viewHolder.text.setText(String.format(
                            context.getString(serversExisted >
                                              1 ? R.string.import_servers_existing_plural :
                                    R.string.import_servers_existing),
                            serversExisted));
            }
            else
            {
                viewHolder.text.setText(String.format(
                        context.getString(serversUnsupported >
                                          1 ? R.string.import_servers_unsupported_plural :
                                R.string.import_servers_unsupported),
                        serversUnsupported));
            }
        }
    }

    @Override
    public int getItemCount()
    {
        int count = servers.size();
        if (serversUnsupported > 0)
            count++;
        if (serversExisted > 0)
            count++;

        return count;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (servers.contains(event.getServer()) && event.didChange())
        {
            final int position = servers.indexOf(event.getServer());
            notifyItemChanged(position);
        }
    }

    public List<Server> getSelectedServers()
    {
        List<Server> selectedServers = new ArrayList<>();

        for (int i = 0; i < servers.size(); i++)
        {
            if (this.selectedServers.get(i, true))
                selectedServers.add(servers.get(i));
        }

        return selectedServers;
    }

    public boolean anyServerSelected()
    {
        for (int i = 0; i < servers.size(); i++)
        {
            if (selectedServers.get(i, true))
                return true;
        }

        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        @BindView(R.id.server_hostname)
        TextView hostname;
        @BindView(R.id.server_map)
        TextView map;
        @BindView(R.id.server_players)
        TextView players;
        @BindView(R.id.game_logo)
        ImageView gameLogo;
        @BindView(R.id.checkbox)
        CheckBox checkBox;

        ViewHolder(View itemView)
        {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View view)
        {
            boolean checked = !checkBox.isChecked();
            checkBox.setChecked(checked);
            selectedServers.put(getAdapterPosition(), checked);
        }
    }

    public class WarningViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.text) TextView text;

        WarningViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
