package me.zachcheatham.gameservers.adapter;

import android.app.Activity;
import android.content.Context;

import com.squareup.picasso.Picasso;

import java.util.Comparator;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.player.MinecraftPlayer;
import me.zachcheatham.gameservers.server.player.Player;

public class MinecraftPlayerListAdapter extends PlayerListAdapter
{
    public MinecraftPlayerListAdapter(Activity a, Server s)
    {
        super(a, s);
    }

    @Override
    void onBindPlayer(PlayerListAdapter.ViewHolder holder, Player player)
    {
        MinecraftPlayer mcPlayer = (MinecraftPlayer) player;

        Context c = holder.details.getContext();
        if (mcPlayer.getUniqueID() != null)
        {
            holder.details.setText(
                    c.getString(R.string.player_details_minecraft, mcPlayer.getUniqueID()));

            Picasso.get().load(String.format("https://minotar.net/avatar/%s", mcPlayer.getUniqueID().replaceAll("-", "")))
                   .into(holder.avatar);
        }
        else
        {

            holder.avatar.setImageResource(R.drawable.ic_empty);
            holder.details.setText(c.getString(R.string.player_details_minecraft,
                    c.getString(R.string.unknown)));
        }
    }

    @Override
    boolean shouldSort(ServerUpdatedEvent event)
    {
        return false;
    }

    @Override
    protected Comparator<Integer> getComparator()
    {
        return new PlayersComparator();
    }

    @Override
    public int getSortMenu()
    {
        return -1;
    }

    @Override
    public int getSortMenuItemID(int sortMode)
    {
        return -1;
    }

    @Override
    public int getDefaultSortMode()
    {
        return SORTMODE_NAME;
    }

    @Override
    public int getSelectedSortMode(int menuItemId)
    {
        return -1;
    }

    @Override
    public String getSortingConfigID()
    {
        return null;
    }

    private class PlayersComparator implements java.util.Comparator<Integer>
    {
        @Override
        public int compare(Integer o1, Integer o2)
        {
            Player p1 = server.getPlayer(o1);
            Player p2 = server.getPlayer(o2);

            switch (sortMode)
            {
            default:
            case SORTMODE_NAME:
                return p1.getName().compareTo(p2.getName());
            }
        }
    }
}
