package me.zachcheatham.gameservers.adapter;

import android.app.Activity;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ChatAddedEvent;
import me.zachcheatham.gameservers.events.ChatTrimEvent;
import me.zachcheatham.gameservers.server.ChatMessage;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.player.AvatarHelper;
import me.zachcheatham.gameservers.server.player.Player;
import me.zachcheatham.gameservers.server.player.SteamAvatarHelper;
import me.zachcheatham.gameservers.ui.CircleImageTransform;

public class ChatAdapter
        extends RecyclerView.Adapter<ChatAdapter.ViewHolder> implements AvatarHelper.AvatarLoadListener
{
    private final Activity activity;
    private final Server server;
    private final AvatarHelper avatarHelper;
    private int cachedSize = 0;
    private RecyclerView recyclerView;

    public ChatAdapter(Activity a, Server s)
    {
        activity = a;
        server = s;
        switch (server.getType())
        {
        case Source:
        case Gold_Src:
            avatarHelper = new SteamAvatarHelper();
            break;
        case Minecraft:
        default:
            avatarHelper = null;
            break;
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
        if (server.getChatLog() != null)
        {
            this.cachedSize = server.getChatLog().size();

            // Fetch avatars for all members of chat
            List<Player> playersToFetch = new ArrayList<>();
            for (ChatMessage message : server.getChatLog())
            {
                Player p = server.getPlayer(message.playerKey);
                if (p != null)
                    playersToFetch.add(server.getPlayer(message.playerKey));
            }

            avatarHelper.loadAvatars(playersToFetch.toArray(new Player[0]), this);
        }

        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        this.recyclerView = null;
        EventBus.getDefault().unregister(this);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.list_item_chat, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position)
    {
        if (server.getRconLog() != null && position < server.getRconLog().size())
        {
            ChatMessage message = server.getChatLog().get(position);
            if (message.playerKey == -1) // Player is the console
            {
                holder.username.setText(R.string.player_console);
                holder.avatar.setImageResource(R.drawable.ic_steam_avatar_unknown);
            }
            else
            {
                Player player = server.getPlayer(message.playerKey);
                if (player == null)
                {
                    holder.username.setText(R.string.chat_disconnected_player);
                    holder.avatar.setImageResource(R.drawable.ic_steam_avatar_unknown);
                }
                else
                {
                    holder.username.setText(player.getName());
                    if (avatarHelper.hasAvatar(player))
                        Picasso.get().load(avatarHelper.getAvatar(player))
                               .transform(new CircleImageTransform()).into(holder.avatar);
                    else
                        avatarHelper.loadAvatars(new Player[]{player}, this);
                }
            }

            holder.message.setText(message.message);
            holder.timestamp.setText(DateUtils.formatDateTime(activity, message.timestamp * 1000L, DateUtils.FORMAT_SHOW_TIME));
            if (message.type != ChatMessage.MessageType.NORMAL)
            {
                holder.targetChat.setVisibility(View.VISIBLE);
                switch (message.type)
                {
                case TEAM:
                    holder.targetChat.setText(String.format(Locale.US, "(%s)", message.target));
                    break;
                case ADMIN:
                    holder.targetChat.setText(R.string.admin_chat_type);
                    break;
                case PM:
                    holder.targetChat.setText(activity.getString(R.string.player_chat_type, message.target));
                    break;
                }
            }
            else
            {
                holder.targetChat.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return cachedSize;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChatMessage(ChatAddedEvent event)
    {
        if (event.getServer() == server)
        {
            cachedSize++;
            notifyItemInserted(cachedSize - 1);
            recyclerView.scrollToPosition(cachedSize - 1);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChatTrim(ChatTrimEvent event)
    {
        if (event.server == server)
        {
            cachedSize -= event.count;
            notifyItemRangeRemoved(0, event.count);
        }
    }

    @Override
    public void avatarsLoaded()
    {
        activity.runOnUiThread(this::notifyDataSetChanged);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.image_avatar) ImageView avatar;
        @BindView(R.id.text_user_name) TextView username;
        @BindView(R.id.text_target_chat) TextView targetChat;
        @BindView(R.id.text_timestamp) TextView timestamp;
        @BindView(R.id.text_message) TextView message;

        ViewHolder(View view)
        {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
