package me.zachcheatham.gameservers.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.ServerDetailsActivity;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.MinecraftServer;

public class MinecraftServerInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements View.OnClickListener
{
    private static final int VIEW_TYPE_DEFAULT = 0;
    private static final DetailsListItem[] CLIENT_METHOD_LIST = new DetailsListItem[]{
            DetailsListItem.ADDRESS,
            DetailsListItem.PING,
            DetailsListItem.GAME,
            DetailsListItem.PLAYERS,
            DetailsListItem.VERSION,
            DetailsListItem.QUERY_TYPE};
    private static final DetailsListItem[] QUERY_METHOD_LIST = new DetailsListItem[]{
            DetailsListItem.ADDRESS,
            DetailsListItem.PING,
            DetailsListItem.GAME,
            DetailsListItem.GAME_TYPE,
            DetailsListItem.PLAYERS,
            DetailsListItem.MAP,
            DetailsListItem.VERSION,
            DetailsListItem.QUERY_TYPE};

    private final static int VIEW_TYPE_MORE = 1;

    private final ServerDetailsActivity activity;
    private final MinecraftServer server;

    public MinecraftServerInfoAdapter(Activity a, MinecraftServer s)
    {
        activity = (ServerDetailsActivity) a;
        server = s;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position == (getItemCount() - 1) && server.hasMods())
            return VIEW_TYPE_MORE;
        else
            return VIEW_TYPE_DEFAULT;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater li = LayoutInflater.from(parent.getContext());
        if (viewType == VIEW_TYPE_DEFAULT)
        {
            View view = li.inflate(R.layout.list_item_server_info, parent, false);
            return new ViewHolder(view);
        }
        else
        {
            View view = li.inflate(R.layout.list_item_view_more, parent, false);
            ((TextView) view.findViewById(R.id.label)).setText(R.string.installed_mods);
            ((ImageView) view.findViewById(R.id.variable_icon))
                    .setImageResource(R.drawable.ic_extension_white);

            return new ViewMoreViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int position)
    {
        if (viewHolder.getItemViewType() == VIEW_TYPE_DEFAULT)
        {
            DetailsListItem detailsListItemType;
            if (server.usesQuery())
                detailsListItemType = QUERY_METHOD_LIST[position];
            else
                detailsListItemType = CLIENT_METHOD_LIST[position];

            ViewHolder holder = (ViewHolder) viewHolder;
            switch (detailsListItemType)
            {
            case ADDRESS:
                holder.variableImage.setImageResource(R.drawable.ic_dns_white);
                holder.variableName.setText(R.string.address);
                holder.variableValue.setText(server.getAddressString());
                break;
            case PING:
                holder.variableImage.setImageResource(R.drawable.ic_network_check_white);
                holder.variableName.setText(R.string.ping);
                holder.variableValue
                        .setText(holder.variableName.getContext().getString(R.string.format_ping,
                                server.getPing()));
                break;
            case GAME:
                holder.variableImage.setImageResource(R.drawable.ic_gamepad_white);
                holder.variableName.setText(R.string.game_desc);
                holder.variableValue.setText(R.string.minecraft);
                break;
            case GAME_TYPE:
                holder.variableImage.setImageResource(R.drawable.ic_gamepad_white);
                holder.variableName.setText(R.string.label_game_type);
                holder.variableValue.setText(server.getGameType());
                break;
            case PLAYERS:
                holder.variableImage.setImageResource(R.drawable.ic_people_white);
                holder.variableName.setText(R.string.players);
                holder.variableValue
                        .setText(holder.variableName.getContext().getString(R.string.label_players,
                                server.getNumPlayers(), server.getMaxPlayers()));
                break;
            case MAP:
                holder.variableImage.setImageResource(R.drawable.ic_map_white);
                holder.variableName.setText(R.string.map);
                holder.variableValue.setText(server.getMap());
                break;
            case QUERY_TYPE:
                holder.variableImage.setImageResource(0);
                holder.variableName.setText(R.string.query_method);
                if (server.usesQuery())
                    holder.variableValue.setText(R.string.query_method_query);
                else
                    holder.variableValue.setText(R.string.query_method_client);
                break;
            case VERSION:
                holder.variableImage.setImageResource(R.drawable.ic_package_variant_white);
                holder.variableName.setText(R.string.game_version);
                holder.variableValue.setText(server.getVersion());
                break;
            }
        }
    }

    @Override
    public int getItemCount()
    {
        if (server == null)
            return 0;
        else if (server.usesQuery())
            return QUERY_METHOD_LIST.length + (server.hasMods() ? 1 : 0);
        else
            return CLIENT_METHOD_LIST.length + (server.hasMods() ? 1 : 0);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.getServer() == server)
            notifyDataSetChanged();
    }

    @Override
    public void onClick(View view)
    {
        activity.showFragment(1);
    }

    private enum DetailsListItem
    {
        ADDRESS, PING, GAME, GAME_TYPE, PLAYERS, MAP, VERSION, MOD_LIST, QUERY_TYPE
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.variable_icon)
        ImageView variableImage;
        @BindView(R.id.variable_name)
        TextView variableName;
        @BindView(R.id.variable_value)
        TextView variableValue;

        ViewHolder(View view)
        {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class ViewMoreViewHolder extends RecyclerView.ViewHolder
    {
        ViewMoreViewHolder(View itemView)
        {
            super(itemView);
            itemView.setOnClickListener(MinecraftServerInfoAdapter.this);
        }
    }
}
