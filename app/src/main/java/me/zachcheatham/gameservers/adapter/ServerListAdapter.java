package me.zachcheatham.gameservers.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.ServerDetailsActivity;
import me.zachcheatham.gameservers.events.ServerAddedEvent;
import me.zachcheatham.gameservers.events.ServerIndexChangedEvent;
import me.zachcheatham.gameservers.events.ServerRemovedEvent;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;

public class ServerListAdapter extends RecyclerView.Adapter<ServerListAdapter.ViewHolder>
{
    public static final int SORTMODE_NUM_PLAYERS = 0;
    public static final int SORTMODE_MAX_PLAYERS = 1;
    public static final int SORTMODE_NAME = 2;
    public static final int SORTMODE_MAP = 3;
    private final Activity activity;
    private final ServerComparator comparator = new ServerComparator();
    private List<Integer> sortedIndexes = new ArrayList<>();
    private ServerManager manager;
    private int sortMode = SORTMODE_NUM_PLAYERS;
    private boolean sorting = false;
    private boolean sortAgain = false;

    public ServerListAdapter(Activity a)
    {
        this.activity = a;
    }

    public void setManager(ServerManager manager)
    {
        this.manager = manager;

        if (manager != null)
        {
            sort();
        }
        else
        {
            activity.runOnUiThread(() ->
            {
                sortedIndexes.clear();
                notifyDataSetChanged();
            });
        }
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        EventBus.getDefault().unregister(this);
    }

    public void setSorting(int sortMode)
    {
        this.sortMode = sortMode;
        sort();
    }

    public int getSortMode()
    {
        return sortMode;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.list_item_server, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position)
    {
        if (position > sortedIndexes.size())
            return;

        Integer index = sortedIndexes.get(position);
        if (index != null && manager != null)
        {
            Server server = manager.getServer(index);

            if (server != null)
            {
                if (server.isOffline())
                {
                    viewHolder.connectivityIndicator.setVisibility(View.VISIBLE);
                    switch (server.getOfflineReason())
                    {
                    case Generic:
                    default:
                        viewHolder.connectivityIndicator.setText(R.string.offline);
                        break;
                    case Unknown_Host:
                        viewHolder.connectivityIndicator.setText(R.string.offline_unknown_host);
                        break;
                    case Timeout:
                        viewHolder.connectivityIndicator.setText(R.string.offline_timeout);
                        break;
                    case Error:
                        viewHolder.connectivityIndicator.setText(R.string.error);
                        break;
                    case Banned:
                        viewHolder.connectivityIndicator.setText(R.string.offline_banned);
                        break;
                    }
                }
                else
                    viewHolder.connectivityIndicator.setVisibility(View.GONE);

                if (server.getName().length() > 0)
                    viewHolder.hostname.setText(server.getName());
                else
                    viewHolder.hostname.setText(server.getAddressString());

                if (server.getMap().length() > 0)
                    viewHolder.map
                            .setText(activity.getString(R.string.label_map, server.getMap()));
                else
                    viewHolder.map.setText("");

                if (server.getRealNumPlayers() < server.getNumPlayers())
                    viewHolder.players
                            .setText(activity.getString(R.string.label_players_bots,
                                    server.getRealNumPlayers(),
                                    server.getNumPlayers(), server.getMaxPlayers()));
                else
                    viewHolder.players
                            .setText(activity.getString(R.string.label_players,
                                    server.getNumPlayers(),
                                    server.getMaxPlayers()));

                if (server.getNumPlayers() == 0)
                    viewHolder.players.setTextColor(
                            activity.getResources().getColor(R.color.colorTextPlayersEmpty));
                else if (server.getMaxPlayers() <= server.getNumPlayers())
                    viewHolder.players
                            .setTextColor(activity.getResources().getColor(R.color.colorTextError));
                else
                    viewHolder.players.setTextColor(
                            activity.getResources().getColor(R.color.colorTextPlayers));

                if (server.getGameLogoResource() != 0)
                    viewHolder.gameLogo.setImageResource(server.getGameLogoResource());
                else
                {
                    Bitmap logo = server.getGameLogoBitmap();
                    if (logo != null)
                        viewHolder.gameLogo.setImageBitmap(logo);
                }
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return sortedIndexes.size();
    }

    public int getSortedIndex(int position)
    {
        return sortedIndexes.get(position);
    }

    private void sort()
    {
        sort(false);
    }

    private void sort(boolean ignoreExistingSort)
    {
        if (manager != null)
        {
            if (!sorting || ignoreExistingSort)
            {
                sorting = true;

                final List<Integer> sortedIndexes = new ArrayList<>();
                for (int i = 0; i < manager.getCount(); i++)
                {
                    sortedIndexes.add(i);
                }

                Collections.sort(sortedIndexes, comparator);

                activity.runOnUiThread(() ->
                {
                    ServerListAdapter.this.sortedIndexes = sortedIndexes;
                    notifyDataSetChanged();
                });

                if (!sortAgain)
                    sorting = false;
                else
                {
                    sortAgain = false;
                    sort(true);
                }
            }
            else
            {
                sortAgain = true;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onServersIndexChanged(ServerIndexChangedEvent event)
    {
        if (manager != null)
        {
            sort();
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (manager != null && manager.hasServer(event.getServer()))
        {
            if (event.didOfflineSortThresholdChange() ||
                (sortMode == SORTMODE_NAME && event.didNameChange()) ||
                (sortMode == SORTMODE_NUM_PLAYERS && event.didNumPlayersChange()) ||
                (sortMode == SORTMODE_MAX_PLAYERS && event.didMaxPlayersChange()) ||
                (sortMode == SORTMODE_MAP && event.didMapChange()))
            {
                sort();
            }
            else if (event.didChange())
            {
                final int position = sortedIndexes
                        .indexOf(manager.getServerIndex(event.getServer()));
                activity.runOnUiThread(() -> notifyItemChanged(position));
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onServerAdded(final ServerAddedEvent event)
    {
        if (manager != null)
            sort();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerRemoved(ServerRemovedEvent event)
    {
        if (manager != null)
        {
            int index = sortedIndexes.indexOf(event.serverIndex);
            sortedIndexes.remove(index);
            notifyItemRemoved(index);
            for (int i = 0; i < sortedIndexes.size(); i++)
            {
                int sortedIndex = sortedIndexes.get(i);
                if (sortedIndexes.get(i) > event.serverIndex)
                {
                    sortedIndexes.remove(i);
                    sortedIndexes.add(i, sortedIndex - 1);
                }
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        @BindView(R.id.server_hostname)
        TextView hostname;
        @BindView(R.id.server_map)
        TextView map;
        @BindView(R.id.server_players)
        TextView players;
        @BindView(R.id.connectivity_indicator)
        TextView connectivityIndicator;
        @BindView(R.id.game_logo)
        ImageView gameLogo;

        ViewHolder(View itemView)
        {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View view)
        {
            if (getAdapterPosition() < sortedIndexes.size() && getAdapterPosition() >= 0)
            {
                Intent intent = new Intent(activity, ServerDetailsActivity.class);
                intent.putExtra(
                        ServerDetailsActivity.EXTRA_KEY_SERVER_INDEX,
                        sortedIndexes.get(getAdapterPosition()));
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_left, R.anim.hold);
            }
        }
    }

    private class ServerComparator implements java.util.Comparator<Integer>
    {
        @Override
        public int compare(Integer o1, Integer o2)
        {
            Server s1 = manager.getServer(o1);
            Server s2 = manager.getServer(o2);

            if (s1.hasReachedOfflineSortThreshold() && s2.hasReachedOfflineSortThreshold())
                return 0;
            if (s1.hasReachedOfflineSortThreshold())
                return 1;
            else if (s2.hasReachedOfflineSortThreshold())
                return -1;

            switch (sortMode)
            {
            case SORTMODE_NAME:
                return s1.getName().compareTo(s2.getName());
            case SORTMODE_NUM_PLAYERS:
            default:
                return (s2.getRealNumPlayers() - s1.getRealNumPlayers());
            case SORTMODE_MAX_PLAYERS:
                return (s2.getMaxPlayers() - s1.getMaxPlayers());
            case SORTMODE_MAP:
                return s1.getMap().compareTo(s2.getMap());
            }
        }
    }

    private class ServerSortCallback extends DiffUtil.Callback
    {
        final List<Integer> oldServers;
        final List<Integer> newServers;

        private ServerSortCallback(List<Integer> oldServers, List<Integer> newServers)
        {
            this.oldServers = oldServers;
            this.newServers = newServers;
        }

        @Override
        public int getOldListSize()
        {
            return oldServers.size();
        }

        @Override
        public int getNewListSize()
        {
            return newServers.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition)
        {
            Server os = manager.getServer(oldServers.get(oldItemPosition));
            Server ns = manager.getServer(newServers.get(newItemPosition));

            return os.getAddressString().equals(ns.getAddressString());
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition)
        {
            return areItemsTheSame(oldItemPosition, newItemPosition);
        }
    }
}
