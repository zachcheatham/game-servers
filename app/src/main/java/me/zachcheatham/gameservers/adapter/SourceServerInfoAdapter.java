package me.zachcheatham.gameservers.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.ServerDetailsActivity;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.BaseSourceServer;

public class SourceServerInfoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements View.OnClickListener
{
    private static final int VIEW_TYPE_DEFAULT = 0;
    private final static int VIEW_TYPE_RULES = 1;
    private static final int VIEW_TYPE_PING = 2;

    private final ServerDetailsActivity activity;
    private final BaseSourceServer server;

    public SourceServerInfoAdapter(Activity a, BaseSourceServer s)
    {
        activity = (ServerDetailsActivity) a;
        server = s;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position == (getItemCount() - 1))
            return VIEW_TYPE_RULES;
        else if (position == 1)
            return VIEW_TYPE_PING;
        else
            return VIEW_TYPE_DEFAULT;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater li = LayoutInflater.from(parent.getContext());
        if (viewType == VIEW_TYPE_DEFAULT)
        {
            View view = li.inflate(R.layout.list_item_server_info, parent, false);
            return new ViewHolder(view);
        }
        else if (viewType == VIEW_TYPE_PING)
        {
            View view = li.inflate(R.layout.list_item_server_info_ping, parent, false);
            return new PingViewHolder(view);
        }
        else
        {
            View view = li.inflate(R.layout.list_item_view_more, parent, false);
            return new ViewMoreViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int position)
    {
        if (viewHolder.getItemViewType() == VIEW_TYPE_DEFAULT)
        {
            ViewHolder holder = (ViewHolder) viewHolder;
            switch (position)
            {
            case 0:
                holder.variableImage.setImageResource(R.drawable.ic_dns_white);
                holder.variableName.setText(R.string.address);
                holder.variableValue.setText(server.getAddressString());
                break;
            case 2:
                holder.variableImage.setImageResource(R.drawable.ic_gamepad_white);
                holder.variableName.setText(R.string.game_desc);
                holder.variableValue.setText(server.getGame());
                break;
            case 3: // Players
                holder.variableImage.setImageResource(R.drawable.ic_people_white);
                holder.variableName.setText(R.string.players);
                if (server.getBotCount() > 0)
                    holder.variableValue.setText(holder.variableName.getContext()
                                                                    .getString(
                                                                            R.string.label_players_bots,
                                                                            server.getRealNumPlayers(),
                                                                            server.getNumPlayers(),
                                                                            server.getMaxPlayers()));
                else
                    holder.variableValue.setText(
                            holder.variableName.getContext().getString(R.string.label_players,
                                    server.getNumPlayers(), server.getMaxPlayers()));
                break;
            case 4:
                holder.variableImage.setImageResource(R.drawable.ic_map_white);
                holder.variableName.setText(R.string.map);
                holder.variableValue.setText(server.getMap());
                break;
            case 5:
                holder.variableImage.setImageResource(R.drawable.ic_shield_white);
                holder.variableName.setText(R.string.vac_status);
                if (server.isVacSecured())
                    holder.variableValue.setText(R.string.enforcing);
                else
                    holder.variableValue.setText(R.string.disabled);
                break;
            case 6:
                holder.variableImage.setImageResource(R.drawable.ic_steam);
                holder.variableName.setText(R.string.app_id);
                holder.variableValue.setText(String.valueOf(server.getGameAppId()));
                break;
            case 7:
                holder.variableName.setText(R.string.operating_system);
                switch (server.getOperatingSystem())
                {
                case 'w':
                    holder.variableImage.setImageResource(R.drawable.ic_windows_white);
                    holder.variableValue.setText(R.string.operating_system_windows);
                    break;
                case 'l':
                    holder.variableImage.setImageResource(R.drawable.ic_linux_white);
                    holder.variableValue.setText(R.string.operating_system_linux);
                    break;
                case 'm':
                case 'o':
                    holder.variableImage.setImageResource(R.drawable.ic_apple_finder_white);
                    holder.variableValue.setText(R.string.operating_system_mac);
                    break;
                }

                break;
            case 8:
                holder.variableImage.setImageResource(R.drawable.ic_package_variant_white);
                holder.variableName.setText(R.string.game_version);
                holder.variableValue.setText(server.getVersion());
                break;
            }
        }
        else if (viewHolder.getItemViewType() == VIEW_TYPE_PING)
        {
            PingViewHolder holder = (PingViewHolder) viewHolder;
            holder.variableValue
                    .setText(holder.variableValue.getContext().getString(R.string.format_ping,
                            server.getPing()));
            if (server.isOffline())
            {
                holder.offlineView.setVisibility(View.VISIBLE);
                switch (server.getOfflineReason())
                {
                case Timeout:
                    holder.offlineView.setText(R.string.offline_timeout);
                    break;
                case Unknown_Host:
                    holder.offlineView.setText(R.string.offline_unknown_host);
                    break;
                case Banned:
                    holder.offlineView.setText(R.string.offline_banned);
                    break;
                case Generic:
                    holder.offlineView.setText(R.string.offline);
                    break;
                }
            }
            else
            {
                holder.offlineView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount()
    {
        if (server == null)
            return 0;
        else
            return 9;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.getServer() == server)
            notifyDataSetChanged();
    }

    @Override
    public void onClick(View view)
    {
        activity.showFragment(0);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.variable_icon)
        ImageView variableImage;
        @BindView(R.id.variable_name)
        TextView variableName;
        @BindView(R.id.variable_value)
        TextView variableValue;

        ViewHolder(View view)
        {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class PingViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.variable_value) TextView variableValue;
        @BindView(R.id.offline) TextView offlineView;

        PingViewHolder(View itemView)
        {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ViewMoreViewHolder extends RecyclerView.ViewHolder
    {
        ViewMoreViewHolder(View itemView)
        {
            super(itemView);
            itemView.setOnClickListener(SourceServerInfoAdapter.this);
        }
    }
}
