package me.zachcheatham.gameservers.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.BaseSourceServer;

public class SourceServerRulesAdapter
        extends RecyclerView.Adapter<SourceServerRulesAdapter.ViewHolder>
{
    private final BaseSourceServer server;

    public SourceServerRulesAdapter(BaseSourceServer s)
    {
        server = s;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        EventBus.getDefault().unregister(this);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.list_item_server_rule, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position)
    {
        String ruleName = server.getRuleName(position);
        String ruleValue = server.getRule(ruleName);

        holder.variableName.setText(ruleName);
        holder.variableValue.setText(ruleValue);
    }

    @Override
    public int getItemCount()
    {
        if (server == null)
            return 0;
        else
            return server.getRulesCount();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.getServer() == server)
            notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.variable_name)
        TextView variableName;
        @BindView(R.id.variable_value)
        TextView variableValue;

        ViewHolder(View view)
        {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
