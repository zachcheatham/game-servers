package me.zachcheatham.gameservers.adapter;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.admin.Command;
import me.zachcheatham.gameservers.server.admin.CommandArgument;
import me.zachcheatham.gameservers.server.admin.ServerAdministration;
import me.zachcheatham.gameservers.server.player.Player;
import me.zachcheatham.gameservers.server.player.SourcePlayer;
import me.zachcheatham.gameservers.server.player.copy.PlayerCopyMenuPopulator;
import me.zachcheatham.gameservers.ui.CommandDialogBuilder;
import me.zachcheatham.gameservers.ui.PlayerCommandsDialogBuilder;

public abstract class PlayerListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    static final int SORTMODE_NAME = 0;
    final Server server;
    final Activity activity;
    private final List<Integer> expandedIndexes = new ArrayList<>();
    protected RecyclerView recyclerView;
    int sortMode = getDefaultSortMode();
    private boolean sorting = false;
    private boolean sortPending = false;
    private List<Integer> sortedIndexes = new ArrayList<>();

    PlayerListAdapter(Activity activity, Server server)
    {
        this.activity = activity;
        this.server = server;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
        EventBus.getDefault().register(this);
        sort();
        onPlayerPrivateInfoUpdated();
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        EventBus.getDefault().unregister(this);
        this.recyclerView = null;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.list_item_player, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position)
    {
        final ViewHolder holder = (ViewHolder) viewHolder;
        final Integer index = sortedIndexes.get(position);
        final Player player = server.getPlayer(index);

        holder.expandView.setExpanded(expandedIndexes.contains(index), false);

        if (player != null)
        {
            if (player.getName().isEmpty())
            {
                holder.name.setText(R.string.status_connecting);
                holder.name
                        .setTextColor(
                                holder.name.getResources().getColor(R.color.textColorSecondary));
            }
            else
            {
                holder.name.setText(player.getName());
                holder.name.setTextColor(
                        holder.name.getResources().getColor(R.color.textColorPrimary));
            }

            holder.adminButtons
                    .setVisibility(server.isRconConnected() ? View.VISIBLE : View.GONE);

            onBindPlayer(holder, player);
        }
    }

    public int getSortMode()
    {
        return sortMode;
    }

    @Override
    public int getItemCount()
    {
        return sortedIndexes.size();
    }

    public void setSorting(int sortMode)
    {
        if (sortMode != this.sortMode)
        {
            this.sortMode = sortMode;
            sort();
        }
    }

    private void sort()
    {
        if (sorting)
        {
            sortPending = true;
        }
        else
        {
            sorting = true;
            final List<Integer> oldSort = new ArrayList<>(sortedIndexes);
            List<Integer> sortedIndexes = new ArrayList<>();
            for (int i = 0; i < server.getPlayersListSize(); i++)
            {
                sortedIndexes.add(server.getPlayerKeyAt(i));
            }
            Collections.sort(sortedIndexes, getComparator());
            this.sortedIndexes = sortedIndexes;

            if (oldSort.isEmpty() || sortedIndexes.isEmpty())
            {
                activity.runOnUiThread(this::notifyDataSetChanged);

                if (sortPending)
                    sort();
                else
                    sorting = false;
            }
            else
            {
                DiffUtil.DiffResult diffResult = DiffUtil
                        .calculateDiff(new SortCallback(oldSort, this.sortedIndexes));
                activity.runOnUiThread(() ->
                {
                    diffResult.dispatchUpdatesTo(this);
                    if (sortPending)
                        new Thread(this::sort).run();
                    else
                        sorting = false;
                });

            }

        }
    }

    abstract boolean shouldSort(ServerUpdatedEvent event);

    abstract protected Comparator<Integer> getComparator();

    abstract public int getSortMenu();

    public abstract int getSortMenuItemID(int sortMode);

    public abstract String getSortingConfigID();

    public abstract int getDefaultSortMode();

    public abstract int getSelectedSortMode(int menuItemId);

    abstract void onBindPlayer(ViewHolder holder, Player player);

    /**
     * Called when information obtained about players via RCON is updated.
     * Useful for batching API calls for steam players' avatars
     */
    void onPlayerPrivateInfoUpdated()
    {
        // override as necessary
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.getServer() == server)
        {
            if (event.didPlayerPrivateInfoUpdate())
                onPlayerPrivateInfoUpdated();

            if (sorting)
            {
                sortPending = true;
            }
            else
            {
                if (event.didPlayerListChange() || shouldSort(event))
                {
                    sort();
                }
                else
                {
                    activity.runOnUiThread(
                            () -> this.notifyItemRangeChanged(0, sortedIndexes.size()));
                }
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder
            implements ExpandableLayout.OnExpansionUpdateListener
    {
        @BindView(R.id.layout) ConstraintLayout layout;
        @BindView(R.id.avatar) ImageView avatar;
        @BindView(R.id.player_name) TextView name;
        @BindView(R.id.score) TextView score;
        @BindView(R.id.reveal) ImageView reveal;
        @BindView(R.id.details) TextView details;
        @BindView(R.id.expand_view) ExpandableLayout expandView;
        @BindView(R.id.group_button_admin) Group adminButtons;

        private Animator animator = null;

        ViewHolder(final View view)
        {
            super(view);
            ButterKnife.bind(this, view);

            expandView.setOnExpansionUpdateListener(this);

            view.setOnClickListener(v ->
            {
                if (animator != null)
                    animator.cancel();

                final boolean closing = expandView.isExpanded();

                reveal.animate().cancel();
                reveal.animate().rotation(closing ? 0.0f : 180.0f).setDuration(300);

                if (closing)
                {
                    expandedIndexes.remove(sortedIndexes.get(getAdapterPosition()));
                    expandView.collapse();
                }
                else
                {
                    expandedIndexes.add(sortedIndexes.get(getAdapterPosition()));
                    expandView.expand();
                }
            });

            view.setOnLongClickListener(v ->
            {
                createCopyDialog(
                        server.getPlayer(sortedIndexes.get(getAdapterPosition())),
                        server.getAdminMod(), activity);

                return true;
            });
        }

        @OnClick(R.id.button_ban)
        public void onBanClicked()
        {
            Player player = server.getPlayer(sortedIndexes.get(getAdapterPosition()));

            Command command = server.getAdminMod().getBanCommand();
            if (command != null)
            {
                CommandDialogBuilder.createDialog(activity, command, server, player).show();
            }
            else
                Toast.makeText(activity, R.string.error_no_ban_command, Toast.LENGTH_SHORT).show();
        }

        @OnClick(R.id.button_kick)
        public void onKickClicked()
        {
            Player player = server.getPlayer(sortedIndexes.get(getAdapterPosition()));

            Command command = server.getAdminMod().getKickCommand();
            if (command != null)
            {
                CommandDialogBuilder.createDialog(activity, command, server, player).show();
            }
            else
                Toast.makeText(activity, R.string.error_no_kick_command, Toast.LENGTH_SHORT).show();
        }

        @OnClick(R.id.button_more)
        void onMoreClicked()
        {
            createDialog(null, server.getPlayer(sortedIndexes.get(getAdapterPosition())));
        }

        private void createDialog(String submenu, Player player)
        {
            final List<ServerAdministration.PlayerMenuItem> items = new ArrayList<>();
            final WeakReference<Activity> activityReference = new WeakReference<>(activity);
            final WeakReference<Server> serverReference = new WeakReference<>(server);

            server.getAdminMod().populatePlayerMenu(submenu, activity, items, player);

            if (submenu == null || submenu.isEmpty())
                items.add(new ServerAdministration.PlayerMenuItem(Constants.PLAYER_ADMIN_COPY,
                        itemView.getContext().getString(R.string.action_open_copy_menu),
                        R.drawable.ic_content_copy));

            PlayerCommandsDialogBuilder.createDialog(activity,
                    items.toArray(new ServerAdministration.PlayerMenuItem[0]),
                    (dialog, which) ->
                    {
                        Server server = serverReference.get();
                        Activity activity = activityReference.get();
                        if (server != null && activity != null)
                        {
                            ServerAdministration.PlayerMenuItem item = items.get(which);
                            switch (item.command)
                            {
                            case Constants.PLAYER_ADMIN_STEAM_PROFILE:
                                SourcePlayer sourcePlayer = (SourcePlayer) player;
                                CustomTabsIntent intent = new CustomTabsIntent.Builder()
                                        .addDefaultShareMenuItem()
                                        .setToolbarColor(activity.getResources()
                                                                 .getColor(R.color.colorPrimary))
                                        .setShowTitle(true)
                                        .build();

                                intent.launchUrl(activity, Uri.parse(String.format(Locale.US,
                                        "https://steamcommunity.com/profiles/%s",
                                        sourcePlayer.getSteamID64())));
                                break;
                            case Constants.PLAYER_ADMIN_COPY:
                                createCopyDialog(player, server.getAdminMod(), activity);
                                break;
                            default:
                            {
                                if (item.command.startsWith(Constants.PLAYER_SUBMENU_PREFIX))
                                {
                                    createDialog(item.command.replace(Constants.PLAYER_SUBMENU_PREFIX, " "), player);
                                }
                                else
                                {
                                    Command command = server.getAdminMod().getCommand(item.command);
                                    if (command != null)
                                    {
                                        if (command.hasNonPlayerArgument() || command.arguments.length > 1)
                                        {
                                            CommandDialogBuilder.createDialog(activity, command, server, player).show();
                                        }
                                        else
                                        {
                                            String[] values = new String[1];
                                            switch (command.arguments[0].type)
                                            {
                                            case CommandArgument.TYPE_SM_PLAYER:
                                                values[0] = "#" + player.getUserID();
                                                break;
                                            case CommandArgument.TYPE_STEAMID:
                                                values[0] = player.getUniqueID();
                                                break;
                                            case CommandArgument.TYPE_IP:
                                                values[0] = player.getIPAddress();
                                                break;
                                            default:
                                                values[0] = player.getName();
                                            }
                                            server.sendInternalAdminCommand(command, values, response ->
                                            {
                                                Activity a = activityReference.get();
                                                if (a != null)
                                                    a.runOnUiThread(() -> Toast
                                                            .makeText(activity, response,
                                                                    Toast.LENGTH_LONG).show());
                                            });
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(activity,
                                                activity.getString(
                                                        R.string.command_not_found,
                                                        item.command), Toast.LENGTH_SHORT).show();
                                    }
                                }
                                break;
                            }
                            }
                        }
                        dialog.dismiss();
                    }).show();
        }

        private void createCopyDialog(Player player, ServerAdministration adminMod, Context context)
        {
            List<PlayerCopyMenuPopulator.PlayerCopyMenuItem> items = player.getCopyMenuPopulator()
                                                                     .populate(context);
            PlayerCopyMenuPopulator.PlayerCopyMenuItem[] arr =
                    items.toArray(new PlayerCopyMenuPopulator.PlayerCopyMenuItem[0]);
            PlayerCommandsDialogBuilder.createCopyDialog(context, arr).show();
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state)
        {
            if (getAdapterPosition() != -1)
            {
                if (state == ExpandableLayout.State.EXPANDING)
                    recyclerView.smoothScrollToPosition(getAdapterPosition());

                if (state == ExpandableLayout.State.EXPANDING ||
                    state == ExpandableLayout.State.COLLAPSING)
                    setIsRecyclable(false);
                else
                    setIsRecyclable(true);
            }
        }
    }

    private class SortCallback extends DiffUtil.Callback
    {
        final List<Integer> oldPlayers;
        final List<Integer> newPlayers;

        private SortCallback(List<Integer> oldPlayers, List<Integer> newPlayers)
        {
            this.oldPlayers = oldPlayers;
            this.newPlayers = newPlayers;
        }

        @Override
        public int getOldListSize()
        {
            return oldPlayers.size();
        }

        @Override
        public int getNewListSize()
        {
            return newPlayers.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition)
        {
            Player p = server.getPlayer(oldPlayers.get(oldItemPosition));
            Player np = server.getPlayer(newPlayers.get(newItemPosition));

            if (p == null || np == null)
                return false;
            else
                return p.equals(np);
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition)
        {
            return false;
        }
    }
}
