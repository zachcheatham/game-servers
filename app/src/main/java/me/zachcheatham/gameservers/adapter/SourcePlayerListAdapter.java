package me.zachcheatham.gameservers.adapter;

import android.app.Activity;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.player.AvatarHelper;
import me.zachcheatham.gameservers.server.player.Player;
import me.zachcheatham.gameservers.server.player.SourcePlayer;
import me.zachcheatham.gameservers.server.player.SteamAvatarHelper;
import me.zachcheatham.gameservers.ui.CircleImageTransform;

public class SourcePlayerListAdapter extends PlayerListAdapter implements AvatarHelper.AvatarLoadListener
{
    private static final int SORTMODE_SCORE = 1;
    private static final int SORTMODE_TIME = 2;

    private AvatarHelper avatarHelper = new SteamAvatarHelper();

    public SourcePlayerListAdapter(Activity activity, Server server)
    {
        super(activity, server);
    }

    @Override
    void onBindPlayer(@NonNull final ViewHolder holder, Player player)
    {
        SourcePlayer sourcePlayer = (SourcePlayer) player;
        holder.score.setText(String.valueOf(sourcePlayer.getScore()));

        if (!server.isRconConnected())
            holder.details.setText(holder.details.getContext()
                                                 .getString(R.string.player_details_source,
                                                         sourcePlayer.getTimeString()));
        else
            holder.details.setText(holder.details.getContext().getString(
                    R.string.player_details_source_rcon, sourcePlayer.getTimeString(),
                    sourcePlayer.getUniqueID(), sourcePlayer.getIPAddress()));

        String avatar = avatarHelper.getAvatar(sourcePlayer);
        if (avatar != null)
            Picasso.get().load(avatar).transform(new CircleImageTransform()).into(holder.avatar);
        else
            holder.avatar.setImageResource(R.drawable.ic_steam_avatar_unknown);
    }

    @Override
    boolean shouldSort(ServerUpdatedEvent event)
    {
        return (event.getPlayerChange(SourcePlayer.PLAYER_CHANGE_SCORE) && sortMode == SORTMODE_SCORE ||
                event.getPlayerChange(SourcePlayer.PLAYER_CHANGE_TIME) && sortMode == SORTMODE_TIME);
    }

    @Override
    protected Comparator<Integer> getComparator()
    {
        return new SourcePlayerListAdapter.PlayersComparator();
    }

    @Override
    public int getSortMenu()
    {
        return R.menu.source_player_list;
    }

    @Override
    public int getSortMenuItemID(int sortMode)
    {
        switch (sortMode)
        {
        default:
        case SORTMODE_NAME:
            return R.id.sort_name;
        case SORTMODE_SCORE:
            return R.id.sort_score;
        case SORTMODE_TIME:
            return R.id.sort_time;
        }
    }

    @Override
    public int getDefaultSortMode()
    {
        return SORTMODE_SCORE;
    }

    @Override
    public int getSelectedSortMode(int menuItemId)
    {
        switch (menuItemId)
        {
        case R.id.sort_name:
            return SORTMODE_NAME;
        case R.id.sort_score:
            return SORTMODE_SCORE;
        case R.id.sort_time:
            return SORTMODE_TIME;
        }

        return SORTMODE_NAME;
    }

    @Override
    public String getSortingConfigID()
    {
        return Constants.PREFS_SOURCE_PLAYER_SORT_MODE;
    }

    @Override
    public void avatarsLoaded()
    {
        activity.runOnUiThread(this::notifyDataSetChanged);
    }

    private class PlayersComparator implements java.util.Comparator<Integer>
    {
        @Override
        public int compare(Integer o1, Integer o2)
        {
            SourcePlayer p1 = (SourcePlayer) server.getPlayer(o1);
            SourcePlayer p2 = (SourcePlayer) server.getPlayer(o2);

            switch (sortMode)
            {
            default:
            case SORTMODE_SCORE:
                return (int) (p2.getScore() - p1.getScore());
            case SORTMODE_NAME:
                return p1.getName().compareTo(p2.getName());
            case SORTMODE_TIME:
                return p2.getTime() - p1.getTime();
            }
        }
    }

    @Override
    void onPlayerPrivateInfoUpdated()
    {
        if (server.getPlayersListSize() > 0)
        {
            List<SourcePlayer> toUpdate = new ArrayList<>();
            for (Player player : server.getPlayers())
            {
                SourcePlayer sourcePlayer = (SourcePlayer) player;
                if (sourcePlayer.hasPrivateInfo() && !avatarHelper.hasAvatar(sourcePlayer))
                    toUpdate.add(sourcePlayer);
            }

            if (toUpdate.size() > 0)
            {
                avatarHelper.loadAvatars(toUpdate.toArray(new Player[0]), this);
            }
        }
    }
}
