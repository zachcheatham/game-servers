package me.zachcheatham.gameservers.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.RconLogAddedEvent;
import me.zachcheatham.gameservers.events.RconLogTrimEvent;
import me.zachcheatham.gameservers.server.LogHolder;
import me.zachcheatham.gameservers.server.Server;

public class ConsoleAdapter
        extends RecyclerView.Adapter<ConsoleAdapter.ViewHolder>
{
    private final Activity activity;
    private final Server server;
    private int cachedSize = 0;
    private RecyclerView recyclerView;

    public ConsoleAdapter(Activity a, Server s)
    {
        activity = a;
        server = s;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
        if (server.getRconLog() != null)
            this.cachedSize = server.getRconLog().size();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onDetachedFromRecyclerView(recyclerView);
        this.recyclerView = null;
        EventBus.getDefault().unregister(this);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.list_item_console, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position)
    {
        if (server.getRconLog() != null && position < server.getRconLog().size())
        {
            LogHolder logMessage = server.getRconLog().get(position);

            switch (logMessage.type)
            {
            case CONNECTED:
                holder.logText
                        .setText(recyclerView.getContext().getString(R.string.rcon_connected));
                holder.itemView.setBackgroundColor(
                        activity.getResources().getColor(android.R.color.transparent));
                holder.logText
                        .setTextColor(activity.getResources().getColor(R.color.textColorPrimary));
                break;
            case DISCONNECTED:
                holder.logText
                        .setText(recyclerView.getContext().getString(R.string.rcon_disconnected));
                holder.itemView.setBackgroundColor(
                        activity.getResources().getColor(android.R.color.transparent));
                holder.logText
                        .setTextColor(activity.getResources().getColor(R.color.textColorPrimary));
                break;
            case AUTHENTICATED:
                holder.logText
                        .setText(recyclerView.getContext().getString(R.string.rcon_authenticated));
                holder.itemView.setBackgroundColor(
                        activity.getResources().getColor(android.R.color.transparent));
                holder.logText
                        .setTextColor(activity.getResources().getColor(R.color.textColorPrimary));
                break;
            case INVALID_PASSWORD:
                holder.logText.setText(
                        recyclerView.getContext().getString(R.string.rcon_authention_failed));
                holder.itemView.setBackgroundColor(
                        activity.getResources().getColor(android.R.color.transparent));
                holder.logText
                        .setTextColor(activity.getResources().getColor(R.color.textColorPrimary));
                break;
            case LOG_AVAILABLE:
                holder.logText.setText(
                        recyclerView.getContext().getString(R.string.log_address_restored));
                holder.itemView.setBackgroundColor(
                        activity.getResources().getColor(android.R.color.transparent));
                holder.logText
                        .setTextColor(activity.getResources().getColor(R.color.textColorPrimary));
                break;
            case LOG_UNAVAILABLE:
                holder.logText.setText(
                        recyclerView.getContext().getString(R.string.log_address_not_working));
                holder.itemView.setBackgroundColor(
                        activity.getResources().getColor(android.R.color.transparent));
                holder.logText
                        .setTextColor(activity.getResources().getColor(R.color.textColorPrimary));
                break;
            case LOG:
                holder.logText.setText(logMessage.message);
                holder.itemView.setBackgroundColor(
                        activity.getResources().getColor(R.color.color_log_received));
                holder.logText
                        .setTextColor(activity.getResources().getColor(R.color.textColorPrimary));
                break;
            case COMMAND:
                holder.logText.setText(logMessage.message);
                holder.itemView.setBackgroundColor(
                        activity.getResources().getColor(android.R.color.black));
                holder.logText
                        .setTextColor(activity.getResources().getColor(android.R.color.white));
                break;
            default:
                holder.logText.setText(logMessage.message);
                holder.itemView.setBackgroundColor(
                        activity.getResources().getColor(android.R.color.transparent));
                holder.logText
                        .setTextColor(activity.getResources().getColor(R.color.textColorPrimary));
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return cachedSize;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRconLog(RconLogAddedEvent event)
    {
        if (event.server == server)
        {
            cachedSize++;
            notifyItemInserted(cachedSize - 1);
            recyclerView.scrollToPosition(cachedSize - 1);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRconTrim(RconLogTrimEvent event)
    {
        if (event.getServer().equals(server))
        {
            cachedSize -= event.getCount();
            notifyItemRangeRemoved(0, event.getCount());
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.log_text)
        TextView logText;

        ViewHolder(View view)
        {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
