package me.zachcheatham.gameservers;

public class Constants
{
    // Preference Keys
    public static final String PREFS_SERVER_UPDATE_FREQUENCY = "server_auto_refresh_frequency";
    public static final String PREFS_SERVER_TIMEOUT = "server_timeout";
    public static final String PREFS_KEY = "me.zachcheatham.gameservers";
    public static final String PREFS_KEY_NOTIFICATIONS = "me.zachcheatham.gameservers.notifications";
    public static final String PREFS_KEY_CACHE = "me.zachcheatham.gameservers.cache";
    public static final String PREFS_SERVER_AUTO_UPDATE_ENABLE = "server_auto_refresh_enable";
    public static final String PREFS_SERVER_AUTO_UPDATE_UNMETERED_ONLY =
            "server_auto_refresh_unmetered_only";
    public static final String PREFS_SOURCE_PLAYER_SORT_MODE = "source_player_sort_mode";
    public static final String PREFS_LOG_SIZE = "rcon_log_size";
    public static final String PREFS_CHAT_SIZE = "chat_log_size";
    public static final String PREFS_NOTIFICATION_HISTORY = "%s_%d";

    // Notification Channels
    public static final String NOTIFICATION_CHANNEL_BACKGROUND = "background_worker";
    public static final String NOTIFICATION_CHANNEL_PLAYER_ALERTS = "player_alerts";
    public static final String NOTIFICATION_CHANNEL_CHAT = "chat";
    public static final String NOTIFICATION_CHANNEL_ADMIN_CHAT = "admin_chat";

    // Hardcoded Configurables
    public static final int OFFLINE_COUNT_THRESHOLD = 2;

    // Player admin menu keys
    public static final String PLAYER_ADMIN_STEAM_PROFILE = "_steamprofile"; // Opens steam profile
    public static final String PLAYER_ADMIN_COPY = "_copy"; // Opens copy menu
    public static final String PLAYER_SUBMENU_PREFIX = "_s_";

    static final String PREFS_SERVER_SORT_MODE = "server_sort_mode";
}
