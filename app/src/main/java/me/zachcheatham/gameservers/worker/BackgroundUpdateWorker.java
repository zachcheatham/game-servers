package me.zachcheatham.gameservers.worker;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import me.zachcheatham.gameservers.service.BackgroundUpdateService;

public class BackgroundUpdateWorker extends Worker
{
    public BackgroundUpdateWorker(@NonNull Context context,
            @NonNull WorkerParameters workerParams)
    {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork()
    {
        Context context = getApplicationContext();
        ContextCompat.startForegroundService(context, new Intent(context, BackgroundUpdateService.class));

        return Result.success();
    }


}
