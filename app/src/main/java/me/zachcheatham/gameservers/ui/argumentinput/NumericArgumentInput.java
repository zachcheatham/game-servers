package me.zachcheatham.gameservers.ui.argumentinput;

import android.content.Context;
import android.text.InputType;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.admin.CommandArgument;

public class NumericArgumentInput extends StringArgumentInput
{

    public NumericArgumentInput(CommandArgument argument, Context context)
    {
        super(argument, context);
    }

    @Override
    void createView(Context context)
    {
        super.createView(context);
        inputLayout.getEditText().setRawInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
    }

    @Override
    public boolean validate()
    {
        if (inputLayout.getEditText().getText().length() == 0)
        {
            if (!argument.optional)
            {
                inputLayout.setError(inputLayout.getContext().getString(R.string.error_required));
                return false;
            }
        }
        else
        {
            float value;
            if (argument.type.equals(CommandArgument.TYPE_FLOAT))
                value = Float.valueOf(inputLayout.getEditText().getText().toString());
            else
                value = Integer.valueOf(inputLayout.getEditText().getText().toString());

            switch (CommandArgument.validateMinMax(value, argument.min, argument.max))
            {
            case 1:
                inputLayout.setError(inputLayout.getContext().getString(
                        R.string.error_out_of_range_min, argument.min));
                return false;
            case 2:
                inputLayout.setError(inputLayout.getContext().getString(
                        R.string.error_out_of_range_max, argument.max));
                return false;
            case 3:
                inputLayout.setError(inputLayout.getContext().getString(
                        R.string.error_out_of_range, argument.min, argument.max));
                return false;
            }
        }

        inputLayout.setErrorEnabled(false);
        return true;
    }
}
