package me.zachcheatham.gameservers.ui;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

public class BitmapHelper
{
    public static Bitmap applyBitmapCircle(Bitmap bitmap)
    {
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        final Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(result);
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, width, height);
        final int color = 0xff424242;

        paint.setAntiAlias(true);
        paint.setColor(color);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(width / 2.0f, height / 2.0f, width / 2.0f, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return result;
    }
}
