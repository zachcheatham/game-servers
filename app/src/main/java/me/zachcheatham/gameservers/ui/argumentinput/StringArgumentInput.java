package me.zachcheatham.gameservers.ui.argumentinput;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.material.textfield.TextInputLayout;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.admin.CommandArgument;

public class StringArgumentInput extends ArgumentInput
{
    TextInputLayout inputLayout;

    public StringArgumentInput(CommandArgument argument, Context context)
    {
        super(argument, context);
    }

    @Override
    void createView(Context context)
    {
        inputLayout = (TextInputLayout) LayoutInflater.from(context).inflate(
                R.layout.text_input_layout, null);

        inputLayout.setHint(getTitle(context));
    }

    @Override
    public View getView()
    {
        return inputLayout;
    }

    @Override
    public boolean validate()
    {
        if (!argument.optional && inputLayout.getEditText().getText().length() == 0)
        {
            inputLayout.setError(inputLayout.getContext().getString(R.string.error_required));
            return false;
        }

        inputLayout.setErrorEnabled(false);
        return true;
    }

    @Override
    public String getValue()
    {
        return inputLayout.getEditText().getText().toString();
    }

    @Override
    public void setChangeListener(final Runnable r)
    {
        inputLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { r.run(); }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }
}
