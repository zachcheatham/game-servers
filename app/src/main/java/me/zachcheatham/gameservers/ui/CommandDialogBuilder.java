package me.zachcheatham.gameservers.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.admin.Command;
import me.zachcheatham.gameservers.server.admin.CommandArgument;
import me.zachcheatham.gameservers.server.admin.CommandResourceHelper;
import me.zachcheatham.gameservers.server.player.Player;
import me.zachcheatham.gameservers.ui.argumentinput.ArgumentInput;
import me.zachcheatham.gameservers.ui.argumentinput.BooleanArgumentInput;
import me.zachcheatham.gameservers.ui.argumentinput.ListArgumentInput;
import me.zachcheatham.gameservers.ui.argumentinput.NumericArgumentInput;
import me.zachcheatham.gameservers.ui.argumentinput.PositionArgumentInput;
import me.zachcheatham.gameservers.ui.argumentinput.StringArgumentInput;
import me.zachcheatham.gameservers.ui.argumentinput.TimeLengthArgumentInput;

public class CommandDialogBuilder
{
    public static AlertDialog createDialog(final Activity activity,
            Command command, Server server, Player player)
    {
        final WeakReference<Server> serverReference = new WeakReference<>(server);
        final WeakReference<Activity> activityReference = new WeakReference<>(activity);

        // Figure out what our dialog title is going to be
        String title;
        if (command.name == null) // Null when we created the command from the database
        {
            // First get a string id specific for dialog titles
            int res = CommandResourceHelper.getCommandDialogTitle(activity, command);
            // If that doesn't exist, get the command name
            if (res == 0)
                res = CommandResourceHelper.getCommandName(activity, command);
            // If it still doesn't work, someone slacked off and we don't have anything
            if (res == 0)
                title = command.command;
            else // if it did work, get the actual string
                title = activity.getString(res);
        }
        else // Command was created based on rcon help
        {
            title = command.name;
        }

        if (player != null)
        {
            /* When a dialog is created specific to a player,
                the title should be an action onto the player.
                So we should be able to just append the player's name.
             */
            title = String.format(Locale.US, "%s %s", title, player.getName());
        }

        /*String description = null;
        if (command.descriptionResourceID != 0)
            description = activity.getString(command.descriptionResourceID);
        else
            description = command.description;*/

        // Figure out what the positive button is going to read
        String positiveText;
        // Check if we defined the string
        int positiveTextRes = CommandResourceHelper.getCommandPositiveAction(activity, command);
        if (positiveTextRes != 0)
            positiveText = activity.getString(positiveTextRes);
        else
            // Default to splitting the first word off the title for the action
            positiveText = title.split(" ")[0];

        LinearLayout view = new LinearLayout(activity);
        view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.WRAP_CONTENT));
        view.setOrientation(LinearLayout.VERTICAL);

        view.setPadding(
                activity.getResources().getDimensionPixelOffset(R.dimen.dialog_padding),
                activity.getResources().getDimensionPixelOffset(R.dimen.input_spacing_vert_dense),
                activity.getResources().getDimensionPixelOffset(R.dimen.dialog_padding),
                0);

        ArgumentInput[] inputs = populateViews(command, player != null, view);

        final AlertDialog dialog = new AlertDialog.Builder(activity)
                .setView(view)
                .setNegativeButton(R.string.action_cancel, null)
                .setPositiveButton(positiveText, null)
                .setTitle(title).create();

        dialog.setOnShowListener(dialogInterface ->
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
                final String[] values = new String[command.arguments.length];
                if (validate(inputs, command.arguments, player, values))
                {
                    dialogInterface.dismiss();
                    new Thread(() ->
                    {
                        final Server s = serverReference.get();
                        if (s != null)
                        {
                            s.sendInternalAdminCommand(command, values, response ->
                            {
                                Activity a = activityReference.get();
                                if (a != null)
                                    a.runOnUiThread(() ->
                                            Toast.makeText(a, response, Toast.LENGTH_LONG).show());
                            });

                        }
                    }).start();
                }
            }
        ));

        return dialog;
    }

    /**
     * Populate ViewGroup with inputs based on their type
     * @param command Command to read arguments from
     * @param providedPlayer <code>true</code> if we have a target player
     * @param viewsParent ViewGroup to populate
     */
    private static ArgumentInput[] populateViews(Command command, boolean providedPlayer, ViewGroup viewsParent)
    {
        if (command.arguments != null)
        {
            boolean filledPlayer = false;
            ArgumentInput[] argumentInputs = new ArgumentInput[command.arguments.length];
            for (int i = 0; i < command.arguments.length; i++)
            {
                CommandArgument argument = command.arguments[i];

                if (!providedPlayer || !argument.playerArgument || filledPlayer)
                {
                    ArgumentInput input = getInput(argument, viewsParent);
                    int finalI = i;
                    input.setChangeListener(() -> updateVisibility(argumentInputs, finalI));
                    argumentInputs[i] = input;
                    viewsParent.addView(input.getView());
                }
                else
                {
                    filledPlayer = true;
                }
            }
            return argumentInputs;
        }
        else
        {
            return new ArgumentInput[0];
        }
    }

    /**
     * Validate and collect user input
     * @param inputs Argument inputs needing validated
     * @param player Target player if applicable or <code>null</code>
     * @param arguments Arguments array
     * @param values String array to fill with values
     * @return <code>true</code> if all fields validated
     */
    private static boolean validate(ArgumentInput[] inputs, CommandArgument[] arguments, Player player, String[] values)
    {
        boolean validated = true;
        for (int i = 0; i < values.length; i++)
        {
            ArgumentInput input = inputs[i];
            if (input != null)
            {
                if (input.getView().getVisibility() == View.VISIBLE)
                {
                    if (!input.validate())
                        validated = false;

                    if (validated)
                        values[i] = input.getValue();
                }
            }
            else if (player != null)
            {
                CommandArgument argument = arguments[i];

                // Automatically fill player argument with provided player
                switch (argument.type)
                {
                case CommandArgument.TYPE_SM_PLAYER:
                    values[i] = "#" + player.getUserID();
                    break;
                case CommandArgument.TYPE_STEAMID:
                    values[i] = player.getUniqueID();
                    break;
                case CommandArgument.TYPE_IP:
                    values[i] = player.getIPAddress();
                    break;
                default:
                    values[i] = player.getName();
                }
            }
        }

        return validated;
    }

    private static ArgumentInput getInput(CommandArgument argument, ViewGroup parent)
    {
        Context c = parent.getContext();
        switch (argument.type)
        {
        case CommandArgument.TYPE_STRING:
        default:
            return new StringArgumentInput(argument, c);
        case CommandArgument.TYPE_FLOAT:
        case CommandArgument.TYPE_INT:
            return new NumericArgumentInput(argument, c);
        case CommandArgument.TYPE_TIME_MINUTES:
        case CommandArgument.TYPE_TIME_SECONDS:
            return new TimeLengthArgumentInput(argument, c);
        case CommandArgument.TYPE_BOOL:
        case CommandArgument.TYPE_INTBOOL:
        case CommandArgument.TYPE_IDBOOL:
            return new BooleanArgumentInput(argument, c);
        case CommandArgument.TYPE_LIST:
            return new ListArgumentInput(argument, c);
        case CommandArgument.TYPE_XYZ:
        case CommandArgument.TYPE_XZ:
            return new PositionArgumentInput(argument, c);
        }
    }

    private static void updateVisibility(ArgumentInput[] inputs, int target)
    {
        for (int i = target + 1; i < inputs.length; i++)
        {
            ArgumentInput input = inputs[i];
            if (input != null)
            {
                CommandArgument argument = input.getArgument();
                boolean shouldShow = false;

                if (argument.dependArg != null)
                {
                    shouldShow = checkDependingArg(inputs, argument.dependArg, argument.dependArgValue);
                }
                else if (argument.dependArgs != null)
                {
                    for (int a = 0; i < argument.dependArgs.length; i++)
                    {
                        if (checkDependingArg(inputs, argument.dependArgs[a],
                                argument.dependArgsValue[a]))
                        {
                            shouldShow = true;
                            break;
                        }
                    }
                }
                else
                {
                    shouldShow = true;
                }

                input.getView().setVisibility(shouldShow ? View.VISIBLE : View.GONE);
            }
        }
    }

    private static boolean checkDependingArg(ArgumentInput[] inputs, String dependantId, String dependantValue)
    {
        int dependantIndex = getArgInputById(inputs, dependantId); // Find the arg by id
        if (dependantIndex == -1)
        {
            return true;
        }
        else
        {
            ArgumentInput input = inputs[dependantIndex];

            // If target view isn't even visible, we shouldn't
            if (input.getView().getVisibility() != View.VISIBLE)
                return false;

            // Time to compare values
            String value = input.getValue();
            String[] dependantValues = dependantValue.split(",");
            for (String v : dependantValues)
                if (v.equals(value))
                    return true;

            return false;
        }
    }

    private static int getArgInputById(ArgumentInput[] inputs, String id)
    {
        for (int i = 0; i < inputs.length; i++)
        {
            if (inputs[i] != null)
            {
                if (inputs[i].getArgument().id.equals(id))
                    return i;
            }
        }

        return -1;
    }
}
