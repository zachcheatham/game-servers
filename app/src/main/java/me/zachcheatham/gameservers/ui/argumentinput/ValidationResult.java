package me.zachcheatham.gameservers.ui.argumentinput;

public enum ValidationResult
{
    MISSING_REQUIRED, INVALID, OUT_OF_RANGE, VALID
}
