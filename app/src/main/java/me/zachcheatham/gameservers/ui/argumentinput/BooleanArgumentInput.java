package me.zachcheatham.gameservers.ui.argumentinput;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.buildware.widget.indeterm.IndeterminateCheckBox;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.admin.CommandArgument;

public class BooleanArgumentInput extends ArgumentInput
{
    private IndeterminateCheckBox checkBox;

    public BooleanArgumentInput(CommandArgument argument, Context context)
    {
        super(argument, context);
    }

    @Override
    void createView(Context context)
    {
        checkBox = new IndeterminateCheckBox(context);

        checkBox.setIndeterminate(argument.optional);
        checkBox.setPadding(context.getResources().getDimensionPixelOffset(R.dimen.input_spacing),0,0,0);
        checkBox.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                context.getResources().getDimensionPixelOffset(R.dimen.input_min_height)));

        checkBox.setText(getTitle(context));
    }

    @Override
    public View getView()
    {
        return checkBox;
    }

    @Override
    public boolean validate()
    {
        return true;
    }

    @Override
    public String getValue()
    {
        if (checkBox.isIndeterminate())
            return "";

        switch (argument.type)
        {
        default:
        case CommandArgument.TYPE_BOOL:
            return checkBox.isChecked() ? "true" : "false";
        case CommandArgument.TYPE_INTBOOL:
            return checkBox.isChecked() ? "1" : "0";
        case CommandArgument.TYPE_IDBOOL:
            return checkBox.isChecked() ? argument.id : "";
        }
    }

    @Override
    public void setChangeListener(Runnable r)
    {
        checkBox.setOnStateChangedListener((checkbox, state) ->
        {
            r.run();
        });
    }
}
