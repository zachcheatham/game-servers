package me.zachcheatham.gameservers.ui.argumentinput;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatSpinner;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.admin.CommandArgument;
import me.zachcheatham.gameservers.server.admin.CommandResourceHelper;

public class ListArgumentInput extends ArgumentInput
{
    private LinearLayout linearLayout;

    public ListArgumentInput(CommandArgument argument, Context context)
    {
        super(argument, context);
    }

    @Override
    void createView(Context context)
    {
        linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(
                R.layout.list_input, null);

        String[] items = new String[argument.optional ?
                argument.listOptions.length + 1 : argument.listOptions.length];
        for (int i = 0; i < argument.listOptions.length; i++)
        {
            String displayText;
            int id = CommandResourceHelper.getListOption(context, argument.listOptions[i]);
            if (id == 0)
                displayText = argument.listOptions[i];
            else
                displayText = context.getString(id);

            items[argument.optional ? i + 1 : i] = displayText;
        }
        if (argument.optional)
            items[0] = "";

        ArrayAdapter adapter = new ArrayAdapter<>(context,
                R.layout.item_spinner_edit_text, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        getSpinner().setAdapter(adapter);

        getText().setText(getTitle(context));
    }

    private TextView getText()
    {
        return (TextView) linearLayout.getChildAt(0);
    }

    private AppCompatSpinner getSpinner()
    {
        return (AppCompatSpinner) linearLayout.getChildAt(1);
    }

    @Override
    public View getView()
    {
        return linearLayout;
    }

    @Override
    public boolean validate()
    {
        return true;
    }

    @Override
    public String getValue()
    {
        int i = getSpinner().getSelectedItemPosition();
        if (argument.optional && i == 0)
            return "";
        else if (argument.optional)
            i--;

        return argument.listOptions[i];
    }

    @Override
    public void setChangeListener(Runnable r)
    {
        getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView,
                    int position, long id)
            {
                r.run();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }
}
