package me.zachcheatham.gameservers.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;

public class ExpandAnimator
{
    public static Animator animateVertical(final View view, final View expandView)
    {
        final ViewGroup.LayoutParams originalLayoutParams = view.getLayoutParams();
        final int before = view.getMeasuredHeight();
        view.measure(
                View.MeasureSpec.makeMeasureSpec(view.getMeasuredWidth(), View.MeasureSpec.AT_MOST),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        final int after = view.getMeasuredHeight();
        final boolean closing = (before > after);

        ValueAnimator animator = ValueAnimator.ofInt(before, after);
        animator.setDuration(view.getContext().getResources()
                                 .getInteger(android.R.integer.config_shortAnimTime));
        animator.addUpdateListener(animation ->
        {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.height = (int) animation.getAnimatedValue();
            view.setLayoutParams(layoutParams);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            {
                int top = layoutParams.height - expandView.getMeasuredHeight();
                int hiddenExpand = (closing ? after : before) - top;
                if (hiddenExpand > 0)
                {
                    Rect clip = new Rect();
                    clip.top = hiddenExpand;
                    clip.bottom = expandView.getMeasuredHeight();
                    clip.left = 0;
                    clip.right = expandView.getMeasuredWidth();
                    expandView.setClipBounds(clip);
                }
                else
                {
                    expandView.setClipBounds(null);
                }
            }
        });
        animator.addListener(new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationStart(Animator animation)
            {
                if (closing)
                    expandView.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2)
                    expandView.setAlpha(0.0f);
            }

            @Override
            public void onAnimationEnd(Animator animation)
            {
                if (closing)
                    expandView.setVisibility(View.GONE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                    expandView.setClipBounds(null);
                else if (closing)
                    expandView.animate().alpha(1.0f);

                view.setLayoutParams(originalLayoutParams);
            }

            @Override
            public void onAnimationCancel(Animator animation)
            {
                if (closing)
                    expandView.setVisibility(View.GONE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                    expandView.setClipBounds(null);
                else if (closing)
                    expandView.setAlpha(1.0f);

                view.setLayoutParams(originalLayoutParams);
            }
        });

        return animator;
    }
}
