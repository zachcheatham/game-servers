package me.zachcheatham.gameservers.ui.argumentinput;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.widget.AppCompatSpinner;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.admin.CommandArgument;

public class TimeLengthArgumentInput extends ArgumentInput
{
    private LinearLayout linearLayout;
    private boolean minutes = false;

    public TimeLengthArgumentInput(CommandArgument argument, Context context)
    {
        super(argument, context);
    }

    @Override
    void createView(Context context)
    {
        linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(
                R.layout.time_length_input, null);

        String[] items;
        if (argument.type.equals(CommandArgument.TYPE_TIME_MINUTES))
        {
            items = context.getResources().getStringArray(R.array.time_units_minutes);
            minutes = true;
        }
        else
        {
            items = context.getResources().getStringArray(R.array.time_units_seconds);
            minutes = false;
        }

        ArrayAdapter adapter = new ArrayAdapter<>(context,
                R.layout.item_spinner_edit_text, items);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        getSpinner().setAdapter(adapter);

        getText().setHint(getTitle(context));
    }

    private TextInputLayout getText()
    {
        return (TextInputLayout) linearLayout.getChildAt(0);
    }

    private AppCompatSpinner getSpinner()
    {
        return (AppCompatSpinner) linearLayout.getChildAt(1);
    }

    @Override
    public View getView()
    {
        return linearLayout;
    }

    @Override
    public boolean validate()
    {
        TextInputLayout inputLayout = getText();
        if (inputLayout.getEditText().getText().length() == 0)
        {
            if (!argument.optional)
            {
                inputLayout.setError(linearLayout.getContext().getString(R.string.error_required));
                return false;
            }
        }
        else
        {

            int value = getTimeValue();
            switch (CommandArgument.validateMinMax(value, argument.min, argument.max))
            {
            case 1:
                inputLayout.setError(linearLayout.getContext().getString(
                        R.string.error_out_of_range_min, argument.min));
                return false;
            case 2:
                inputLayout.setError(linearLayout.getContext().getString(
                        R.string.error_out_of_range_max, argument.max));
                return false;
            case 3:
                inputLayout.setError(linearLayout.getContext().getString(
                        R.string.error_out_of_range, argument.min, argument.max));
                return false;
            }
        }

        inputLayout.setErrorEnabled(false);
        return true;
    }

    @Override
    public String getValue()
    {
        return String.valueOf(getTimeValue());
    }

    @Override
    public void setChangeListener(Runnable r)
    {
        getText().getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                r.run();
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

        getSpinner().setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id)
            {
                r.run();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private int getTimeValue()
    {
        float length = Float.parseFloat(getText().getEditText().getText().toString());
        float multiplier = 1.0f;

        AppCompatSpinner spinner = getSpinner();

        if (minutes)
        {
            switch (spinner.getSelectedItemPosition())
            {
            case 1:
                multiplier = 60.0f; // hours
                break;
            case 2:
                multiplier = 1440.0f; // days
                break;
            case 3:
                multiplier = 10080.0f; // weeks
                break;
            case 4:
                multiplier = 43200.0f; // months
                break;
            case 5:
                multiplier = 525600.0f; // years
                break;
            }
        }
        else
        {
            switch (spinner.getSelectedItemPosition())
            {
            case 1:
                multiplier = 60.0f; // minutes
                break;
            case 2:
                multiplier = 3600.0f; // hours
            }
        }

        return Math.round(multiplier * length);
    }
}
