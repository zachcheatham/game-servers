package me.zachcheatham.gameservers.ui;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

public class CircleImageTransform implements Transformation
{
    @Override
    public Bitmap transform(Bitmap source)
    {
        Bitmap circled =  BitmapHelper.applyBitmapCircle(source);
        source.recycle();
        return circled;
    }

    @Override
    public String key()
    {
        return "circle";
    }
}
