package me.zachcheatham.gameservers.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TwoLineListItem;

import androidx.appcompat.app.AlertDialog;
import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.admin.ServerAdministration;
import me.zachcheatham.gameservers.server.player.copy.PlayerCopyMenuPopulator;

public class PlayerCommandsDialogBuilder
{
    public static AlertDialog createDialog(Context context, ServerAdministration.PlayerMenuItem[] menuItems, DialogInterface.OnClickListener listener)
    {
        return new AlertDialog.Builder(context)
                .setAdapter(new PlayerCommandsAdapter(menuItems), listener)
                .create();
    }

    public static AlertDialog createCopyDialog(Context context, final PlayerCopyMenuPopulator.PlayerCopyMenuItem[] menuItems)
    {
        return new AlertDialog.Builder(context)
                .setAdapter(new PlayerCopyMenuAdapter(menuItems),
                        (dialog, which) ->
                        {
                            ClipboardManager clipboard = (ClipboardManager)
                                    context.getSystemService(Context.CLIPBOARD_SERVICE);

                            ClipData clip = ClipData.newPlainText(menuItems[which].title,
                                    menuItems[which].value);

                            clipboard.setPrimaryClip(clip);

                            Toast.makeText(context,
                                    context.getString(R.string.copy_confirmation, menuItems[which].value),
                                    Toast.LENGTH_SHORT).show();
                        })
                .create();
    }

    private static class PlayerCommandsAdapter extends BaseAdapter
    {
        private final ServerAdministration.PlayerMenuItem[] items;

        PlayerCommandsAdapter(ServerAdministration.PlayerMenuItem[] items)
        {
            this.items = items;
        }

        @Override
        public int getCount()
        {
            return items.length;
        }

        @Override
        public Object getItem(int position)
        {
            return items[position];
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            TextView textView = (TextView) convertView;

            if (textView == null)
            {
                textView =
                        (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_command, parent, false);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                {
                    for (Drawable drawable : textView.getCompoundDrawables())
                        if (drawable != null)
                            drawable.setColorFilter(new PorterDuffColorFilter(parent.getContext().getResources().getColor(R.color.textColorPrimary), PorterDuff.Mode.SRC_IN));
                }
            }

            ServerAdministration.PlayerMenuItem item = items[position];
            int rightDrawable = 0;
            if (item.command.equals(Constants.PLAYER_ADMIN_COPY) ||
                item.command.startsWith(Constants.PLAYER_SUBMENU_PREFIX))
                rightDrawable = R.drawable.ic_menu_right;

            textView.setCompoundDrawablesWithIntrinsicBounds(item.iconResource, 0, rightDrawable, 0);
            textView.setText(item.text);

            return textView;
        }
    }

    private static class PlayerCopyMenuAdapter extends BaseAdapter
    {
        private final PlayerCopyMenuPopulator.PlayerCopyMenuItem[] items;

        PlayerCopyMenuAdapter(PlayerCopyMenuPopulator.PlayerCopyMenuItem[] items)
        {
            this.items = items;
        }

        @Override
        public int getCount()
        {
            return items.length;
        }

        @Override
        public Object getItem(int position)
        {
            return items[position];
        }

        @Override
        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            TwoLineListItem view = (TwoLineListItem) convertView;

            if (view == null)
            {
                view = (TwoLineListItem) LayoutInflater.from(parent.getContext()).inflate(
                                R.layout.list_item_copy, parent, false);
            }

            PlayerCopyMenuPopulator.PlayerCopyMenuItem item = items[position];
            ((TextView) view.findViewById(R.id.text_title)).setText(item.title);
            ((TextView) view.findViewById(R.id.text_value)).setText(item.value);

            return view;
        }
    }
}
