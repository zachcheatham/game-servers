package me.zachcheatham.gameservers.ui.argumentinput;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.admin.CommandArgument;

public class PositionArgumentInput extends ArgumentInput
{
    private ConstraintLayout layout;

    public PositionArgumentInput(CommandArgument argument, Context context)
    {
        super(argument, context);
    }

    @Override
    void createView(Context context)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        layout = (ConstraintLayout) inflater.inflate(R.layout.position_input, null);

        getLabel().setText(getTitle(context));

        for (int i = 0; i < argument.type.length(); i++)
            getTextByI(i).getEditText().setHint(String.valueOf(Character.toUpperCase(argument.type.charAt(i))));

        if (argument.type.length() == 2)
            getText3().setVisibility(View.GONE);
    }

    private TextView getLabel()
    {
        return (TextView) layout.findViewById(R.id.label);
    }

    private TextInputLayout getText1()
    {
        return (TextInputLayout) layout.findViewById(R.id.text1);
    }

    private TextInputLayout getText2()
    {
        return (TextInputLayout) layout.findViewById(R.id.text2);
    }

    private TextInputLayout getText3()
    {
        return (TextInputLayout) layout.findViewById(R.id.text3);
    }

    @Override
    public View getView()
    {
        return layout;
    }

    @Override
    public boolean validate()
    {
        boolean valid = true;
        if (!argument.optional)
        {
            for (int i = 0; i < argument.type.length(); i++)
            {
                TextInputLayout text = getTextByI(i);
                if (text.getEditText().getText().toString().isEmpty())
                {
                    text.setError(text.getContext().getString(R.string.error_required));
                    valid = false;
                }
                else
                {
                    text.setErrorEnabled(false);
                }
            }
        }

        return valid;
    }

    @Override
    public String getValue()
    {
        StringBuilder sb = new StringBuilder(getText1().getEditText().getText());
        for (int i = 1; i < argument.type.length(); i++)
        {
            sb.append(" ");
            sb.append(getTextByI(i).getEditText().getText());
        }
        return sb.toString();
    }

    @Override
    public void setChangeListener(Runnable r)
    {
    }

    private TextInputLayout getTextByI(int i)
    {
        switch (i)
        {
        default:
        case 0:
            return getText1();
        case 1:
            return getText2();
        case 2:
            return getText3();
        }
    }

}
