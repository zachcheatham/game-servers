package me.zachcheatham.gameservers.ui.argumentinput;

import android.content.Context;
import android.view.View;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.admin.CommandArgument;

public abstract class ArgumentInput
{
    final CommandArgument argument;

    ArgumentInput(CommandArgument argument, Context context)
    {
        this.argument = argument;
        createView(context);
    }

    abstract void createView(Context context);
    public abstract View getView();
    public abstract boolean validate();
    public abstract String getValue();

    String getTitle(Context context)
    {
        String hint;
        if (argument.nameResourceId == 0)
            hint = argument.name;
        else
            hint = context.getString(argument.nameResourceId);

        if (argument.optional)
            hint += " " + context.getString(R.string.optional_command);

        return hint;
    }

    public CommandArgument getArgument()
    {
        return argument;
    }

    public abstract void setChangeListener(Runnable r);
}
