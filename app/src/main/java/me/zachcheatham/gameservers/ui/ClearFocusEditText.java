package me.zachcheatham.gameservers.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

import androidx.appcompat.widget.AppCompatEditText;

public class ClearFocusEditText extends AppCompatEditText
{
    public ClearFocusEditText(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public ClearFocusEditText(Context context)
    {
        super(context);
    }

    public ClearFocusEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            clearFocus();
        }

        return super.onKeyPreIme(keyCode, event);
    }
}
