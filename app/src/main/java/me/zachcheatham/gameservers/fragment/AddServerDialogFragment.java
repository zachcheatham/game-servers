package me.zachcheatham.gameservers.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerDetector;
import me.zachcheatham.gameservers.server.ServerExtraOption;
import timber.log.Timber;

public class AddServerDialogFragment extends DialogFragment implements ServerDetector.ServerDetectedListener
{
    public static final String TAG = AddServerDialogFragment.class.getSimpleName();

    private final static String IP_REGEX =
            "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}" +
            "([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(:[0-9]{1,5})?$";
    private final static String HOST_REGEX =
            "(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)+" +
            "([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])(:[0-9]{1,5})?$";

    @BindView(R.id.text_input_layout_server_address)
    TextInputLayout serverAddressInputLayout;
    @BindView(R.id.edit_text_server_address)
    TextInputEditText serverAddressEditText;
    @BindView(R.id.text_input_layout_rcon_password)
    TextInputLayout rconPasswordInputLayout;
    @BindView(R.id.edit_text_rcon_password)
    TextInputEditText rconPasswordEditText;
    @BindView(R.id.spinner_game)
    Spinner gameTypeSpinner;
    private List<TextInputLayout> extraTextInputLayouts = new ArrayList<>();
    private LinearLayout layout;
    private AddServerListener listener;
    private ServerDetector serverDetector = null;
    private Unbinder unbinder;

    private static Server.ServerType getServerTypeFromSpinner(Spinner spinner)
    {
        return Server.ServerType.values()[spinner.getSelectedItemPosition()];
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                   .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        listener = (AddServerListener) context;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Activity activity = getActivity();
        assert activity != null;
        LayoutInflater inflater = activity.getLayoutInflater();

        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(getContext(), R.style.AppTheme));
        builder.setTitle(R.string.action_add_server);

        layout = (LinearLayout) inflater.inflate(R.layout.dialog_fragment_add_server, null);
        unbinder = ButterKnife.bind(this, layout);

        ArrayAdapter adapter =
                ArrayAdapter.createFromResource(activity, R.array.games,
                        R.layout.item_spinner_edit_text);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gameTypeSpinner.setAdapter(adapter);

        builder.setView(layout);
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.setPositiveButton(R.string.action_add, null);

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(dialogInterface ->
        {
            Button button = ((AlertDialog) dialogInterface)
                    .getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(view1 ->
            {
                Server.ServerType type = getServerTypeFromSpinner(gameTypeSpinner);
                if (validate(type))
                {
                    String[] sep = serverAddressEditText.getText().toString().split(":", 2);
                    String address = sep[0];
                    int port;
                    if (sep.length > 1)
                        port = Integer.parseInt(sep[1]);
                    else
                        port = Server.getDefaultPort(type);

                    if (port > 0)
                    {
                        serverAddressInputLayout.setErrorEnabled(false);
                        CreateServerTask task = new CreateServerTask(AddServerDialogFragment.this,
                                address, port,
                                type, rconPasswordEditText.getText().toString());

                        ServerExtraOption[] options = ServerExtraOption.get(type);
                        if (options != null)
                        {
                            for (int i = 0; i < options.length; i++)
                            {
                                TextInputLayout inputLayout = extraTextInputLayouts.get(i);
                                TextInputEditText editText = inputLayout
                                        .findViewById(R.id.edit_text);
                                ServerExtraOption option = options[i];

                                task.extras.put(option.saveKey,
                                        option.convertInput(editText.getText().toString()));
                            }
                        }

                        task.execute();
                    }
                    else
                    {
                        serverAddressInputLayout
                                .setError(getResources().getString(R.string.error_invalid_address));
                    }
                }
            });
        });

        Bundle arguments = getArguments();
        if (arguments != null)
        {
            String preAddress = arguments.getString("preAddress");
            String[] addressParts = preAddress.split(":", 2);
            String address = addressParts[0];
            if (addressParts.length == 1)
            {
                serverAddressEditText.setText(address);
            }
            else
            {
                int port = Integer.parseInt(addressParts[1]);
                serverAddressEditText.setText(String.format(Locale.US, "%s:%d", address, port));
            }
        }

        return dialog;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        unbinder.unbind();
        if (serverDetector != null)
        {
            serverDetector.stop();
            serverDetector = null;
        }
    }

    private void getServerType(String address, int port)
    {
        if (serverDetector != null)
            serverDetector.stop();

        serverDetector = new ServerDetector(address, port, this);
        new ServerTypeTask(serverDetector).execute();
    }

    private boolean validate(Server.ServerType type)
    {
        boolean validated = true;

        if (serverAddressEditText.length() == 0)
        {
            validated = false;
            serverAddressInputLayout
                    .setError(getResources().getString(R.string.error_empty_zone_name));
        }
        else
        {
            String address = serverAddressEditText.getText().toString();
            if (!address.matches(IP_REGEX) && !address.matches(HOST_REGEX))
            {
                validated = false;
                serverAddressInputLayout
                        .setError(getResources().getString(R.string.error_invalid_address));
            }
            else
            {
                String[] sep = address.split(":", 2);

                if (sep.length > 1)
                {
                    int port = Integer.parseInt(sep[1]);

                    if (port > 65535 || port < 1)
                    {
                        validated = false;
                        serverAddressInputLayout
                                .setError(getResources().getString(R.string.error_invalid_address));
                    }
                    else
                    {
                        serverAddressInputLayout.setErrorEnabled(false);
                    }
                }
                else
                {
                    serverAddressInputLayout.setErrorEnabled(false);
                }

                if (validated)
                {
                    ServerExtraOption[] options = ServerExtraOption.get(type);
                    if (options != null)
                    {
                        for (int i = 0; i < options.length; i++)
                        {
                            TextInputLayout inputLayout = extraTextInputLayouts.get(i);
                            TextInputEditText editText = inputLayout.findViewById(R.id.edit_text);
                            ServerExtraOption option = options[i];

                            String errorMessage = option
                                    .validate(editText.getText().toString(), getContext());
                            if (errorMessage != null)
                                inputLayout.setError(errorMessage);
                            else
                                inputLayout.setErrorEnabled(false);
                        }
                    }
                }
            }
        }

        return validated;
    }

    @OnTextChanged(R.id.edit_text_server_address)
    void onAddressTextChanged(CharSequence text)
    {
        if (text.toString().matches(HOST_REGEX))
        {
            String[] p = text.toString().split(":");
            getServerType(p[0], p.length > 1 ? Integer.parseInt(p[1]) : 0);
        }
    }

    @OnItemSelected(R.id.spinner_game)
    void onGameSpinnerItemSelected()
    {
        if (extraTextInputLayouts.size() > 0)
        {
            for (TextInputLayout l : extraTextInputLayouts)
                layout.removeView(l);

            extraTextInputLayouts.clear();
        }

        ServerExtraOption[] options = ServerExtraOption
                .get(getServerTypeFromSpinner(gameTypeSpinner));
        if (options != null)
        {
            LayoutInflater inflater = getActivity().getLayoutInflater();

            for (ServerExtraOption option : options)
            {
                TextInputLayout l =
                        (TextInputLayout) inflater
                                .inflate(R.layout.text_input_layout, layout, false);

                extraTextInputLayouts.add(l);

                EditText editText = l.findViewById(R.id.edit_text);
                if (option.defaultValue != null)
                    editText.setText(option.defaultValue);
                l.setHint(getString(option.hintStringId));
                l.setHintEnabled(true);
                l.setHintAnimationEnabled(true);
                switch (option.fieldType)
                {
                case PORT:
                    editText.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
                    break;
                }

                layout.addView(l);
            }
        }
    }

    @Override
    public void serverGameTypeFound(Server.ServerType type)
    {
        final int i = Arrays.binarySearch(Server.ServerType.values(), type);
        Activity a = getActivity();
        if (a != null)
            a.runOnUiThread(() -> gameTypeSpinner.setSelection(i));
    }

    public interface AddServerListener
    {
        void addServer(Server server);

        boolean serverExists(InetAddress address, int port);
    }

    private static class CreateServerTask extends AsyncTask<Void, Void, Void>
    {
        private final WeakReference<AddServerDialogFragment> dialogReference;
        private final String hostname;
        private final int port;
        private final Server.ServerType type;
        private final String adminPassword;
        private final Map<String, Object> extras = new HashMap<>();

        CreateServerTask(AddServerDialogFragment dialog, String address, int port,
                Server.ServerType type,
                String adminPassword)
        {
            dialogReference = new WeakReference<>(dialog);
            this.hostname = address;
            this.port = port;
            this.type = type;
            this.adminPassword = adminPassword;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            final AddServerDialogFragment dialog = dialogReference.get();
            if (dialog != null)
            {
                Activity activity = dialog.getActivity();
                assert activity != null;

                InetAddress host;

                try
                {
                    host = InetAddress.getByName(hostname);
                }
                catch (UnknownHostException e)
                {
                    activity.runOnUiThread(() ->
                    {
                        dialog.serverAddressInputLayout
                                .setError(dialog.getString(R.string.unknown_host));
                        ((AlertDialog) dialog.getDialog())
                                .getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        ((AlertDialog) dialog.getDialog())
                                .getButton(AlertDialog.BUTTON_NEGATIVE).setEnabled(true);
                        dialog.setCancelable(true);
                    });
                    return null;
                }

                if (dialog.listener.serverExists(host, port))
                {
                    activity.runOnUiThread(() ->
                    {
                        dialog.serverAddressInputLayout
                                .setError(dialog.getString(R.string.error_server_exists));
                        ((AlertDialog) dialog.getDialog())
                                .getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        ((AlertDialog) dialog.getDialog())
                                .getButton(AlertDialog.BUTTON_NEGATIVE).setEnabled(true);
                        dialog.setCancelable(true);
                    });

                    return null;
                }

                final Server server = Server.create(hostname, port, type);
                if (adminPassword != null && adminPassword.trim().length() > 0)
                    server.setAdminPassword(adminPassword);

                for (Map.Entry<String, Object> entry : extras.entrySet())
                    server.setExtraOption(entry.getKey(), entry.getValue());

                activity.runOnUiThread(() ->
                {
                    dialog.listener.addServer(server);
                    dialog.dismiss();
                });
            }

            return null;
        }
    }

    private static class ServerTypeTask extends AsyncTask<Void, Void, Void>
    {
        private final ServerDetector detector;

        ServerTypeTask(ServerDetector detector)
        {
           this.detector = detector;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            detector.detect();
            return null;
        }
    }
}
