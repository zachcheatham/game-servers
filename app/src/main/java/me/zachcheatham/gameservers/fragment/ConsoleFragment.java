package me.zachcheatham.gameservers.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import butterknife.Unbinder;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.ServerDetailsActivity;
import me.zachcheatham.gameservers.adapter.ConsoleAdapter;
import me.zachcheatham.gameservers.server.Server;

public class ConsoleFragment extends Fragment
        implements ServerDetailsActivity.ServiceConnectionListener
{
    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.console_input)
    EditText input;
    private ConsoleAdapter adapter;
    private Server server;

    private Unbinder unbinder;

    public ConsoleFragment()
    {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_console, container, false);

        unbinder = ButterKnife.bind(this, view);

        Context context = view.getContext();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        if (adapter != null)
            recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        server = ((ServerDetailsActivity) getActivity()).getServer();
        if (server != null)
            serviceConnected(server);
    }

    @Override
    public void onDetach()
    {
        server = null;
        super.onDetach();
    }

    @SuppressWarnings("SameReturnValue")
    @OnEditorAction(R.id.console_input)
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
    {
        server.sendUserAdminCommand(input.getText().toString());
        input.setText("");
        return true;
    }

    @OnFocusChange(R.id.console_input)
    public void onFocusChange(View v, boolean hasFocus)
    {
        ((ServerDetailsActivity) getActivity()).setBottomNavigationVisible(!hasFocus);
    }

    @Override
    public void serviceConnected(Server server)
    {
        this.server = server;
        adapter = new ConsoleAdapter(getActivity(), server);
        if (recyclerView != null)
            recyclerView.setAdapter(adapter);
    }
}
