package me.zachcheatham.gameservers.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.ServerDetailsActivity;
import me.zachcheatham.gameservers.adapter.MinecraftPlayerListAdapter;
import me.zachcheatham.gameservers.adapter.PlayerListAdapter;
import me.zachcheatham.gameservers.adapter.SourcePlayerListAdapter;
import me.zachcheatham.gameservers.events.AutoRefreshChangeEvent;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.Server;

public class PlayerListFragment extends Fragment
        implements ServerDetailsActivity.ServiceConnectionListener,
        SwipeRefreshLayout.OnRefreshListener
{
    @BindView(R.id.list) RecyclerView recyclerView;
    @BindView(R.id.empty_state) View emptyState;
    @BindView(R.id.empty_state_text) TextView emptyStateTextView;
    @BindView(R.id.empty_state_subtext) TextView emptyStateSubTextView;

    private ServerDetailsActivity activity;
    private PlayerListAdapter adapter;
    private Server server;
    private Unbinder unbinder;
    private SwipeRefreshLayout refreshLayout;

    public PlayerListFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_player_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        Context context = view.getContext();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        if (adapter != null)
            recyclerView.setAdapter(adapter);

        /*DividerItemDecoration decoration = new DividerItemDecoration(context, layoutManager
        .getOrientation());
        recyclerView.addItemDecoration(decoration);*/

        refreshLayout = (SwipeRefreshLayout) view;
        refreshLayout.setOnRefreshListener(this);
        if (activity.getServerManager() != null)
        {
            refreshLayout.setEnabled(!activity.getServerManager().shouldAutoRefresh());
            checkEmptyState();
        }

        return view;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        EventBus.getDefault().register(this);

        activity = ((ServerDetailsActivity) getActivity());
        Server server = activity.getServer();
        if (server != null)
            serviceConnected(server);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        EventBus.getDefault().unregister(this);
        activity = null;
        server = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        if (adapter != null && adapter.getSortMenu() != -1)
        {
            inflater.inflate(adapter.getSortMenu(), menu);
            menu.findItem(adapter.getSortMenuItemID(adapter.getSortMode())).setChecked(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getGroupId() == R.id.sort_group)
        {
            adapter.setSorting(adapter.getSelectedSortMode(item.getItemId()));
            item.setChecked(true);

            SharedPreferences.Editor preferenceEditor =
                    activity.getSharedPreferences(Constants.PREFS_KEY, Context.MODE_PRIVATE).edit();
            preferenceEditor.putInt(adapter.getSortingConfigID(), adapter.getSortMode());
            preferenceEditor.apply();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh()
    {
        server.update(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.getServer() == server && refreshLayout != null)
        {
            refreshLayout.setRefreshing(false);
            checkEmptyState();
        }
    }

    private void checkEmptyState()
    {
        if (server.getPlayersListSize() > 0)
        {
            recyclerView.setVisibility(View.VISIBLE);
            emptyState.setVisibility(View.GONE);
        }
        else
        {
            recyclerView.setVisibility(View.GONE);
            emptyState.setVisibility(View.VISIBLE);

            if (server.isOffline())
            {
                if (server.getOfflineReason() == Server.OfflineReason.Timeout)
                {
                    emptyStateTextView.setText(R.string.error_server_timed_out);
                    emptyStateSubTextView.setText(R.string.error_server_offline_details);
                }
                else if (server.getOfflineReason() == Server.OfflineReason.Banned)
                {
                    emptyStateTextView.setText(R.string.error_banned);
                    emptyStateSubTextView.setText(R.string.error_banned_details);
                }
                else
                {
                    emptyStateTextView.setText(R.string.error_server_offline);
                    emptyStateSubTextView.setText(R.string.error_server_offline_details);
                }
            }
            else if (server.getNumPlayers() > 0)
            {
                emptyStateTextView.setText(getString(R.string.error_no_players));
                emptyStateSubTextView.setText(getString(R.string.error_no_players_details));
            }
            else
            {
                emptyStateTextView.setText(getString(R.string.empty_server));
                emptyStateSubTextView.setText(getString(R.string.empty_server_details));
            }
        }
    }

    @Override
    public void serviceConnected(Server server)
    {
        this.server = server;
        switch (server.getType())
        {
        case Source:
        case Gold_Src:
            adapter = new SourcePlayerListAdapter(activity, server);
            break;
        case Minecraft:
            adapter = new MinecraftPlayerListAdapter(activity, server);
        }

        if (recyclerView != null)
        {
            recyclerView.setAdapter(adapter);
            refreshLayout.setEnabled(!activity.getServerManager().shouldAutoRefresh());
            checkEmptyState();
        }

        if (adapter.getSortingConfigID() != null)
        {
            SharedPreferences preferences = activity
                    .getSharedPreferences(Constants.PREFS_KEY, Context.MODE_PRIVATE);
            adapter.setSorting(
                    preferences.getInt(adapter.getSortingConfigID(), adapter.getDefaultSortMode()));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAutoRefreshChange(AutoRefreshChangeEvent event)
    {
        refreshLayout.setEnabled(!event.autoRefreshEnabled);
    }
}
