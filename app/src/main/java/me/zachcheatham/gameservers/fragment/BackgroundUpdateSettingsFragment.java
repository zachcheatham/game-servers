package me.zachcheatham.gameservers.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.SettingsActivity;
import me.zachcheatham.gameservers.adapter.SelectServersListAdapter;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;

public class BackgroundUpdateSettingsFragment extends Fragment implements
        SettingsActivity.ServiceConnectionListener, SelectServersListAdapter.ServerClickedListener
{
    private RecyclerView recyclerView = null;
    private SelectServersListAdapter adapter = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState)
    {
        recyclerView = new RecyclerView(inflater.getContext());

        recyclerView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        recyclerView.setLayoutManager(new LinearLayoutManager(inflater.getContext()));

        TypedValue attr = new TypedValue();
        getActivity().getTheme().resolveAttribute(android.R.attr.windowBackground, attr, true);
        if (attr.type >= TypedValue.TYPE_FIRST_COLOR_INT && attr.type <= TypedValue.TYPE_LAST_COLOR_INT)
            recyclerView.setBackgroundColor(attr.data);
        else
            recyclerView.setBackgroundResource(attr.resourceId);

        if (adapter != null)
            recyclerView.setAdapter(adapter);

        return recyclerView;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        SettingsActivity activity = ((SettingsActivity) getActivity());
        if (activity != null)
        {
            ActionBar ab = activity.getSupportActionBar();
            if (ab != null)
                ab.setTitle(R.string.settings_background_update_enabled);

            ServerManager manager = activity.getServerManager();
            if (manager != null)
                onServiceConnected(manager);
        }
    }

    @Override
    public void onServiceConnected(ServerManager manager)
    {
        adapter = new SelectServersListAdapter(manager);
        adapter.setServerClickedListener(this);

        if (recyclerView != null)
            recyclerView.setAdapter(adapter);
    }

    @Override
    public void onServerClicked(Server server, boolean checked)
    {
        server.setFlag(Server.FLAG_BACKGROUND_UPDATE, checked);
    }

    @Override
    public boolean shouldServerBeChecked(Server server)
    {
        return server.hasFlag(Server.FLAG_BACKGROUND_UPDATE);
    }
}
