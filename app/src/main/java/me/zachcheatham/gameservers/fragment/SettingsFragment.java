package me.zachcheatham.gameservers.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;
import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.LicensesActivity;
import me.zachcheatham.gameservers.R;

public class SettingsFragment extends PreferenceFragmentCompat
{
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
    {
        getPreferenceManager().setSharedPreferencesName(Constants.PREFS_KEY);

        addPreferencesFromResource(R.xml.settings);

        final ListPreference autoUpdateFrequency =
                (ListPreference) findPreference(Constants.PREFS_SERVER_UPDATE_FREQUENCY);
        autoUpdateFrequency.setSummary(autoUpdateFrequency.getEntry());
        autoUpdateFrequency.setOnPreferenceChangeListener(
                (preference, newValue) ->
                {
                    ListPreference listPreference = (ListPreference) preference;
                    listPreference.setSummary(
                            listPreference.getEntries()[listPreference
                                    .findIndexOfValue((String) newValue)]);
                    return true;
                }
        );

        final SwitchPreferenceCompat unmeteredOnly =
                (SwitchPreferenceCompat) findPreference(
                        Constants.PREFS_SERVER_AUTO_UPDATE_UNMETERED_ONLY);
        SwitchPreferenceCompat autoUpdate =
                (SwitchPreferenceCompat) findPreference(
                        Constants.PREFS_SERVER_AUTO_UPDATE_ENABLE);
        unmeteredOnly.setEnabled(unmeteredOnly.isChecked());
        autoUpdateFrequency.setEnabled(unmeteredOnly.isChecked());
        autoUpdate.setOnPreferenceChangeListener((preference, newValue) ->
        {
            boolean b = (boolean) newValue;
            autoUpdateFrequency.setEnabled(b);
            unmeteredOnly.setEnabled(b);

            return true;
        });

        ListPreference serverTimeout =
                (ListPreference) findPreference(Constants.PREFS_SERVER_TIMEOUT);
        serverTimeout.setSummary(serverTimeout.getEntry());
        serverTimeout.setOnPreferenceChangeListener(
                (preference, newValue) ->
                {
                    ListPreference listPreference = (ListPreference) preference;
                    listPreference.setSummary(
                            listPreference.getEntries()[listPreference
                                    .findIndexOfValue((String) newValue)]);
                    return true;
                }
        );

        Preference backgroundUpdatedServers = findPreference("background_update_servers");
        backgroundUpdatedServers.setOnPreferenceClickListener(preference ->
                {
                    FragmentManager fm = SettingsFragment.this.getFragmentManager();

                    if (fm != null)
                    {
                        FragmentTransaction transaction = fm.beginTransaction();
                        transaction.addToBackStack(null);
                        transaction.setCustomAnimations(R.anim.slide_left, R.anim.hold, R.anim.hold, R.anim.slide_right);
                        transaction.add(android.R.id.content, new BackgroundUpdateSettingsFragment());
                        transaction.commit();
                    }

                    return true;
                });

        Preference openSource = findPreference("about_open_source");
        openSource.setOnPreferenceClickListener(preference ->
        {
            Activity activity = getActivity();

            if (activity != null)
            {
                Intent intent = new Intent(activity, LicensesActivity.class);
                startActivity(intent);
                activity.overridePendingTransition(R.anim.slide_left, R.anim.fade_out);
            }

            return true;
        });
    }
}
