package me.zachcheatham.gameservers.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Group;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.ServerDetailsActivity;
import me.zachcheatham.gameservers.adapter.MinecraftServerInfoAdapter;
import me.zachcheatham.gameservers.adapter.SourceServerInfoAdapter;
import me.zachcheatham.gameservers.events.AutoRefreshChangeEvent;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.BaseSourceServer;
import me.zachcheatham.gameservers.server.MinecraftServer;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.web.MapImage;

public class ServerInfoFragment extends Fragment
        implements ServerDetailsActivity.ServiceConnectionListener,
        SwipeRefreshLayout.OnRefreshListener
{
    @BindView(R.id.scroll_view) NestedScrollView scrollView;
    @BindView(R.id.list) RecyclerView recyclerView;
    @BindView(R.id.map_image_group) Group mapImageGroup;
    @BindView(R.id.server_map) ImageView mapImage;
    @BindView(R.id.server_hostname) TextView serverName;
    @BindView(R.id.empty_state) View emptyState;
    @BindView(R.id.empty_state_text) TextView emptyStateTextView;
    @BindView(R.id.empty_state_subtext) TextView emptyStateSubTextView;

    private Unbinder unbinder;
    private ServerDetailsActivity activity;
    private RecyclerView.Adapter adapter;
    private Server server;
    private SwipeRefreshLayout refreshLayout;
    private boolean checkedForMapImage = false;
    private Handler handler = new Handler();
    private boolean animateHeader = false;
    private boolean haveMapImage = false;
    private boolean hidHeader = false;

    public ServerInfoFragment()
    {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_server_info, container, false);
        view.addOnLayoutChangeListener(
                (v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
                    if (oldTop != top || oldBottom != bottom)
                        recyclerView.setMinimumHeight(bottom - top);
                });
        Context context = view.getContext();

        unbinder = ButterKnife.bind(this, view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        DividerItemDecoration decoration = new DividerItemDecoration(context,
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(decoration);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);

        refreshLayout = (SwipeRefreshLayout) view;
        refreshLayout.setOnRefreshListener(this);
        initialApplyServer();

        scrollView.getViewTreeObserver().addOnScrollChangedListener(
                this::updateHeaderTextVisibility);

        return view;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        EventBus.getDefault().register(this);

        activity = ((ServerDetailsActivity) getActivity());
        if (activity != null)
        {
            Server server = activity.getServer();
            if (server != null)
                serviceConnected(server);
        }

        handler.postDelayed(() -> {animateHeader = true;handler = null;},
                getResources().getInteger(android.R.integer.config_shortAnimTime));
    }

    @Override
    public void onHiddenChanged(boolean hidden)
    {
        super.onHiddenChanged(hidden);
        updateHeaderTextVisibility();
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        EventBus.getDefault().unregister(this);
        activity = null;
        server = null;
    }

    @Override
    public void serviceConnected(Server server)
    {
        this.server = server;

        switch (server.getType())
        {
        case Source:
        case Gold_Src:
            adapter = new SourceServerInfoAdapter(getActivity(), (BaseSourceServer) server);
            break;
        case Minecraft:
            adapter = new MinecraftServerInfoAdapter(getActivity(), (MinecraftServer) server);
        }

        initialApplyServer();
    }

    private void checkEmptyState()
    {
        if (server.getPing() == -1)
        {
            if (emptyState.getVisibility() == View.GONE)
            {
                scrollView.setVisibility(View.GONE);
                emptyState.setVisibility(View.VISIBLE);
                if (server.getOfflineReason() == Server.OfflineReason.Timeout)
                {
                    emptyStateTextView.setText(R.string.error_server_timed_out);
                    emptyStateSubTextView.setText(R.string.error_server_offline_details);
                }
                else if (server.getOfflineReason() == Server.OfflineReason.Banned)
                {
                    emptyStateTextView.setText(R.string.error_banned);
                    emptyStateSubTextView.setText(R.string.error_banned_details);
                }
                else
                {
                    emptyStateTextView.setText(R.string.error_server_offline);
                    emptyStateSubTextView.setText(R.string.error_server_offline_details);
                }

                updateHeaderTextVisibility();
            }
        }
        else if (scrollView.getVisibility() == View.GONE)
        {
            scrollView.setVisibility(View.VISIBLE);
            emptyState.setVisibility(View.GONE);
            updateHeaderTextVisibility();
        }
    }

    private void updateHeaderTextVisibility()
    {
        if (!checkedForMapImage || (haveMapImage && !isHidden() && server.getPing() != -1 &&
            scrollView.getScrollY() < serverName.getBottom()))
        {
            if (!hidHeader)
            {
                hidHeader = true;
                activity.setActionBarTitleVisible(false, animateHeader);
            }
        }
        else if (hidHeader)
        {
            hidHeader = false;
            activity.setActionBarTitleVisible(true, animateHeader);
        }
    }

    private void initialApplyServer()
    {
        // First check that the view has been created and the service is connected
        if (adapter != null && recyclerView != null)
        {
            recyclerView.setAdapter(adapter);
            refreshLayout.setEnabled(!activity.getServerManager().shouldAutoRefresh());

            serverName.setText(server.getName());
            if (!server.getMap().isEmpty())
            {
                String mapImage = MapImage.getInstance().getMapImageURL(server.getMap(), true);
                if (mapImage != null)
                {
                    onMapImageDetermined(mapImage);
                }
                else
                {
                    updateHeaderTextVisibility();
                    new MapImageTask(this, server.getMap()).execute();
                }
            }
            checkedForMapImage = true;

            checkEmptyState();
        }
    }

    private void onMapImageDetermined(String url)
    {
        haveMapImage = url != null;
        if (url != null)
        {
            Picasso.get().load(url).fit().centerCrop().into(mapImage);
            mapImageGroup.setVisibility(View.VISIBLE);
        }
        else
        {
            mapImageGroup.setVisibility(View.GONE);
        }

        updateHeaderTextVisibility();
    }

    @Override
    public void onRefresh()
    {
        server.update(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.getServer() == server && refreshLayout != null)
        {
            serverName.setText(server.getName());
            refreshLayout.setRefreshing(false);
            checkEmptyState();

            if (event.didMapChange() || (event.didOfflineChange() && !server.isOffline()))
                new MapImageTask(this, server.getMap()).execute();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAutoRefreshChange(AutoRefreshChangeEvent event)
    {
        refreshLayout.setEnabled(!event.autoRefreshEnabled);
    }

    private static class MapImageTask extends AsyncTask<Void, Void, Void>
    {
        private final WeakReference<ServerInfoFragment> listenerWeakReference;
        private final String map;

        MapImageTask(ServerInfoFragment fragment, String map)
        {
            listenerWeakReference = new WeakReference<>(fragment);
            this.map = map;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            ServerInfoFragment fragment = listenerWeakReference.get();
            if (fragment != null && map != null && !map.isEmpty())
            {
                MapImage api = MapImage.getInstance();
                String url = api.getMapImageURL(map, false);
                Activity activity = fragment.getActivity();
                if (activity != null)
                    activity.runOnUiThread(() -> fragment.onMapImageDetermined(url));
            }

            return null;
        }
    }
}
