package me.zachcheatham.gameservers.fragment;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.InetAddress;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.adapter.ServerImportListAdapter;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerImporter;
import me.zachcheatham.gameservers.server.ServerManager;
import timber.log.Timber;

public class ImportServersFragment extends DialogFragment
        implements ServerImporter.ImportCompleteListener
{
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.server_list)
    RecyclerView recyclerView;
    private ServerImportListAdapter adapter;
    private Unbinder unbinder;
    private ServerManager serverManager = null;
    private boolean unsupportedFile = false;

    public ImportServersFragment()
    {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_Dialog_FullScreen);

        Bundle arguments = getArguments();
        if (arguments != null)
        {
            Uri uri = arguments.getParcelable("uri");
            if (uri != null)
            {
                ContentResolver resolver = getActivity().getContentResolver();

                String[] projection = {MediaStore.MediaColumns.DISPLAY_NAME};
                try (Cursor cursor = resolver.query(uri, projection, null, null, null))
                {
                    if (cursor != null && cursor.moveToFirst())
                    {
                        String filePath = cursor.getString(0);
                        String extension = filePath.substring(filePath.lastIndexOf('.') + 1);
                        if (!extension.equals("sslf"))
                        {
                            Timber.w("Unsupported file type %s", extension);
                            unsupportedFile = true;
                        }
                    }
                }

                if (!unsupportedFile)
                {
                    try
                    {
                        InputStream stream = resolver.openInputStream(uri);
                        ServerImporter.read(stream, ServerImporter.FileType.SSLF, this);
                    }
                    catch (FileNotFoundException e)
                    {
                        Timber.e(e, "Unable to open file");
                        Toast.makeText(getContext(), R.string.unable_to_import_open,
                                Toast.LENGTH_LONG).show();
                        dismiss();
                    }
                }
                else
                {
                    Toast.makeText(getContext(), R.string.unable_to_import_unsupported,
                            Toast.LENGTH_LONG).show();
                    dismiss();
                }
            }
            else
            {
                Timber.w("URI didn't exist in arguments bundle.");
                dismiss();
            }
        }
        else
        {
            Timber.w("Arguments were null.");
            dismiss();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState)
    {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            dialog.getWindow()
                  .addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_import_servers, container, false);
        unbinder = ButterKnife.bind(this, view);

        toolbar.setNavigationIcon(R.drawable.ic_clear_white);
        toolbar.setNavigationOnClickListener(v -> dismiss());

        Context context = view.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        return view;
    }

    @Override
    public void onDestroyView()
    {
        unbinder.unbind();
        super.onDestroyView();
    }

    public void setServerManager(ServerManager manager)
    {
        this.serverManager = manager;
        if (manager == null)
        {
            Timber.e("Server manager was null.");
            dismiss();
        }
    }

    @OnClick(R.id.import_servers)
    public void onImportServersClick(Button button)
    {
        List<Server> selectedServers = adapter.getSelectedServers();
        serverManager.addServers(selectedServers);
        serverManager.saveServers();
        dismiss();
    }

    @Override
    public void onServerImportCompleted(final List<Server> servers, int serversExisted,
            int serversUnsupported)
    {
        adapter = new ServerImportListAdapter(servers, serversExisted, serversUnsupported);
        getActivity().runOnUiThread(() ->
        {
            recyclerView.setAdapter(adapter);
            for (Server server : servers)
            {
                server.update(false);
            }
        });
    }

    @Override
    public boolean serverExists(InetAddress address, int port)
    {
        return serverManager.getServer(address, port) != null;
    }

    @Override
    public boolean serverExists(String hostname, int port)
    {
        return serverManager.getServer(hostname, port) != null;
    }
}
