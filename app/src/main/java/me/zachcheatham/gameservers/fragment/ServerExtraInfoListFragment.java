package me.zachcheatham.gameservers.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.ServerDetailsActivity;
import me.zachcheatham.gameservers.adapter.MinecraftModsListAdapter;
import me.zachcheatham.gameservers.adapter.SourceServerRulesAdapter;
import me.zachcheatham.gameservers.events.AutoRefreshChangeEvent;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.BaseSourceServer;
import me.zachcheatham.gameservers.server.MinecraftServer;
import me.zachcheatham.gameservers.server.Server;

public class ServerExtraInfoListFragment extends Fragment
        implements ServerDetailsActivity.ServiceConnectionListener,
        SwipeRefreshLayout.OnRefreshListener
{
    private static final int MODE_SERVER_RULES = 0;
    private static final int MODE_INSTALLED_MODS = 1;

    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.empty_state)
    View emptyState;
    private ServerDetailsActivity activity;
    private RecyclerView.Adapter adapter;
    private Server server;
    private Unbinder unbinder;
    private SwipeRefreshLayout refreshLayout;
    private int mode = -1;

    public ServerExtraInfoListFragment()
    {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        mode = getArguments().getInt("mode");

        View view = inflater.inflate(R.layout.fragment_server_extra_info, container, false);
        unbinder = ButterKnife.bind(this, view);

        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration decoration = new DividerItemDecoration(context,
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(decoration);

        refreshLayout = (SwipeRefreshLayout) view;
        refreshLayout.setOnRefreshListener(this);
        if (activity.getServerManager() != null)
        {
            refreshLayout.setEnabled(!activity.getServerManager().shouldAutoRefresh());
            checkEmptyState();
        }

        if (adapter != null)
            recyclerView.setAdapter(adapter);
        else if (server != null)
            serviceConnected(server);

        return view;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        EventBus.getDefault().register(this);

        activity = ((ServerDetailsActivity) getActivity());
        Server server = activity.getServer();
        if (server != null)
            serviceConnected(server);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        EventBus.getDefault().unregister(this);
        activity = null;
        server = null;
    }

    private void checkEmptyState()
    {
        if (adapter != null && adapter.getItemCount() > 0)
        {
            recyclerView.setVisibility(View.VISIBLE);
            emptyState.setVisibility(View.GONE);
        }
        else
        {
            recyclerView.setVisibility(View.GONE);
            emptyState.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh()
    {
        server.update(true);
    }

    @Override
    public void serviceConnected(Server server)
    {
        this.server = server;

        if (mode == MODE_SERVER_RULES)
            adapter = new SourceServerRulesAdapter((BaseSourceServer) server);
        else if (mode == MODE_INSTALLED_MODS)
            adapter = new MinecraftModsListAdapter(getActivity(), (MinecraftServer) server);

        if (recyclerView != null)
        {
            checkEmptyState();
            recyclerView.setAdapter(adapter);
            refreshLayout.setEnabled(!activity.getServerManager().shouldAutoRefresh());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAutoRefreshChange(AutoRefreshChangeEvent event)
    {
        refreshLayout.setEnabled(!event.autoRefreshEnabled);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.getServer() == server && refreshLayout != null)
        {
            refreshLayout.setRefreshing(false);
            checkEmptyState();
        }
    }
}
