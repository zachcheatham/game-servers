package me.zachcheatham.gameservers.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import butterknife.OnFocusChange;
import butterknife.Unbinder;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.ServerDetailsActivity;
import me.zachcheatham.gameservers.adapter.ChatAdapter;
import me.zachcheatham.gameservers.events.ServerCommandSearchCompleteEvent;
import me.zachcheatham.gameservers.server.ChatMessage;
import me.zachcheatham.gameservers.server.ChatTarget;
import me.zachcheatham.gameservers.server.Server;

public class ChatFragment extends Fragment
        implements ServerDetailsActivity.ServiceConnectionListener
{
    @BindView(R.id.list) RecyclerView recyclerView;
    @BindView(R.id.edit_text_input) EditText input;
    @BindView(R.id.spinner_chat_target) AppCompatSpinner targetChatSpinner;

    private ChatAdapter adapter;
    private Server server;
    private ChatTarget[] chatTargets = null;
    private Unbinder unbinder;

    public ChatFragment()
    {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        unbinder = ButterKnife.bind(this, view);

        Context context = view.getContext();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        if (adapter != null)
            recyclerView.setAdapter(adapter);

        if (server != null)
            populateChatTargets();

        return view;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        server = ((ServerDetailsActivity) getActivity()).getServer();
        if (server != null)
            serviceConnected(server);
    }

    @Override
    public void onDetach()
    {
        server = null;
        super.onDetach();
    }

    @OnEditorAction(R.id.edit_text_input)
    boolean onEditorAction(TextView v, int actionId, KeyEvent event)
    {
        ChatTarget target = chatTargets[targetChatSpinner.getSelectedItemPosition()];
        server.sendChatMessage(input.getText().toString(), target);

        input.setText("");
        return true;
    }

    @OnFocusChange(R.id.edit_text_input)
    void onFocusChange(View v, boolean hasFocus)
    {
        ((ServerDetailsActivity) getActivity()).setBottomNavigationVisible(!hasFocus);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    private void onAdminModDetected(ServerCommandSearchCompleteEvent e)
    {
        populateChatTargets();
    }

    private void populateChatTargets()
    {
        if (getContext() != null)
        {
            chatTargets = server.getChatTargets();
            String[] targetNames = new String[chatTargets.length];

            for (int i = 0; i < targetNames.length; i++)
            {
                ChatTarget target = chatTargets[i];
                if (target.type == ChatMessage.MessageType.NORMAL)
                    targetNames[i] = getString(R.string.chat_target_everyone);
                else if (target.type == ChatMessage.MessageType.ADMIN)
                    targetNames[i] = getString(R.string.chat_target_admin);
            }

            ArrayAdapter adapter = new ArrayAdapter<>(getContext(),
                    R.layout.item_spinner_edit_text, targetNames);
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            targetChatSpinner.setAdapter(adapter);

            targetChatSpinner.setVisibility(chatTargets.length > 1 ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void serviceConnected(Server server)
    {
        this.server = server;
        adapter = new ChatAdapter(getActivity(), server);
        if (recyclerView != null)
        {
            recyclerView.setAdapter(adapter);
            populateChatTargets();
        }
    }
}
