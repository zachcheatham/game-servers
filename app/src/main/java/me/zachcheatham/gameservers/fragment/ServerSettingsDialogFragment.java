package me.zachcheatham.gameservers.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.Unbinder;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.ServerDetailsActivity;
import me.zachcheatham.gameservers.events.ServerSettingsUpdatedEvent;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerExtraOption;

public class ServerSettingsDialogFragment extends DialogFragment
{
    public static final String TAG = "server_settings";

    @BindView(R.id.text_input_layout_rcon_password) TextInputLayout rconPasswordInputLayout;
    @BindView(R.id.edit_text_rcon_password) TextInputEditText rconPasswordEditText;
    @BindView(R.id.checkbox_background_update) CheckBox backgroundUpdateCheckbox;
    @BindView(R.id.checkbox_notify_admin_chat) CheckBox notifyAdminChatCheckbox;
    @BindView(R.id.checkbox_notify_all_chat) CheckBox notifyAllChatCheckbox;
    @BindView(R.id.checkbox_notify_empty) CheckBox notifyEmptyCheckbox;
    @BindView(R.id.checkbox_notify_players) CheckBox notifyPlayersCheckbox;
    @BindView(R.id.checkbox_notify_full) CheckBox notifyFullCheckbox;

    private ServerDetailsActivity activity;
    private List<TextInputLayout> extraTextInputLayouts = new ArrayList<>();
    private Unbinder unbinder;
    private ServerExtraOption[] options;
    private Server server;

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        activity = (ServerDetailsActivity) getActivity();
        LayoutInflater inflater = activity.getLayoutInflater();

        server = activity.getServer();

        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(getContext(), R.style.AppTheme));
        builder.setTitle(R.string.title_server_settings);

        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.dialog_fragment_server_config, null);
        unbinder = ButterKnife.bind(this, view);

        LinearLayout layout = (LinearLayout) view.getChildAt(0);

        if (server.hasAdminPassword())
            rconPasswordEditText.setText("******");

        backgroundUpdateCheckbox.setChecked(server.hasFlag(Server.FLAG_BACKGROUND_UPDATE));
        notifyAllChatCheckbox.setChecked(server.hasFlag(Server.FLAG_NOTIFY_ALL_CHAT));
        notifyAdminChatCheckbox.setChecked(server.hasFlag(Server.FLAG_NOTIFY_ADMIN_CHAT));
        notifyEmptyCheckbox.setChecked(server.hasFlag(Server.FLAG_NOTIFY_EMPTY));
        notifyPlayersCheckbox.setChecked(server.hasFlag(Server.FLAG_NOTIFY_PLAYERS));
        notifyFullCheckbox.setChecked(server.hasFlag(Server.FLAG_NOTIFY_FULL));

        if (!server.supportsChat() || !server.hasAdminPassword())
        {
            notifyAdminChatCheckbox.setVisibility(View.GONE);
            notifyAdminChatCheckbox.setChecked(false);
            notifyAllChatCheckbox.setVisibility(View.GONE);
            notifyAllChatCheckbox.setChecked(false);
        }

        updateBackgroundUpdateLogic();
        updateChatLogic();

        options = ServerExtraOption.get(server.getType());
        if (options != null)
        {
            for (int i = 0; i < options.length; i++)
            {
                ServerExtraOption option = options[i];
                TextInputLayout l =
                        (TextInputLayout) inflater
                                .inflate(R.layout.text_input_layout, layout, false);

                extraTextInputLayouts.add(l);

                EditText editText = l.findViewById(R.id.edit_text);
                editText.setText(server.getExtraOption(option.saveKey).toString());
                l.setHint(getString(option.hintStringId));
                l.setHintEnabled(true);
                l.setHintAnimationEnabled(true);
                switch (option.fieldType)
                {
                case PORT:
                    editText.setInputType(EditorInfo.TYPE_CLASS_NUMBER);
                    break;
                }

                layout.addView(l, 1+i);
            }
        }

        builder.setView(view);
        builder.setPositiveButton(R.string.action_done, null);

        final AlertDialog dialog = builder.create();

        dialog.setOnShowListener(dialogInterface ->
        {
            Button button = ((AlertDialog) dialogInterface)
                    .getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(view1 ->
            {
                if (validate())
                {
                    if (!rconPasswordEditText.getText().toString().equals("******"))
                        server.setAdminPassword(rconPasswordEditText.getText().toString());

                    server.setFlag(Server.FLAG_BACKGROUND_UPDATE, backgroundUpdateCheckbox.isChecked());
                    server.setFlag(Server.FLAG_NOTIFY_ADMIN_CHAT, notifyAdminChatCheckbox.isChecked());
                    server.setFlag(Server.FLAG_NOTIFY_ALL_CHAT, notifyAllChatCheckbox.isChecked());
                    server.setFlag(Server.FLAG_NOTIFY_EMPTY, notifyEmptyCheckbox.isChecked());
                    server.setFlag(Server.FLAG_NOTIFY_PLAYERS, notifyPlayersCheckbox.isChecked());
                    server.setFlag(Server.FLAG_NOTIFY_FULL, notifyFullCheckbox.isChecked());



                    if (options != null)
                    {
                        for (int i = 0; i < options.length; i++)
                        {
                            TextInputLayout inputLayout = extraTextInputLayouts.get(i);
                            TextInputEditText editText = inputLayout.findViewById(R.id.edit_text);
                            ServerExtraOption option = options[i];

                            server.setExtraOption(option.saveKey,
                                    option.convertInput(editText.getText().toString()));
                        }
                    }

                    activity.getServerManager().saveServers();

                    EventBus.getDefault().post(new ServerSettingsUpdatedEvent(server));
                    dismiss();
                }
            });
        });

        return dialog;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnCheckedChanged(R.id.checkbox_background_update)
    void updateBackgroundUpdateLogic()
    {
        boolean checked = backgroundUpdateCheckbox.isChecked();
        notifyPlayersCheckbox.setEnabled(checked);
        notifyEmptyCheckbox.setEnabled(checked);
        notifyFullCheckbox.setEnabled(checked);
    }

    @OnCheckedChanged(R.id.checkbox_notify_all_chat)
    void updateChatLogic()
    {
        notifyAdminChatCheckbox.setEnabled(!notifyAllChatCheckbox.isChecked());
    }

    private boolean validate()
    {
        if (options != null)
        {
            for (int i = 0; i < options.length; i++)
            {
                TextInputLayout inputLayout = extraTextInputLayouts.get(i);
                TextInputEditText editText = inputLayout.findViewById(R.id.edit_text);
                ServerExtraOption option = options[i];

                String errorMessage = option.validate(editText.getText().toString(), getContext());
                if (errorMessage != null)
                    inputLayout.setError(errorMessage);
                else
                    inputLayout.setErrorEnabled(false);
            }
        }

        return true;
    }
}
