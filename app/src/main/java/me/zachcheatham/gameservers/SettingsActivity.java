package me.zachcheatham.gameservers;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import me.zachcheatham.gameservers.fragment.SettingsFragment;
import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.ServerManagerService;

public class SettingsActivity extends AppCompatActivity
{
    private ServerManager manager;

    private final ServiceConnection serviceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder)
        {
            ServerManagerService.LocalBinder binder = (ServerManagerService.LocalBinder) iBinder;
            manager = binder.getService().getManager();
            manager.setResourceLevel(ServerManager.ResourceLevel.APP);

            for (Fragment fragment : getSupportFragmentManager().getFragments())
                if (fragment instanceof ServiceConnectionListener)
                    ((ServiceConnectionListener) fragment).onServiceConnected(manager);
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            manager = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        getSupportFragmentManager().beginTransaction()
                                   .replace(android.R.id.content, new SettingsFragment()).commit();
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        Intent intent = new Intent(this, ServerManagerService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop()
    {
        if (manager != null)
            unbindService(serviceConnection);

        super.onStop();
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        finish();
        return true;
    }

    @Override
    public void finish()
    {
        FragmentManager fm = getSupportFragmentManager();
        int backStackSize = fm.getBackStackEntryCount();
        if (backStackSize > 0)
        {
            fm.popBackStack();
            if (backStackSize == 1)
            {
                ActionBar ab = getSupportActionBar();
                if (ab != null)
                    ab.setTitle(R.string.settings);
            }
        }
        else
            super.finish();
    }

    public ServerManager getServerManager()
    {
        return manager;
    }

    public interface ServiceConnectionListener
    {
        void onServiceConnected(ServerManager manager);
    }
}
