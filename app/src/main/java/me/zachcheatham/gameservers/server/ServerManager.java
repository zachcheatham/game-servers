package me.zachcheatham.gameservers.server;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.preference.PreferenceManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.events.AutoRefreshChangeEvent;
import me.zachcheatham.gameservers.events.ServerAddedEvent;
import me.zachcheatham.gameservers.events.ServerIndexChangedEvent;
import me.zachcheatham.gameservers.events.ServerRemovedEvent;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.events.ServersCompletedUpdatingEvent;
import me.zachcheatham.gameservers.server.notifications.Notifier;
import timber.log.Timber;

public class ServerManager implements SharedPreferences.OnSharedPreferenceChangeListener
{
    private static WeakReference<ServerManager> instanceWeakReference = null;
    private static final String SAVED_SERVERS_PREF_KEY = "saved_servers";
    public static int queryTimeout = 1000;
    private static int maxLogSize = 100;
    private static int maxChatLogSize = 100;

    private final List<Server> servers = new ArrayList<>();
    private final Context context;
    private final BroadcastReceiver networkStateReceiver = new NetworkChangeBroadcastReceiver();
    private final ConnectivityManager connectivityManager;
    private final Notifier notifier;
    private ResourceLevel resourceLevel = ResourceLevel.PERSIST_RCON;
    private boolean autoRefreshEnabled;
    private boolean autoRefreshOnlyUnmetered;
    private boolean autoRefresh = false;

    ServerManager(Context context)
    {
        this.context = context;
        notifier = new Notifier(this);

        // Don't overwrite existing reference
        if (instanceWeakReference == null || instanceWeakReference.get() == null)
            instanceWeakReference = new WeakReference<>(this);

        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_KEY, 0);
        autoRefreshEnabled = settings.getBoolean(Constants.PREFS_SERVER_AUTO_UPDATE_ENABLE, true);
        autoRefreshOnlyUnmetered = settings
                .getBoolean(Constants.PREFS_SERVER_AUTO_UPDATE_UNMETERED_ONLY, true);
        queryTimeout = Integer.valueOf(settings.getString(Constants.PREFS_SERVER_TIMEOUT, "1000"));

        settings.registerOnSharedPreferenceChangeListener(this);

        connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public void setResourceLevel(ResourceLevel level)
    {
        if (level != resourceLevel)
        {
            while (resourceLevel != level)
            {
                ResourceLevel newLevel;
                if (resourceLevel.getValue() > level.getValue())
                    newLevel = resourceLevel.getLower();
                else
                    newLevel = resourceLevel.getUpper();
                newLevel.apply(this, resourceLevel);
                resourceLevel = newLevel;
            }
        }
    }

    public ResourceLevel getResourceLevel()
    {
        return resourceLevel;
    }

    public void addServer(Server server)
    {
        servers.add(server);
        EventBus.getDefault().post(new ServerAddedEvent(this.servers.size() - 1));
        server.setTimeout(queryTimeout);
        server.setMaxChatLogSize(maxChatLogSize);
        server.setMaxLogSize(maxLogSize);
        server.update(false);
    }

    public void addServers(List<Server> servers)
    {
        this.servers.addAll(servers);
        for (Server server : this.servers)
        {
            server.setTimeout(queryTimeout);
        }

        EventBus.getDefault().post(new ServerIndexChangedEvent());
    }

    public void removeServer(int index)
    {
        Server server = servers.get(index);
        server.cleanup();

        servers.remove(index);
        EventBus.getDefault().post(new ServerRemovedEvent(index));
    }

    public Server getServer(int index)
    {
        if (index < servers.size())
            return servers.get(index);
        else
            return null;
    }

    public Server getServer(InetAddress address, int port)
    {
        for (Server server : servers)
        {
            if (server.address == null)
                if (!server.lookupAddress())
                    continue;

            if (server.address.equals(address) && server.port == port)
                return server;
        }

        return null;
    }

    // Do not use this method unless you cannot retrieve the ip address of a hostname
    public Server getServer(String hostname, int port)
    {
        for (Server server : servers)
        {
            if (server.hostname.equals(hostname) && server.port == port)
                return server;
        }

        return null;
    }

    public int getCount()
    {
        return servers.size();
    }

    private boolean updating()
    {
        for (Server server : servers)
        {
            if (server.isUpdating())
                return true;
        }

        return false;
    }

    public int getServerIndex(Server s)
    {
        return servers.indexOf(s);
    }

    public Context getContext()
    {
        return context;
    }

    public Notifier getNotifier()
    {
        return notifier;
    }

    public void updateAll()
    {
        for (Server s : servers)
        {
            if (!s.isUpdating())
                s.update(false);
            else
                Timber.d("Not updating %s because its still updating...", s.getAddressString());
        }
    }

    public void saveServers()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        if (servers.size() > 0)
        {
            JSONArray savedServers = new JSONArray();

            for (Server server : servers)
            {
                try
                {
                    JSONObject serverJSON = new JSONObject();
                    server.save(serverJSON);
                    savedServers.put(serverJSON);
                }
                catch (JSONException e)
                {
                    Timber.w(e, "Error saving server: ");
                }
            }

            editor.putString(SAVED_SERVERS_PREF_KEY, savedServers.toString());
        }
        else
        {
            editor.remove(SAVED_SERVERS_PREF_KEY);
        }

        editor.apply();
    }

    void loadServers()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String savedServers = preferences.getString(SAVED_SERVERS_PREF_KEY, null);
        if (savedServers != null)
        {
            try
            {
                JSONArray serversJSON = new JSONArray(savedServers);
                for (int i = 0; i < serversJSON.length(); i++)
                {
                    try
                    {
                        JSONObject serverJSON = serversJSON.getJSONObject(i);
                        Server server = Server.createFromJSON(serverJSON);
                        servers.add(server);
                    }
                    catch (JSONException e)
                    {
                        Timber.w(e, "Unable to load server.");
                    }
                }

                Timber.i("Loaded server list");
                EventBus.getDefault().post(new ServerIndexChangedEvent());
            }
            catch (JSONException e)
            {
                Timber.e(e, "UNABLE TO LOAD SAVED SERVERS");
            }
        }
    }

    void cleanup()
    {
        notifier.cleanup();
        setResourceLevel(ResourceLevel.PERSIST_RCON);
        cleanupServers();
        instanceWeakReference = null;
    }

    private void cleanupServers()
    {
        for (Server s : servers)
            s.cleanup();
    }

    public boolean shouldAutoRefresh()
    {
        return autoRefresh;
    }

    private void handleMeteredNetworkAutoRefresh()
    {
        if (!autoRefreshEnabled || Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN ||
            (autoRefreshOnlyUnmetered &&
             connectivityManager.isActiveNetworkMetered()))
        {
            EventBus.getDefault().post(new AutoRefreshChangeEvent(false));
            autoRefresh = false;
        }
        else
        {
            if (!autoRefresh)
            {
                EventBus.getDefault().post(new AutoRefreshChangeEvent(true));
                autoRefresh = true;
            }
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences settings, String key)
    {
        switch (key)
        {
        case Constants.PREFS_SERVER_AUTO_UPDATE_ENABLE:
        case Constants.PREFS_SERVER_AUTO_UPDATE_UNMETERED_ONLY:
            autoRefreshEnabled = settings
                    .getBoolean(Constants.PREFS_SERVER_AUTO_UPDATE_ENABLE, true);
            autoRefreshOnlyUnmetered = settings
                    .getBoolean(Constants.PREFS_SERVER_AUTO_UPDATE_UNMETERED_ONLY, true);

            handleMeteredNetworkAutoRefresh();
            break;
        case Constants.PREFS_SERVER_TIMEOUT:
            queryTimeout = Integer
                    .valueOf(settings.getString(Constants.PREFS_SERVER_TIMEOUT, "1000"));
            for (Server server : servers)
                server.setTimeout(queryTimeout);
            break;
        case Constants.PREFS_LOG_SIZE:
            maxLogSize = Integer.valueOf(settings.getString(Constants.PREFS_LOG_SIZE, "100"));
            for (Server server : servers)
                server.setMaxLogSize(maxLogSize);
            break;
        case Constants.PREFS_CHAT_SIZE:
            maxChatLogSize = Integer.valueOf(settings.getString(Constants.PREFS_CHAT_SIZE, "100"));
            for (Server server : servers)
                server.setMaxChatLogSize(maxChatLogSize);
        }
    }

    @Subscribe
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (servers.contains(event.getServer()) && !updating())
            EventBus.getDefault().post(new ServersCompletedUpdatingEvent());
    }

    public boolean hasServer(Server server)
    {
        return servers.contains(server);
    }

    public static ServerManager getInstance()
    {
        if (instanceWeakReference != null)
            return instanceWeakReference.get();
        return null;
    }

    private class NetworkChangeBroadcastReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            handleMeteredNetworkAutoRefresh();
        }
    }

    public enum ResourceLevel {
        APP(3) {
            @Override
            void enable(ServerManager sm)
            {
                sm.handleMeteredNetworkAutoRefresh();

                IntentFilter networkIntentFilter = new IntentFilter();
                networkIntentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
                sm.context.registerReceiver(sm.networkStateReceiver, networkIntentFilter);
            }

            @Override
            void disable(ServerManager sm)
            {
                sm.context.unregisterReceiver(sm.networkStateReceiver);
            }

            @Override
            ResourceLevel getUpper() { return null; }

            @Override
            ResourceLevel getLower() { return BG_SYNC; }
        },
        BG_SYNC(2) {
            @Override
            void enable(ServerManager sm)
            {
                EventBus.getDefault().register(sm);
            }

            @Override
            void disable(ServerManager sm)
            {
                EventBus.getDefault().unregister(sm);
            }

            @Override
            ResourceLevel getUpper() { return APP; }

            @Override
            ResourceLevel getLower() { return PERSIST_RCON; }
        },
        PERSIST_RCON(1) {
            @Override
            void enable(ServerManager sm) {}

            @Override
            void disable(ServerManager sm) {}

            @Override
            ResourceLevel getUpper() { return BG_SYNC; }

            @Override
            ResourceLevel getLower() { return null; }
        };

        private final int value;

        ResourceLevel(int value)
        {
            this.value = value;
        }

        public int getValue()
        {
            return value;
        }

        void apply(ServerManager sm, ResourceLevel oldLevel)
        {
            if (getValue() > oldLevel.getValue())
                enable(sm);
            else
                oldLevel.disable(sm);
        }

        abstract void disable(ServerManager sm);
        abstract void enable(ServerManager sm);
        abstract ResourceLevel getLower();
        abstract ResourceLevel getUpper();
    }
}
