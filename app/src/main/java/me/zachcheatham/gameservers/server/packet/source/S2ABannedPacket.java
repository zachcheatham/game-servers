package me.zachcheatham.gameservers.server.packet.source;

import me.zachcheatham.gameservers.server.packet.UnexpectedPacketException;

public class S2ABannedPacket extends SourcePacket
{
    public static final byte HEADER = 0x6C;

    public S2ABannedPacket() throws UnexpectedPacketException
    {
        super(new byte[]{HEADER});

        if (getHeader() != HEADER)
            throw new UnexpectedPacketException(HEADER, getHeader(), "S2ABannedPacket");
    }
}
