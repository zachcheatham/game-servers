package me.zachcheatham.gameservers.server.packet.minecraft;

public class MinecraftClientHandshakePacket extends MinecraftClientPacket
{
    private static final byte PACKET_ID = 0x00;

    public MinecraftClientHandshakePacket(String hostname, int port)
    {
        super(PACKET_ID);
        writeVarInt(420); // Protocol version
        writeString(hostname);
        writeUnsignedShort(port);
        writeVarInt(1); // Next State
    }
}
