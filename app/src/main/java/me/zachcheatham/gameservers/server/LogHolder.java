package me.zachcheatham.gameservers.server;

public class LogHolder
{
    public final String message;
    public final LogType type;

    public LogHolder(LogType type, String message)
    {
        this.type = type;
        this.message = message;
    }

    public LogHolder(LogType type)
    {
        this.type = type;
        this.message = null;
    }

    public enum LogType
    {
        DISCONNECTED, AUTHENTICATED, CONNECTED, INVALID_PASSWORD, COMMAND, COMMAND_RESPONSE, LOG,
        LOG_UNAVAILABLE, LOG_AVAILABLE, BANNED
    }
}