package me.zachcheatham.gameservers.server.socket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

import me.zachcheatham.gameservers.server.packet.UnexpectedPacketException;
import me.zachcheatham.gameservers.server.packet.source.S2AChallengePacket;
import me.zachcheatham.gameservers.server.packet.source.S2AGoldSrcInfoPacket;
import me.zachcheatham.gameservers.server.packet.source.S2AGoldSrcRCONResponsePacket;
import me.zachcheatham.gameservers.server.packet.source.S2AInfoPacket;
import me.zachcheatham.gameservers.server.packet.source.S2ALogPacket;
import me.zachcheatham.gameservers.server.packet.source.S2APlayerPacket;
import me.zachcheatham.gameservers.server.packet.source.S2ARulesPacket;
import me.zachcheatham.gameservers.server.packet.source.SourcePacket;
import timber.log.Timber;

public abstract class BaseSourceSocket
{
    private final InetAddress address;
    private final int port;
    private final SocketListener listener;
    DatagramSocket socket;
    private boolean punched = false;
    private boolean punching = false;
    private boolean running = false;

    BaseSourceSocket(InetAddress address, int port, SocketListener listener)
    {
        this.address = address;
        this.port = port;
        this.listener = listener;
    }

    private static SourcePacket createPacket(byte[] data)
    {
        try
        {
            switch (data[0])
            {
            case S2AChallengePacket.HEADER:
                return new S2AChallengePacket(data);
            case S2AGoldSrcInfoPacket.HEADER:
                return new S2AGoldSrcInfoPacket(data);
            case S2AInfoPacket.HEADER:
                return new S2AInfoPacket(data);
            case S2APlayerPacket.HEADER:
                return new S2APlayerPacket(data);
            case S2ARulesPacket.HEADER:
                return new S2ARulesPacket(data);
            case S2ALogPacket.HEADER:
                return new S2ALogPacket(data);
            case S2AGoldSrcRCONResponsePacket.HEADER:
                return new S2AGoldSrcRCONResponsePacket(data);
            default:
                return new SourcePacket(data, false);
            }
        }
        catch (UnexpectedPacketException ignored)
        {
        }

        return null;
    }

    public void send(SourcePacket packet)
    {
        try
        {
            if (socket == null)
                socket = new DatagramSocket();

            if (!running)
            {
                socket.setSoTimeout(5000);
                socket.connect(address, port);
                socket.setSoTimeout(0);
                punched = false;
                running = true;
                new Thread(new ListenRunnable()).start();
            }

            byte[] data = packet.getData();
            socket.send(new DatagramPacket(data, data.length, address, port));
        }
        catch (IOException e)
        {
            close();
            listener.socketClosed(e);
        }
    }

    protected abstract byte[] receivePacket() throws IOException;

    public void close()
    {
        if (socket != null)
        {
            socket.close();
        }

        socket = null;
        punched = false;
        punching = false;
        running = false;
    }

    public InetSocketAddress getLocalAddress()
    {
        if (socket != null)
            return (InetSocketAddress) socket.getLocalSocketAddress();
        else
            return null;
    }

    public void punch(int port)
    {
        try
        {
            if (!punched && socket != null)
            {
                punching = true;
                socket.close();
                socket = new DatagramSocket(port);
                //socket.bind(new InetSocketAddress(address, port));
                punched = true;
                punching = false;
            }
        }
        catch (IOException e)
        {
            Timber.e(e);
            close();
        }
    }

    public boolean isPunched()
    {
        return punched;
    }

    public interface SocketListener
    {
        void socketClosed(Exception e);

        void packetReceived(SourcePacket packet);

        void socketErrorOccurred(Exception e);
    }

    private class ListenRunnable implements Runnable
    {
        @Override
        public void run()
        {
            while (running)
            {
                while (punching)
                {
                    try
                    {
                        Thread.sleep(100);
                    }
                    catch (InterruptedException ignored)
                    {
                    }
                }

                try
                {
                    if (socket != null)
                    {
                        byte[] data = receivePacket();
                        if (data != null)
                        {
                            SourcePacket packet = createPacket(data);
                            listener.packetReceived(packet);
                        }
                    }
                }
                catch (SocketException ignored)
                {
                }
                catch (IOException e)
                {
                    if (!punching && running)
                        close();
                    listener.socketClosed(e);
                }
                catch (Exception e)
                {
                    Timber.w(e, "Error while receiving packet...");
                    listener.socketErrorOccurred(e);
                }
            }
        }
    }
}
