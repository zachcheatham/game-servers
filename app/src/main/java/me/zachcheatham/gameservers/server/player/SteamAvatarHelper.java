package me.zachcheatham.gameservers.server.player;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import me.zachcheatham.gameservers.web.SteamWebAPIRelay;

public class SteamAvatarHelper extends AvatarHelper
{
    private final Map<String, String> avatarURLs = new HashMap<>();

    @Override
    public void loadAvatars(Player[] players, AvatarLoadListener listener)
    {
        SourcePlayer[] sourcePlayers = Arrays.copyOf(players, players.length, SourcePlayer[].class);
        new SteamAvatarsTask(avatarURLs, listener, sourcePlayers).execute();
    }

    @Override
    public boolean hasAvatar(Player player)
    {
        String steamid64 = ((SourcePlayer) player).getSteamID64();
        return avatarURLs.containsKey(steamid64);
    }

    @Override
    public String getAvatar(Player player)
    {
        String steamid64 = ((SourcePlayer) player).getSteamID64();
        return avatarURLs.get(steamid64);
    }

    private static class SteamAvatarsTask extends AsyncTask<Void, Void, Void>
    {
        private final WeakReference<AvatarLoadListener> listenerWeakReference;
        private final Map<String, String> avatarCache;
        private final SourcePlayer[] players;

        SteamAvatarsTask(Map<String, String> avatarCache, AvatarLoadListener listener, SourcePlayer[] players)
        {
            listenerWeakReference = new WeakReference<>(listener);
            this.players = players;
            this.avatarCache = avatarCache;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            AvatarLoadListener listener = listenerWeakReference.get();
            if (listener != null)
            {
                SteamWebAPIRelay api = SteamWebAPIRelay.getInstance();
                Map<String, String> avatars = api.getAvatars(players);
                avatarCache.putAll(avatars);
                listener.avatarsLoaded();
            }

            return null;
        }
    }
}
