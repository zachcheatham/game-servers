package me.zachcheatham.gameservers.server.packet.source;

public class A2SRulesPacket extends SourcePacket
{
    public static final byte HEADER = 0x56;

    public A2SRulesPacket(long challenge)
    {
        super(HEADER);
        writeLong(challenge);
    }
}
