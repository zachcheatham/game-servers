package me.zachcheatham.gameservers.server;

import java.util.HashMap;
import java.util.Map;

public class WordFormatter
{
    private static Map<String, String> knownWords = new HashMap<>();

    static
    {
        knownWords.put("steamid", "Steam ID");
        knownWords.put("ip", "IP");
        knownWords.put("#userid", "User ID");
        knownWords.put("userid", "User ID");
        knownWords.put("addban", "Add Ban");
        knownWords.put("banip", "Ban IP");
    }

    public static boolean format(StringBuffer word)
    {
        String wordKey = word.toString().toLowerCase();
        if (knownWords.containsKey(wordKey))
        {
            word.delete(0, word.length());
            word.append(knownWords.get(wordKey));
            return true;
        }
        return false;
    }
}
