package me.zachcheatham.gameservers.server.packet.source;

import me.zachcheatham.gameservers.server.packet.UnexpectedPacketException;

public class S2AGoldSrcRCONResponsePacket extends SourcePacket
{
    public static final byte HEADER = 0x6C;

    public final String response;
    public final boolean isBanned;
    public final boolean badPassword;

    public S2AGoldSrcRCONResponsePacket(byte[] data) throws UnexpectedPacketException
    {
        super(data);

        if (getHeader() != HEADER)
            throw new UnexpectedPacketException(HEADER, getHeader(), getClass().getSimpleName());

        response = readString().trim();
        if (response.equals("You have been banned from this server."))
        {
            isBanned = true;
            badPassword = false;
        }
        else
        {
            isBanned = false;
            badPassword = response.equals("Bad rcon_password.");
        }
    }
}
