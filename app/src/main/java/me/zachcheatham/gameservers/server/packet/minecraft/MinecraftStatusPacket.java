package me.zachcheatham.gameservers.server.packet.minecraft;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import me.zachcheatham.gameservers.server.packet.PacketException;
import me.zachcheatham.gameservers.server.packet.UnexpectedPacketException;
import me.zachcheatham.gameservers.server.player.MinecraftPlayer;
import me.zachcheatham.gameservers.ui.BitmapHelper;
import timber.log.Timber;

public class MinecraftStatusPacket extends MinecraftClientPacket
{
    private static final byte PACKET_ID = 0x00;
    private static final Pattern FAVICON_PATTERN = Pattern.compile("^data:([^;]+);base64,(.+)$");
    private final JSONObject status;

    public MinecraftStatusPacket(byte[] data) throws PacketException
    {
        super(data);

        if (packetID != PACKET_ID)
            throw new UnexpectedPacketException(PACKET_ID, (byte) packetID,
                    this.getClass().getSimpleName());

        try
        {
            status = new JSONObject(readString());
        }
        catch (JSONException e)
        {
            Timber.e(e, "Received invalid JSON for MinecraftStatusPacket");
            throw new PacketException("Invalid JSON");
        }
    }

    private static String stripMinecraftFormatting(String string)
    {
        return string.replaceAll("§.", "");
    }

    public String getDescription()
    {
        try
        {
            Object description = status.get("description");
            if (description instanceof JSONObject)
                return stripMinecraftFormatting(((JSONObject) description).getString("text")).trim()
                                                                                             .replaceAll(
                                                                                                     "\\s+",
                                                                                                     " ");
            else
                return stripMinecraftFormatting((String) description).trim()
                                                                     .replaceAll("\\s+", " ");
        }
        catch (JSONException e)
        {
            Timber.w(e, "Unable to get description from JSON.");
            //Timber.d(status.toString());
            return "<error>";
        }
    }

    public int getOnlinePlayers()
    {
        try
        {
            return status.getJSONObject("players").getInt("online");
        }
        catch (JSONException e)
        {
            Timber.w(e, "Unable to get online players from JSON.");
            //Timber.d(status.toString());
            return 0;
        }
    }

    public int getMaxPlayers()
    {
        try
        {
            return status.getJSONObject("players").getInt("max");
        }
        catch (JSONException e)
        {
            Timber.w(e, "Unable to get online players from JSON.");
            //Timber.d(status.toString());
            return 0;
        }
    }

    public String getVersion()
    {
        try
        {
            return status.getJSONObject("version").getString("name");
        }
        catch (JSONException e)
        {
            Timber.w(e, "Unable to get version from JSON.");
            //Timber.d(status.toString());
            return "<error>";
        }
    }

    public String[][] getInstalledMods()
    {
        try
        {
            if (status.has("modinfo"))
            {
                JSONArray modListJSON = status.getJSONObject("modinfo").getJSONArray("modList");
                String[][] modList = new String[modListJSON.length()][2];
                for (int i = 0; i < modList.length; i++)
                {
                    JSONObject modJSON = modListJSON.getJSONObject(i);
                    modList[i][0] = modJSON.getString("modid");
                    modList[i][1] = modJSON.getString("version");
                }
                return modList;
            }
            else
            {
                return null;
            }
        }
        catch (JSONException e)
        {
            Timber.w(e, "Unable to get mods from JSON.");
            //Timber.d(status.toString());
            return null;
        }
    }

    public Bitmap getFavicon()
    {
        if (status.has("favicon"))
        {
            try
            {
                String data = status.getString("favicon");
                byte[] bytes = Base64.decode(data.substring(data.indexOf(",")), 0);
                return BitmapHelper.applyBitmapCircle(BitmapFactory.decodeByteArray(bytes, 0, bytes.length));

            }
            catch (JSONException e)
            {
                Timber.w(e, "Unable to get favicon from JSON.");
                //Timber.d(status.toString());
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    public List<MinecraftPlayer> getPlayers()
    {
        try
        {
            List<MinecraftPlayer> playerList = new ArrayList<>();

            JSONArray players = status.getJSONObject("players").getJSONArray("sample");
            for (int i = 0; i < players.length(); i++)
            {
                JSONObject player = players.getJSONObject(i);
                String uniqueID = player.getString("id");
                if (!uniqueID.equals("00000000-0000-0000-0000-000000000000"))
                {
                    MinecraftPlayer minecraftPlayer = new MinecraftPlayer(player.getString("name"));
                    minecraftPlayer.setUniqueId(player.getString("id"));
                    playerList.add(minecraftPlayer);
                }
            }

            return playerList;
        }
        catch (JSONException e)
        {
            Timber.w(e, "Unable to get online players from JSON.");
            //Timber.d(status.toString());
        }

        return null;
    }
}
