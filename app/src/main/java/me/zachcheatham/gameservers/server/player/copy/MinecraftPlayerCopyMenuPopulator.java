package me.zachcheatham.gameservers.server.player.copy;

import android.content.Context;

import java.util.List;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.player.MinecraftPlayer;
import me.zachcheatham.gameservers.server.player.Player;

public class MinecraftPlayerCopyMenuPopulator extends PlayerCopyMenuPopulator
{
    public MinecraftPlayerCopyMenuPopulator(Player player)
    {
        super(player);
    }

    @Override
    public List<PlayerCopyMenuItem> populate(Context context)
    {
        List<PlayerCopyMenuItem> items = super.populate(context);
        MinecraftPlayer minecraftPlayer = (MinecraftPlayer) player;
        items.add(new PlayerCopyMenuItem(
                context.getString(R.string.action_copy_uuid),
                minecraftPlayer.getUniqueID()));
        return items;
    }
}
