package me.zachcheatham.gameservers.server.admin;

import me.zachcheatham.gameservers.server.BaseSourceServer;
import me.zachcheatham.gameservers.server.Server;
import timber.log.Timber;

public class ServerAdminModDetector
{
    private static final String SOURCEMOD_DETECT_STRING = "sm";
    private static final String ULX_DETECT_STRING = "ulx";

    private Server server;
    private int attempt = 0;

    public ServerAdminModDetector(Server server)
    {
        this.server = server;
    }

    public void start()
    {
        switch (server.getType())
        {
        case Source:
            if (((BaseSourceServer) server).getGameAppId() == 4000) // detect gmod admin mods
            {
                detectGmodAdminMod();
            }
            else // all other source games
            {
                detectSourceAdminMod();
            }
            break;
        case Minecraft:
            server.setAdminMod(new MinecraftServerAdministration(server));
            break;
        }
    }

    private void detectSourceAdminMod()
    {
        server.sendInternalAdminCommand(SOURCEMOD_DETECT_STRING, response ->
        {
            if (server.getType() == Server.ServerType.Source)
            {
                if (response.contains("Unknown command"))
                {
                    Timber.i("%s has no admin mod installed. Using default commands.",
                            server.getName());
                    server.setAdminMod(new SourceServerAdministration(server));
                }
                else
                {
                    Timber.i("%s has SourceMod installed. Initializing...", server.getName());
                    server.setAdminMod(new SourceModServerAdministration((BaseSourceServer) server));
                }
            }
            else if (server.getType() == Server.ServerType.Gold_Src)
            {
                // todo
            }
        });

        attempt++;
    }

    private void detectGmodAdminMod()
    {
        String command;
        switch (attempt)
        {
        case 0:
        default:
            command = ULX_DETECT_STRING;
            break;
        }

        server.sendInternalAdminCommand(command, response ->
        {
            if (response.contains("Unknown Command"))
            {
                attempt++;
                if (attempt > 0) // attempt limit. increase when we had mods to the switch statement
                {
                    Timber.i("%s has no admin mod installed. Using default commands.",
                            server.getName());
                    server.setAdminMod(new SourceServerAdministration(server));
                }
                else
                {
                    detectGmodAdminMod();
                }
            }
            else
            {
                switch (attempt)
                {
                case 0:
                default:
                    Timber.i("%s has ULX installed. Initializing...", server.getName());
                    server.setAdminMod(new ULXServerAdministration(server));
                    break;
                }
            }
        });
    }
}
