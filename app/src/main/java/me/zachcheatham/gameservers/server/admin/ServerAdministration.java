package me.zachcheatham.gameservers.server.admin;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import me.zachcheatham.gameservers.server.ChatTarget;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.player.Player;

public abstract class ServerAdministration
{
    final Map<String, Command> serverCommands = new HashMap<>();
    final Map<String, Command> playerCommands = new HashMap<>();

    public abstract void populateServerMenu(Context context, MenuInflater inflater, Menu menu);
    public abstract void populatePlayerMenu(@Nullable String subMenu, Context context,
            List<PlayerMenuItem> items, Player player);

    public Command getCommand(String command)
    {
        if (serverCommands.containsKey(command))
            return serverCommands.get(command);
        else if (playerCommands.containsKey(command))
            return playerCommands.get(command);
        else
            return null;
    }

    public boolean hasCommand(String command)
    {
        return serverCommands.containsKey(command);
    }

    public boolean hasPlayerCommand(String command)
    {
        return playerCommands.containsKey(command);
    }

    public abstract Command getKickCommand();
    public abstract Command getBanCommand();
    public abstract Command getMenuItemCommand(MenuItem menuItem);
    public abstract boolean sendChatMessage(String message, ChatTarget target, Server server);
    public abstract boolean handlesChatTarget(ChatTarget target);
    public abstract ChatTarget[] getChatTargets();

    public boolean parseLogLine(String line, Server server)
    {
        return false;
    }

    public static class PlayerMenuItem
    {
        public final String command;
        public final CharSequence text;
        public final int iconResource;

        public PlayerMenuItem(String command, CharSequence text, int iconResource)
        {
            this.command = command;
            this.text = text;
            this.iconResource = iconResource;
        }
    }
}
