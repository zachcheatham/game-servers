package me.zachcheatham.gameservers.server.packet.source;

import me.zachcheatham.gameservers.server.packet.UnexpectedPacketException;

public class S2AInfoPacket extends SourcePacket
{
    public static final byte HEADER = 0x49;

    public final String hostname;
    public final String map;
    public final String gameDirectory;
    public final String game;
    public final int appID;
    public final int numPlayers;
    public final int maxPlayers;
    public final int botCount;
    public final char serverType;
    public final char operatingSystem;
    public final boolean password;
    public final boolean isProtected;
    public final String version;

    private final int edfPort;
    private final int edfSteamID;
    private final int edfSourceTVPort;
    private final String edfKeywords;

    public S2AInfoPacket(byte[] data) throws UnexpectedPacketException
    {
        super(data);

        if (getHeader() != HEADER)
            throw new UnexpectedPacketException(HEADER, getHeader(), "S2AInfo");

        readUnsignedByte(); // Discarding the protocol version

        hostname = readString();
        map = readString();
        gameDirectory = readString();
        game = readString();
        appID = readShort();
        numPlayers = readUnsignedByte();
        maxPlayers = readUnsignedByte();
        botCount = readUnsignedByte();
        serverType = (char) readUnsignedByte();
        operatingSystem = (char) readUnsignedByte();
        password = readBoolean();
        isProtected = readBoolean();
        if (appID == 2400) // special data for THE SHIP
        {
            readUnsignedByte();
            readUnsignedByte();
            readUnsignedByte();
        }
        version = readString();

        byte edf = readByte();

        if ((edf & 0x80) == 0x80)
            edfPort = readShort();
        else
            edfPort = 0;

        if ((edf & 0x10) == 0x10)
            edfSteamID = readInt();
        else
            edfSteamID = 0;

        if ((edf & 0x40) == 0x40)
            edfSourceTVPort = readShort();
        else
            edfSourceTVPort = 0;

        if ((edf & 0x20) == 0x20)
            edfKeywords = readString();
        else
            edfKeywords = null;
    }
}
