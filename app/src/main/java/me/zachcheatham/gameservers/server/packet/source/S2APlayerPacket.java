package me.zachcheatham.gameservers.server.packet.source;

import java.nio.BufferUnderflowException;
import java.util.ArrayList;
import java.util.List;

import me.zachcheatham.gameservers.server.packet.UnexpectedPacketException;
import me.zachcheatham.gameservers.server.player.SourcePlayer;

public class S2APlayerPacket extends SourcePacket
{
    public static final byte HEADER = 0x44;

    public final List<SourcePlayer> players = new ArrayList<>();

    public S2APlayerPacket(byte[] data) throws UnexpectedPacketException
    {
        super(data);

        if (getHeader() != HEADER)
            throw new UnexpectedPacketException(HEADER, getHeader(), "S2APlayer");

        int size = readUnsignedByte();

        for (int i = 0; i < size; i++)
        {
            try
            {
                readUnsignedByte(); // Discard index

                String name = readString();
                long score = readInt(); // Read long doesn't seem to work right
                float time = Float
                        .intBitsToFloat(readInt()); // Read float doesn't seem to work right either

                SourcePlayer p = new SourcePlayer(name, score, time);
                players.add(p);
            }
            catch (BufferUnderflowException ignored)
            {
                break;
            }
        }
    }
}
