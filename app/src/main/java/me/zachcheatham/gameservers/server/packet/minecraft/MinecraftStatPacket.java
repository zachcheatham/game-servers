package me.zachcheatham.gameservers.server.packet.minecraft;

public class MinecraftStatPacket extends MinecraftQueryPacket
{
    public MinecraftStatPacket(boolean full, long sessionId, long challenge)
    {
        super(PACKET_TYPE_STAT, sessionId);

        writeUnsignedInt(challenge);
        if (full)
            buffer.put(new byte[]{0x00, 0x00, 0x00, 0x00});
    }
}
