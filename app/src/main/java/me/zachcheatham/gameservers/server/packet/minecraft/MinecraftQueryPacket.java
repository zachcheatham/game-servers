package me.zachcheatham.gameservers.server.packet.minecraft;

import me.zachcheatham.gameservers.server.packet.Packet;

public abstract class MinecraftQueryPacket extends Packet
{
    static final byte PACKET_TYPE_HANDSHAKE = 0x09;
    static final byte PACKET_TYPE_STAT = 0x00;

    final byte packetType;
    private final long sessionId;

    MinecraftQueryPacket(byte packetType, long sessionId)
    {
        super(1400);
        this.packetType = packetType;

        writeByte((byte) 0xFE);
        writeByte((byte) 0xFD);
        writeByte(packetType);
        writeUnsignedInt(sessionId);
        this.sessionId = sessionId;
    }

    MinecraftQueryPacket(byte[] data)
    {
        super(data);
        packetType = readByte();
        sessionId = readUnsignedInt();
    }

    public byte getPacketType()
    {
        return packetType;
    }

    public long getSessionId()
    {
        return sessionId;
    }
}
