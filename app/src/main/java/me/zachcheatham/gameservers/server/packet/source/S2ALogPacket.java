package me.zachcheatham.gameservers.server.packet.source;

import me.zachcheatham.gameservers.server.packet.UnexpectedPacketException;

public class S2ALogPacket extends SourcePacket
{
    public static final byte HEADER = 0x52;

    public final String message;

    public S2ALogPacket(byte[] data) throws UnexpectedPacketException
    {
        super(data);

        if (getHeader() != HEADER)
            throw new UnexpectedPacketException(HEADER, getHeader(), "S2ALogPacket");

        message = readString();
    }
}
