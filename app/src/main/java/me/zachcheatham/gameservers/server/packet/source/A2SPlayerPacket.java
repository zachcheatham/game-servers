package me.zachcheatham.gameservers.server.packet.source;

public class A2SPlayerPacket extends SourcePacket
{
    public static final byte HEADER = 0x55;

    public A2SPlayerPacket(long challenge)
    {
        super(HEADER);
        writeLong(challenge);
    }
}
