package me.zachcheatham.gameservers.server.mod;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.zachcheatham.gameservers.TimeHelper;
import me.zachcheatham.gameservers.server.BaseSourceServer;
import me.zachcheatham.gameservers.server.ChatMessage;
import me.zachcheatham.gameservers.server.player.SourcePlayer;

public class SourceGameMod
{
    private static final Pattern CHAT_PATTERN = Pattern.compile("\"(\\w+)<(\\d+)><([\\w:\\[\\]]+)><(\\w+)>\"\\s+(say|say_team)\\s+\"(.+)\"$");

    public static SourceGameMod detectMod(BaseSourceServer server)
    {
        if (server.getGameAppId() == 440)
            return new TF2GameMod();

        return new SourceGameMod();
    }

    public boolean parseLogLine(String string, BaseSourceServer server)
    {
        // Chat message
        Matcher m = CHAT_PATTERN.matcher(string);
        if (m.find())
        {
            String playerName = m.group(1);
            int playerKey;
            if (playerName.equals("Console"))
                playerKey = -1;
            else
                playerKey = playerName.hashCode();
            String team = m.group(4);
            String method = m.group(5);
            String message = m.group(6);

            SourcePlayer player = (SourcePlayer) server.getPlayer(playerKey);
            // todo player.setTeam(team);

            ChatMessage chatMessage;
            if (method.equals("say"))
                chatMessage = new ChatMessage(message, playerKey, TimeHelper.getUnixTimestamp());
            else
                chatMessage = new ChatMessage(message, playerKey, TimeHelper.getUnixTimestamp(), ChatMessage.MessageType.TEAM, team);
            server.addToChatLog(chatMessage);

            return true;
        }
        return false;
    }
}
