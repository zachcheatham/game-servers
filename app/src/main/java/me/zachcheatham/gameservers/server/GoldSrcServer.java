package me.zachcheatham.gameservers.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.packet.source.A2SGoldSrcRCONRequestPacket;
import me.zachcheatham.gameservers.server.packet.source.S2AGoldSrcInfoPacket;
import me.zachcheatham.gameservers.server.packet.source.S2AGoldSrcRCONResponsePacket;
import me.zachcheatham.gameservers.server.packet.source.S2AInfoPacket;
import me.zachcheatham.gameservers.server.packet.source.S2APlayerPacket;
import me.zachcheatham.gameservers.server.packet.source.S2ARulesPacket;
import me.zachcheatham.gameservers.server.packet.source.SourcePacket;
import me.zachcheatham.gameservers.server.player.Player;
import me.zachcheatham.gameservers.server.player.SourcePlayer;
import me.zachcheatham.gameservers.server.socket.GoldSrcSocket;
import timber.log.Timber;

public class GoldSrcServer extends BaseSourceServer
{
    private long rconChallenge = -1;
    private boolean awaitingRconChallenge = false;
    private boolean testedRconPassword = false;
    private boolean awaitingRconPasswordTest = false;
    private boolean rconBadPassword = false;
    private List<Object[]> pendingChallengeRconCommands = new ArrayList<>();
    private List<Integer> rconIdQueue = new ArrayList<>();

    GoldSrcServer(String hostname, int port)
    {
        super(hostname, port);
    }

    @Override
    public ServerType getType()
    {
        return ServerType.Gold_Src;
    }

    @Override
    protected void query(boolean extended)
    {
        if (socket == null)
            socket = new GoldSrcSocket(address, port, this);

        if (extended)
            doOpenRCONConnection();

        super.query(extended);
    }

    @Override
    void handlePacket(SourcePacket packet)
    {
        switch (packet.getHeader())
        {
        case S2AInfoPacket.HEADER:
        {
            S2AInfoPacket infoResponse = (S2AInfoPacket) packet;
            ping = (short) (System.currentTimeMillis() - pingStart);
            game = infoResponse.game;
            name = infoResponse.hostname.trim();
            map = infoResponse.map;
            numPlayers = infoResponse.numPlayers;
            maxPlayers = infoResponse.maxPlayers;
            botCount = infoResponse.botCount;
            gameDir = infoResponse.gameDirectory;
            hasPassword = infoResponse.password;
            version = infoResponse.version;
            serverType = infoResponse.serverType;
            operatingSystem = infoResponse.operatingSystem;
            vacSecured = infoResponse.isProtected;
            gameAppId = infoResponse.appID;
            break;
        }
        case S2AGoldSrcInfoPacket.HEADER:
        {
            S2AGoldSrcInfoPacket infoResponse = (S2AGoldSrcInfoPacket) packet;
            game = infoResponse.game;
            name = infoResponse.hostname.trim();
            map = infoResponse.map;
            numPlayers = infoResponse.numPlayers;
            maxPlayers = infoResponse.maxPlayers;
            botCount = infoResponse.numBots;
            gameDir = infoResponse.gameDirectory;
            hasPassword = infoResponse.password;
            serverType = infoResponse.serverType;
            operatingSystem = infoResponse.operatingSystem;
            vacSecured = infoResponse.isProtected;
            break;
        }
        case S2ARulesPacket.HEADER:
            ((S2ARulesPacket) packet).getRules(rules);
            rulesVariables = rules.keySet().toArray(new String[0]);
            break;
        case S2APlayerPacket.HEADER:
            List<SourcePlayer> incomingPlayers = ((S2APlayerPacket) packet).players;
            addPlayers((List<Player>) (List<?>) incomingPlayers);
            break;
        case S2AGoldSrcRCONResponsePacket.HEADER:
        {
            if (rconChallenge != -1)
            {
                S2AGoldSrcRCONResponsePacket rconResponsePacket =
                        (S2AGoldSrcRCONResponsePacket) packet;
                if (rconResponsePacket.isBanned || rconResponsePacket.badPassword)
                {
                    rconBadPassword = true;
                    testedRconPassword = false;
                    onRconAuthenticationFailed(rconResponsePacket.isBanned);
                    pendingChallengeRconCommands.clear();
                    rconIdQueue.clear();
                }
                else if (awaitingRconPasswordTest)
                {
                    testedRconPassword = true;
                    awaitingRconPasswordTest = false;
                    for (Object[] c : pendingChallengeRconCommands)
                        sendRconCommand((int) c[0], (String) c[1]);
                    pendingChallengeRconCommands.clear();
                }
                else
                {
                    int requestId = rconIdQueue.get(0);
                    rconIdQueue.remove(0);
                    onRconResponse(requestId, rconResponsePacket.response);

                    Timber.d("Request found %d, Response: %s", requestId,
                            rconResponsePacket.response);
                }
            }
            else
            {
                Timber.w("Got RCON response while we seemingly had no challenge.");
            }
            break;
        }
        case 0: // RCON is GoldSrc is fun, it comes with no header until we send real commands
        {
            String response = new String(packet.getData()).trim();
            if (response.equals("You have been banned from this server."))
            {
                awaitingRconChallenge = false;
                rconBadPassword = true;
                onRconAuthenticationFailed(true);
                pendingChallengeRconCommands.clear();
                rconIdQueue.clear();
            }
            else if (awaitingRconChallenge && response.startsWith("challenge rcon "))
            {
                awaitingRconChallenge = false;
                rconChallenge = Long.valueOf(response.substring(15));
                sendRconCommand(-1, "echo gs_test");
                awaitingRconPasswordTest = true;
                /*onRconAuthenticated();
                 */
            }
            else
            {
                Timber.w("Received unexpected message from %s: %s", getName(), response);
            }
        }
        }
    }

    @Override
    void sendRconCommand(int requestId, String command)
    {
        if (!rconBadPassword)
        {
            if (rconChallenge == -1)
            {
                getRconChallenge();
                pendingChallengeRconCommands.add(new Object[]{requestId, command});
            }
            else
            {
                if (requestId >= 0) // < 0 is used for our internal password test
                    rconIdQueue.add(requestId);

                socket.send(new A2SGoldSrcRCONRequestPacket(
                        String.format(Locale.US, "rcon %s \"%s\" %s",
                                rconChallenge,
                                adminPassword,
                                command)));
            }
        }
    }

    private void getRconChallenge()
    {
        if (rconChallenge == -1 && !awaitingRconChallenge && !rconBadPassword)
        {
            awaitingRconChallenge = true;
            socket.send(new A2SGoldSrcRCONRequestPacket("challenge rcon"));
        }
    }

    @Override
    public int getGameLogoResource()
    {
        switch (gameDir)
        {
        case "cstrike":
            return R.drawable.ic_game_cs16;
        case "tfc":
            return R.drawable.ic_game_tfc;
        case "left4dead":
            return R.drawable.ic_game_l4d;
        default:
            return R.drawable.ic_game_hl2;
        }
    }

    @Override
    public boolean hasBadAdminPassword()
    {
        return rconBadPassword;
    }

    /*
        RCON occurs over the standard query socket in goldsrc, so we're
        really just checking that we have a challenge and the password was right
     */
    @Override
    public boolean isRconConnected()
    {
        return rconChallenge != -1 && !rconBadPassword && testedRconPassword;
    }

    @Override
    protected boolean doOpenRCONConnection()
    {
        if (socket == null)
            socket = new GoldSrcSocket(address, port, this);

        if (hasAdminPassword() && !isRconConnected())
            getRconChallenge();

        return true;
    }

    /*
            RCON occurs over the standard query socket in goldsrc, so we are only going to reset
            our RCON state.
         */
    @Override
    public void closeRconConnection()
    {
        rconChallenge = -1;
        rconBadPassword = false;
        awaitingRconPasswordTest = false;
        testedRconPassword = false;
        awaitingRconChallenge = false;
        pendingChallengeRconCommands.clear();
        rconIdQueue.clear();
    }
}