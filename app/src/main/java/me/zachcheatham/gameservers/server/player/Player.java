package me.zachcheatham.gameservers.server.player;

import androidx.annotation.Nullable;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.player.copy.PlayerCopyMenuPopulator;

public abstract class Player
{
    private final String name;
    protected int timeUpdated;

    Player(String name)
    {
        this.name = name;
        this.timeUpdated = currentTime();
    }

    public String getName()
    {
        return name;
    }

    public abstract String getIPAddress();

    /**
     * Get player's internal server ID
     * @return Server ID
     */
    public abstract int getUserID();

    /**
     * Get player's account identity (such as a Steam ID)
     * @return Unique ID
     */
    public abstract String getUniqueID();

    public int getTimeUpdated()
    {
        return timeUpdated;
    }

    public void setTimeUpdated(int time)
    {
        timeUpdated = time;
    }

    /**
     * @return Information only obtainable by RCON exists for this player
     */
    public abstract boolean hasPrivateInfo();

    @Override
    public int hashCode()
    {
        return name.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object obj)
    {
        if (obj instanceof Player)
        {
            Player player = (Player) obj;
            return name.equals(player.name);
        }
        else
        {
            return false;
        }
    }

    public abstract void merge(Player player, ServerUpdatedEvent event);

    @SuppressWarnings("IntegerDivisionInFloatingPointContext")
    static int currentTime()
    {
        return (int) Math.floor(System.currentTimeMillis() / 1000L);
    }

    public PlayerCopyMenuPopulator getCopyMenuPopulator()
    {
        return new PlayerCopyMenuPopulator(this);
    }
}
