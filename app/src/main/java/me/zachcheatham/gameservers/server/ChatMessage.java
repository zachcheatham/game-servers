package me.zachcheatham.gameservers.server;

public class ChatMessage
{
    public final String message;
    public final int playerKey;
    public final int timestamp;
    public final MessageType type;
    public final String target;

    /**
     * Chat message to hold a generic message
     * @param message Contents of message
     * @param playerKey Key to lookup player object from server
     * @param timestamp Unix timestamp when the message was created
     */
    public ChatMessage(String message, int playerKey, int timestamp)
    {
        this.message = message;
        this.playerKey = playerKey;
        this.timestamp = timestamp;
        this.type = MessageType.NORMAL;
        this.target = null;
    }

    /**
     * Chat message to hold a special message such as admin chat
     * @param message Contents of message
     * @param playerKey Key to lookup player object from server
     * @param timestamp Unix timestamp when the message was created
     * @param type Type of message see {@link MessageType}
     */
    public ChatMessage(String message, int playerKey, int timestamp, MessageType type)
    {
        if (type == MessageType.TEAM || type == MessageType.PM)
            throw new IllegalArgumentException("Must use the team message constructor to specify a team name.");

        this.message = message;
        this.playerKey = playerKey;
        this.timestamp = timestamp;
        this.type = type;
        this.target = null;
    }

    /**
     * Chat message targeted at a team or player
     * @param message Contents of message
     * @param playerKey Key to lookup player object from server
     * @param timestamp Unix timestamp when the message was created
     * @param target Name of the message's target or team
     */
    public ChatMessage(String message, int playerKey, int timestamp, MessageType type, String target)
    {
        this.message = message;
        this.playerKey = playerKey;
        this.timestamp = timestamp;
        this.type = type;
        this.target = target;
    }

    public enum MessageType {
        NORMAL, TEAM, ADMIN, PM
    }
}
