package me.zachcheatham.gameservers.server;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.mod.SourceGameMod;
import me.zachcheatham.gameservers.server.packet.Packet;
import me.zachcheatham.gameservers.server.packet.source.A2SInfoPacket;
import me.zachcheatham.gameservers.server.packet.source.A2SPlayerPacket;
import me.zachcheatham.gameservers.server.packet.source.A2SRulesPacket;
import me.zachcheatham.gameservers.server.packet.source.S2ABannedPacket;
import me.zachcheatham.gameservers.server.packet.source.S2AChallengePacket;
import me.zachcheatham.gameservers.server.packet.source.S2AGoldSrcInfoPacket;
import me.zachcheatham.gameservers.server.packet.source.S2AGoldSrcRCONResponsePacket;
import me.zachcheatham.gameservers.server.packet.source.S2AInfoPacket;
import me.zachcheatham.gameservers.server.packet.source.S2ALogPacket;
import me.zachcheatham.gameservers.server.packet.source.S2APlayerPacket;
import me.zachcheatham.gameservers.server.packet.source.S2ARulesPacket;
import me.zachcheatham.gameservers.server.packet.source.SourcePacket;
import me.zachcheatham.gameservers.server.player.SourcePlayer;
import me.zachcheatham.gameservers.server.socket.BaseSourceSocket;
import me.zachcheatham.gameservers.web.PublicAddress;
import timber.log.Timber;

public abstract class BaseSourceServer extends Server implements BaseSourceSocket.SocketListener
{
    static final int DEFAULT_PORT = 27015;

    private static final int RCON_TAG_PLAYERS = 44;
    private static final int RCON_TAG_COMMAND = 56;
    private static final int RCON_TAG_INTERNAL = 54;
    private static final Pattern PLAYER_REGEX =
            Pattern.compile(
                    "^#\\s+(\\d+)\\s+\"(.+)\"\\s+(\\S+)\\s+(\\S+)?\\s+(\\d+)?\\s+(\\d+)?\\s+" +
                    "(\\S+)?(?:\\s+(\\S+))?$", Pattern.MULTILINE);
    final Map<String, String> rules = new LinkedHashMap<>();
    private final List<byte[]> queuedPackets = new ArrayList<>();
    BaseSourceSocket socket = null;
    long pingStart = -1;
    int botCount;
    String gameDir = "";
    boolean vacSecured;
    int gameAppId;
    boolean hasPassword;
    String version;
    char serverType;
    char operatingSystem;
    String[] rulesVariables = null;
    private int challenge = -1;
    private byte expectingPacket = -1;
    private byte sentPacket = -1;
    private int timeout = 1000;
    private Thread timeoutThread = null;
    private Thread logAddressTimeoutThread = null;
    private int logAddressTimeoutNum = -1;
    private boolean logAddressTimeoutOccurred = false;
    private SourceGameMod mod = null;

    BaseSourceServer(String hostname, int port)
    {
        super(hostname, port);
    }

    @Override
    protected void query(boolean extended)
    {
        sendPacket(A2SInfoPacket.HEADER, S2AInfoPacket.HEADER);

        if (extended)
        {
            sendPacket(A2SPlayerPacket.HEADER, S2APlayerPacket.HEADER);
            sendPacket(A2SRulesPacket.HEADER, S2ARulesPacket.HEADER);

            if (isRconConnected())
            {
                if (!socket.isPunched())
                    requestRCONLogging();
            }
        }
    }

    private void sendPacket(byte header, byte expected)
    {
        if (expectingPacket != -1)
            queuedPackets.add(new byte[]{header, expected});
        else
        {
            expectingPacket = expected;
            sentPacket = header;

            SourcePacket packet;
            switch (header)
            {
            case A2SInfoPacket.HEADER:
                pingStart = System.currentTimeMillis();
                packet = new A2SInfoPacket();
                break;
            case A2SPlayerPacket.HEADER:
                packet = new A2SPlayerPacket(challenge);
                break;
            case A2SRulesPacket.HEADER:
                packet = new A2SRulesPacket(challenge);
                break;
            default:
                throw new IllegalArgumentException(
                        "Asked to send unknown packet " + Packet.getHex(header));
            }

            startTimeout();
            socket.send(packet);
        }
    }

    @Override
    public void packetReceived(SourcePacket packet)
    {
        cancelTimeout();

        if (packet == null)
        {
            sendNextPacket();
        }
        else if (getType() == ServerType.Source && packet.getHeader() == S2ABannedPacket.HEADER)
        {
            Timber.w("We were banned from %s!", getAddressString());
            queryFailed(OfflineReason.Banned);
        }
        else if (packet.getHeader() == S2AChallengePacket.HEADER)
        {
            S2AChallengePacket challengePacket = (S2AChallengePacket) packet;
            challenge = challengePacket.challenge;

            if (sentPacket != -1) // Packet may have reached timeout, don't attempt to resend
            {
                byte sent = sentPacket;
                byte expect = expectingPacket;
                sentPacket = -1;
                expectingPacket = -1;
                sendPacket(sent, expect);
            }
        }
        else if (packet.getHeader() == S2ALogPacket.HEADER)
        {
            S2ALogPacket logPacket = (S2ALogPacket) packet;
            if (logAddressTimeoutNum > -1 &&
                logPacket.message.contains(String.valueOf(logAddressTimeoutNum)))
            {
                if (logAddressTimeoutOccurred)
                    addToRconLog(new LogHolder(LogHolder.LogType.LOG_AVAILABLE,
                            logPacket.message.trim()));

                logAddressTimeoutNum = -1;
                logAddressTimeoutOccurred = false;
                cancelLogAddressTimeout();
            }
            else
            {
                String message = logPacket.message.trim();
                boolean handled = false;
                if (mod != null)
                    handled = mod.parseLogLine(message, this);

                if (!handled && adminMod != null)
                    adminMod.parseLogLine(message, this);

                addToRconLog(new LogHolder(LogHolder.LogType.LOG, message));
            }
        }
        else
        {
            // When we're goldsrc and have received a goldsrc info packet, a normal info packet
            // will arrive shortly after
            if (getType() == ServerType.Gold_Src && expectingPacket == S2AInfoPacket.HEADER &&
                packet.getHeader() == S2AGoldSrcInfoPacket.HEADER)
            {
                startTimeout();
                handlePacket(packet);
            }
            else if (getType() == ServerType.Gold_Src &&
                     (packet.getHeader() == 0 ||
                      packet.getHeader() == S2AGoldSrcRCONResponsePacket.HEADER))
            {
                // We received a GoldSrc RCON related packet, pass it through, ignore
                handlePacket(packet);
            }
            else
            {
                if (packet.getHeader() != expectingPacket)
                    Timber.w("Didn't receive expected packet %s waiting for %s. Got %s instead.",
                            SourcePacket.getPacketName(expectingPacket),
                            SourcePacket.getPacketName(sentPacket),
                            SourcePacket.getPacketName(packet.getHeader()));

                handlePacket(packet);
                sendNextPacket();
            }
        }
    }

    @Override
    public void socketErrorOccurred(Exception e)
    {
        if (expectingPacket != -1) // Make sure we were expecting something when it failed
        {
            closeQueryConnection();
            queryFailed(OfflineReason.Error);
        }
    }

    private void sendNextPacket()
    {
        expectingPacket = -1;
        sentPacket = -1;

        if (queuedPackets.size() > 0)
        {
            byte[] pending = queuedPackets.get(0);
            queuedPackets.remove(0);

            sendPacket(pending[0], pending[1]);
        }
        else
        {
            querySuccessful();
        }
    }

    private void startTimeout()
    {
        if (timeoutThread == null)
        {
            timeoutThread = new Thread(new TimeoutRunnable(timeout, this));
            timeoutThread.start();
        }
    }

    private void startLogAddressTimeout()
    {
        if (logAddressTimeoutThread == null)
        {
            logAddressTimeoutThread = new Thread(new LogAddressTimeoutRunnable(this));
            logAddressTimeoutThread.start();
        }
    }

    private void cancelTimeout()
    {
        if (timeoutThread != null)
            timeoutThread.interrupt();
    }

    private void cancelLogAddressTimeout()
    {
        if (logAddressTimeoutThread != null)
            logAddressTimeoutThread.interrupt();
    }

    abstract void handlePacket(SourcePacket packet);

    @Override
    public void socketClosed(Exception e)
    {
        Timber.w(e, "SourceSocket failed %s (%s)", getAddressString(), getName());
        if (expectingPacket != -1)
        {
            queryFailed(OfflineReason.Generic);
            cancelTimeout();
        }
        else
            closeQueryConnection();
    }

    @Override
    public void closeQueryConnection()
    {
        cancelTimeout();
        cancelLogAddressTimeout();
        logAddressTimeoutNum = -1;
        logAddressTimeoutOccurred = false;
        if (socket != null)
            socket.close();
        socket = null;
        sentPacket = -1;
        expectingPacket = -1;
        queuedPackets.clear();
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    @Override
    public void setExtraOption(String key, Object value) {}

    @Override
    public Object getExtraOption(String key)
    {
        return null;
    }

    public int getBotCount()
    {
        return botCount;
    }

    public int getRealNumPlayers()
    {
        return numPlayers - botCount;
    }

    @Override
    void requestPrivatePlayerInfo()
    {
        if (isRconConnected())
            sendRconCommand(RCON_TAG_PLAYERS, "status");
    }

    public String getVersion()
    {
        return version;
    }

    public boolean isUpToDate()
    {
        // http://api.steampowered.com/ISteamApps/UpToDateCheck/v1?appid=346110&version=858381
        // &format=json TODO
        return true;
    }

    public char getServerType()
    {
        return serverType;
    }

    public boolean hasPassword()
    {
        return hasPassword;
    }

    public String getGameDir()
    {
        return gameDir;
    }

    public char getOperatingSystem()
    {
        return operatingSystem;
    }

    public boolean isVacSecured()
    {
        return vacSecured;
    }

    public int getGameAppId()
    {
        return gameAppId;
    }

    public String getRule(String rule)
    {
        return rules.get(rule);
    }

    public int getRulesCount()
    {
        if (rulesVariables != null)
            return rulesVariables.length;
        else
            return 0;
    }

    public String getRuleName(int i)
    {
        return rulesVariables[i];
    }

    void requestRCONLogging()
    {
        if (socket != null)
        {
            InetSocketAddress localSocket = socket.getLocalAddress();

            if (localSocket != null)
            {
                String externalIP = PublicAddress.getIP();
                if (externalIP != null)
                {
                    sendRconCommand(RCON_TAG_INTERNAL,
                            String.format(Locale.US, "logaddress_del %s:%d",
                                    externalIP, localSocket.getPort()));
                    sendRconCommand(RCON_TAG_INTERNAL,
                            String.format(Locale.US, "logaddress_add %s:%d",
                                    externalIP, localSocket.getPort()));
                    socket.punch(localSocket.getPort());

                    logAddressTimeoutOccurred = false;
                    logAddressTimeoutNum = new Random().nextInt(999999);
                    sendRconCommand(RCON_TAG_INTERNAL,
                            String.format(Locale.US, "echo %d", logAddressTimeoutNum));
                    startLogAddressTimeout();
                }
            }
            else
            {
                update(true);
            }
        }
    }

    @Override
    public boolean supportsLogging()
    {
        return true;
    }

    @Override
    public boolean loggingUnavailable()
    {
        return socket == null || !socket.isPunched() || logAddressTimeoutOccurred;
    }

    abstract void sendRconCommand(int requestId, String command);

    @Override
    public void onRconAuthenticated()
    {
        super.onRconAuthenticated();
        mod = SourceGameMod.detectMod(this);
    }

    public void onRconResponse(int id, String response)
    {
        if (id == RCON_TAG_PLAYERS)
        {
            Matcher matcher = PLAYER_REGEX.matcher(response);
            while (matcher.find())
            {
                SourcePlayer p = (SourcePlayer) findPlayer(matcher.group(2));
                if (p != null)
                {
                    try
                    {
                        p.setUserID(Integer.parseInt(matcher.group(1)));
                    }
                    catch (NumberFormatException ignored) {}
                    p.setSteamID(matcher.group(3));
                    try
                    {
                        p.setPing(Integer.parseInt(matcher.group(5)));
                    }
                    catch (NumberFormatException ignored) {}
                    p.setIPAddress(matcher.group(8));
                }
            }

            ServerUpdatedEvent e = new ServerUpdatedEvent(this);
            e.setPlayerPrivateInfoUpdated(true);
            EventBus.getDefault().post(e);
        }
        else if (id == RCON_TAG_COMMAND)
        {
            addToRconLog(new LogHolder(LogHolder.LogType.COMMAND_RESPONSE, response));
        }
        else if (id >= RCON_TAG_INTERNAL_MIN)
        {
            handleInternalAdminCommand(id, response);
        }
    }

    @Override
    public void doSendUserAdminCommand(String s)
    {
        sendRconCommand(RCON_TAG_COMMAND, s);
    }

    @Override
    protected void doSendInternalAdminCommand(int requestId, String command)
    {
        sendRconCommand(requestId, command);
    }

    @Override
    public boolean supportsChat()
    {
        return true;
    }

    @Override
    public boolean isChatPossible()
    {
        return hasAdminPassword();
    }

    @Override
    void doGetChatTargets(List<ChatTarget> targetList)
    {
        targetList.add(new ChatTarget(ChatMessage.MessageType.NORMAL));
    }

    @Override
    boolean doSendChatMessage(String message, ChatTarget targetChat)
    {
        sendInternalAdminCommand(String.format("say %s", message), null);
        return loggingUnavailable(); // Only add messages to the chat if we have no log
    }

    private static class TimeoutRunnable implements Runnable
    {
        private final WeakReference<BaseSourceServer> serverWeakReference;
        private final int timeout;

        TimeoutRunnable(int timeout, BaseSourceServer server)
        {
            this.timeout = timeout;
            this.serverWeakReference = new WeakReference<>(server);
        }

        @Override
        public void run()
        {
            try
            {
                Thread.sleep(timeout);

                BaseSourceServer server = serverWeakReference.get();
                if (server != null)
                {
                    Timber.w("Receive timeout reached %s (%s) while waiting for packet %s",
                            server.getAddressString(), server.getName(),
                            SourcePacket.getPacketName(server.expectingPacket));
                    if (server.sentPacket == A2SInfoPacket.HEADER)
                        server.queryFailed(OfflineReason.Timeout);
                    else
                        server.sendNextPacket();
                }
            }
            catch (InterruptedException ignored)
            {
            }

            BaseSourceServer server = serverWeakReference.get();
            if (server != null)
                server.timeoutThread = null;
        }
    }

    private static class LogAddressTimeoutRunnable implements Runnable
    {
        private final WeakReference<BaseSourceServer> serverWeakReference;

        LogAddressTimeoutRunnable(BaseSourceServer server)
        {
            this.serverWeakReference = new WeakReference<>(server);
        }

        @Override
        public void run()
        {
            try
            {
                Thread.sleep(10000);

                BaseSourceServer server = serverWeakReference.get();
                if (server != null)
                {
                    Timber.w("Timeout reached waiting for log address to show an echo.");
                    server.logAddressTimeoutOccurred = true;
                    server.addToRconLog(new LogHolder(LogHolder.LogType.LOG_UNAVAILABLE));
                }
            }
            catch (InterruptedException ignored)
            {
            }

            BaseSourceServer server = serverWeakReference.get();
            if (server != null)
                server.logAddressTimeoutThread = null;
        }
    }
}