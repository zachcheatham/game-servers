package me.zachcheatham.gameservers.server.packet.source;

public class A2SInfoPacket extends SourcePacket
{
    public static final byte HEADER = 0x54;
    private static final String PAYLOAD = "Source Engine Query";

    public A2SInfoPacket()
    {
        super(HEADER);
        writeString(PAYLOAD);
    }
}
