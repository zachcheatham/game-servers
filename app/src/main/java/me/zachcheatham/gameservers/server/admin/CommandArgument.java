package me.zachcheatham.gameservers.server.admin;

import com.google.gson.annotations.SerializedName;

public class CommandArgument
{
    public static final String TYPE_LIST = "list";
    public static final String TYPE_BOOL = "bool";
    public static final String TYPE_INTBOOL = "intbool";
    public static final String TYPE_IDBOOL = "idbool";
    public static final String TYPE_TIME_MINUTES = "time-minutes";
    public static final String TYPE_TIME_SECONDS = "time-seconds";
    public static final String TYPE_INT = "int";
    public static final String TYPE_FLOAT = "float";
    public static final String TYPE_STRING = "string";
    public static final String TYPE_MC_PLAYER = "mc-player";
    public static final String TYPE_MC_BLOCK = "mc-block";
    public static final String TYPE_MC_ITEM = "mc-item";
    public static final String TYPE_SM_PLAYER = "sm-player";
    public static final String TYPE_ULX_PLAYER = "ulx-player";
    public static final String TYPE_XYZ = "xyz";
    public static final String TYPE_XZ = "xz";
    public static final String TYPE_STEAMID = "steamid";
    public static final String TYPE_IP = "ip";

    CommandArgument() {}

    CommandArgument(String name, boolean playerArgument, boolean optional, String type, float min, float max)
    {
        this.name = name;
        this.playerArgument = playerArgument;
        this.optional = optional;
        this.type = type;
        this.min = min;
        this.max = max;
    }


    /**
     * Internal ID of argument
     */
    public String id;

    /**
     * Human readable name of argument
     */
    public String name;

    /**
     * Android ResourceID of argument.
     * {@link #name} will be ignored when this is non zero
     */
    public int nameResourceId = 0;

    /**
     * Type of argument. See TYPE_ constants
     */
    public String type;

    /**
     * Determines if a command is optional
     */
    public boolean optional = false;

    /**
     * Enable / Disable prepending space
     */
    @SerializedName("no-space")
    public boolean noSpace = false;

    /**
     * String to prepend before space and argument value
     */
    public String prepend;

    /**
     * Minimum value of ints and floats.
     * If minimum > maximum, no minimum.
     */
    public float min = 0.0f;

    /**
     * Maximum value of ints and floats.
     * if maximum == minimum, no maximum
     */
    public float max = 0.0f;

    /**
     * Options when argument is {@value #TYPE_LIST}
     */
    @SerializedName("options")
    public String[] listOptions;

    /**
     * Set weather argument can be extracted from {@link me.zachcheatham.gameservers.server.player.Player}
     */
    @SerializedName("player")
    public boolean playerArgument = false;

    /**
     * Depend visibility of this argument on specified argument id
     */
    @SerializedName("depend-arg")
    public String dependArg;

    /**
     * Depend visibility of this argument based on specified value of {@link #dependArg}
     */
    @SerializedName("depend-arg-value")
    public String dependArgValue;

    /**
     * Depend visibility of this argument on any of the specified argument ids
     */
    @SerializedName("depend-args")
    public String[] dependArgs;

    /**
     * Depend visibility of this argument based on corresponding values of {@link #dependArgs}
     */
    @SerializedName("depend-args-value")
    public String[] dependArgsValue;

    /**
     * Check if value is in argument's range
     * @return 0 if all is good. Else: 1 if there's only a minimum; 2 if there's only a maximum; 3 if both
     */
    public static int validateMinMax(float value, float min, float max)
    {
        if (min == 0 && max == 0)
            return 0;
        else if (max < min)
             return value < min ? 1 : 0;
        else if (min == max && value > max)
            return 2;
        else
            return value >= min && value <= max ? 0 : 3;
    }
}

