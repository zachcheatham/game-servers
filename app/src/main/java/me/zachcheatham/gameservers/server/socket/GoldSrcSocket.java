package me.zachcheatham.gameservers.server.socket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import me.zachcheatham.gameservers.server.packet.PacketException;
import me.zachcheatham.gameservers.server.packet.source.SourcePacket;

public class GoldSrcSocket extends BaseSourceSocket
{
    public GoldSrcSocket(InetAddress address, int port, SocketListener listener)
    {
        super(address, port, listener);
    }

    public byte[] receivePacket() throws IOException
    {
        byte[] data = new byte[SourcePacket.PACKET_LENGTH];
        DatagramPacket response = new DatagramPacket(data, data.length);
        socket.receive(response);

        ByteBuffer buffer = ByteBuffer.wrap(data);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        long header = buffer.getInt();

        // Not split
        if (header == -1)
        {
            return Arrays.copyOfRange(data, buffer.position(), response.getLength());
        }
        // Split packet
        else if (header == -2)
        {
            // Get info about the split packet
            long packetID = buffer.getInt();
            byte packetNumber = buffer.get();
            int totalPackets = packetNumber & 0xF;
            int packetIndex = packetNumber >> 4;

            byte[][] packets = new byte[totalPackets][];
            packets[packetIndex] = Arrays.copyOfRange(data, buffer.position(), response.getLength());

            int allocatedPackets = 1;
            while (allocatedPackets < totalPackets)
            {
                Arrays.fill(data, (byte) 0x00);
                response = new DatagramPacket(data, data.length);
                socket.receive(response);

                buffer = ByteBuffer.wrap(data);
                buffer.order(ByteOrder.LITTLE_ENDIAN);

                header = buffer.getInt();
                long id = buffer.getInt();
                packetNumber = buffer.get();
                packetIndex = packetNumber >> 4;

                if (header != -2)
                    throw new PacketException(
                            "Unexpected packet while reconstructing split packet.");

                if (id != packetID)
                    throw new PacketException(
                            "Received a different split-packet while reconstructing.");

                packets[packetIndex] = Arrays.copyOfRange(data, buffer.position(), response.getLength());
                allocatedPackets++;
            }

            int completePacketSize = 0;
            for (byte[] packet : packets)
            {
                completePacketSize += packet.length;
            }

            byte[] built = new byte[completePacketSize - 4];
            int i = 0;
            int ignoreHeader = 0;
            for (byte[] packet : packets)
            {
                for (byte b : packet)
                {
                    if (ignoreHeader < 4)
                        ignoreHeader++;
                    else
                    {
                        built[i] = b;
                        i++;
                    }
                }
            }

            return built;
        }

        // If we got to here, we have an issue!
        throw new PacketException("Couldn't tell if a packet was split.");
    }
}
