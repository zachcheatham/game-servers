package me.zachcheatham.gameservers.server.admin;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.List;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.ChatTarget;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.player.Player;

public class SourceServerAdministration extends ServerAdministration
{
    final Server server;

    SourceServerAdministration(Server server)
    {
        this.server = server;
        populateCommands();
    }

    void populateCommands()
    {
        playerCommands.put("banid", new Command("banid", "Ban", "",
                new CommandArgument[]{
                        new CommandArgument("Minutes", false, false, CommandArgument.TYPE_TIME_MINUTES, 0, 0),
                        new CommandArgument("Steam ID", true, false, CommandArgument.TYPE_STEAMID, 0, 0),
                        new CommandArgument("Reason", false, true, CommandArgument.TYPE_STRING, 0,0)
                }));
        playerCommands.put("kickid", new Command("kickid", "Kick", "",
                new CommandArgument[]{
                        new CommandArgument("Steam ID", true, false, CommandArgument.TYPE_STEAMID, 0, 0),
                        new CommandArgument("Reason", false, true, CommandArgument.TYPE_STRING, 0,0)
                }));
        playerCommands.put("addip", new Command("addip", "Ban IP", "",
                new CommandArgument[]{
                        new CommandArgument("IP Address", true, false, CommandArgument.TYPE_IP, 0, 0),
                        new CommandArgument("Reason", false, true, CommandArgument.TYPE_STRING, 0,0)
                }));
    }

    @Override
    public void populateServerMenu(Context context, MenuInflater inflater, Menu menu)
    {
        inflater.inflate(R.menu.server_admin_source, menu);
    }

    @Override
    public void populatePlayerMenu(String subMenu, Context context, List<PlayerMenuItem> items, Player player)
    {
        items.add(new PlayerMenuItem("_steamprofile", context.getString(R.string.action_view_steam_profile, player.getName()), R.drawable.ic_steam));
        items.add(new PlayerMenuItem("kickid", context.getText(R.string.action_kick), R.drawable.ic_close_white));
        items.add(new PlayerMenuItem("banid", context.getText(R.string.action_ban), R.drawable.ic_block_white));
        items.add(new PlayerMenuItem("addip", context.getText(R.string.action_ban_ip), R.drawable.ic_block_white));
    }

    @Override
    public Command getMenuItemCommand(MenuItem menuItem)
    {
        return null;
    }

    @Override
    public boolean sendChatMessage(String message, ChatTarget target, Server server)
    {
        return false;
    }

    @Override
    public ChatTarget[] getChatTargets()
    {
        // We have no admin related chat targets without a mod like SourceMod
        return null;
    }

    @Override
    public boolean handlesChatTarget(ChatTarget target)
    {
        return false;
    }

    @Override
    public Command getKickCommand()
    {
        return playerCommands.get("kickid");
    }

    @Override
    public Command getBanCommand()
    {
        return playerCommands.get("banid");
    }
}
