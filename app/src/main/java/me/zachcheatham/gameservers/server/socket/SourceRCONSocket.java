package me.zachcheatham.gameservers.server.socket;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.zachcheatham.gameservers.server.packet.source.SourceRCONPacket;
import timber.log.Timber;

public class SourceRCONSocket
{
    private static final int AUTH_TAG = 42;
    private static final byte[] SPLIT_PACKET_TERMINATOR = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                                                                     0, 0, 0, 0};
    private final ReconnectRunnable reconnectRunnable =  new ReconnectRunnable(this);
    private final InetAddress address;
    private final int port;
    private final Listener listener;

    private SocketChannel channel;
    private String password;
    private boolean running = false;
    private boolean authenticated = false;
    private boolean badPassword = false;
    private int timeout = 1000;
    private boolean reconnecting = false;

    public SourceRCONSocket(InetAddress address, int port, Listener listener)
    {
        this.address = address;
        this.port = port;
        this.listener = listener;
    }

    public synchronized void connect(String password)
    {
        this.password = password;
        if (!reconnecting && !running)
        {
            running = true;
            new Thread(new SocketRunnable()).start();
        }
    }

    private void listen()
    {
        if (channel == null)
        {
            authenticated = false;
            badPassword = false;

            try
            {
                channel = SocketChannel.open();
                channel.socket().setSoTimeout(timeout);
                channel.connect(new InetSocketAddress(address, port));

                Timber.i("Attempting to authenticate RCON for " + address.getCanonicalHostName() +
                         ":" + port);
                send(new SourceRCONPacket(AUTH_TAG, SourceRCONPacket.PACKET_TYPE_AUTH, password));
                readChannel();
            }
            catch (IOException e)
            {
                Timber.e(e, "Unable to listen to rcon socket.");
                disconnect(true);
            }
            catch (Exception e)
            {
                Timber.e(e, "Fatal rcon error.");
                disconnect(true);
            }
        }
    }

    private void readChannel()
    {
        int bytesRead;
        ByteBuffer incomingBuffer = ByteBuffer.allocate(SourceRCONPacket.PACKET_LENGTH);
        incomingBuffer.order(ByteOrder.LITTLE_ENDIAN);

        int pendingRemainingBytes = -1;
        int totalBytes = -1;
        int readLength;
        byte[] pendingData = new byte[0];
        List<byte[]> splitPacketHolder = new ArrayList<>();

        try
        {
            while (running && channel.isOpen())
            {
                bytesRead = channel.read(incomingBuffer);
                if (bytesRead == -1)
                {
                    channel.close();
                }
                else
                {
                    incomingBuffer.rewind();
                    incomingBuffer.limit(bytesRead);

                    while (incomingBuffer.remaining() > 0)
                    {
                        if (totalBytes == -1)
                        {
                            totalBytes = incomingBuffer.getInt();
                            pendingRemainingBytes = totalBytes;
                            pendingData = new byte[totalBytes];
                        }

                        if (pendingRemainingBytes < incomingBuffer.remaining())
                            readLength = pendingRemainingBytes;
                        else
                            readLength = incomingBuffer.remaining();

                        incomingBuffer
                                .get(pendingData, totalBytes - pendingRemainingBytes, readLength);
                        pendingRemainingBytes -= readLength;

                        if (pendingRemainingBytes <= 0)
                        {
                            // Build a split packet if its a response and we're not authenticating
                            if (isAuthenticated() && pendingData[4] == 0)
                            {
                                // Empty packet is the end of a split packet
                                if (isEmptyPacket(readLength, pendingData))
                                {
                                    if (splitPacketHolder.size() > 0)
                                    {
                                        SourceRCONPacket packet = constructPacket(
                                                splitPacketHolder);
                                        splitPacketHolder.clear();
                                        routePacket(packet);
                                    }
                                }
                                else if (!ignorePacket(pendingData))
                                {
                                    splitPacketHolder.add(pendingData);
                                }
                            }
                            else
                            {
                                // We received a non-response packet while handling a multipacket
                                // . Guess its over
                                if (splitPacketHolder.size() > 0)
                                {
                                    SourceRCONPacket packet = constructPacket(splitPacketHolder);
                                    splitPacketHolder.clear();
                                    routePacket(packet);
                                }

                                routePacket(new SourceRCONPacket(pendingData));
                            }

                            totalBytes = -1;
                            pendingRemainingBytes = -1;
                        }
                    }
                }
                incomingBuffer.clear();
            }
        }
        catch (IOException ignored)
        {

        }

        disconnect(true);
    }

    private void routePacket(SourceRCONPacket packet)
    {
        if (!authenticated)
        {
            if (packet.type == SourceRCONPacket.PACKET_TYPE_AUTH_RESPONSE)
            {
                if (packet.id == -1)
                {
                    Timber.e("RCON authentication failed: Invalid Password.");
                    badPassword = true;
                    listener.onRconAuthenticationFailed(false);
                    disconnect(false);
                }
                else if (packet.id != AUTH_TAG)
                {
                    Timber.e("RCON responded with packet id #" + packet.id +
                             " during authentication.");
                    disconnect(true);
                }
                else
                {
                    Timber.i("RCON Authenticated!");
                    authenticated = true;
                    listener.onRconAuthenticated();
                }
            }
            else if (packet.type != SourceRCONPacket.PACKET_TYPE_RESPONSE_VALUE) // Ignore getting RESPONSE_VALUE
            {
                Timber.e("RCON responded with packet type #" + packet.type +
                         " during authentication.");
                disconnect(true);
            }
        }
        else
        {
            listener.onRconResponse(packet.id, packet.body);
        }
    }

    boolean isEmptyPacket(int readLength, byte[] pendingData)
    {
        return readLength == 10 && pendingData[8] == 0;
    }

    boolean ignorePacket(byte[] pendingData)
    {
        return Arrays.equals(pendingData, SPLIT_PACKET_TERMINATOR);
    }

    private SourceRCONPacket constructPacket(List<byte[]> packets)
    {
        byte[] combined;
        if (packets.size() > 1)
        {
            int totalPacketSize = 0;
            for (byte[] p : packets)
                totalPacketSize += p.length;

            totalPacketSize -= ((packets.size() - 1) *
                                10); // Account for duplicate headers + terminations

            combined = new byte[totalPacketSize];

            int startingIndex;
            int length;
            int currentIndex = 0;
            for (int i = 0; i < packets.size(); i++)
            {
                byte[] p = packets.get(i);
                if (i == 0)
                    startingIndex = 0;
                else
                    startingIndex = 8;

                length = p.length - startingIndex -
                         2; // Discard headers (unless first) and terminations

                System.arraycopy(p, startingIndex, combined, currentIndex, length);
                currentIndex += length;
            }
            combined[combined.length - 1] = 0; // Add body termination
            combined[combined.length - 2] = 0; // Add packet termination
        }
        else
            combined = packets.get(0);

        return new SourceRCONPacket(combined);
    }

    public void send(SourceRCONPacket packet)
    {
        if (channel != null)
        {
            try
            {
                channel.write(packet.getBuffer());
                channel.write(new SourceRCONPacket(0,
                        SourceRCONPacket.PACKET_TYPE_RESPONSE_VALUE, "").getBuffer());
            }
            catch (IOException e)
            {
                disconnect(true);
            }
        }
    }

    public boolean isConnected()
    {
        return (channel != null || running);
    }

    public boolean isAuthenticated()
    {
        return authenticated;
    }

    public boolean badPassword()
    {
        return badPassword;
    }

    public void disconnect()
    {
        disconnect(false);
        password = null;
    }

    private void disconnect(boolean error)
    {
        running = false;
        if (channel != null)
        {
            try
            {
                channel.close();
            }
            catch (IOException ignored)
            {
            }
        }
        authenticated = false;
        channel = null;

        listener.onRconDisconnected();

        if (error && !badPassword)
        {
            new Thread(reconnectRunnable).start();
        }
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
        if (channel != null)
        {
            try
            {
                channel.socket().setSoTimeout(timeout);
            }
            catch (SocketException ignored)
            {
            }
        }
    }

    private class SocketRunnable implements Runnable
    {
        @Override
        public void run()
        {
            SourceRCONSocket.this.listen();
        }
    }

    private static class ReconnectRunnable implements Runnable
    {
        private final WeakReference<SourceRCONSocket> socketReference;

        ReconnectRunnable(SourceRCONSocket socket)
        {
            socket.reconnecting = true;
            socketReference = new WeakReference<>(socket);
        }

        @Override
        public void run()
        {
            try
            {
                Thread.sleep(5000);
            }
            catch (InterruptedException ignored)
            {
            }

            SourceRCONSocket socket = socketReference.get();
            if (socket != null && !socket.isConnected() && socket.password != null)
            {
                socket.reconnecting = false;
                socket.connect(socket.password);
            }
        }
    }

    public interface Listener
    {
        void onRconDisconnected();
        void onRconResponse(int id, String body);
        void onRconAuthenticationFailed(boolean b);
        void onRconAuthenticated();
    }
}
