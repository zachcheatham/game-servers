package me.zachcheatham.gameservers.server.admin;

import android.content.Context;

import java.util.Locale;

import timber.log.Timber;

public class CommandResourceHelper
{
    /**
     * Lookup resource id for command names
     */
    public static int getCommandName(Context context, Command command)
    {
        String cmd = getCommandID(command);

        int id = context.getResources().getIdentifier(
                String.format(Locale.US, "command_name_%s", cmd),
                "string",
                context.getPackageName());

        if (id == 0)
            Timber.w("Unable to find name for command %s", cmd);

        return id;
    }

    public static int getCommandDescription(Context context, Command command)
    {
        String cmd = getCommandID(command);

        int id = context.getResources().getIdentifier(
                String.format(Locale.US, "command_desc_%s", cmd),
                "string",
                context.getPackageName());

        if (id == 0)
            Timber.w("Unable to find description for command %s", cmd);

        return id;
    }

    /**
     * Lookup resource id for command confirm action
     *
     * @param command Target command
     * @return String resource id for command confirm action or <code>0</code> if not found.
     */
    public static int getCommandPositiveAction(Context context, Command command)
    {
        String cmd = getCommandID(command);

        int id = context.getResources().getIdentifier(
                String.format(Locale.US, "command_action_%s", cmd),
                "string",
                context.getPackageName());

        if (id == 0)
            Timber.w("Unable to find confirm action for command %s", cmd);

        return id;
    }

    public static int getArgumentName(Context context, String aid)
    {
        aid = aid.replaceAll("[\\s|-]", "_");

        int id = context.getResources().getIdentifier(
                String.format(Locale.US, "argument_name_%s", aid),
                "string",
                context.getPackageName());

        if (id == 0)
            Timber.w("Unable to find name for argument %s", aid);

        return id;
    }

    public static int getCommandDialogTitle(Context context, Command command)
    {
        String cmd = getCommandID(command);

        return context.getResources().getIdentifier(
                String.format(Locale.US, "command_title_%s", cmd),
                "string",
                context.getPackageName());
    }

    public static int getListOption(Context context, String listOption)
    {
        listOption = listOption.replaceAll("[\\s|-]", "_");

        return context.getResources().getIdentifier(
                String.format(Locale.US, "list_option_%s", listOption),
                "string",
                context.getPackageName());
    }

    private static String getCommandID(Command command)
    {
        String cmd = command.command.replaceAll("[\\s|-]", "_");
        if (command.mod != null)
            cmd = command.mod + "_" + cmd;

        return cmd;
    }
}
