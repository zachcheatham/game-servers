package me.zachcheatham.gameservers.server.socket;

import java.net.InetAddress;

public class MinecraftRCONSocket extends SourceRCONSocket
{
    public MinecraftRCONSocket(InetAddress address, int port,
            Listener listener)
    {
        super(address, port, listener);
    }

    @Override
    boolean isEmptyPacket(int readLength, byte[] pendingData)
    {
        return readLength == 27 && pendingData[8] == 0x55;
    }

    @Override
    boolean ignorePacket(byte[] pendingData)
    {
        return false;
    }
}
