package me.zachcheatham.gameservers.server.packet.source;

class A2SServerQueryGetChallenge extends SourcePacket
{
    public static final byte HEADER = 0x57;

    public A2SServerQueryGetChallenge()
    {
        super(HEADER);
    }
}
