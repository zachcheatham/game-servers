package me.zachcheatham.gameservers.server;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

public class ServerManagerService extends Service
{
    private final Handler handler = new Handler();
    private final IBinder binder = new LocalBinder();
    private final Runnable delayedShutdown = ServerManagerService.this::stopSelf;
    private ServerManager manager;
    private boolean started = false;

    @Override
    public void onCreate()
    {
        super.onCreate();
        manager = new ServerManager(this);
        manager.loadServers();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        manager.saveServers();
        manager.cleanup();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        if (!started)
        {
            started = true;
            startService(new Intent(this, ServerManagerService.class));
        }

        cancelShutdown();
        return binder;
    }

    @Override
    public void onRebind(Intent intent)
    {
        super.onRebind(intent);
        cancelShutdown();
    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        handler.postDelayed(delayedShutdown, 1000);
        return true;
    }

    public ServerManager getManager()
    {
        return manager;
    }

    private void cancelShutdown()
    {
        handler.removeCallbacks(delayedShutdown);
    }

    public class LocalBinder extends Binder
    {
        public ServerManagerService getService()
        {
            return ServerManagerService.this;
        }
    }
}
