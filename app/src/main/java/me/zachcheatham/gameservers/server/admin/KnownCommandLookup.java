package me.zachcheatham.gameservers.server.admin;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class KnownCommandLookup
{
    static final String MOD_IDENTIFIER_ULX = "ulx";
    static final String MOD_IDENTIFIER_SOURCEMOD = "sm";
    static final String MOD_IDENTIFIER_MINECRAFT = "mc";

    final JsonObject commands;
    private final boolean prefixMod;
    private final String modid;
    private final Context context;

    /**
     * @param modid Identifier of Mod you're going to lookup commands for.
     *              (See static MOD_IDENTIFIER variables)
     */
    KnownCommandLookup(Context context, String modid) throws IOException
    {
        this.context = context;
        this.modid = modid;
        InputStream in = context.getAssets().open("known_commands_" + modid + ".json");
        InputStreamReader reader = new InputStreamReader(in);
        commands = new JsonParser().parse(reader).getAsJsonObject();
        prefixMod = needsModPrefix(modid);
    }

    /**
     * Lookup a predefined command by its console representation.
     * @param command Command's console representation (Such as sm_ban)
     * @return Command with arguments or <code>null</code> if not found.
     */
    Command lookupCommand(String command)
    {
        if (commands.has(command))
        {
            Gson gson = new Gson();
            Command cmd = gson.fromJson(commands.get(command), Command.class);
            cmd.command = command;

            if (prefixMod)
                cmd.mod = modid;

            cmd.nameResourceID = CommandResourceHelper.getCommandName(context, cmd);
            cmd.descriptionResourceID = CommandResourceHelper.getCommandDescription(context, cmd);

            if (cmd.arguments != null)
                for (CommandArgument argument : cmd.arguments)
                    argument.nameResourceId = CommandResourceHelper.getArgumentName(context, argument.id);

            if (cmd.iconName != null)
                cmd.iconResourceID = context.getResources().getIdentifier(
                        String.format("ic_%s_white", cmd.iconName),
                        "drawable",
                        context.getPackageName());

            return cmd;
        }

        return null;
    }

    private static boolean needsModPrefix(String modid)
    {
        switch (modid)
        {
        case MOD_IDENTIFIER_MINECRAFT:
            return true;
        default:
            return false;
        }
    }
}
