package me.zachcheatham.gameservers.server.packet;

public class UnexpectedPacketException extends PacketException
{
    private final byte expectedHeader;
    private final byte receivedHeader;
    private final String packetName;

    public UnexpectedPacketException(byte expected, byte received, String packetName)
    {
        super();
        expectedHeader = expected;
        receivedHeader = received;
        this.packetName = packetName;
    }

    private byte getExpectedHeader()
    {
        return expectedHeader;
    }

    private byte getReceivedHeader()
    {
        return receivedHeader;
    }

    private String getPacketName()
    {
        return packetName;
    }

    @Override
    public String getMessage()
    {
        return byteToHex(getReceivedHeader()) + " received for " + getPacketName() + " (" +
               byteToHex(
                       getExpectedHeader()) + ")";
    }

    private String byteToHex(byte b)
    {
        return String.format("0x%02x", b & 0xFF);
    }
}
