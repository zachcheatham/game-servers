package me.zachcheatham.gameservers.server.player;

public abstract class AvatarHelper
{
    public abstract void loadAvatars(Player[] players,
            AvatarLoadListener listener);
    public abstract boolean hasAvatar(Player player);
    public abstract String getAvatar(Player player);

    public interface AvatarLoadListener{
        void avatarsLoaded();
    }
}
