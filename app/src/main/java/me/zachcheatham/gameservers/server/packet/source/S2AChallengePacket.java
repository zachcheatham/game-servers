package me.zachcheatham.gameservers.server.packet.source;

import me.zachcheatham.gameservers.server.packet.UnexpectedPacketException;

public class S2AChallengePacket extends SourcePacket
{
    public static final byte HEADER = 0x41;

    public final int challenge;

    public S2AChallengePacket(byte[] data) throws UnexpectedPacketException
    {
        super(data);

        if (getHeader() != HEADER)
            throw new UnexpectedPacketException(HEADER, getHeader(), "S2AChallenge");

        challenge = readInt();
    }
}
