package me.zachcheatham.gameservers.server;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.SparseArray;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.TimeHelper;
import me.zachcheatham.gameservers.events.ChatAddedEvent;
import me.zachcheatham.gameservers.events.ChatTrimEvent;
import me.zachcheatham.gameservers.events.RconLogAddedEvent;
import me.zachcheatham.gameservers.events.RconLogTrimEvent;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.admin.Command;
import me.zachcheatham.gameservers.server.admin.CommandArgument;
import me.zachcheatham.gameservers.server.admin.ServerAdminModDetector;
import me.zachcheatham.gameservers.server.admin.ServerAdministration;
import me.zachcheatham.gameservers.server.player.Player;
import timber.log.Timber;

public abstract class Server
{
    public static final int FLAG_BACKGROUND_UPDATE = 1;
    public static final int FLAG_NOTIFY_ADMIN_CHAT = 2;
    public static final int FLAG_NOTIFY_ALL_CHAT = 4;
    public static final int FLAG_NOTIFY_EMPTY = 8;
    public static final int FLAG_NOTIFY_PLAYERS = 16;
    public static final int FLAG_NOTIFY_FULL = 32;

    static final int RCON_TAG_INTERNAL_MIN = 100;

    final String hostname;
    final int port;
    InetAddress address;
    String adminPassword = null;
    private int flags = 0;
    boolean offline = false;
    private int offlineCount = 0;
    private OfflineReason offlineReason = OfflineReason.Generic;
    String name = "";
    String map = "";
    int maxPlayers = 0;
    short ping = -1;
    String game = "";
    int numPlayers = 0;
    SparseArray<Player> players = new SparseArray<>();
    private SparseArray<RCONResponseCallback> pendingRCONCallbacks = new SparseArray<>();
    ServerAdministration adminMod = null;
    private boolean updating = false;
    private boolean updatingExtended = false;
    private ServerUpdatedEvent event = null;
    private List<LogHolder> rconLog; // List is initialized when needed
    private List<ChatMessage> chatLog; // List is initialized when needed
    private int logSize = 100;
    private int chatLogSize = 100;

    public Server(String hostname, int port)
    {
        this.hostname = hostname;
        this.port = port;
    }

    public static Server create(String hostname, int port, ServerType type)
    {
        switch (type)
        {
        case Source:
            return new SourceServer(hostname, port);
        case Gold_Src:
            return new GoldSrcServer(hostname, port);
        default:
            return new MinecraftServer(hostname, port);
        }
    }

    static Server createFromJSON(JSONObject serverJSON) throws JSONException
    {
        Server server = Server.create(serverJSON.getString("host"), serverJSON.getInt("port"),
                getServerType(serverJSON.getString("type")));
        server.load(serverJSON);
        return server;
    }

    private static String getTypeString(ServerType type)
    {
        switch (type)
        {
        default:
        case Source:
            return "hl2";
        case Gold_Src:
            return "goldsrc";
        case Minecraft:
            return "mc";
        }
    }

    private static ServerType getServerType(String type)
    {
        switch (type)
        {
        default:
        case "hl2":
            return ServerType.Source;
        case "goldsrc":
            return ServerType.Gold_Src;
        case "mc":
            return ServerType.Minecraft;
        }
    }

    public synchronized void update(boolean extended)
    {
        if (!updating)
        {
            Timber.i("Querying <" + getType() + "> " + getAddressString() + " (" + getName() + ")");

            updating = true;
            updatingExtended = extended;
            event = new ServerUpdatedEvent(this);

            new ServerQueryTask(this, extended).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    protected abstract void query(boolean extended);

    void queryFailed(OfflineReason reason)
    {
        offline = true;

        if (offlineReason != reason)
            offlineCount = 0;
        offlineReason = reason;

        if (offlineCount < Constants.OFFLINE_COUNT_THRESHOLD)
            offlineCount++;

        closeQueryConnection();
        queryComplete();

        Timber.w("Error during query %s (%s)", getAddressString(), getName());
    }

    void querySuccessful()
    {
        offline = false;
        offlineCount = 0;

        queryComplete();
    }

    private void queryComplete()
    {
        event.detectChanges();
        updating = false;
        EventBus.getDefault().post(event);

        Timber.i("Query Completed <%s> %s (%s)", getType(), getAddressString(), getName());
    }

    InetAddress getAddress()
    {
        return address;
    }

    public abstract ServerType getType();

    private int getPort()
    {
        return port;
    }

    public String getAddressString()
    {
        return hostname + ":" + getPort();
    }

    boolean lookupAddress()
    {
        try
        {
            address = InetAddress.getByName(hostname);
            return true;
        }
        catch (UnknownHostException e)
        {
            Timber.w(e, "Host resolution failed for %s", getName());
            return false;
        }
    }

    public boolean hasFlag(int flag)
    {
        return ((flags & flag) == flag);
    }

    public void setFlag(int flag, boolean active)
    {
        if (active)
            flags |= flag;
        else
            flags &= ~flag;
    }

    public abstract void setExtraOption(String key, Object value);

    public abstract Object getExtraOption(String key);

    protected abstract void setTimeout(int timeout);

    public boolean isOffline()
    {
        return offline;
    }

    public boolean hasReachedOfflineSortThreshold()
    {
        return offlineCount >= Constants.OFFLINE_COUNT_THRESHOLD;
    }

    public OfflineReason getOfflineReason()
    {
        return offlineReason;
    }

    boolean isUpdating()
    {
        return updating;
    }

    public boolean isUpdatingExtended()
    {
        return updating && updatingExtended;
    }

    public short getPing()
    {
        return ping;
    }

    public String getGame()
    {
        return game;
    }

    public String getName()
    {
        return name;
    }

    public String getMap()
    {
        return map;
    }

    public int getNumPlayers()
    {
        return numPlayers;
    }

    public abstract int getRealNumPlayers();

    public int getMaxPlayers()
    {
        return maxPlayers;
    }

    public int getPlayersListSize()
    {
        return players.size();
    }

    public int getGameLogoResource()
    {
        return 0;
    }

    public Bitmap getGameLogoBitmap()
    {
        return null;
    }

    public Player getPlayer(int key)
    {
        return players.get(key);
    }

    void addPlayers(List<Player> incomingPlayers)
    {
        final int updateTime = (int) Math.floor(System.currentTimeMillis() / 1000l);

        boolean needPrivateInfo = false;

        for (Player p : incomingPlayers)
        {
            Player existingPlayer = players.get(p.hashCode());
            if (existingPlayer != null)
            {
                existingPlayer.merge(p, event);
                existingPlayer.setTimeUpdated(updateTime);
                if (!existingPlayer.hasPrivateInfo())
                    needPrivateInfo = true;
            }
            else
            {
                // New player we've never seen before
                event.setPlayerListChanged(true);
                players.put(p.hashCode(), p);
                if (!p.hasPrivateInfo())
                    needPrivateInfo = true;
            }
        }

        List<Integer> toDelete = new ArrayList<>();
        for (int i = 0; i < players.size(); i++)
        {
            Player p = players.valueAt(i);
            if (p.getTimeUpdated() != updateTime)
            {
                int key = players.keyAt(i);
                toDelete.add(key);
                event.setPlayerListChanged(true);
            }
        }
        if (toDelete.size() > 0)
        {
            for (int key : toDelete)
                players.remove(key);
        }

        if (needPrivateInfo)
            requestPrivatePlayerInfo();
    }

    Player findPlayer(String name)
    {
        return players.get(name.hashCode());
    }

    public int getPlayerKeyAt(int index)
    {
        return players.keyAt(index);
    }

    public void setAdminPassword(String password)
    {
        if (password != null && password.length() > 0)
            adminPassword = password;
        else
            adminPassword = null;

        closeRconConnection();
    }

    public boolean hasAdminPassword()
    {
        return adminPassword != null;
    }

    public abstract boolean hasBadAdminPassword();

    public abstract boolean isRconConnected();

    /**
     * Request player information only available by RCON
     */
    abstract void requestPrivatePlayerInfo();

    public void onRconAuthenticated()
    {
        addToRconLog(new LogHolder(LogHolder.LogType.AUTHENTICATED));
        new ServerAdminModDetector(this).start();
    }

    public void onRconAuthenticationFailed(boolean banned)
    {
        if (banned)
        {
            addToRconLog(new LogHolder(LogHolder.LogType.BANNED));
            Timber.i("Banned from connecting to RCON on %s", getName());
        }
        else
        {
            addToRconLog(new LogHolder(LogHolder.LogType.INVALID_PASSWORD));
            Timber.i("Invalid RCON password for %s", getName());
        }
    }

    public void onRconDisconnected()
    {
        pendingRCONCallbacks.clear();
        this.adminMod = null;
    }

    public abstract boolean supportsLogging();
    public abstract boolean loggingUnavailable();

    public List<LogHolder> getRconLog()
    {
        return rconLog;
    }

    void addToRconLog(LogHolder logHolder)
    {
        // Initialize the log
        if (rconLog == null)
            rconLog = new ArrayList<>();

        int excess = (rconLog.size() + 1) - logSize;
        if (excess > 0)
        {
            rconLog.subList(0, excess).clear();
            EventBus.getDefault().post(new RconLogTrimEvent(this, excess));
        }

        rconLog.add(logHolder);
        EventBus.getDefault().post(new RconLogAddedEvent(this, logHolder));
    }

    void setMaxLogSize(int size)
    {
        chatLogSize = size;
    }

    /**
     * Check weather the server supports sending and receiving chat messages
     **/
    public abstract boolean supportsChat();

    /**
     * Check if the connection to the server's chat is established
     **/
    public abstract boolean isChatPossible();

    /**
     * Used to determine which chats we can send messages to on this server
     *
     * @return List of unique chat targets
     */
    public ChatTarget[] getChatTargets()
    {
        List<ChatTarget> chatTargets = new ArrayList<>();

        // Ask the server about its targets
        doGetChatTargets(chatTargets);

        // Ask the admin mod about its targets
        if (adminMod != null)
        {
            ChatTarget[] adminChatTargets = adminMod.getChatTargets();
            if (adminChatTargets != null)
                chatTargets.addAll(Arrays.asList(adminChatTargets));
        }

        return chatTargets.toArray(new ChatTarget[0]);
    }

    abstract void doGetChatTargets(List<ChatTarget> targetList);

    /**
     * Send a chat message to the server
     *
     * @param message    Message contents
     * @param targetChat Chat to send the message to {@link #getChatTargets()}
     */
    public void sendChatMessage(String message, ChatTarget targetChat)
    {
        boolean addToLog;

        // Check if the admin mod will handle the target
        if (adminMod != null && adminMod.handlesChatTarget(targetChat))
            addToLog = adminMod.sendChatMessage(message, targetChat, this);
        // Otherwise, forward it on to the server
        else
            addToLog = doSendChatMessage(message, targetChat);

        if (addToLog)
        {
            // todo tag with target chat
            ChatMessage msg = new ChatMessage(message, -1, TimeHelper.getUnixTimestamp());
            addToChatLog(msg);
        }
    }

    abstract boolean doSendChatMessage(String message, ChatTarget targetChat);

    /**
     * Add a message to the server's chat log
     *
     * @param message ChatMessage object
     */
    public void addToChatLog(ChatMessage message)
    {
        if (chatLog == null)
            chatLog = new ArrayList<>();

        int excess = (chatLog.size() + 1) - chatLogSize;
        if (excess > 0)
        {
            chatLog.subList(0, excess).clear();
            EventBus.getDefault().post(new ChatTrimEvent(this, excess));
        }

        chatLog.add(message);
        EventBus.getDefault().post(new ChatAddedEvent(this, message));
    }

    public List<ChatMessage> getChatLog()
    {
        return chatLog;
    }

    /**
     * Used by server manager to update our log size
     * @param size log size
     */
    void setMaxChatLogSize(int size)
    {
        logSize = size;
    }

    public ServerAdministration getAdminMod()
    {
        return adminMod;
    }

    public void setAdminMod(ServerAdministration serverAdministration)
    {
        this.adminMod = serverAdministration;
    }

    public void sendUserAdminCommand(String command)
    {
        if (Thread.currentThread() == Looper.getMainLooper().getThread())
        {
            new SendRconCommandTask(this, command).execute();
        }
        else
        {
            addToRconLog(new LogHolder(LogHolder.LogType.COMMAND, command));
            doSendUserAdminCommand(command);
        }
    }

    public void sendInternalAdminCommand(String s, RCONResponseCallback callback)
    {
        Random rnd = new Random();
        int requestId = -1;
        while (requestId < 0 || pendingRCONCallbacks.get(requestId) != null)
            requestId = rnd.nextInt(999999) + RCON_TAG_INTERNAL_MIN;

        if (Thread.currentThread() == Looper.getMainLooper().getThread())
        {
            new SendInternalRconCommandTask(this, s, requestId).execute();
        }
        else
        {
            if (isRconConnected())
            {
                pendingRCONCallbacks.put(requestId, callback);
                doSendInternalAdminCommand(requestId, s);
            }
        }
    }

    public void sendInternalAdminCommand(Command command, String[] values, RCONResponseCallback callback)
    {
        StringBuilder builder = new StringBuilder(command.command);
        for (int i = 0; i < command.arguments.length; i++)
        {
            if (values[i] != null && !values[i].isEmpty())
            {
                CommandArgument argument = command.arguments[i];
                if (!argument.noSpace)
                    builder.append(" ");
                if (argument.prepend != null)
                    builder.append(argument.prepend);

                builder.append(values[i]);
            }
        }

        sendInternalAdminCommand(builder.toString(), callback);
    }

    abstract void doSendUserAdminCommand(String s);

    abstract void doSendInternalAdminCommand(int requestId, String command);

    void handleInternalAdminCommand(int requestId, String response)
    {
        RCONResponseCallback callback = pendingRCONCallbacks.get(requestId);
        if (callback != null)
            callback.onResponse(response);
    }

    void cleanup()
    {
        closeAllConnections();
    }

    public void closeAllConnections()
    {
        closeQueryConnection();
        closeRconConnection();
    }

    public abstract void closeQueryConnection();

    public boolean openRCONConnection()
    {
        // Ensure we have an ip
        if (address == null)
            if (!lookupAddress())
                return false;

        return doOpenRCONConnection();
    }

    protected abstract boolean doOpenRCONConnection();

    public abstract void closeRconConnection();

    void save(JSONObject serverJSON) throws JSONException
    {
        serverJSON.put("type", getTypeString(getType()));
        serverJSON.put("host", hostname);
        serverJSON.put("port", port);
        serverJSON.put("flags", flags);

        if (name != null && !name.isEmpty())
            serverJSON.put("name", getName());

        if (getRealNumPlayers() > 0)
            serverJSON.put("players", getRealNumPlayers());

        if (getMaxPlayers() > 0)
            serverJSON.put("max_players", getMaxPlayers());

        if (hasAdminPassword())
            serverJSON.put("admin_password", adminPassword);
    }

    /**
     * Override to get extra values from the save file
     * REMEMBER IF YOU ADD ONE TO THIS, CHECK IF ITS IN THE JSON FIRST
     *  OR YOU'LL ERASE THE USER'S SERVER!!!!
     * @param serverJSON The saved JSON representation of the server.
     */
    void load(JSONObject serverJSON) throws JSONException
    {
        if (serverJSON.has("name"))
            name = serverJSON.getString("name");

        if (serverJSON.has("players"))
            numPlayers = serverJSON.getInt("players");

        if (serverJSON.has("max_players"))
            maxPlayers = serverJSON.getInt("max_players");

        if (serverJSON.has("admin_password"))
            adminPassword = serverJSON.getString("admin_password");

        if (serverJSON.has("flags"))
            flags = serverJSON.getInt("flags");
    }

    public int getOfflineCount()
    {
        return offlineCount;
    }

    public Player[] getPlayers()
    {
        Player[] a = new Player[players.size()];
        for (int i = 0; i < players.size(); i++)
            a[i] = players.get(players.keyAt(i));
        return a;
    }

    public static int getDefaultPort(Server.ServerType type)
    {
        switch (type)
        {
        case Gold_Src:
        case Source:
        default:
            return BaseSourceServer.DEFAULT_PORT;
        case Minecraft:
            return MinecraftServer.DEFAULT_PORT;
        }
    }

    public enum ServerType
    {
        Source, Gold_Src, Minecraft
    }

    public enum OfflineReason
    {
        Unknown_Host, Timeout, Banned, Error, Generic
    }

    public interface RCONResponseCallback
    {
        void onResponse(String response);
    }

    private static class ServerQueryTask extends AsyncTask<Void, Void, Void>
    {
        private final WeakReference<Server> serverReference;
        private final boolean extended;

        ServerQueryTask(Server server, boolean extended)
        {
            serverReference = new WeakReference<>(server);
            this.extended = extended;
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            final Server server = serverReference.get();
            if (server != null)
            {
                try
                {
                    if (server.address == null)
                        if (server.lookupAddress())
                            server.query(extended);
                        else
                        {
                            server.queryFailed(OfflineReason.Unknown_Host);
                        }
                    else
                        server.query(extended);
                }
                catch (Exception e)
                {
                    server.queryFailed(OfflineReason.Error);
                    Timber.e(e, "Error during server %s query", server.getName());
                }
            }

            return null;
        }
    }

    private static class SendRconCommandTask extends AsyncTask<Void, Void, Void>
    {
        private final WeakReference<Server> reference;
        private final String command;

        SendRconCommandTask(Server server, String command)
        {
            reference = new WeakReference<>(server);
            this.command = command;
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Server server = reference.get();
            if (server != null)
            {
                server.addToRconLog(new LogHolder(LogHolder.LogType.COMMAND, command));
                server.doSendUserAdminCommand(command);
            }
            return null;
        }
    }

    private static class SendInternalRconCommandTask extends AsyncTask<Void, Void, Void>
    {
        private final WeakReference<Server> reference;
        private final String command;
        private final int requestId;

        SendInternalRconCommandTask(Server server, String command, int requestId)
        {
            reference = new WeakReference<>(server);
            this.command = command;
            this.requestId = requestId;
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Server server = reference.get();
            if (server != null)
                server.doSendInternalAdminCommand(requestId, command);
            return null;
        }
    }
}