package me.zachcheatham.gameservers.server.admin;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.Map;

public class Command
{
    public Command() {}

    public Command(String command, String name, String description, CommandArgument[] arguments)
    {
        this.command = command;
        this.name = name;
        this.description = description;
        this.arguments = arguments;
    }

    /**
     * The command's name to the console and internal ID
     */
    public String command;

    /**
     * Mod identifier for resources
     */
    public String mod;

    /**
     * Human readable name of command
     */
    public String name;

    /**
     * Description of command
     */
    public String description;

    /**
     * Android resource ID of command name
     * {@link #name} will be ignored when this is non-zero
     */
    public int nameResourceID = 0;

    /**
     * Android resource ID of command description
     * {@link #description} will be ignored when this is non-zero
     */
    public int descriptionResourceID = 0;

    /**
     * Array of arguments used with this command
     */
    @JsonAdapter(ArgumentDeserializer.class)
    @SerializedName("args")
    public CommandArgument[] arguments;

    /**
     * Android resource name of icon
     * Usually comes from the known commands database
     */
    @SerializedName("icon")
    public String iconName;

    /**
     * Android resource ID of icon
     * Determined using {@link #iconName}
     */
    public int iconResourceID = 0;

    /**
     * When <code>true</code>, prompts user before executing command.
     */
    public boolean destructive = false;

    boolean hasPlayerArgument()
    {
        if (arguments != null)
        {
            for (CommandArgument arg : arguments)
            {
                if (arg.playerArgument)
                    return true;
            }
        }

        return false;
    }

    public boolean hasNonPlayerArgument()
    {
        if (arguments != null)
        {
            for (CommandArgument arg : arguments)
            {
                if (!arg.playerArgument)
                    return true;
            }
        }

        return false;
    }

    private static class ArgumentDeserializer implements JsonDeserializer<CommandArgument[]>
    {
        @Override
        public CommandArgument[] deserialize(JsonElement json, Type typeOfT,
                JsonDeserializationContext context) throws JsonParseException
        {
            JsonObject obj = json.getAsJsonObject();
            CommandArgument[] args = new CommandArgument[obj.size()];

            int i = 0;
            for (Map.Entry<String, JsonElement> entry : obj.entrySet())
            {
                CommandArgument arg = context.deserialize(entry.getValue(), CommandArgument.class);
                arg.id = entry.getKey();
                args[i++] = arg;
            }

            return args;
        }
    }
}
