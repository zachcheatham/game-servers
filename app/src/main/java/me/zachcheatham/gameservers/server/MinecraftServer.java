package me.zachcheatham.gameservers.server;

import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.packet.minecraft.MinecraftBasicStatResponsePacket;
import me.zachcheatham.gameservers.server.packet.minecraft.MinecraftFullStatResponsePacket;
import me.zachcheatham.gameservers.server.packet.minecraft.MinecraftQueryHandshakePacket;
import me.zachcheatham.gameservers.server.packet.minecraft.MinecraftRequestPacket;
import me.zachcheatham.gameservers.server.packet.minecraft.MinecraftStatPacket;
import me.zachcheatham.gameservers.server.packet.minecraft.MinecraftStatusPacket;
import me.zachcheatham.gameservers.server.packet.source.SourceRCONPacket;
import me.zachcheatham.gameservers.server.player.MinecraftPlayer;
import me.zachcheatham.gameservers.server.player.Player;
import me.zachcheatham.gameservers.server.socket.MinecraftClientSocket;
import me.zachcheatham.gameservers.server.socket.MinecraftQuerySocket;
import me.zachcheatham.gameservers.server.socket.MinecraftRCONSocket;
import me.zachcheatham.gameservers.server.socket.SourceRCONSocket;
import me.zachcheatham.gameservers.web.MojangUniqueIDLookup;
import timber.log.Timber;

public class MinecraftServer extends Server implements SourceRCONSocket.Listener
{
    public static final short DEFAULT_PORT = 25565;
    public static final short DEFAULT_RCON_PORT = 25575;
    private static final String EXTRA_OPTION_RCON_PORT = "rcon_port";
    private static final int RCON_TAG_COMMAND = 56;

    private int rconPort;
    private MinecraftClientSocket clientSocket = null;
    private MinecraftQuerySocket querySocket = null;
    private SourceRCONSocket rconSocket = null;
    private QueryMode queryMode = QueryMode.UNKNOWN;
    private long queryChallenge = -1;
    private boolean adjustedClientSocket = false;
    private boolean initialQuery = true;

    private Bitmap favicon = null;
    private String gameType = "";
    private String version = "";
    private String[][] modList = null;
    private boolean rconDidConnect = false;

    MinecraftServer(String hostname, int port)
    {
        super(hostname, port);
        rconPort = DEFAULT_RCON_PORT;
    }

    @Override
    protected void query(boolean extended)
    {
        if (clientSocket == null)
            clientSocket = new MinecraftClientSocket(address, port, hostname);
        if (querySocket == null)
            querySocket = new MinecraftQuerySocket(address, port);

        if (extended)
            doOpenRCONConnection();

        // Attempt to query via query protocol if we haven't determined we can only use
        // client protocol.
        if (queryMode != QueryMode.CLIENT)
        {
            try
            {
                // Keep track of subsequent requests for when our challenge number expires
                boolean firstAttempt = true;
                while (true) // Loop for sending until our challenge is accepted (this should only run twice max)
                {
                    try
                    {
                        // If we haven't requested a challenge yet, do it
                        if (queryChallenge == -1)
                            queryChallenge = getQueryChallenge();

                        long pingStart = System.currentTimeMillis(); // Set the start of our ping measurement
                        querySocket.send(new MinecraftStatPacket(extended, 1, queryChallenge));
                        if (extended)
                        {
                            // Parse an extended query
                            MinecraftFullStatResponsePacket stats =
                                    new MinecraftFullStatResponsePacket(
                                            querySocket.receivePacket());
                            queryMode = QueryMode.BOTH; // We now know query protocol works
                            ping = (short) (System.currentTimeMillis() - pingStart); // end the ping measurement
                            offline = false;
                            gameType = stats.getGameType();
                            map = stats.getMap();
                            numPlayers = stats.getNumPlayers();

                            // Parse this information if this is the first time we've queried the server
                            // If a client query runs, this information seems to be less accurate.
                            if (initialQuery)
                            {
                                name = stats.getMOTD();
                                maxPlayers = stats.getMaxPlayers();
                            }

                            addPlayers((List<Player>) (List<?>) stats.getPlayers());

                            // Recreate the client protocol socket if this response includes
                            // the host/port clients are supposed to use
                            if (!adjustedClientSocket)
                                adjustClientSocket(stats.getHostIP(), stats.getHostPort());
                        }
                        else
                        {
                            MinecraftBasicStatResponsePacket stats =
                                    new MinecraftBasicStatResponsePacket(
                                            querySocket.receivePacket());

                            queryMode = QueryMode.BOTH;
                            ping = (short) (System.currentTimeMillis() - pingStart);
                            offline = false;
                            gameType = stats.getGameType();
                            map = stats.getMap();
                            numPlayers = stats.getNumPlayers();

                            // Parse this information if this is the first time we've queried the server
                            // If a client query runs, this information seems to be less accurate.
                            if (initialQuery)
                            {
                                name = stats.getMOTD();
                                maxPlayers = stats.getMaxPlayers();
                            }

                            // Recreate the client protocol socket if this response includes
                            // the host/port clients are supposed to use
                            if (!adjustedClientSocket)
                                adjustClientSocket(stats.getHostIP(), stats.getHostPort());
                        }

                        break; // Everything worked out, lets break out of the loop
                    }
                    catch (SocketTimeoutException e)
                    {
                        // if first attempt and we have a challenge, we need to start over and try again.
                        if (firstAttempt && queryChallenge != -1)
                        {
                            firstAttempt = false;
                            queryChallenge = -1;
                            Timber.i(
                                    "Server query timeout for %s. Attempting new challenge number.",
                                    getName());
                        }
                        // We tried to get a new challenge and the server didn't respond.
                        // Time to call it quits for the query protocol
                        else
                        {
                            if (queryChallenge == -1)
                                Timber.w("Unable to get challenge for %s", getName());

                            throw e; // escalate our problems to the outer catch
                        }
                    }
                }
            }
            catch (IOException e)
            {
                Timber.w(e, "Minecraft query protocol did not work for %s.", getName());

                // The server didn't respond, and we don't need to figure out
                // if we can do a client query anymore.
                if (queryMode != QueryMode.UNKNOWN)
                {
                    initialQuery = true; // Let future queries know it can start over
                    queryFailed(e instanceof SocketTimeoutException ? OfflineReason.Timeout : OfflineReason.Generic);
                    return;
                }

                // Otherwise, this method is going to continue and attempt a client query
            }
        }

        // Only run this is our only query option, we haven't queried before, or we're doing
        // an extended query.
        if (queryMode != QueryMode.BOTH || initialQuery || extended)
        {
            boolean firstAttempt = true;
            /*
            Loop in case the server closes the connection on our first attempt.
            This happens when we've connected before and we can't tell the connection was
            closed until we receivePacket()
             */
            while (true)
            {
                try
                {
                    long pingStart = 0;
                    // Only do another ping measurement if a query protocol query didn't happen
                    if (queryMode != QueryMode.BOTH)
                        pingStart = System.currentTimeMillis();

                    clientSocket.send(new MinecraftRequestPacket());
                    byte[] data = clientSocket.receivePacket();
                    if (data != null)
                    {
                        MinecraftStatusPacket status = new MinecraftStatusPacket(data);

                        if (queryMode == QueryMode.UNKNOWN)
                            queryMode = QueryMode.CLIENT;

                        if (queryMode == QueryMode.CLIENT)
                            ping = (short) (System.currentTimeMillis() - pingStart);

                        numPlayers = status.getOnlinePlayers();
                        maxPlayers = status.getMaxPlayers();
                        version = status.getVersion();

                        if (queryMode == QueryMode.BOTH)
                        {
                            String n = status.getDescription();
                            if (n != null && n.length() > 0)
                                name = n;
                        }
                        else
                        {
                            name = status.getDescription();
                        }

                        if (favicon == null)
                            favicon = status.getFavicon();

                        if (numPlayers > 0 && queryMode == QueryMode.CLIENT)
                        {
                            List<MinecraftPlayer> p = status.getPlayers();
                            if (p != null)
                            {
                                addPlayers((List<Player>) (List<?>) p);
                            }
                        }

                        modList = status.getInstalledMods();

                        initialQuery = false;
                        break;
                    }
                    else if (!firstAttempt) // Server returned null and we tried again. Time to give up.
                    {
                        Timber.w("Minecraft client protocol did not work for %s", getName());
                        queryFailed(OfflineReason.Generic);
                        return;
                    }
                    else
                    {
                        // Let's try one more time
                        firstAttempt = false;
                    }
                }
                catch (IOException e)
                {
                    Timber.w(e, "Minecraft client protocol did not work for %s", getName());

                    clientSocket.close();

                    // Only decide that the server is offline if we know query protocol didn't run
                    if (queryMode != QueryMode.BOTH)
                    {
                        queryFailed(e instanceof SocketTimeoutException ? OfflineReason.Timeout : OfflineReason.Generic);
                        initialQuery = true; // Reset for later
                    }
                    // I guess we just assume its fine...
                    else
                    {
                        querySuccessful();
                    }

                    return;
                }
            }
        }

        if (players.size() > 0)
            loadUUIDs(); // Get all the UUIDs from the API

        querySuccessful();
    }

    private void loadUUIDs()
    {
        MojangUniqueIDLookup lookup = MojangUniqueIDLookup.getInstance();
        for (int i = 0; i < players.size(); i++)
        {
            MinecraftPlayer mp = (MinecraftPlayer) players.valueAt(i);
            if (mp.getUniqueID() == null)
                mp.setUniqueId(lookup.getUniqueID(mp.getName()));
        }
    }

    private long getQueryChallenge() throws IOException
    {
        MinecraftQueryHandshakePacket handshake = new MinecraftQueryHandshakePacket(1);
        querySocket.send(handshake);

        return new MinecraftQueryHandshakePacket(querySocket.receivePacket()).getChallenge();
    }

    private void adjustClientSocket(String hostIP, int port)
    {
        InetAddress hostAddress;

        if (hostIP.equals("0.0.0.0"))
        {
            hostAddress = address;
        }
        else
        {
            try
            {
                hostAddress = InetAddress.getByName(hostIP);
            }
            catch (UnknownHostException e)
            {
                Timber.w(e, "Unable to adjust MinecraftClientSocket");
                hostAddress = null;
            }
        }

        if (hostAddress != null &&
            (address.equals(hostAddress) || port != this.port))
        {
            clientSocket.close();
            clientSocket = new MinecraftClientSocket(hostAddress, port,
                    hostname);
            clientSocket.setTimeout(querySocket.getTimeout());
            adjustedClientSocket = true;
        }
    }

    @Override
    public ServerType getType()
    {
        return ServerType.Minecraft;
    }

    @Override
    public void setExtraOption(String key, Object value)
    {
        if (key.equals(EXTRA_OPTION_RCON_PORT))
        {
            this.rconPort = (Integer) value;
            if (rconSocket != null)
            {
                this.rconSocket.disconnect();
                rconSocket = new SourceRCONSocket(address, rconPort, this);
                rconSocket.connect(adminPassword);
            }
        }
    }

    @Override
    public Object getExtraOption(String key)
    {
        if (key.equals(EXTRA_OPTION_RCON_PORT))
        {
            return rconPort;
        }

        return null;
    }

    public String getVersion()
    {
        return version;
    }

    @Override
    public int getRealNumPlayers()
    {
        return numPlayers;
    }

    public String[][] getModList()
    {
        return modList;
    }

    public boolean hasMods()
    {
        return modList != null;
    }

    public Bitmap getFavicon()
    {
        return favicon;
    }

    public String getGameType()
    {
        return gameType;
    }

    public boolean usesQuery()
    {
        return queryMode == QueryMode.BOTH;
    }

    @Override
    public int getGameLogoResource()
    {
        if (favicon == null)
            return R.drawable.ic_game_minecraft;
        else
            return 0;
    }

    @Override
    public Bitmap getGameLogoBitmap()
    {
        return favicon;
    }

    @Override
    public boolean hasBadAdminPassword()
    {
        return false;
    }

    @Override
    public boolean isRconConnected()
    {
        return (rconSocket != null && rconSocket.isConnected() && rconSocket.isAuthenticated());
    }

    @Override
    void requestPrivatePlayerInfo()
    {
        // Do nothing because we can't
    }

    @Override
    public void onRconAuthenticated()
    {
        super.onRconAuthenticated();

        rconDidConnect = true;
    }

    @Override
    public void onRconDisconnected()
    {
        super.onRconDisconnected();

        if (rconDidConnect)
        {
            addToRconLog(new LogHolder(LogHolder.LogType.DISCONNECTED));
        }

        if (rconSocket != null)
            rconDidConnect = false;
    }

    @Override
    protected void setTimeout(int timeout)
    {
        if (querySocket != null)
            querySocket.setTimeout(timeout);
        if (clientSocket != null)
            clientSocket.setTimeout(timeout);
    }

    @Override
    public boolean supportsLogging()
    {
        return false;
    }

    @Override
    public boolean loggingUnavailable()
    {
        return true;
    }

    @Override
    public boolean supportsChat()
    {
        return false;
    }

    @Override
    public boolean isChatPossible()
    {
        return false;
    }

    @Override
    public ChatTarget[] getChatTargets()
    {
        return null;
    }

    @Override
    void doGetChatTargets(List<ChatTarget> targetList) {}

    @Override
    boolean doSendChatMessage(String message, ChatTarget targetChat)
    {
        return false;
    }

    @Override
    protected void doSendUserAdminCommand(String s)
    {
        rconSocket.send(
                new SourceRCONPacket(RCON_TAG_COMMAND, SourceRCONPacket.PACKET_TYPE_EXECCOMMAND, s));
    }

    @Override
    protected void doSendInternalAdminCommand(int requestId, String command)
    {
        rconSocket.send(new SourceRCONPacket(
                requestId, SourceRCONPacket.PACKET_TYPE_EXECCOMMAND, command));
    }

    @Override
    public void closeQueryConnection()
    {
        if (clientSocket != null)
            clientSocket.close();
    }

    @Override
    public boolean doOpenRCONConnection()
    {
        if (rconSocket == null)
            rconSocket = new MinecraftRCONSocket(address, rconPort, this);

        if (hasAdminPassword() && !rconSocket.badPassword() && !rconSocket.isConnected())
            rconSocket.connect(adminPassword);

        return true;
    }

    @Override
    public void closeRconConnection()
    {
        if (rconSocket != null)
            rconSocket.disconnect();
        rconSocket = null;
    }

    @Override
    public void onRconResponse(int id, String response)
    {
        if (id == RCON_TAG_COMMAND)
        {
            addToRconLog(new LogHolder(LogHolder.LogType.COMMAND_RESPONSE, response));
        }
        else if (id >= RCON_TAG_INTERNAL_MIN)
        {
            handleInternalAdminCommand(id, response);
        }
    }

    @Override
    void load(JSONObject serverJSON) throws JSONException
    {
        super.load(serverJSON);

        if (serverJSON.has(EXTRA_OPTION_RCON_PORT))
            rconPort = serverJSON.getInt(EXTRA_OPTION_RCON_PORT);
    }

    @Override
    void save(JSONObject serverJSON) throws JSONException
    {
        super.save(serverJSON);
        serverJSON.put(EXTRA_OPTION_RCON_PORT, rconPort);
    }

    private enum QueryMode
    {
        BOTH, UNKNOWN, CLIENT
    }
}
