package me.zachcheatham.gameservers.server.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import androidx.core.app.NotificationCompat;
import androidx.core.app.Person;
import androidx.core.app.RemoteInput;
import androidx.core.app.TaskStackBuilder;
import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.ServerDetailsActivity;
import me.zachcheatham.gameservers.events.ChatAddedEvent;
import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.ChatMessage;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.player.Player;
import me.zachcheatham.gameservers.service.NotificationHelperService;

/**
 * Used to create system notifications from server events
 */
public class Notifier
{
    public static final int NOTIFICATION_TYPE_CHAT = 0;
    public static final int NOTIFICATION_TYPE_ADMIN_CHAT = 1;
    private static final int PLAYER_SUMMARY_NOTIFICATION_ID_OFFSET = 10000;
    private static final int NOTIFICATION_TYPE_PLAYER_ALERT = 2;
    private static final int REQUEST_CODE_OFFSET = 100;
    private static final int UNIQUE_REQUEST_CODES = 6;
    private static final int REQUEST_CODE_OFFSET_OPEN_SERVER = 0;
    private static final int REQUEST_CODE_OFFSET_OPEN_CHAT = 1;
    private static final int REQUEST_CODE_OFFSET_REPLY_CHAT = 2;
    private static final int REQUEST_CODE_OFFSET_REPLY_ADMIN_CHAT = 3;
    private static final int REQUEST_CODE_OFFSET_DELETE_CHAT = 5;
    private static final int REQUEST_CODE_OFFSET_DELETE_ADMIN_CHAT = 6;
    public static final int DISABLE_NONE = -2;
    public static final int DISABLE_ALL = -1;

    private final Context context;
    private final ServerManager manager;

    private int disabledServerChat = DISABLE_NONE;
    private int disabledPlayerUpdates = DISABLE_NONE;

    public Notifier(ServerManager serverManager)
    {
        this.manager = serverManager;
        this.context = manager.getContext();

        EventBus.getDefault().register(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            /* Setup player notifications */
            NotificationChannel channel = new NotificationChannel(
                    Constants.NOTIFICATION_CHANNEL_PLAYER_ALERTS,
                    context.getString(R.string.notification_player_alerts),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(context.getString(R.string.notification_player_alerts_desc));
            notificationManager.createNotificationChannel(channel);

            /* Setup chat notifications */
            channel = new NotificationChannel(
                    Constants.NOTIFICATION_CHANNEL_CHAT,
                    context.getString(R.string.notification_chat),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(context.getString(R.string.notification_chat_desc));
            notificationManager.createNotificationChannel(channel);

            /* Setup chat notifications */
            channel = new NotificationChannel(
                    Constants.NOTIFICATION_CHANNEL_ADMIN_CHAT,
                    context.getString(R.string.notification_admin_chat),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(context.getString(R.string.notification_admin_chat_desc));
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void cleanup()
    {
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.didNumPlayersChange())
        {
            Server server = event.getServer();

            if (server.hasFlag(Server.FLAG_NOTIFY_EMPTY) && server.getRealNumPlayers() == 0)
                notifyPlayers(context.getString(R.string.notification_empty_server), server);
            else if (server.hasFlag(Server.FLAG_NOTIFY_FULL) &&
                     server.getRealNumPlayers() == server.getMaxPlayers())
                notifyPlayers(context.getString(R.string.notification_full_server), server);
            else if (server.hasFlag(Server.FLAG_NOTIFY_PLAYERS) &&
                     event.getPreviousPlayerCount() == 0)
            {
                notifyPlayers(
                        context.getString(
                                R.string.notification_players_server,
                                server.getRealNumPlayers()),
                        server);
            }
        }
    }

    @Subscribe
    public void onChatMessage(ChatAddedEvent event)
    {
        Server server = event.getServer();
        ChatMessage message = event.getMessage();

        if (server.hasFlag(Server.FLAG_NOTIFY_ALL_CHAT) ||
            (message.type == ChatMessage.MessageType.ADMIN &&
             server.hasFlag(Server.FLAG_NOTIFY_ADMIN_CHAT)))
        {
            notifyChat(message, server);
        }
    }

    private void notifyPlayers(CharSequence contentText, Server server)
    {
        int serverIndex = manager.getServerIndex(server);
        if (disabledPlayerUpdates != serverIndex && disabledPlayerUpdates != DISABLE_ALL)
        {
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            Intent detailsIntent = new Intent(context, ServerDetailsActivity.class);
            detailsIntent.putExtra(ServerDetailsActivity.EXTRA_KEY_SERVER_INDEX, serverIndex);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntentWithParentStack(detailsIntent);
            PendingIntent pendingIntent = stackBuilder
                    .getPendingIntent(serverIndex +
                                      REQUEST_CODE_OFFSET + REQUEST_CODE_OFFSET_OPEN_SERVER,
                            PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder notification = new NotificationCompat.Builder(context,
                    Constants.NOTIFICATION_CHANNEL_PLAYER_ALERTS)
                    .setContentTitle(server.getName())
                    .setContentText(contentText)
                    .setSmallIcon(R.drawable.ic_people_white)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent);

            notificationManager.notify(
                    PLAYER_SUMMARY_NOTIFICATION_ID_OFFSET + serverIndex,
                    notification.build());
        }
    }

    private void notifyChat(ChatMessage message, Server server)
    {
        int serverIndex = manager.getServerIndex(server);
        if (disabledServerChat != serverIndex && disabledServerChat != DISABLE_ALL)
        {
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            int notificationType = message.type ==
                                   ChatMessage.MessageType.ADMIN ? NOTIFICATION_TYPE_ADMIN_CHAT :
                    NOTIFICATION_TYPE_CHAT;
            int notificationId = serverIndex * 2;
            if (notificationType == NOTIFICATION_TYPE_ADMIN_CHAT)
                notificationId++;
            Player player = null;
            if (message.playerKey != -1)
                player = server.getPlayer(message.playerKey);

            String contents;
            switch (message.type)
            {
            case TEAM:
                contents = context.getString(
                        R.string.notification_chat_message_team,
                        message.target,
                        message.message);
                break;
            default:
                contents = message.message;
                break;
            }

            SharedPreferences preferences = context
                    .getSharedPreferences(Constants.PREFS_KEY_NOTIFICATIONS, Context.MODE_PRIVATE);

            int requestCode = serverIndex * UNIQUE_REQUEST_CODES;

            Intent chatIntent = new Intent(context, ServerDetailsActivity.class);
            chatIntent.putExtra(ServerDetailsActivity.EXTRA_KEY_SERVER_INDEX, serverIndex);
            chatIntent.setAction(ServerDetailsActivity.ACTION_OPEN_CHAT);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntentWithParentStack(chatIntent);
            PendingIntent chatPendingIntent = stackBuilder
                    .getPendingIntent(requestCode + REQUEST_CODE_OFFSET_OPEN_CHAT,
                            PendingIntent.FLAG_UPDATE_CURRENT);

            Intent deleteIntent = new Intent(context, NotificationHelperService.class);
            deleteIntent.setAction(NotificationHelperService.ACTION_DELETE);
            deleteIntent.putExtra(
                    NotificationHelperService.EXTRA_KEY_NOTIFICATION_TYPE, notificationType);
            deleteIntent.putExtra(NotificationHelperService.EXTRA_KEY_SERVER_INDEX, serverIndex);
            PendingIntent deletePendingIntent = PendingIntent
                    .getService(context, requestCode +
                                         (notificationType == NOTIFICATION_TYPE_ADMIN_CHAT ?
                                                 REQUEST_CODE_OFFSET_DELETE_ADMIN_CHAT :
                                                 REQUEST_CODE_OFFSET_DELETE_CHAT),
                            deleteIntent, 0);

            Intent replyIntent = new Intent(context, NotificationHelperService.class);
            replyIntent.setAction(NotificationHelperService.ACTION_REPLY);
            replyIntent.putExtra(
                    NotificationHelperService.EXTRA_KEY_NOTIFICATION_TYPE, notificationType);
            replyIntent.putExtra(NotificationHelperService.EXTRA_KEY_SERVER_INDEX, serverIndex);
            PendingIntent replyPendingIntent = PendingIntent
                    .getService(context, requestCode +
                                         (notificationType == NOTIFICATION_TYPE_ADMIN_CHAT ?
                                                 REQUEST_CODE_OFFSET_REPLY_ADMIN_CHAT :
                                                 REQUEST_CODE_OFFSET_REPLY_CHAT),
                            replyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Person consolePerson = new Person.Builder()
                    .setName(context.getString(R.string.player_console))
                    .build();

            List<NotificationCompat.MessagingStyle.Message> messages =
                    MessageMarshaller.getChatHistory(preferences, notificationType, serverIndex);

            String playerName;
            if (message.playerKey == -1)
                playerName = context.getString(R.string.player_console);
            else if (player == null)
                playerName = context.getString(R.string.chat_disconnected_player);
            else
                playerName = player.getName();

            Person sender = new Person.Builder().setName(playerName).build();
            NotificationCompat.MessagingStyle.Message notificationMessage =
                    new NotificationCompat.MessagingStyle.Message(contents,
                            System.currentTimeMillis(),
                            sender);
            messages.add(notificationMessage);

            String conversationTitle;
            if (notificationType == NOTIFICATION_TYPE_ADMIN_CHAT)
                conversationTitle = context
                        .getString(R.string.notification_chat_name_admin, server.getName());
            else
                conversationTitle = server.getName();

            NotificationCompat.MessagingStyle notificationStyle =
                    new NotificationCompat.MessagingStyle(consolePerson)
                            .setConversationTitle(conversationTitle)
                            .setGroupConversation(true);

            for (NotificationCompat.MessagingStyle.Message m : messages)
                notificationStyle.addMessage(m);

            RemoteInput remoteInput
                    = new RemoteInput.Builder(NotificationHelperService.BUNDLE_KEY_REPLY_MESSAGE)
                    .setLabel(context.getString(R.string.action_send_message))
                    .build();

            NotificationCompat.Action action =
                    new NotificationCompat.Action.Builder(
                            R.drawable.ic_send_white,
                            context.getString(R.string.action_send_message),
                            replyPendingIntent)
                            .addRemoteInput(remoteInput)
                            .build();

            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(context, notificationType ==
                                                            NOTIFICATION_TYPE_ADMIN_CHAT ?
                            Constants.NOTIFICATION_CHANNEL_ADMIN_CHAT :
                            Constants.NOTIFICATION_CHANNEL_CHAT)
                            .setSmallIcon(R.drawable.ic_chat_notification)
                            .setStyle(notificationStyle)
                            .addAction(action)
                            .setContentIntent(chatPendingIntent)
                            .setDeleteIntent(deletePendingIntent)
                            .setColor(context.getResources().getColor(R.color.colorPrimaryDark))
                            .setAutoCancel(true);

            notificationManager.notify(notificationId, builder.build());

            MessageMarshaller.saveChatHistory(preferences, notificationType, serverIndex, messages);
        }
    }

    public void removeChatNotifications(int serverIndex)
    {
        int notificationId = serverIndex * 2;
        int notificationIdAdmin = notificationId + 1;

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.cancel(notificationId);
        notificationManager.cancel(notificationIdAdmin);

        SharedPreferences preferences = context.getSharedPreferences(Constants.PREFS_KEY_NOTIFICATIONS,0);
        MessageMarshaller.removeChatHistory(preferences, NOTIFICATION_TYPE_CHAT, serverIndex);
        MessageMarshaller.removeChatHistory(preferences, NOTIFICATION_TYPE_ADMIN_CHAT, serverIndex);
    }

    /**
     * Disable chat notifications for server
     * @param serverIndex Server to disable or {@link #DISABLE_ALL}
     */
    public void setDisabledServerChat(int serverIndex)
    {
        disabledServerChat = serverIndex;
    }

    /**
     * Re-Enable server chat notifications
     */
    public void clearDisabledServerChat()
    {
        disabledServerChat = DISABLE_NONE;
    }

    /**
     * Disable player update notifications for server
     * @param serverIndex Server to disable or {@link #DISABLE_ALL}
     */
    public void setDisabledPlayerUpdates(int serverIndex)
    {
        disabledPlayerUpdates = serverIndex;
    }

    /**
     * Re-Enable server player update notifications
     */
    public void clearDisabledPlayerUpdates()
    {
        disabledPlayerUpdates = DISABLE_NONE;
    }
}
