package me.zachcheatham.gameservers.server.player.copy;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.player.Player;

public class PlayerCopyMenuPopulator
{
    final Player player;

    public PlayerCopyMenuPopulator(Player player)
    {
        this.player = player;
    }

    public List<PlayerCopyMenuItem> populate(Context context)
    {
        List<PlayerCopyMenuItem> items = new ArrayList<>();
        items.add(new PlayerCopyMenuItem(context.getString(R.string.action_copy_name), player.getName()));
        return items;
    }

    public static class PlayerCopyMenuItem
    {
        public final String title;
        public final String value;

        PlayerCopyMenuItem(String title, String value)
        {
            this.title = title;
            this.value = value;
        }
    }
}
