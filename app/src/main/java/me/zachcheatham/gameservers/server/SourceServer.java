package me.zachcheatham.gameservers.server;

import java.util.List;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.packet.source.S2AInfoPacket;
import me.zachcheatham.gameservers.server.packet.source.S2APlayerPacket;
import me.zachcheatham.gameservers.server.packet.source.S2ARulesPacket;
import me.zachcheatham.gameservers.server.packet.source.SourcePacket;
import me.zachcheatham.gameservers.server.packet.source.SourceRCONPacket;
import me.zachcheatham.gameservers.server.player.Player;
import me.zachcheatham.gameservers.server.player.SourcePlayer;
import me.zachcheatham.gameservers.server.socket.SourceRCONSocket;
import me.zachcheatham.gameservers.server.socket.SourceSocket;

public class SourceServer extends BaseSourceServer implements SourceRCONSocket.Listener
{
    private SourceRCONSocket rconSocket = null;
    private boolean rconDidConnect = false;

    SourceServer(String hostname, int port)
    {
        super(hostname, port);
    }

    @Override
    public ServerType getType()
    {
        return ServerType.Source;
    }

    @Override
    protected void query(boolean extended)
    {
        if (socket == null)
            socket = new SourceSocket(address, port, this);

        if (extended)
            doOpenRCONConnection();

        super.query(extended);
    }

    @Override
    void handlePacket(SourcePacket packet)
    {
        switch (packet.getHeader())
        {
        case S2AInfoPacket.HEADER:
            S2AInfoPacket infoResponse = (S2AInfoPacket) packet;
            ping = (short) (System.currentTimeMillis() - pingStart);
            game = infoResponse.game;
            name = infoResponse.hostname.trim();
            map = infoResponse.map;
            numPlayers = infoResponse.numPlayers;
            maxPlayers = infoResponse.maxPlayers;
            botCount = infoResponse.botCount;
            gameDir = infoResponse.gameDirectory;
            hasPassword = infoResponse.password;
            version = infoResponse.version;
            serverType = infoResponse.serverType;
            operatingSystem = infoResponse.operatingSystem;
            vacSecured = infoResponse.isProtected;
            gameAppId = infoResponse.appID;
            break;
        case S2ARulesPacket.HEADER:
            ((S2ARulesPacket) packet).getRules(rules);
            rulesVariables = rules.keySet().toArray(new String[0]);
            break;
        case S2APlayerPacket.HEADER:
            List<SourcePlayer> incomingPlayers = ((S2APlayerPacket) packet).players;
            addPlayers((List<Player>) (List<?>) incomingPlayers);
            break;
        }
    }

    @Override
    protected boolean doOpenRCONConnection()
    {
        if (rconSocket == null)
            rconSocket = new SourceRCONSocket(address, port, this);

        if (hasAdminPassword() && !rconSocket.badPassword() && !rconSocket.isConnected())
            rconSocket.connect(adminPassword);

        return true;
    }

    @Override
    public void closeRconConnection()
    {
        if (rconSocket != null)
            rconSocket.disconnect();
        rconSocket = null;
    }

    @Override
    public void setTimeout(int timeout)
    {
        super.setTimeout(timeout);

        if (rconSocket != null)
            rconSocket.setTimeout(timeout);
    }

    @Override
    void sendRconCommand(int tag, String command)
    {
        rconSocket
                .send(new SourceRCONPacket(tag, SourceRCONPacket.PACKET_TYPE_EXECCOMMAND, command));
    }

    @Override
    public boolean isRconConnected()
    {
        return (rconSocket != null && rconSocket.isConnected() && rconSocket.isAuthenticated());
    }

    @Override
    public void onRconAuthenticated()
    {
        super.onRconAuthenticated();

        rconDidConnect = true;
        requestRCONLogging();
    }

    @Override
    public void onRconDisconnected()
    {
        super.onRconDisconnected();

        if (rconDidConnect)
            addToRconLog(new LogHolder(LogHolder.LogType.DISCONNECTED));

        if (rconSocket != null)
            rconDidConnect = false;
    }

    @Override
    public int getGameLogoResource()
    {
        switch (gameAppId)
        {
        case 240:
            return R.drawable.ic_game_css;
        case 300:
            return R.drawable.ic_game_dod;
        case 440:
            return R.drawable.ic_game_tf2;
        case 500:
        case 550:
            return R.drawable.ic_game_l4d;
        case 730:
            return R.drawable.ic_game_csgo;
        case 4000:
            return R.drawable.ic_game_gmod;
        default:
            return R.drawable.ic_game_hl2;
        }
    }

    @Override
    public boolean hasBadAdminPassword()
    {
        return (rconSocket != null && rconSocket.badPassword());
    }
}