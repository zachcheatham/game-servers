package me.zachcheatham.gameservers.server.packet.minecraft;

import java.nio.BufferUnderflowException;
import java.util.ArrayList;
import java.util.List;

import me.zachcheatham.gameservers.server.packet.PacketException;
import me.zachcheatham.gameservers.server.player.MinecraftPlayer;
import timber.log.Timber;

public class MinecraftFullStatResponsePacket extends MinecraftQueryPacket
{
    private final List<MinecraftPlayer> players = new ArrayList<>();
    private String MOTD;
    private String gameType;
    private String map;
    private int numPlayers;
    private int maxPlayers;
    private short hostport;
    private String hostip;
    private String plugins;

    // [SERVER_MOD_NAME[: PLUGIN_NAME(; PLUGIN_NAME...)]]

    public MinecraftFullStatResponsePacket(byte[] data) throws PacketException
    {
        super(data);
        buffer.position(buffer.position() + 11); // skip padding

        try
        {
            while (true)
            {
                String key = readString();
                if (key.length() == 0)
                    break;
                String value = readString();

                switch (key)
                {
                case "hostname":
                    MOTD = value;
                    break;
                case "gametype":
                    gameType = value;
                    break;
                case "plugins":
                    plugins = value;
                    break;
                case "map":
                    map = value;
                    break;
                case "numplayers":
                    numPlayers = Integer.valueOf(value);
                    break;
                case "maxplayers":
                    maxPlayers = Integer.valueOf(value);
                    break;
                case "hostport":
                    hostport = Short.valueOf(value);
                    break;
                case "hostip":
                    hostip = value;
                    break;
                }
            }

            buffer.position(buffer.position() + 10);

            while (true)
            {
                String name = readString();
                if (name.length() == 0)
                    break;

                players.add(new MinecraftPlayer(name));
            }
        }
        catch (BufferUnderflowException e)
        {
            Timber.w("Buffer ran out while reading minecraft packet.");
        }
    }

    public String getMOTD()
    {
        return MOTD;
    }

    public String getGameType()
    {
        return gameType;
    }

    public String getMap()
    {
        return map;
    }

    public int getNumPlayers()
    {
        return numPlayers;
    }

    public int getMaxPlayers()
    {
        return maxPlayers;
    }

    public short getHostPort()
    {
        return hostport;
    }

    public String getHostIP()
    {
        return hostip;
    }

    public List<MinecraftPlayer> getPlayers()
    {
        return players;
    }
}
