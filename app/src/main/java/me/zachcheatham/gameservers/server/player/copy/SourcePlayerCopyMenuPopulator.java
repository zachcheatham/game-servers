package me.zachcheatham.gameservers.server.player.copy;

import android.content.Context;

import java.util.List;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.server.player.Player;
import me.zachcheatham.gameservers.server.player.SourcePlayer;

public class SourcePlayerCopyMenuPopulator extends PlayerCopyMenuPopulator
{
    public SourcePlayerCopyMenuPopulator(Player player)
    {
        super(player);
    }

    @Override
    public List<PlayerCopyMenuItem> populate(Context context)
    {
        List<PlayerCopyMenuItem> items = super.populate(context);
        SourcePlayer sourcePlayer = (SourcePlayer) player;
        if (sourcePlayer.hasPrivateInfo())
        {
            items.add(new PlayerCopyMenuItem(
                    context.getString(R.string.action_copy_steam_id),
                    sourcePlayer.getSteamID()));
            items.add(new PlayerCopyMenuItem(
                    context.getString(R.string.action_copy_steam_id_3),
                    sourcePlayer.getSteamID3()));
            items.add(new PlayerCopyMenuItem(
                    context.getString(R.string.action_copy_steam_id_64),
                    sourcePlayer.getSteamID64()));
            items.add(new PlayerCopyMenuItem(
                    context.getString(R.string.action_copy_ip_address),
                    sourcePlayer.getIPAddress()));
        }
        items.add(new PlayerCopyMenuItem(
                context.getString(R.string.action_copy_time_connected),
                sourcePlayer.getTimeString()));
        return items;
    }
}
