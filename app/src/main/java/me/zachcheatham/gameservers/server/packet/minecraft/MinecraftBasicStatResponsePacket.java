package me.zachcheatham.gameservers.server.packet.minecraft;

import me.zachcheatham.gameservers.server.packet.PacketException;

public class MinecraftBasicStatResponsePacket extends MinecraftQueryPacket
{
    private final String MOTD;
    private final String gameType;
    private final String map;
    private final int numPlayers;
    private final int maxPlayers;
    private final short hostport;
    private final String hostip;

    public MinecraftBasicStatResponsePacket(byte[] data) throws PacketException
    {
        super(data);
        MOTD = readString();
        gameType = readString();
        map = readString();
        String nP = readString();
        String mP = readString();
        if (nP.length() > 0)
            numPlayers = Integer.valueOf(nP);
        else
            numPlayers = 0;
        if (mP.length() > 0)
            maxPlayers = Integer.valueOf(mP);
        else
            maxPlayers = 0;
        hostport = readShort();
        hostip = readString();
    }

    public String getMOTD()
    {
        return MOTD;
    }

    public String getGameType()
    {
        return gameType;
    }

    public String getMap()
    {
        return map;
    }

    public int getNumPlayers()
    {
        return numPlayers;
    }

    public int getMaxPlayers()
    {
        return maxPlayers;
    }

    public short getHostPort()
    {
        return hostport;
    }

    public String getHostIP()
    {
        return hostip;
    }
}
