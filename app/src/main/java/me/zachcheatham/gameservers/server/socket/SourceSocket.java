package me.zachcheatham.gameservers.server.socket;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.zip.CRC32;

import me.zachcheatham.gameservers.server.packet.PacketException;
import me.zachcheatham.gameservers.server.packet.source.SourcePacket;

public class SourceSocket extends BaseSourceSocket
{
    public SourceSocket(InetAddress address, int port, SocketListener listener)
    {
        super(address, port, listener);
    }

    public byte[] receivePacket() throws IOException
    {
        byte[] data = new byte[SourcePacket.PACKET_LENGTH];
        DatagramPacket response = new DatagramPacket(data, data.length);
        socket.receive(response);

        ByteBuffer buffer = ByteBuffer.wrap(data);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        long header = buffer.getInt();

        // Not split
        if (header == -1)
        {
            return Arrays.copyOfRange(data, buffer.position(), response.getLength());
        }
        // Split packet
        else if (header == -2)
        {
            // Get info about the split packet
            int packetID = buffer.getInt();
            int totalPackets = buffer.get() & 0xff;
            int packetIndex = buffer.get() & 0xff;

            boolean compressed = ((packetID >> 31) & 1) == 1;
            int uncompressedSize = -1;
            long uncompressedCrc = -1;
            if (compressed)
            {
                uncompressedSize = buffer.getInt();
                uncompressedCrc = buffer.getInt();
            }
            else
            {
                // Max packet size
                // Contrary to the wiki, this seems to only be sent when the packet is uncompressed.
                buffer.getShort(); // This is also useless to us
            }

            byte[][] packets = new byte[totalPackets][];
            packets[packetIndex] = Arrays.copyOfRange(data, buffer.position(), response.getLength());

            int allocatedPackets = 1;
            while (allocatedPackets < totalPackets)
            {
                Arrays.fill(data, (byte) 0x00);
                response = new DatagramPacket(data, data.length);
                socket.receive(response);

                buffer = ByteBuffer.wrap(data);
                buffer.order(ByteOrder.LITTLE_ENDIAN);

                header = buffer.getInt();
                long id = buffer.getInt();
                buffer.get(); // Discard total packets
                packetIndex = buffer.get() & 0xff;
                buffer.getShort(); // Discard size

                if (header != -2)
                    throw new PacketException(
                            "Unexpected packet while reconstructing split packet.");

                if (id != packetID)
                    throw new PacketException(
                            "Received a different split-packet while reconstructing.");

                packets[packetIndex] = Arrays
                        .copyOfRange(data, buffer.position(), response.getLength());

                allocatedPackets++;
            }

            int completePacketSize = 0;
            for (byte[] packet : packets)
            {
                completePacketSize += packet.length;
            }

            byte[] built = new byte[completePacketSize];
            int i = 0;
            for (byte[] packet : packets)
            {
                for (byte b : packet)
                {
                    built[i] = b;
                    i++;
                }
            }

            if (compressed)
            {
                byte[] decompressed = new byte[uncompressedSize];

                ByteArrayInputStream stream = new ByteArrayInputStream(built);
                BZip2CompressorInputStream decompressStream = new BZip2CompressorInputStream(
                        stream);
                decompressStream.read(decompressed, 0, uncompressedSize);
                stream.close();
                decompressStream.close();

                CRC32 checksum = new CRC32();
                checksum.update(decompressed);
                if ((int) checksum.getValue() == uncompressedCrc)
                    built = decompressed;
                else
                    throw new PacketException(
                            String.format("Compressed packet checksum mismatch. Got %d Wanted %d",
                                    (int) checksum.getValue(), uncompressedCrc));
            }

            return Arrays.copyOfRange(built, 4,
                    built.length); // Rids of duplicate header in resulting packet.
        }

        // If we got to here, we have an issue!
        throw new PacketException("Couldn't tell if a packet was split.");
    }
}
