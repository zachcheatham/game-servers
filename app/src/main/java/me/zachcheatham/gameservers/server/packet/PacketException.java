package me.zachcheatham.gameservers.server.packet;

import java.io.IOException;

public class PacketException extends IOException
{
    PacketException()
    {
        super();
    }

    public PacketException(String s)
    {
        super(s);
    }
}
