package me.zachcheatham.gameservers.server.packet.minecraft;

import me.zachcheatham.gameservers.server.packet.PacketException;

public class MinecraftQueryHandshakePacket extends MinecraftQueryPacket
{
    private final long challenge;

    public MinecraftQueryHandshakePacket(long sessionId)
    {
        super(PACKET_TYPE_HANDSHAKE, sessionId);
        challenge = -1;
    }

    public MinecraftQueryHandshakePacket(byte[] data) throws PacketException
    {
        super(data);

        if (packetType != PACKET_TYPE_HANDSHAKE)
            throw new PacketException("Tried to construct handshake response from bad packet");

        challenge = Long.parseLong(readString());
    }

    public long getChallenge()
    {
        return challenge;
    }
}
