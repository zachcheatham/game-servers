package me.zachcheatham.gameservers.server.packet.minecraft;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import me.zachcheatham.gameservers.server.packet.Packet;
import me.zachcheatham.gameservers.server.packet.PacketException;
import timber.log.Timber;

public abstract class MinecraftClientPacket extends Packet
{
    final int packetID;

    MinecraftClientPacket(int packetID)
    {
        super(1400);
        this.packetID = packetID;

        writeVarInt(packetID);
    }

    MinecraftClientPacket(byte[] data) throws PacketException
    {
        super(data);
        this.packetID = readVarInt();
    }

    @Override
    public byte[] getData()
    {
        int packetLength = buffer.position();
        byte[] packetLengthVarInt = createVariableInt(packetLength);

        byte[] data = new byte[packetLength + packetLengthVarInt.length];
        System.arraycopy(packetLengthVarInt, 0, data, 0, packetLengthVarInt.length);
        System.arraycopy(buffer.array(), 0, data, packetLengthVarInt.length, packetLength);

        return data;
    }

    private byte[] createVariableInt(int value)
    {
        int initialValue = value;

        ByteBuffer buffer = ByteBuffer.allocate(5);
        do
        {
            byte temp = (byte) (value & 0b01111111);
            value >>>= 7;
            if (value != 0)
                temp |= 0b10000000;
            buffer.put(temp);
        }
        while (value != 0);

        int length = buffer.position();
        byte[] varInt = new byte[length];
        System.arraycopy(buffer.array(), 0, varInt, 0, length);

        return varInt;
    }

    private int readVarInt() throws PacketException
    {
        int numRead = 0;
        int result = 0;
        byte read;
        do
        {
            read = buffer.get();
            int value = (read & 0b01111111);
            result |= (value << (7 * numRead));

            numRead++;
            if (numRead > 5)
                throw new PacketException("Received invalid (too big) VarInt.");
        }
        while ((read & 0b10000000) != 0);

        return result;
    }

    void writeVarInt(int value)
    {
        buffer.put(createVariableInt(value));
    }

    @Override
    protected String readString()
    {
        int length;
        try
        {
            length = readVarInt();
        }
        catch (Exception e)
        {
            Timber.w(e, "Unable to read Minecraft string.");
            return "";
        }

        byte[] data = new byte[length];
        buffer.get(data);
        try
        {
            return new String(data, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            return "";
        }
    }

    @Override
    protected void writeString(String s)
    {
        byte[] data = s.getBytes(Charset.forName("UTF-8"));
        writeVarInt(data.length);
        buffer.put(data);
    }
}
