package me.zachcheatham.gameservers.server;

public class ChatTarget
{
    public final ChatMessage.MessageType type;
    public final String teamName;

    /**
     * Use when targeting a specific team
     * @param team Name of the team
     */
    public ChatTarget(String team)
    {
        type = ChatMessage.MessageType.TEAM;
        teamName = team;
    }

    /**
     * Chat targets that are NOT a team.
     * @param type Type of chat
     */
    public ChatTarget(ChatMessage.MessageType type)
    {
        this.type = type;
        teamName = null;
    }
}
