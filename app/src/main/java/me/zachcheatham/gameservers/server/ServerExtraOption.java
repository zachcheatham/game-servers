package me.zachcheatham.gameservers.server;

import android.content.Context;

import me.zachcheatham.gameservers.R;

public class ServerExtraOption
{
    public static final ServerExtraOption[] minecraft = new ServerExtraOption[]{
            new ServerExtraOption(R.string.property_rcon_port, false, "rcon_port", FieldType.PORT,
                    String.valueOf(MinecraftServer.DEFAULT_RCON_PORT))};

    public final int hintStringId;
    public final boolean required;
    public final String saveKey;
    public final FieldType fieldType;
    public final String defaultValue;

    private ServerExtraOption(int hintStringId, boolean required, String saveKey,
            FieldType fieldType, String defaultValue)
    {
        this.required = required;
        this.hintStringId = hintStringId;
        this.saveKey = saveKey;
        this.fieldType = fieldType;
        this.defaultValue = defaultValue;
    }

    public static ServerExtraOption[] get(Server.ServerType type)
    {
        switch (type)
        {
        case Minecraft:
            return minecraft;
        default:
            return null;
        }
    }

    public String validate(String input, Context context)
    {
        if (input.length() > 0)
        {
            switch (fieldType)
            {
            case PORT:
                try
                {
                    int p = Integer.valueOf(input);
                    if (p < 2 || p > 65534)
                    {
                        return context.getString(R.string.error_invalid_port);
                    }
                }
                catch (NumberFormatException e)
                {
                    return context.getString(R.string.error_invalid_port);
                }
                break;
            }
        }
        else if (required)
        {
            return context.getString(R.string.error_required);
        }

        return null;
    }

    public Object convertInput(String input)
    {
        switch (fieldType)
        {
        case PORT:
            return Integer.parseInt(input);
        }

        return input;
    }

    public enum FieldType
    {
        PORT, GENERIC
    }
}
