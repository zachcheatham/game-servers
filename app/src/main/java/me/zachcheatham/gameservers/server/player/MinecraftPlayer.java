package me.zachcheatham.gameservers.server.player;

import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.player.copy.MinecraftPlayerCopyMenuPopulator;
import me.zachcheatham.gameservers.server.player.copy.PlayerCopyMenuPopulator;

public class MinecraftPlayer extends Player
{
    private String uniqueId = null;

    public MinecraftPlayer(String name)
    {
        super(name);
    }

    @Override
    public String getIPAddress()
    {
        return "";
    }

    @Override
    public int getUserID()
    {
        return 0;
    }

    public void setUniqueId(String uniqueId)
    {
        this.uniqueId = uniqueId;
    }

    @Override
    public String getUniqueID()
    {
        return uniqueId;
    }

    @Override
    public boolean hasPrivateInfo()
    {
        return false;
    }

    @Override
    public void merge(Player player, ServerUpdatedEvent event)
    {
        // do nothing, minecraft is boring
    }

    @Override
    public PlayerCopyMenuPopulator getCopyMenuPopulator()
    {
        return new MinecraftPlayerCopyMenuPopulator(this);
    }
}
