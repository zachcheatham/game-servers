package me.zachcheatham.gameservers.server.packet;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public abstract class Packet
{
    protected final ByteBuffer buffer;

    protected Packet(int bufferSize)
    {
        buffer = ByteBuffer.allocate(bufferSize);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
    }

    protected Packet(byte[] data)
    {
        buffer = ByteBuffer.wrap(data).asReadOnlyBuffer();
        buffer.order(ByteOrder.LITTLE_ENDIAN);
    }

    Packet(ByteBuffer buffer)
    {
        this.buffer = buffer;
        this.buffer.rewind();
    }

    public static String getHex(byte b)
    {
        return String.format("0x%02x", b & 0xFF);
    }

    public static String createHexString(byte[] data)
    {
        StringBuilder sb = new StringBuilder();
        for (byte b : data)
        {
            sb.append(String.format("%02x ", b & 0xFF));
        }

        return sb.toString();
    }

    protected byte readByte()
    {
        return buffer.get();
    }

    protected int readUnsignedByte()
    {
        return (buffer.get() & 0xff);
    }

    protected void writeByte(byte b)
    {
        buffer.put(b);
    }

    protected boolean readBoolean()
    {
        return buffer.get() == 1;
    }

    protected short readShort()
    {
        return buffer.getShort();
    }

    protected void writeUnsignedShort(int s)
    {
        buffer.put((byte) ((s >> 8) & 0xFF));
        buffer.put((byte) (s & 0xFF));
    }

    protected int readInt()
    {
        return buffer.getInt();
    }

    protected void writeInt(int i)
    {
        buffer.putInt(i);
    }

    protected long readUnsignedInt()
    {
        return ((long)
                        (readByte() << 24) |
                (readByte() << 16) |
                (readByte() << 8) |
                (readByte()));
    }

    protected void writeUnsignedInt(long i)
    {
        buffer.put((byte) ((i >> 24) & 0xFF));
        buffer.put((byte) ((i >> 16) & 0xFF));
        buffer.put((byte) ((i >> 8) & 0xFF));
        buffer.put((byte) (i & 0xFF));
    }

    protected long readLong()
    {
        return buffer.getLong();
    }

    protected void writeLong(long l)
    {
        buffer.putLong(l);
    }

    protected float readFloat()
    {
        return buffer.getFloat();
    }

    protected void writeFloat(float f)
    {
        buffer.putFloat(f);
    }

    protected String readString()
    {
        int startPos = buffer.position();
        while (true)
        {
            if ((char) buffer.get() == '\0')
                break;
        }

        int size = buffer.position() - startPos;

        byte[] str = new byte[size];
        buffer.position(startPos);
        buffer.get(str, 0, size);

        try
        {
            return new String(str, 0, size - 1, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            return "";
        }
    }

    protected void writeString(String s)
    {
        byte[] bytes = (s + "\0").getBytes();
        buffer.put(bytes);
    }

    public String getHexString()
    {
        return createHexString(getData());
    }

    public byte[] getData()
    {
        if (buffer.isReadOnly())
        {
            // Save the position, since we have to reset it
            int p = buffer.position();
            buffer.position(0);

            // Get the data from the buffer
            byte[] data = new byte[buffer.capacity()];
            buffer.get(data, 0, buffer.capacity());

            buffer.position(p); // Set the position back to where we were

            return data;
        }
        else
        {
            int length = buffer.position();
            byte[] data = new byte[length];

            System.arraycopy(buffer.array(), 0, data, 0, length);
            return data;
        }
    }

    public ByteBuffer getBuffer()
    {
        buffer.flip();
        return buffer;
    }
}
