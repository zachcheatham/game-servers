package me.zachcheatham.gameservers.server.socket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.packet.minecraft.MinecraftQueryPacket;

public class MinecraftQuerySocket
{
    private final InetAddress address;
    private final int port;
    private int timeout = ServerManager.queryTimeout;
    private DatagramSocket socket;

    public MinecraftQuerySocket(InetAddress address, int port)
    {
        this.address = address;
        this.port = port;
    }

    public void send(MinecraftQueryPacket packet) throws IOException
    {
        if (socket == null)
            socket = new DatagramSocket();

        if (!socket.isConnected() || socket.isClosed())
        {
            socket.connect(address, port);
            socket.setSoTimeout(timeout);
        }

        byte[] data = packet.getData();
        socket.send(new DatagramPacket(data, data.length));
    }

    public byte[] receivePacket() throws IOException
    {
        byte[] data = new byte[5120];
        DatagramPacket response = new DatagramPacket(data, data.length);
        socket.receive(response);

        return data;
    }

    public void close()
    {
        if (socket != null)
        {
            socket.close();
            socket = null;
        }
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
        if (socket != null)
        {
            try
            {
                socket.setSoTimeout(timeout);
            }
            catch (SocketException ignored)
            {
            }
        }
    }
}
