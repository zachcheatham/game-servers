package me.zachcheatham.gameservers.server.packet.source;

public class A2SGoldSrcRCONRequestPacket extends SourcePacket
{
    public A2SGoldSrcRCONRequestPacket(String request)
    {
        super((byte) 0);
        buffer.put(request.getBytes());
    }
}
