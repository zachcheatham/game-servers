package me.zachcheatham.gameservers.server.packet.minecraft;

public class MinecraftRequestPacket extends MinecraftClientPacket
{
    private static final byte PACKET_ID = 0x00;

    public MinecraftRequestPacket()
    {
        super(PACKET_ID);
    }
}
