package me.zachcheatham.gameservers.server;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

public class ServerImporter
{
    private static final Pattern VALUE_REGEX = Pattern.compile("\"[^\"]+\"|[\\S]+");

    public static void read(InputStream stream, FileType type, ImportCompleteListener listener)
    {
        new ImportTask(stream, listener, type).execute();
    }

    private static void doReadSSLF(InputStream stream, ImportCompleteListener listener)
    {
        try
        {
            List<Server> servers = new ArrayList<>();
            int serversExisting = 0;
            int serversUnsupported = 0;
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line;
            int item;

            lineloop:
            while ((line = reader.readLine()) != null)
            {
                item = 0;

                /*if (line.startsWith("Name="))
                {
                    //name = line.substring(6, line.length() - 1);
                }
                else */
                if (line.startsWith("Server="))
                {
                    Matcher m = VALUE_REGEX.matcher(line.substring(7));

                    String itemValue;
                    String[] addressParts;

                    Server.ServerType type = null;
                    String hostname = null;
                    InetAddress address = null;
                    int port = -1;
                    String adminPassword = null;
                    String serverName = null;

                    while (m.find())
                    {
                        itemValue = m.group().replace("\"", "");
                        switch (item)
                        {
                        case 0:
                            type = getServerTypeFromSSLF(itemValue);
                            if (type == null)
                            {
                                Timber.w("Unknown server type %s", itemValue);
                                serversUnsupported++;
                                continue lineloop;
                            }
                            break;
                        case 1:
                            addressParts = itemValue.split(":", 2);
                            hostname = addressParts[0];
                            port = Integer.valueOf(addressParts[1]);

                            try
                            {
                                address = InetAddress.getByName(hostname);
                            }
                            catch (UnknownHostException ignored)
                            {
                            }

                            break;
                        case 2:
                            adminPassword = itemValue;
                            break;
                        case 3:
                            serverName = itemValue;
                            break;
                        }
                        item++;
                    }

                    if (port == -1)
                    {
                        Timber.w("Bad server entry in SSLF: %s", line);
                    }
                    else if (!((address != null && listener.serverExists(address, port)) ||
                               listener.serverExists(hostname, port)))
                    {
                        Server s = Server.create(hostname, port, type);
                        if (serverName != null)
                            s.name = serverName;
                        if (adminPassword != null && adminPassword.length() > 0)
                            s.adminPassword = adminPassword;
                        servers.add(s);
                    }
                    else
                    {
                        serversExisting++;
                    }
                }
            }
            reader.close();

            listener.onServerImportCompleted(servers, serversExisting, serversUnsupported);
        }
        catch (FileNotFoundException e)
        {
            Timber.e(e, "Unable to import list: ");
            //Toast.makeText(context, R.string.unable_to_import, Toast.LENGTH_SHORT).show();
        }
        catch (IOException e)
        {
            Timber.e(e, "Unable to import list: ");
            //Toast.makeText(context, R.string.unable_to_import, Toast.LENGTH_SHORT).show();
        }
    }

    private static Server.ServerType getServerTypeFromSSLF(String type)
    {
        switch (type)
        {
        case "HL2":
            return Server.ServerType.Source;
        case "HL":
        case "L4D":
            return Server.ServerType.Gold_Src;
        default:
            return null;
        }
    }

    public enum FileType
    {
        SSLF
    }

    public interface ImportCompleteListener
    {
        void onServerImportCompleted(List<Server> servers, int serversExisted,
                int serversUnsupported);

        boolean serverExists(InetAddress address, int port);

        boolean serverExists(String hostname,
                int port); // Called when host was unknown, never know if it'll come back.
    }

    private static class ImportTask extends AsyncTask<Void, Void, Void>
    {
        private final InputStream stream;
        private final WeakReference<ImportCompleteListener> listenerReference;
        private final FileType fileFormat;

        ImportTask(InputStream stream, ImportCompleteListener listener, FileType fileFormat)
        {
            listenerReference = new WeakReference<>(listener);
            this.stream = stream;
            this.fileFormat = fileFormat;
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            ImportCompleteListener listener = listenerReference.get();
            if (listener != null)
            {
                switch (fileFormat)
                {
                case SSLF:
                    doReadSSLF(stream, listener);
                }
            }

            return null;
        }
    }
}
