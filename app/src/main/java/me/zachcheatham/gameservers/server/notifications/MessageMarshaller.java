package me.zachcheatham.gameservers.server.notifications;

import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.core.app.NotificationCompat;
import androidx.core.app.Person;
import me.zachcheatham.gameservers.Constants;
import timber.log.Timber;

public class MessageMarshaller
{
    private static final String JSON_KEY_MESSAGE = "m";
    private static final String JSON_KEY_SENDER = "s";
    private static final String JSON_KEY_TIME = "t";

    static List<NotificationCompat.MessagingStyle.Message> getChatHistory(
            SharedPreferences preferences, int type, int server)
    {
        String key = String.format(Locale.US,
                Constants.PREFS_NOTIFICATION_HISTORY, type, server);
        String raw = preferences.getString(key, null);

        if (raw == null)
            return new ArrayList<>();

        try
        {
            List<NotificationCompat.MessagingStyle.Message> history = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(raw);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject json = jsonArray.getJSONObject(i);
                NotificationCompat.MessagingStyle.Message message = createMessage(json);
                history.add(message);
            }
            return history;
        }
        catch (JSONException e)
        {
            Timber.w(e, "Unable to load chat history for notification %s", key);
            return new ArrayList<>();
        }
    }

    static void saveChatHistory(SharedPreferences preferences, int type, int server,
            List<NotificationCompat.MessagingStyle.Message> messages)
    {
        String key = String.format(Locale.US, Constants.PREFS_NOTIFICATION_HISTORY, type, server);

        JSONArray array = new JSONArray();
        for (int i = 0; i < messages.size(); i++)
        {
            try
            {
                JSONObject object = createMessage(messages.get(i));
                array.put(i, object);
            }
            catch (JSONException e)
            {
                Timber.w(e, "Unable to turn Message into JSON");
            }
        }

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, array.toString());
        editor.apply();
    }

    public static void removeChatHistory(SharedPreferences preferences, int type, int server)
    {
        String key = String.format(Locale.US, Constants.PREFS_NOTIFICATION_HISTORY, type, server);

        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.apply();
    }

    private static NotificationCompat.MessagingStyle.Message createMessage(JSONObject object)
    {
        Person person;

        try
        {
            person = new Person.Builder().setName(object.getString(JSON_KEY_SENDER)).build();
        }
        catch (JSONException e)
        {
            Timber.w(e, "Unable to get sender name from saved message.");
            person = new Person.Builder().setName("JSONException").build();
        }

        String message;
        long timestamp;

        try
        {
            message = object.getString(JSON_KEY_MESSAGE);
        }
        catch (JSONException e)
        {
            Timber.w(e, "Unable to get contents from saved message.");
            message = "JSONException";
        }

        try
        {
            timestamp = object.getLong(JSON_KEY_TIME);
        }
        catch (JSONException e)
        {
            Timber.w(e, "Unable to get timestamp from saved message.");
            timestamp = 0;
        }

        return new NotificationCompat.MessagingStyle.Message(message, timestamp, person);
    }

    private static JSONObject createMessage(NotificationCompat.MessagingStyle.Message message) throws
            JSONException
    {
        JSONObject object = new JSONObject();

        object.put(JSON_KEY_SENDER, message.getPerson().getName());
        object.put(JSON_KEY_TIME, message.getTimestamp());
        object.put(JSON_KEY_MESSAGE, message.getText());

        return object;
    }
}
