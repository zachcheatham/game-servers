package me.zachcheatham.gameservers.server.socket;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.packet.PacketException;
import me.zachcheatham.gameservers.server.packet.minecraft.MinecraftClientHandshakePacket;
import me.zachcheatham.gameservers.server.packet.minecraft.MinecraftClientPacket;
import timber.log.Timber;

public class MinecraftClientSocket
{
    private final int port;
    private final InetAddress address;
    private final String hostname;
    private Socket socket = null;
    private BufferedInputStream bufferedInputStream;
    private int timeout = ServerManager.queryTimeout;

    public MinecraftClientSocket(InetAddress address, int port, String hostname)
    {
        this.hostname = hostname;
        this.address = address;
        this.port = port;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public void send(MinecraftClientPacket packet) throws IOException
    {
        if (socket == null || socket.isClosed())
            socket = new Socket();

        if (!socket.isConnected() || socket.isClosed())
        {
            socket.connect(new InetSocketAddress(address, port), timeout);
            socket.setSoTimeout(timeout);
            bufferedInputStream = new BufferedInputStream(socket.getInputStream());

            MinecraftClientHandshakePacket p = new MinecraftClientHandshakePacket(hostname, port);
            socket.getOutputStream().write(p.getData());
        }

        socket.getOutputStream().write(packet.getData());
    }

    public byte[] receivePacket() throws IOException
    {
        InputStream inputStream = socket.getInputStream();

        // Read total packet length
        int numRead = 0;
        int packetLength = 0;
        byte[] read = new byte[1];
        do
        {
            int bytesRead = inputStream.read(read);
            if (bytesRead == -1)
            {
                close();
                return null;
            }

            int value = (read[0] & 0b01111111);
            packetLength |= (value << (7 * numRead));

            numRead++;
            if (numRead > 5)
                throw new PacketException("Invalid VarInt while receiving packet!");
        }
        while ((read[0] & 0b10000000) != 0);

        byte[] packet = new byte[packetLength];
        int bytesRead = 0;

        while (bytesRead < packetLength)
        {
            packet[bytesRead++] = (byte) inputStream.read();
        }

        if (bytesRead == 0)
        {
            close();
            return null;
        }

        if (bytesRead != packetLength)
            Timber.w("Packet length does not match bytes read: %d v %d", packetLength, bytesRead);

        return packet;
    }

    public void close()
    {
        if (bufferedInputStream != null)
        {
            try
            {
                bufferedInputStream.close();
            }
            catch (IOException ignored)
            {
            }
        }

        if (socket != null)
        {
            try
            {
                socket.close();
            }
            catch (IOException ignored)
            {
            }
        }

        bufferedInputStream = null;
        socket = null;
    }
}
