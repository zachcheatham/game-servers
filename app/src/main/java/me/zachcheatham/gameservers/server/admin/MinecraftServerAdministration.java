package me.zachcheatham.gameservers.server.admin;

import android.content.Context;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ServerCommandSearchCompleteEvent;
import me.zachcheatham.gameservers.server.ChatTarget;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.player.Player;
import timber.log.Timber;

public class MinecraftServerAdministration extends ServerAdministration
{
    private final SparseArray<String> serverMenuItems =  new SparseArray<>();
    final Server server;

    private int menuItemId = 100;

    MinecraftServerAdministration(Server server)
    {
        this.server = server;
        populateCommands();
    }

    private void populateCommands()
    {
        if (ServerManager.getInstance() != null)
        {
            try
            {
                final KnownCommandLookup commandLookup = new KnownCommandLookup(
                        ServerManager.getInstance().getContext(),
                        KnownCommandLookup.MOD_IDENTIFIER_MINECRAFT);

                Server.RCONResponseCallback helpCallback = response ->
                {
                    String[] commands = response.trim().split("/");
                    for (String c : commands)
                    {
                        if (!c.isEmpty() && !c.contains("->")) // ignore alias commands and empty lines
                        {
                            String cmd = c;
                            int s = c.indexOf(' ');
                            if (s > 0)
                                cmd = cmd.substring(0, s);
                            Command command = commandLookup.lookupCommand(cmd);
                            if (command != null)
                            {
                                if (command.hasPlayerArgument())
                                    playerCommands.put(cmd, command);
                                else
                                    serverCommands.put(cmd, command);
                            }
                        }
                    }
                    EventBus.getDefault().post(new ServerCommandSearchCompleteEvent(server));
                };
                server.sendInternalAdminCommand("help", helpCallback);
            }
            catch (IOException e)
            {
                Timber.e(e, "IO Error while opening command JSON");
            }
        }
        else
        {
            Timber.w("Cannot lookup known commands without a ServerManager instance.");
        }
    }

    @Override
    public void populateServerMenu(Context context, MenuInflater inflater, Menu menu)
    {
        addMenuItem(menu, "bossbar", R.string.command_name_mc_bossbar);

        SubMenu serverMenu = menu.addSubMenu(R.string.action_control_server);
        addMenuItem(serverMenu, "function", R.string.action_run_function);
        addMenuItem(serverMenu, "reload", R.string.action_reload_server);
        addMenuItem(serverMenu, "save-all", R.string.action_save_server);
        addMenuItem(serverMenu, "save-off", R.string.action_disable_saving);
        addMenuItem(serverMenu, "save-on", R.string.action_enable_saving);
        addMenuItem(serverMenu, "stop", R.string.action_stop_server);

        SubMenu configMenu = menu.addSubMenu(R.string.server_config);
        addMenuItem(configMenu, "datapack", R.string.action_manage_datapacks);
        addMenuItem(configMenu, "defaultgamemode", R.string.action_set_default_gamemode);
        addMenuItem(configMenu, "difficulty", R.string.action_set_difficulty);
        addMenuItem(configMenu, "gamerule", R.string.action_manage_gamerules);
        addMenuItem(configMenu, "save-on", R.string.action_enable_saving);
        addMenuItem(configMenu, "scoreboard", R.string.action_manage_scoreboard);
        addMenuItem(configMenu, "seed", R.string.action_get_seed);
        addMenuItem(configMenu, "setidletimeout", R.string.action_set_idle_timeout);
        addMenuItem(configMenu, "setworldspawn", R.string.action_set_world_spawn);
        addMenuItem(configMenu, "tag", R.string.action_manage_tags);
        addMenuItem(configMenu, "team", R.string.action_manage_teams);
        addMenuItem(configMenu, "whitelist", R.string.action_manage_whitelist);

        SubMenu manipulationMenu = menu.addSubMenu(R.string.action_manipulate_world);
        addMenuItem(manipulationMenu, "clone", R.string.action_clone_blocks);
        addMenuItem(manipulationMenu, "data", R.string.block_data);
        addMenuItem(manipulationMenu, "fill", R.string.action_fill_region);
        addMenuItem(manipulationMenu, "particle", R.string.action_create_particle);
        addMenuItem(manipulationMenu, "playsound", R.string.action_play_sound);
        addMenuItem(manipulationMenu, "replaceitem", R.string.action_replace_item);
        addMenuItem(manipulationMenu, "setblock", R.string.action_set_block);
        addMenuItem(manipulationMenu, "stopsound", R.string.action_stop_sound);
        addMenuItem(manipulationMenu, "summon", R.string.action_summon_entity);
        addMenuItem(manipulationMenu, "time", R.string.action_set_time);
        addMenuItem(manipulationMenu, "trigger", R.string.action_activate_trigger);
        addMenuItem(manipulationMenu, "weather", R.string.action_control_weather);
        addMenuItem(manipulationMenu, "worldborder", R.string.action_manage_world_border);

        SubMenu playersMenu = menu.addSubMenu(R.string.action_manage_players);
        addMenuItem(playersMenu, "ban", R.string.action_ban_player);
        addMenuItem(playersMenu, "ban-ip", R.string.action_ban_ip);
        addMenuItem(playersMenu, "gamemode", R.string.action_set_gamemode);
        addMenuItem(playersMenu, "op", R.string.action_add_operator);
        addMenuItem(playersMenu, "deop", R.string.action_remove_operator);
        addMenuItem(playersMenu, "give", R.string.action_give_items);
        addMenuItem(playersMenu, "kick", R.string.action_kick_player);
        addMenuItem(playersMenu, "kill", R.string.action_kill_player);
        addMenuItem(playersMenu, "pardon", R.string.action_unban_player);
        addMenuItem(playersMenu, "recipe", R.string.action_manage_recipes);
        addMenuItem(playersMenu, "spawnpoint", R.string.action_set_spawn_point);
        addMenuItem(playersMenu, "spreadplayers", R.string.action_spread_players);
        addMenuItem(playersMenu, "teleport", R.string.action_teleport);
        addMenuItem(playersMenu, "title", R.string.action_manage_title);
        addMenuItem(playersMenu, "xp", R.string.action_manage_experience);

        SubMenu chatMenu = menu.addSubMenu(R.string.action_message_server);
        addMenuItem(chatMenu, "me", R.string.action_send_me_message);
        addMenuItem(chatMenu, "msg", R.string.action_send_private_message);
        addMenuItem(chatMenu, "say", R.string.action_say_message);
    }

    @Override
    public void populatePlayerMenu(String subMenu, Context context, List<PlayerMenuItem> items, Player player)
    {
        for (Map.Entry<String, Command> entry : playerCommands.entrySet())
        {
            Command command = entry.getValue();
            String title;
            if (command.nameResourceID != 0)
                title = context.getString(command.nameResourceID);
            else
                title = command.name;

            items.add(new PlayerMenuItem(command.command, title, command.iconResourceID));
        }
    }

    @Override
    public Command getMenuItemCommand(MenuItem menuItem)
    {
        return getCommand(serverMenuItems.get(menuItem.getItemId()));
    }

    @Override
    public boolean sendChatMessage(String message, ChatTarget target, Server server)
    {
        return false;
    }

    @Override
    public boolean handlesChatTarget(ChatTarget target)
    {
        return false;
    }

    @Override
    public ChatTarget[] getChatTargets()
    {
        return null;
    }

    @Override
    public Command getKickCommand()
    {
        return playerCommands.get("kickid");
    }

    @Override
    public Command getBanCommand()
    {
        return playerCommands.get("banid");
    }

    private void addMenuItem(Menu menu, String command, int titleResource)
    {
        if (hasCommand(command) || hasPlayerCommand(command))
        {
            serverMenuItems.put(menuItemId, command);
            menu.add(Menu.NONE, menuItemId++, Menu.NONE, titleResource);
        }
    }
}
