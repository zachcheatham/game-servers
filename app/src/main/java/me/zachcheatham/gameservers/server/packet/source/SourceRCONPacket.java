package me.zachcheatham.gameservers.server.packet.source;

import me.zachcheatham.gameservers.server.packet.Packet;

public class SourceRCONPacket extends Packet
{
    public static final short PACKET_LENGTH = 4096;
    public static final int PACKET_TYPE_AUTH = 3;
    public static final int PACKET_TYPE_AUTH_RESPONSE = 2;
    public static final int PACKET_TYPE_EXECCOMMAND = 2;
    public static final int PACKET_TYPE_RESPONSE_VALUE = 0;

    public final int id;
    public final int type;
    public final String body;

    public SourceRCONPacket(int id, int packetType, String body)
    {
        super(PACKET_LENGTH);
        this.id = id;
        this.type = packetType;
        this.body = null;
        writeInt(0); // Size
        writeInt(id);
        writeInt(packetType);
        writeString(body);
        writeString("");
        buffer.putInt(0, buffer.position() - 4); // Override size ignoring placeholder size variable
    }

    public SourceRCONPacket(byte[] data)
    {
        super(data);
        id = readInt();
        type = readInt();

        body = readString();
    }
}
