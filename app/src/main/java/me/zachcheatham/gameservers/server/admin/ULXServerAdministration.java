package me.zachcheatham.gameservers.server.admin;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.TimeHelper;
import me.zachcheatham.gameservers.events.ServerCommandSearchCompleteEvent;
import me.zachcheatham.gameservers.server.ChatMessage;
import me.zachcheatham.gameservers.server.ChatTarget;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.player.Player;

public class ULXServerAdministration extends SourceServerAdministration
{
    // Regex
    private static final Pattern PATTERN_CATEGORY = Pattern.compile("Category:\\s(.+)");
    private static final Pattern PATTERN_COMMAND = Pattern.compile("o\\sulx\\s(\\w+)\\s([^-]+)" +
                                                                   "?-\\s(.+)$");
    private static final Pattern PATTERN_OPPOSITE = Pattern.compile("\\(opposite: ([^)]+)\\)");
    private static final Pattern PATTERN_ADMIN_CHAT = Pattern.compile("\\[ULX]\\s+(.+) to " +
                                                                      "(\\w+): (.+)");

    // Command detection commands
    private static final String ULX_HTLP = "ulx help";

    // Menu ID
    private static final int MENU_ID_CHANGE_MAP = 1;
    private static final int MENU_ID_ADD_BAN = 2;
    private static final int MENU_ID_UNBAN_PLAYER = 3;

    // A map containing which category a command belongs to
    private Map<String, String> commandCategories = new HashMap<>();
    // A map containing unique categories and a boolean representing if they contain player commands
    private Map<String, Boolean> categories = new HashMap<>();

    ULXServerAdministration(Server server)
    {
        super(server);
    }

    @Override
    public void populateCommands()
    {
        Server.RCONResponseCallback helpCallback = response ->
        {
            BufferedReader reader = new BufferedReader(new StringReader(response));
            try
            {
                String line;
                String category = "";
                while ((line = reader.readLine()) != null)
                {
                    Matcher matcher = PATTERN_CATEGORY.matcher(line);
                    if (matcher.find())
                    {
                        category = matcher.group(1);
                        categories.put(category, false); // Add newly found category to mapping with default "no player cmds"
                    }
                    else if (!category.isEmpty())
                    {
                        matcher = PATTERN_COMMAND.matcher(line);
                        if (matcher.find())
                        {
                            String name = matcher.group(1);
                            String args = matcher.group(2);
                            String desc = matcher.group(3);

                            createCommand(category, name, args, desc);
                        }
                    }
                }
            }
            catch (IOException ignored)
            {
                // don't know why this would ever happen
            }
            EventBus.getDefault().post(new ServerCommandSearchCompleteEvent(server));
        };
        server.sendInternalAdminCommand(ULX_HTLP, helpCallback); // Request plugin list
    }

    @Override
    public void populateServerMenu(Context context, MenuInflater inflater, Menu menu)
    {
        if (hasCommand("ulx map"))
            menu.add(Menu.NONE, MENU_ID_CHANGE_MAP, Menu.NONE, R.string.action_change_map);
        if (hasCommand("ulx banid"))
            menu.add(Menu.NONE, MENU_ID_ADD_BAN, Menu.NONE, R.string.action_add_ban);
        if (hasCommand("ulx unban"))
            menu.add(Menu.NONE, MENU_ID_UNBAN_PLAYER, Menu.NONE, R.string.action_unban_player);

        // todo create command browser
    }

    @Override
    public Command getMenuItemCommand(MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
        case MENU_ID_CHANGE_MAP:
            return serverCommands.get("ulx map");
        case MENU_ID_ADD_BAN:
            return serverCommands.get("ulx banid");
        case MENU_ID_UNBAN_PLAYER:
            return serverCommands.get("ulx unban");
        default:
            return null;
        }
    }

    @Override
    public void populatePlayerMenu(String subMenu, Context context, List<PlayerMenuItem> items, Player player)
    {
        if (subMenu == null)
        {
            // View Steam Profile
            items.add(new PlayerMenuItem(Constants.PLAYER_ADMIN_STEAM_PROFILE,
                    context.getString(R.string.action_view_steam_profile, player.getName()),
                    R.drawable.ic_steam));

            // Add categories
            for (String category : categories.keySet())
            {
                // Check if category has player commands first
                if (categories.get(category) == Boolean.TRUE)
                {
                    items.add(new PlayerMenuItem("_s_" + category, category,
                            getCategoryIcon(category)));
                }
            }
        }
        else
        {
            String category = subMenu.substring(Constants.PLAYER_SUBMENU_PREFIX.length() - 2);
            // Add known player commands
            for (Map.Entry<String, String> entry : commandCategories.entrySet())
            {
                if (entry.getValue().equals(category)) // Value is the category of the key (command)
                {
                    Command command = playerCommands.get(entry.getKey());

                    if (command != null) // Command is actually player command
                    {
                        String title;
                        if (command.name == null)
                        {
                            if (command.nameResourceID != 0)
                                title = context.getString(command.nameResourceID);
                            else
                                title = command.command;
                        }
                        else
                        {
                            title = command.name;
                        }

                        items.add(new PlayerMenuItem(command.command, title, R.drawable.ic_empty));
                    }
                }
            }
        }
    }

    @Override
    public Command getKickCommand()
    {
        if (hasPlayerCommand("ulx kick")) // Check that sm_kick is installed
            return playerCommands.get("ulx kick");
        else
            // Default to Source Engine Ban Command
            return super.getKickCommand();
    }

    @Override
    public Command getBanCommand()
    {
        if (hasPlayerCommand("ulx ban")) // Check that sm_ban is installed
            return playerCommands.get("ulx ban");
        else
            // Default to Source Engine Ban Command
            return super.getBanCommand();
    }

    /**
     * Create and store a command
     * @param name Command Name (eg "ulx who")
     * @param unparsedArgs Arguments portion of the help line
     * @param description Description portion of the help line
     */
    private void createCommand(String category, String name, String unparsedArgs, String description)
    {
        Command command = new Command();
        command.command = "ulx " + name;

        // Attempt to find predefined string resources for command
        if (ServerManager.getInstance() != null)
        {
            Context context = ServerManager.getInstance().getContext();
            command.nameResourceID = CommandResourceHelper.getCommandName(context, command);
            command.descriptionResourceID = CommandResourceHelper.getCommandDescription(context, command);
        }

        // Generate name if one wasn't found
        if (command.nameResourceID == 0)
            command.name = name.substring(0,1).toUpperCase() + name.substring(1);

        // Use help description if one wasn't found
        if (command.descriptionResourceID == 0)
        {
            command.description = description.replaceAll("\\(say: [^)]+\\)", "")
                                             .replaceAll("\\(opposite: [^)]+\\)", "")
                                             .trim();
        }

        // Determine arguments
        if (unparsedArgs != null)
            command.arguments = gatherArguments(unparsedArgs);

        // Create an opposite command if defined in description
        Matcher oppositeMatcher = PATTERN_OPPOSITE.matcher(description);
        if (oppositeMatcher.find())
            createOppositeCommand(category, oppositeMatcher.group(1), command.arguments);


        commandCategories.put(command.command, category); // Associate command to category
        if (command.hasPlayerArgument())
        {
            playerCommands.put(command.command, command); // Add to player commands list
            categories.put(category, true); // Indicate category to has player commands
        }
        else
            serverCommands.put(command.command,
                    command); // Stick in in the plain old regular commands list*/
    }

    private void createOppositeCommand(String category, String name, CommandArgument[] parentArgs)
    {
        int cutArgIndex = -1;
        for (int i = 0; i < parentArgs.length; i++)
        {
            if (parentArgs[i].optional)
            {
                cutArgIndex = i;
                break;
            }
        }

        CommandArgument[] arguments;
        if (cutArgIndex != -1)
            arguments = Arrays.copyOfRange(parentArgs, 0, cutArgIndex);
        else
            arguments = Arrays.copyOf(parentArgs, parentArgs.length);

        Command command = new Command();
        command.command = name;

        // Attempt to find predefined string resources for command
        if (ServerManager.getInstance() != null)
        {
            Context context = ServerManager.getInstance().getContext();
            command.nameResourceID = CommandResourceHelper.getCommandName(context, command);
            command.descriptionResourceID = CommandResourceHelper.getCommandDescription(context, command);
        }

        // Generate name if one wasn't found
        if (command.nameResourceID == 0)
            command.name =  name.substring(4,5).toUpperCase() + name.substring(5);

        command.arguments = arguments;

        commandCategories.put(name, category); // Associate command to category
        if (command.hasPlayerArgument())
        {
            playerCommands.put(name, command); // Add to player commands list
            categories.put(category, true); // Indicate category to has player commands
        }
        else
            serverCommands.put(name, command); // Add to server commands list
    }

    private CommandArgument[] gatherArguments(String unparsedArgs)
    {
        List<CommandArgument> args = new ArrayList<>();

        boolean readingArg = false;
        boolean optional = false;
        boolean finalize = false;
        boolean firstLetter = true;
        String[] bounds;
        int boundsStart;
        int boundsEnd;
        char c;
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < unparsedArgs.length(); i++)
        {
            c = unparsedArgs.charAt(i);

            if (!readingArg)
            {
                if (c == '<' || c == '{')
                {
                    readingArg = true;
                    optional = false;
                }
                else if (c == '[')
                {
                    readingArg = true;
                    optional = true;
                }
            }
            else
            {
                if (c == '>' || c == '}')
                {
                    if (!optional)
                        finalize = true;
                }
                else if (c == ']')
                {
                    if (optional)
                        finalize = true;
                }

                if (!finalize)
                {
                    if (firstLetter)
                    {
                        if (Character.isLetter(c))
                        {
                            c = Character.toUpperCase(c);
                            firstLetter = false;
                        }
                    }
                    else if (c == ' ')
                        firstLetter = true;

                    name.append(c);
                }
            }

            if (finalize)
            {
                finalize = false;
                readingArg = false;
                CommandArgument arg = new CommandArgument();
                arg.type = CommandArgument.TYPE_STRING;

                if (name.charAt(0) == '<' || name.charAt(0) == '{')
                {
                    name.deleteCharAt(0);
                    name.deleteCharAt(name.length()-1);
                }

                String n = name.toString();
                name = new StringBuilder();

                if (n.contains(": 0/1")) // Going to be a boolean argument
                {
                    arg.type = CommandArgument.TYPE_INTBOOL;
                    n = n.replace(": 0/1", "");
                }
                else if (n.contains(": ")) // Going to be a numeric argument
                {
                    // Check if argument is time
                    if (n.startsWith("Seconds"))
                        arg.type = CommandArgument.TYPE_TIME_SECONDS;
                    else if (n.startsWith("Minutes"))
                        arg.type = CommandArgument.TYPE_TIME_MINUTES;
                    else
                        arg.type = CommandArgument.TYPE_INT;

                    boundsStart = n.indexOf(": ");
                    boundsEnd = n.toLowerCase().indexOf(", default");
                    if (boundsEnd == -1)
                        boundsEnd = n.length();
                    bounds = n.substring(boundsStart + 2, boundsEnd).split("<=");
                    n = n.substring(0, boundsStart) + n.substring(boundsEnd);

                    if (bounds[0].equals("x"))
                    {
                        arg.max = Float.valueOf(bounds[1]);
                        arg.min = arg.max; // if min == max, we know there is no min
                    }
                    else if (bounds.length == 2)
                    {
                        arg.min = Float.valueOf(bounds[0]);
                        arg.max = arg.min - 1; // if max < min, we know there is no max
                    }
                    else
                    {
                        arg.min = Float.valueOf(bounds[0]);
                        arg.max = Float.valueOf(bounds[2]);
                    }
                }

                // Because we're not in game, a defaults to self player argument is required.
                if (n.contains(", defaults to self"))
                {
                    optional = false;
                    // remove that string from our name
                    n = n.replace(", defaults to self", "");
                }


                arg.id = n.toLowerCase();

                if (ServerManager.getInstance() != null)
                {
                    Context context = ServerManager.getInstance().getContext();
                    arg.nameResourceId = CommandResourceHelper.getArgumentName(context, arg.id);
                }

                if (arg.nameResourceId == 0)
                    arg.name = n;

                arg.playerArgument =  n.toLowerCase().equals("player") || n.toLowerCase().equals("players");
                arg.optional = optional;
                args.add(arg);

                firstLetter = true;
            }
        }

        return args.toArray(new CommandArgument[0]);
    }

    private static int getCategoryIcon(String category)
    {
        switch (category)
        {
        case "Utility":
            return R.drawable.ic_toolbox;
        case "Voting":
            return R.drawable.ic_ballot_white;
        case "Rcon":
            return R.drawable.ic_console_line_white;
        case "User Management":
            return R.drawable.ic_people_white;
        case "TTT":
            return R.drawable.ic_ttt;
        case "Chat":
            return R.drawable.ic_message_white;
        case "Fun":
            return R.drawable.ic_ghost_white;
        case "Teleport":
            return R.drawable.ic_redo;
        }

        return R.drawable.ic_empty;
    }

    @Override
    public boolean parseLogLine(String line, Server server)
    {
        // Chat message
        Matcher m = PATTERN_ADMIN_CHAT.matcher(line);
        if (m.find())
        {
            String playerName = m.group(1);
            int playerKey;
            if (playerName.equals("(Console)"))
                playerKey = -1;
            else
                playerKey = playerName.hashCode();

            String target = m.group(2);
            String message = m.group(3);

            ChatMessage chatMessage;
            if (target.equals("admins"))
            {
                chatMessage = new ChatMessage(message, playerKey,
                        TimeHelper.getUnixTimestamp(),
                        ChatMessage.MessageType.ADMIN);
            }
            else
            {
                chatMessage = new ChatMessage(message, playerKey,
                        TimeHelper.getUnixTimestamp(),
                        ChatMessage.MessageType.PM, target);
            }
            server.addToChatLog(chatMessage);

            return true;
        }

        return false;
    }

    @Override
    public ChatTarget[] getChatTargets()
    {
        return new ChatTarget[] {
                new ChatTarget(ChatMessage.MessageType.ADMIN)
        };
    }

    @Override
    public boolean handlesChatTarget(ChatTarget target)
    {
        return target.type == ChatMessage.MessageType.ADMIN;
    }

    @Override
    public boolean sendChatMessage(String message, ChatTarget target, Server server)
    {
        server.sendInternalAdminCommand(String.format("ulx asay %s", message), null);
        return server.loggingUnavailable();
    }
}
