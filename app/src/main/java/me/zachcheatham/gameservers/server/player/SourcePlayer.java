package me.zachcheatham.gameservers.server.player;

import java.util.Locale;

import me.zachcheatham.gameservers.events.ServerUpdatedEvent;
import me.zachcheatham.gameservers.server.player.copy.PlayerCopyMenuPopulator;
import me.zachcheatham.gameservers.server.player.copy.SourcePlayerCopyMenuPopulator;

public class SourcePlayer extends Player
{
    private static final long STEAM64_CHANGE = 76561197960265728L;

    public static final int PLAYER_CHANGE_SCORE = 10;
    public static final int PLAYER_CHANGE_PING = 11;
    public static final int PLAYER_CHANGE_TIME = 12;

    private long score;
    private int time;
    private int userID = -1;
    private String steamID = null;
    private int ping = -1;
    private String address = null;
    private SteamIDType steamIDType = SteamIDType.Unknown;

    public SourcePlayer(String name, long score, float time)
    {
        super(name);
        this.score = score;
        this.time = (int) Math.floor(time);
    }

    @Override
    public boolean hasPrivateInfo()
    {
        return userID != -1;
    }

    public long getScore()
    {
        return score;
    }

    public int getTime()
    {
        return time + (currentTime() - timeUpdated);
    }

    public String getTimeString()
    {
        int t = getTime();
        return String.format(Locale.US, "%02d:%02d:%02d", t / 3600, (t % 3600) / 60, t % 60);
    }

    @Override
    public int getUserID()
    {
        return userID;
    }

    public void setUserID(int userID)
    {
        this.userID = userID;
    }

    public String getSteamID()
    {
        if (steamIDType == SteamIDType.Unknown)
            detectSteamID();

        return convertSteamIDToSteamID(steamID, steamIDType);
    }

    public String getSteamID3()
    {
        if (steamIDType == SteamIDType.Unknown)
            detectSteamID();

        return convertSteamIDToSteamID3(steamID, steamIDType);
    }

    public String getSteamID64()
    {
        if (steamIDType == SteamIDType.Unknown)
            detectSteamID();

        return convertSteamIDToSteamID64(steamID, steamIDType);
    }

    public void setSteamID(String steamID)
    {
        this.steamID = steamID;
    }

    private void detectSteamID()
    {
        this.steamIDType = detectSteamIDType(steamID);
    }

    public int getPing()
    {
        return ping;
    }

    public void setPing(int ping)
    {
        this.ping = ping;
    }

    public void setIPAddress(String address)
    {
        this.address = address;
    }

    @Override
    public String getIPAddress()
    {
        return address;
    }

    @Override
    public String getUniqueID()
    {
        return steamID;
    }

    @Override
    public PlayerCopyMenuPopulator getCopyMenuPopulator()
    {
        return new SourcePlayerCopyMenuPopulator(this);
    }

    @Override
    public void merge(Player player, ServerUpdatedEvent event)
    {
        SourcePlayer sourcePlayer = (SourcePlayer) player;

        if (sourcePlayer.score != score)
        {
            score = sourcePlayer.score;
            if (event != null) event.addPlayerChange(PLAYER_CHANGE_SCORE);
        }

        if (sourcePlayer.time != getTime())
        {
            time = sourcePlayer.time;
            if (event != null) event.addPlayerChange(PLAYER_CHANGE_TIME);
        }

        timeUpdated = getTime();
    }

    public static SteamIDType detectSteamIDType(String input)
    {
        if (input != null && input.length() > 4)
        {
            if (input.charAt(0) == 'S')
                return SteamIDType.SteamID;
            else if (input.charAt(1) == 'U')
                return SteamIDType.SteamID3;
            else if (Character.isDigit(input.charAt(0)) && input.length() == 17)
                return SteamIDType.SteamID64;
        }

        return SteamIDType.Unknown;
    }

    public static String convertSteamIDToSteamID3(String input, SteamIDType inputType)
    {
        if (inputType == SteamIDType.Unknown)
            inputType = detectSteamIDType(input);

        if (inputType == SteamIDType.SteamID)
        {
            int server = Integer.parseInt(input.substring(8,9));
            int id = Integer.parseInt(input.substring(10));

            return String.format(Locale.US, "[U:1:%d]", 2 * id + server);
        }
        else if (inputType == SteamIDType.SteamID64)
        {
            int id = (int) (Long.parseLong(input) - STEAM64_CHANGE);
            return String.format(Locale.US, "[U:1:%d]", id);
        }
        else if (inputType == SteamIDType.SteamID3)
        {
            return input;
        }

        return null;
    }

    public static String convertSteamIDToSteamID64(String input, SteamIDType inputType)
    {
        if (inputType == SteamIDType.Unknown)
            inputType = detectSteamIDType(input);

        if (inputType == SteamIDType.SteamID)
        {
            int server = Integer.parseInt(input.substring(8,9));
            int id = Integer.parseInt(input.substring(10));

            long result = (2 * id + server) + STEAM64_CHANGE;
            return String.valueOf(result);
        }
        else if (inputType == SteamIDType.SteamID3)
        {
            long result = Long.parseLong(input.substring(5, input.length() - 1)) + STEAM64_CHANGE;
            return String.valueOf(result);
        }
        else if (inputType == SteamIDType.SteamID64)
        {
            return input;
        }

        return null;
    }

    public static String convertSteamIDToSteamID(String input, SteamIDType inputType)
    {
        if (inputType == SteamIDType.Unknown)
            inputType = detectSteamIDType(input);

        if (inputType == SteamIDType.SteamID3)
        {
            long id = Long.parseLong(input.substring(5, input.length() - 1));
            System.out.println(id);
            return String.format(Locale.US, "STEAM_0:%d:%d",
                    (id % 2 == 0) ? 0 : 1, (int) Math.floor((double) id / 2L));
        }
        else if (inputType == SteamIDType.SteamID64)
        {
            long id = Long.parseLong(input) - STEAM64_CHANGE;
            return String.format(Locale.US, "STEAM_0:%d:%d",
                    (id % 2 == 0) ? 0 : 1, (int) Math.floor((double) id / 2L));
        }
        else if (inputType == SteamIDType.SteamID)
        {
            return input;
        }

        return null;
    }

    public enum SteamIDType {
        SteamID, SteamID3, SteamID64, Unknown
    }
}
