package me.zachcheatham.gameservers.server.admin;

import android.content.Context;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.R;
import me.zachcheatham.gameservers.events.ServerCommandSearchCompleteEvent;
import me.zachcheatham.gameservers.server.BaseSourceServer;
import me.zachcheatham.gameservers.server.ChatMessage;
import me.zachcheatham.gameservers.server.ChatTarget;
import me.zachcheatham.gameservers.server.Server;
import me.zachcheatham.gameservers.server.ServerManager;
import me.zachcheatham.gameservers.server.WordFormatter;
import me.zachcheatham.gameservers.server.player.Player;
import timber.log.Timber;

class SourceModServerAdministration extends SourceServerAdministration
{
    // Regex
    //01 "Admin File Reader" (1.9.0.6275) by AlliedModders LLC
    private static final Pattern MOD_MATCH = Pattern
            .compile("^\\s+(\\d+)\\s+\"(.+)\"\\s+\\((.+)\\)\\s+by\\s+(.+)$", Pattern.MULTILINE);
    //sm_chat           admin        sm_chat <message> - sends message to admins
    private static final Pattern CMD_MATCH = Pattern
            .compile("^\\s+(\\S+)\\s+(\\w+)\\s+?(.+)$", Pattern.MULTILINE);
    private static final Pattern ARG_MATCH = Pattern.compile("[<\\[][^>\\]]+[>\\]]");
    private static final Pattern ARG_NAME_MATCH = Pattern.compile("[^<>\\[\\]|]+");

    // Command detection commands
    private static final String SM_PLUGINS = "sm plugins list";
    private static final String SM_CMDS = "sm cmds %d";

    // Menu ID
    private static final int MENU_ID_MESSAGE_SERVER = 1;
    private static final int MENU_ID_CHANGE_MAP = 2;
    private static final int MENU_ID_ADD_BAN = 3;
    private static final int MENU_ID_ADD_IP_BAN = 4;
    private static final int MENU_ID_UNBAN_PLAYER = 5;

    private int searchedPlugins = 0;
    private SparseArray<Plugin> installedPlugins = new SparseArray<>();
    private KnownCommandLookup commandLookup;

    private Server.RCONResponseCallback commandListCallback = response ->
    {
        Matcher matcher = CMD_MATCH.matcher(response);
        while (matcher.find())
        {
            String cmd = matcher.group(1);
            Command command = null;

            if (commandLookup != null)
                command = commandLookup.lookupCommand(cmd);

            if (command == null)
            {
                command = new Command();
                command.command = cmd;

                // Attempt to find predefined string resources for command
                if (ServerManager.getInstance() != null)
                {
                    Context context = ServerManager.getInstance().getContext();
                    command.nameResourceID = CommandResourceHelper.getCommandName(context, command);
                    command.descriptionResourceID = CommandResourceHelper.getCommandDescription(context, command);
                }

                if (command.nameResourceID == 0)
                {
                    StringBuilder nameBuilder = new StringBuilder();
                    String[] cmdarr = cmd.split("_");
                    for (String pt : cmdarr)
                        if (!pt.equals("sm"))
                        {
                            StringBuffer ptb = new StringBuffer(pt);
                            if (WordFormatter.format(ptb))
                            {
                                nameBuilder.append(ptb.toString());
                            }
                            else
                            {
                                nameBuilder.append(pt.substring(0, 1).toUpperCase());
                                nameBuilder.append(pt.substring(1));
                            }
                            nameBuilder.append(' ');
                        }
                    command.name = nameBuilder.toString().trim();
                }

                // Get command arguments from help text
                String[] help = matcher.group(3).replace(matcher.group(1), "").trim().split("-");
                if (help[0].contains("<") || help[0].contains("[")) // we extracted arguments
                {
                    if (command.descriptionResourceID == 0 && help.length > 1) // Help text contained description
                        command.description = help[1].trim();

                    List<CommandArgument> arguments = new ArrayList<>();
                    Matcher argMatcher = ARG_MATCH.matcher(help[0]);
                    while (argMatcher.find())
                    {
                        CommandArgument arg = new CommandArgument();
                        String argString = argMatcher.group(0);
                        StringBuilder argNameBuilder = new StringBuilder();
                        Matcher argNameMatcher = ARG_NAME_MATCH.matcher(argString);
                        while (argNameMatcher.find())
                        {
                            StringBuffer argName = new StringBuffer(argNameMatcher.group(0));
                            if (WordFormatter.format(argName))
                            {
                                argNameBuilder.append(argName.toString());
                            }
                            else
                            {
                                argNameBuilder
                                        .append(argNameMatcher.group(0).substring(0, 1)
                                                              .toUpperCase());
                                argNameBuilder.append(argNameMatcher.group(0).substring(1));
                            }
                            argNameBuilder.append(" / ");
                        }
                        arg.name  = argNameBuilder.toString();
                        arg.name = arg.name.substring(0, arg.name.length() - 3);
                        arg.id = arg.name.toLowerCase().replaceAll(" / ", "_");

                        // Attempt to determine argument type
                        if (arg.id.contains("minutes") ||
                            arg.id.toLowerCase().contains("time") ||
                            arg.id.toLowerCase().contains("length"))
                            arg.type = CommandArgument.TYPE_TIME_MINUTES;
                        else if (arg.id.equals("0/1"))
                        {
                            // Boolean argument
                            arg.id = "active";
                            arg.type = CommandArgument.TYPE_INTBOOL;
                        }
                        else if (arg.id.contains("userid"))
                        {
                            arg.id = "player";
                            arg.type = CommandArgument.TYPE_SM_PLAYER;
                        }
                        else
                            // Default to a String
                            arg.type = CommandArgument.TYPE_STRING;

                        // Attempt to find predefined string resources
                        if (ServerManager.getInstance() != null)
                        {
                            Context context = ServerManager.getInstance().getContext();
                            arg.nameResourceId = CommandResourceHelper.getArgumentName(context, arg.id);
                        }

                        arg.optional = argString.startsWith("[");
                        arguments.add(arg);
                    }

                    command.arguments = arguments.toArray(new CommandArgument[0]);
                }
                else if (command.descriptionResourceID == 0)
                {
                    command.description = help[0];
                }
            }
            else
            {
                Timber.d("Successfully found predefined command for %s", cmd);
            }

            if (command.hasPlayerArgument())
                playerCommands.put(command.command, command);
            else
                serverCommands.put(command.command, command);
        }

        if (++searchedPlugins == installedPlugins.size())
        {
            EventBus.getDefault().post(new ServerCommandSearchCompleteEvent(server));
            commandLookup = null;
        }
    };

    SourceModServerAdministration(BaseSourceServer server)
    {
        super(server);
    }

    @Override
    void populateCommands()
    {
        if (ServerManager.getInstance() != null)
        {
            try
            {
                commandLookup = new KnownCommandLookup(
                        ServerManager.getInstance().getContext(),
                        KnownCommandLookup.MOD_IDENTIFIER_SOURCEMOD);
            }
            catch (IOException e)
            {
                Timber.e(e, "Unable to create known command lookup.");
            }
        }
        else
            Timber.w("Cannot lookup known commands without a ServerManager instance.");

        Server.RCONResponseCallback pluginListCallback = response ->
        {
            // Match each line of sm plugins output
            Matcher matcher = MOD_MATCH.matcher(response);
            while (matcher.find())
                // Save plugin existence
                installedPlugins.put(
                        Integer.parseInt(matcher.group(1)), // plugin index
                        new Plugin(
                                matcher.group(2), // plugin name
                                matcher.group(3), // plugin version
                                matcher.group(4))); // plugin author

            // Request cmds for each plugin
            for (int i = 0; i < installedPlugins.size(); i++)
            {
                int pluginID = installedPlugins.keyAt(i);
                server.sendInternalAdminCommand(String.format(Locale.US, SM_CMDS, pluginID),
                        commandListCallback);
            }
        };
        server.sendInternalAdminCommand(SM_PLUGINS, pluginListCallback); // Request plugin list
    }

    @Override
    public void populateServerMenu(Context context, MenuInflater inflater, Menu menu)
    {
        menu.add(Menu.NONE, MENU_ID_MESSAGE_SERVER, Menu.NONE, R.string.action_message_server);
        if (hasCommand("sm_map"))
            menu.add(Menu.NONE, MENU_ID_CHANGE_MAP, Menu.NONE, R.string.action_change_map);
        if (hasCommand("sm_addban"))
            menu.add(Menu.NONE, MENU_ID_ADD_BAN, Menu.NONE, R.string.action_add_ban);
        if (hasCommand("sm_banip"))
            menu.add(Menu.NONE, MENU_ID_ADD_IP_BAN, Menu.NONE, R.string.action_add_ip_ban);
        if (hasCommand("sm_unban"))
            menu.add(Menu.NONE, MENU_ID_UNBAN_PLAYER, Menu.NONE, R.string.action_unban_player);
    }

    @Override
    public Command getMenuItemCommand(MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
        case MENU_ID_CHANGE_MAP:
            return serverCommands.get("sm_map");
        case MENU_ID_ADD_BAN:
            return serverCommands.get("sm_addban");
        case MENU_ID_ADD_IP_BAN:
            return serverCommands.get("sm_banip");
        case MENU_ID_UNBAN_PLAYER:
            return serverCommands.get("sm_unban");
        }

        return null;
    }

    @Override
    public void populatePlayerMenu(String subMenu, Context context, List<PlayerMenuItem> items, Player player)
    {
        // View Steam Profile
        items.add(new PlayerMenuItem(Constants.PLAYER_ADMIN_STEAM_PROFILE, context.getString(R.string.action_view_steam_profile, player.getName()), R.drawable.ic_steam));

        // Add known player commands
        for (Map.Entry<String, Command> entry : playerCommands.entrySet())
        {
            Command command = entry.getValue();
            String title;
            if (command.nameResourceID != 0)
                title = context.getString(command.nameResourceID);
            else
                title = command.name;

            items.add(new PlayerMenuItem(command.command, title, command.iconResourceID));
        }
    }

    static class Plugin
    {
        final String name;
        final String version;
        final String author;

        Plugin(String name, String version, String author)
        {
            this.name = name;
            this.version = version;
            this.author = author;
        }
    }

    @Override
    public Command getKickCommand()
    {
        if (hasPlayerCommand("sm_kick")) // Check that sm_kick is installed
            return playerCommands.get("sm_kick");
        else
            // Default to Source Engine Ban Command
            return super.getKickCommand();
    }

    @Override
    public Command getBanCommand()
    {
        if (hasPlayerCommand("sm_ban")) // Check that sm_ban is installed
            return playerCommands.get("sm_ban");
        else
            // Default to Source Engine Ban Command
            return super.getBanCommand();
    }

    @Override
    public ChatTarget[] getChatTargets()
    {
        return null; // We have no new chat targets to add
    }

    @Override
    public boolean handlesChatTarget(ChatTarget target)
    {
        // BaseSourceServer adds this target, but when we handle it, we can have a
        // green message that stands out to players
        return target.type == ChatMessage.MessageType.NORMAL;
    }

    @Override
    public boolean sendChatMessage(String message, ChatTarget target, Server server)
    {
        server.sendInternalAdminCommand(String.format("sm_say %s", message), null);
        return true;
    }
}
