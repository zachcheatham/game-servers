package me.zachcheatham.gameservers.server.packet.source;

import java.nio.BufferUnderflowException;
import java.util.Map;

import me.zachcheatham.gameservers.server.packet.UnexpectedPacketException;

public class S2ARulesPacket extends SourcePacket
{
    public static final byte HEADER = 0x45;

    public S2ARulesPacket(byte[] data) throws UnexpectedPacketException
    {
        super(data);

        if (getHeader() != HEADER)
            throw new UnexpectedPacketException(HEADER, getHeader(), "S2ARules");
    }

    public void getRules(Map<String, String> rules)
    {
        short rulesCount = readShort();
        rules.clear();
        for (short i = 0; i < rulesCount; i++)
        {
            try
            {
                rules.put(readString(), readString());
            }
            catch (BufferUnderflowException ignored)
            {
            }
        }
    }
}
