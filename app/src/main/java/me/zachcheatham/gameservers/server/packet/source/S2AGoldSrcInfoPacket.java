package me.zachcheatham.gameservers.server.packet.source;

import me.zachcheatham.gameservers.server.packet.UnexpectedPacketException;

public class S2AGoldSrcInfoPacket extends SourcePacket
{
    public static final byte HEADER = 0x6D;

    public final String hostname;
    public final String map;
    public final String gameDirectory;
    public final String game;
    public final int numPlayers;
    public final int maxPlayers;
    public final int numBots;
    public final char serverType;
    public final char operatingSystem;
    public final boolean password;
    public final boolean isProtected;
    private final boolean runningMod;
    private final String modURL;
    private final String modDownloadURL;
    private final long modVersion;

    public S2AGoldSrcInfoPacket(byte[] data) throws UnexpectedPacketException
    {
        super(data);

        if (getHeader() != HEADER)
            throw new UnexpectedPacketException(HEADER, getHeader(), "S2AGoldSrcInfo");

        readString(); // Discarding server address
        hostname = readString();
        map = readString();
        gameDirectory = readString();
        game = readString();
        numPlayers = readUnsignedByte();
        maxPlayers = readUnsignedByte();
        readUnsignedByte(); // Discarding protocol version
        serverType = (char) readUnsignedByte();
        operatingSystem = (char) readUnsignedByte();
        password = readBoolean();
        runningMod = readBoolean();

        if (runningMod)
        {
            modURL = readString();
            modDownloadURL = readString();
            readByte();
            modVersion = readInt();
            readInt();
            readByte();
            readByte();
        }
        else
        {
            modURL = null;
            modDownloadURL = null;
            modVersion = -1;
        }
        isProtected = readBoolean();
        numBots = readUnsignedByte();
    }
}
