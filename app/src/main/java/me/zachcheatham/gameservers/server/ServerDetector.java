package me.zachcheatham.gameservers.server;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;

import me.zachcheatham.gameservers.events.ServerUpdatedEvent;

public class ServerDetector
{
    private final Server.ServerType[] types = new Server.ServerType[] {
            Server.ServerType.Source,
            Server.ServerType.Minecraft};

    private final String host;
    private final int port;

    private WeakReference<ServerDetectedListener> listenerWeakReference;
    private Server server;
    private int game = 0;

    public ServerDetector(String host, int port, ServerDetectedListener listener)
    {
        listenerWeakReference = new WeakReference<>(listener);
        this.host = host;
        this.port = port;
    }

    public void detect()
    {
        EventBus.getDefault().register(this);

        int port = this.port;
        if (port == 0)
            port = Server.getDefaultPort(types[game]);

        server = Server.create(host, port, types[game]);
        server.update(false);
    }

    public void stop()
    {
        this.game = Integer.MAX_VALUE - 1; // - 1 incase we're about to increment it.
        this.server.closeAllConnections();
        EventBus.getDefault().unregister(this);
        listenerWeakReference.clear();
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onServerUpdated(ServerUpdatedEvent event)
    {
        if (event.getServer() == server)
        {
            EventBus.getDefault().unregister(this);

            server.closeAllConnections();

            if (!server.offline)
            {
                if (types[game] == Server.ServerType.Source)
                {
                    checkSourceServer();
                }
                else
                {
                    notifyListener(types[game]);
                }
            }
            else
            {
                if (++game < types.length)
                    detect();
            }
        }
    }

    private void checkSourceServer()
    {
        SourceServer server = (SourceServer) this.server;
        if (server.gameAppId < 100)
            notifyListener(Server.ServerType.Gold_Src);
        else
            notifyListener(Server.ServerType.Source);
    }

    private void notifyListener(Server.ServerType type)
    {
        ServerDetectedListener listener = listenerWeakReference.get();
        if (listener != null)
            listener.serverGameTypeFound(type);
    }

    public interface ServerDetectedListener
    {
        void serverGameTypeFound(Server.ServerType type);
    }
}
