package me.zachcheatham.gameservers.server.packet.source;

import me.zachcheatham.gameservers.server.packet.Packet;

public class SourcePacket extends Packet
{
    public static final short PACKET_LENGTH = 1400;
    private final byte header;

    SourcePacket(byte header)
    {
        super(PACKET_LENGTH);

        this.header = header;

        buffer.put((byte) 0xFF);
        buffer.put((byte) 0xFF);
        buffer.put((byte) 0xFF);
        buffer.put((byte) 0xFF);
        if (header != 0)
            buffer.put(header);
    }

    SourcePacket(byte[] data)
    {
        this(data, true);
    }

    public SourcePacket(byte[] data, boolean hasHeader)
    {
        super(data);
        if (hasHeader)
            header = buffer.get();
        else
            header = 0;
    }

    public static String getPacketName(byte b)
    {
        switch (b)
        {
        case 0x41:
            return "S2AChallengePacket";
        case 0x44:
            return "S2APlayerPacket";
        case 0x45:
            return "S2ARulesPacket";
        case 0x49:
            return "S2AInfoPacket";
        case 0x54:
            return "A2SInfoPacket";
        case 0x55:
            return "A2SPlayerPacket";
        case 0x56:
            return "A2SRulesPacket";
        case 0x6D:
            return "S2AGoldSrcInfoPacket";
        case 0x6C:
            return "S2AGoldSrcRconResponse";
        default:
            return "Unknown";
        }
    }

    public byte getHeader()
    {
        return header;
    }
}
