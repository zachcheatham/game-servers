package me.zachcheatham.gameservers.web;

import java.io.IOException;

import timber.log.Timber;

public class PublicAddress extends WebAPI
{
    private static String ip = null;

    public static String getIP()
    {
        if (ip == null)
        {
            try
            {
                return getString("https://checkip.amazonaws.com").trim();
            }
            catch (IOException e)
            {
                Timber.w(e, "Unable to get public address.");
            }
        }

        return ip;
    }
}
