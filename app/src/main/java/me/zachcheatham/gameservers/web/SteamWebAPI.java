package me.zachcheatham.gameservers.web;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class SteamWebAPI extends WebAPI
{
    private static final String API_URL = "https://api.steampowered.com/";

    private static SteamWebAPI instance = null;

    public static SteamWebAPI getInstance()
    {
        if (instance == null)
            instance = new SteamWebAPI();

        return instance;
    }

    private JSONObject getData(String method, String args) throws IOException, JSONException,
            SteamAPIException
    {
        StringBuilder urlBuilder = new StringBuilder(API_URL);
        urlBuilder.append(method);
        if (args != null)
        {
            urlBuilder.append(args);
            urlBuilder.append('&');
        }
        else
            urlBuilder.append('?');

        urlBuilder.append("&format=json");

        JSONObject contents = getJSONObject(urlBuilder.toString());
        if (contents.has("response"))
        {
            return contents.getJSONObject("response");
        }
        else
        {
            throw new SteamAPIException("No response object in response.");
        }
    }

    public static class SteamAPIException extends Exception
    {
        SteamAPIException(String msg)
        {
            super(msg);
        }
    }
}
