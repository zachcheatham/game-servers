package me.zachcheatham.gameservers.web;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import me.zachcheatham.gameservers.server.player.SourcePlayer;
import timber.log.Timber;

public class SteamWebAPIRelay extends GSWebAPI
{
    private static final String METHOD_AVATARS = "/steam/avatar/";

    private static SteamWebAPIRelay instance = null;
    private Map<String, String> playerAvatarCache = new HashMap<>();

    public static SteamWebAPIRelay getInstance()
    {
        if (instance == null)
            instance = new SteamWebAPIRelay();

        return instance;
    }

    public Map<String, String> getAvatars(SourcePlayer[] players)
    {
        StringBuilder avatarsToFetch = new StringBuilder();
        Map<String, String> avatars = new HashMap<>();

        for (SourcePlayer player : players)
        {
            if (player != null)
            {
                String steamid64 = player.getSteamID64();
                if (playerAvatarCache.containsKey(steamid64))
                {
                    avatars.put(steamid64, playerAvatarCache.get(steamid64));
                }
                else
                {
                    avatarsToFetch.append(steamid64);
                    avatarsToFetch.append(",");
                }
            }
        }

        if (avatarsToFetch.length() > 0)
        {
            Timber.d(avatarsToFetch.toString());
            avatarsToFetch.deleteCharAt(avatarsToFetch.length()-1);
            try
            {
                JSONObject response = getJSONObject(METHOD_AVATARS, avatarsToFetch.toString());
                Iterator<String> it = response.keys();
                while (it.hasNext())
                {
                    String steamid64 = it.next();
                    String url = response.getJSONObject(steamid64).getString("url");
                    avatars.put(steamid64, url);
                    playerAvatarCache.put(steamid64, url);
                }
            }
            catch (IOException e)
            {
                Timber.w(e, "Unable to fetch avatars: ");
            }
            catch (JSONException e)
            {
                Timber.w(e, "Unable to fetch avatars: ");
            }
        }

        return avatars;
    }
}
