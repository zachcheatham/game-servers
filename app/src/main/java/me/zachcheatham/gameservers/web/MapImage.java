package me.zachcheatham.gameservers.web;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import me.zachcheatham.gameservers.Constants;
import me.zachcheatham.gameservers.server.ServerManager;
import timber.log.Timber;

public class MapImage extends GSWebAPI
{
    private static final String METHOD = "/map-image/";
    private static final String PREFERENCE_KEY = "map_images";

    private static MapImage instance = null;

    private final boolean saveCache;
    private final Map<String, String> cache = new HashMap<>();

    public static MapImage getInstance()
    {
        if (instance == null)
            instance = new MapImage();

        return instance;
    }

    private MapImage()
    {
        ServerManager manager = ServerManager.getInstance();
        if (manager != null)
        {
            load(manager.getContext());
            saveCache = true;
        }
        else
        {
            Timber.w("Created MapImage instance without a ServerManager instance in existence.");
            saveCache = false;
        }
    }

    public String getMapImageURL(String map, boolean cacheOnly)
    {
        if (cacheOnly || cache.containsKey(map))
        {
            String id = cache.get(map);
            if (id != null)
                return String.format("https://i.imgur.com/%s.jpg", id);
            else
                return null;
        }
        else
        {
            try
            {
                String id = getString(METHOD, map);

                cache.put(map, id);
                if (saveCache)
                    save();

                return String.format("https://i.imgur.com/%s.jpg", id);
            }
            catch (FileNotFoundException e)
            {
                cache.put(map, null);
                return null;
            }
            catch (IOException e)
            {
                Timber.w(e, "Unable to fetch map image: ");
                return null;
            }
        }
    }

    private void load(Context context)
    {
        SharedPreferences preferences =
                context.getSharedPreferences(Constants.PREFS_KEY_CACHE, Context.MODE_PRIVATE);

        String cachedJSON = preferences.getString(PREFERENCE_KEY, null);
        if (cachedJSON != null)
        {
            try
            {
                JSONObject jsonObject = new JSONObject(cachedJSON);
                for (Iterator<String> it = jsonObject.keys(); it.hasNext(); )
                {
                    String key = it.next();
                    cache.put(key, jsonObject.getString(key));
                }
            }
            catch (JSONException e)
            {
                Timber.w(e, "Unable to load MapImage cache from preferences: ");
            }
        }
    }

    private void save()
    {
        ServerManager manager = ServerManager.getInstance();
        if (manager != null)
        {
            Context context = manager.getContext();
            SharedPreferences preferences =
                    context.getSharedPreferences(Constants.PREFS_KEY_CACHE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            JSONObject jsonObject = new JSONObject();
            for (Map.Entry<String, String> entry : cache.entrySet())
            {
                if (entry.getValue() != null)
                {
                    try
                    {
                        jsonObject.put(entry.getKey(), entry.getValue());
                    }
                    catch (JSONException e)
                    {
                        Timber.w(e, "Couldn't cache map image: ");
                    }
                }
            }

            editor.putString(PREFERENCE_KEY, jsonObject.toString());
            editor.apply();
        }
    }
}
