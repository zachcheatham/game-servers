package me.zachcheatham.gameservers.web;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import timber.log.Timber;

public class MojangUniqueIDLookup extends WebAPI
{
    private static MojangUniqueIDLookup instance = null;
    private final Map<String, UUID> uniqueIDs = new HashMap<>();

    public static MojangUniqueIDLookup getInstance()
    {
        if (instance == null)
            instance = new MojangUniqueIDLookup();

        return instance;
    }

    public String getUniqueID(String name)
    {
        UUID uuid = uniqueIDs.get(name);

        if (uuid != null && uuid.isOld())
            uuid = null;

        if (uuid == null)
        {
            try
            {
                int timestamp = (int) (System.currentTimeMillis() / 1000L);

                JSONObject responseJSON = getJSONObject(String.format(Locale.US,
                        "https://api.mojang.com/users/profiles/minecraft/%s?at=%d",
                        URLEncoder.encode(name), timestamp));

                if (responseJSON.has("error"))
                {
                    Timber.w("Unable to retrieve UUID for %s: %s %s", name,
                            responseJSON.getString("error"),
                            responseJSON.getString("errorMessage"));
                }
                else
                {
                    String uniqueId = responseJSON.getString("id");
                    uuid = new UUID(uniqueId);
                    uniqueIDs.put(name, uuid);
                }
            }
            catch (MalformedURLException e)
            {
                Timber.w(e);
            }
            catch (IOException e)
            {
                Timber.w(e);
            }
            catch (JSONException e)
            {
                Timber.w(e);
            }
        }

        if (uuid == null)
            return null;
        else
            return uuid.uuid;
    }

    public void save(Context c)
    {
        // TODO
    }

    private class UUID
    {
        final String uuid;
        final int timestamp;

        UUID(String uuid)
        {
            this.uuid = uuid;
            timestamp = (int) (System.currentTimeMillis() / 1000L);
        }

        boolean isOld()
        {
            return ((int) (System.currentTimeMillis() / 1000L) - timestamp) >= 604800;
        }
    }
}
