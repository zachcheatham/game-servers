package me.zachcheatham.gameservers.web;

import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import me.zachcheatham.gameservers.BuildConfig;
import timber.log.Timber;

public abstract class GSWebAPI extends WebAPI
{
    private final static String API_URL = "https://gs.zachcheatham.me";
    private static MessageDigest digest = null;

    static {
        try
        {
            digest = MessageDigest.getInstance("SHA-256");
        }
        catch (NoSuchAlgorithmException e)
        {
            Timber.e(e ,"Unable to create SHA-256 digest object: ");
        }
    }

    public static String getString(String method, String data) throws IOException
    {
        data = URLEncoder.encode(data, "utf-8");
        String key = getAPIKey(method, data);
        String url = String.format("%s%s%s?key=%s", API_URL, method, data, key);
        return getString(url);
    }

    public static JSONObject getJSONObject(String method, String data) throws IOException,
            JSONException
    {
        data = URLEncoder.encode(data, "utf-8");
        String key = getAPIKey(method, data);
        String url = String.format("%s%s%s?key=%s", API_URL, method, data, key);
        return getJSONObject(url);
    }

    private static String getAPIKey(String method, String data)
    {
        String keyBuilder = data + method + BuildConfig.GS_API_KEY;
        return Base64.encodeToString(digest.digest(keyBuilder.getBytes()), Base64.URL_SAFE);
    }
}
