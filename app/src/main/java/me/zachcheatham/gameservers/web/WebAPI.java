package me.zachcheatham.gameservers.web;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public abstract class WebAPI
{
    public static String getString(String url) throws IOException
    {
        InputStream stream = new URL(url).openStream();
        InputStreamReader reader = new InputStreamReader(stream);
        StringBuilder response = new StringBuilder();
        int c;
        while ((c = reader.read()) != -1)
            response.append((char) c);

        reader.close();
        stream.close();

        return response.toString();
    }

    public static JSONObject getJSONObject(String url) throws IOException, JSONException
    {
        return new JSONObject(getString(url));
    }

    public static JSONArray getJSONArray(String url) throws IOException, JSONException
    {
        return new JSONArray(getString(url));
    }
}
