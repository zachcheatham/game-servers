package me.zachcheatham.gameservers.server;

import org.junit.Test;
import org.junit.Assert;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import androidx.test.filters.MediumTest;

@MediumTest
public class ServerDetectionTest implements ServerDetector.ServerDetectedListener
{
    private CountDownLatch latch = new CountDownLatch(1);
    private Server.ServerType type = null;

    @Test
    public void testMinecraftWithoutPort() throws InterruptedException
    {
        testServer("minesaga.org", 0, Server.ServerType.Minecraft);
    }

    @Test
    public void testMinecraftWithPort() throws InterruptedException
    {
        testServer("Chaoticmc.beastmc.com", 25585, Server.ServerType.Minecraft);
    }

    @Test
    public void testGoldSrcWithoutPort() throws InterruptedException
    {
        testServer("5.254.86.244", 0, Server.ServerType.Gold_Src);
    }

    @Test
    public void testGoldSrcWithPort() throws InterruptedException
    {
        testServer("138.197.15.155", 27020, Server.ServerType.Gold_Src);
    }

    @Test
    public void testSourceWithoutPort() throws InterruptedException
    {
        testServer("31.186.251.170", 0, Server.ServerType.Source);
    }

    @Test
    public void testSourceWithPort() throws InterruptedException
    {
        testServer("93.186.198.123", 28315, Server.ServerType.Source);
    }

    @Test
    public void testUndetectable() throws InterruptedException
    {
        testServer("185.50.105.209", 0, null);
    }

    @Test
    public void testOffline() throws InterruptedException
    {
        testServer("127.0.0.1", 0, null);
    }

    private void testServer(String address, int port, Server.ServerType expected) throws
            InterruptedException
    {
        ServerDetector serverDetector = new ServerDetector(address, port, this);
        serverDetector.detect();

        latch.await(5000, TimeUnit.MILLISECONDS);

        Assert.assertEquals(expected, type);
    }

    @Override
    public void serverGameTypeFound(Server.ServerType type)
    {
        this.type = type;
        latch.countDown();
    }
}
