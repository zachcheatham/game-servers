package me.zachcheatham.gameservers.server.admin;

import android.content.Context;

import com.google.gson.JsonElement;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.Map;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;
import androidx.test.platform.app.InstrumentationRegistry;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class ServerCommandLookupTest
{
    private Context context;

    @Before
    public void setup()
    {
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @Test
    public void testMinecraftDecode() throws IOException
    {
        KnownCommandLookup lookup = new KnownCommandLookup(
                context, KnownCommandLookup.MOD_IDENTIFIER_MINECRAFT);

        for (Map.Entry<String, JsonElement> entry : lookup.commands.entrySet())
            lookup.lookupCommand(entry.getKey());
    }

    @Test
    public void testMinecraftSimple() throws IOException
    {
        KnownCommandLookup lookup = new KnownCommandLookup(
                context, KnownCommandLookup.MOD_IDENTIFIER_MINECRAFT);

        Command banCommand = lookup.lookupCommand("ban");
        Assert.assertEquals("ban", banCommand.command);
        Assert.assertEquals("block", banCommand.iconName);

        CommandArgument arg1 = banCommand.arguments[0];
        Assert.assertEquals("name", arg1.id);
        Assert.assertEquals("string", arg1.type);
        Assert.assertTrue(arg1.playerArgument);

        CommandArgument arg2 = banCommand.arguments[1];
        Assert.assertEquals("reason", arg2.id);
        Assert.assertEquals("string", arg2.type);
        Assert.assertTrue(arg2.optional);
    }

    @Test
    public void testMinecraftMultipleDepend() throws IOException
    {
        KnownCommandLookup lookup = new KnownCommandLookup(
                context, KnownCommandLookup.MOD_IDENTIFIER_MINECRAFT);

        Command banCommand = lookup.lookupCommand("forceload");
        Assert.assertEquals("forceload", banCommand.command);

        CommandArgument arg1 = banCommand.arguments[0];
        Assert.assertEquals("action", arg1.id);
        Assert.assertEquals("list", arg1.type);
        Assert.assertArrayEquals(new String[]{"add", "remove", "query"}, arg1.listOptions);

        CommandArgument arg2 = banCommand.arguments[2];
        Assert.assertEquals("chunk", arg2.id);
        Assert.assertEquals("xz", arg2.type);
        Assert.assertArrayEquals(new String[]{"action", "remove"}, arg2.dependArgs);
        Assert.assertArrayEquals(new String[]{"remove", "false"}, arg2.dependArgsValue);

        CommandArgument arg3 = banCommand.arguments[4];
        Assert.assertEquals("query_chunk", arg3.id);
        Assert.assertEquals("xz", arg3.type);
        Assert.assertEquals("action", arg3.dependArg);
        Assert.assertEquals("query", arg3.dependArgValue);
    }

    @Test
    public void testMinecraftResources() throws IOException
    {
        KnownCommandLookup lookup = new KnownCommandLookup(
                context, KnownCommandLookup.MOD_IDENTIFIER_MINECRAFT);

        for (Map.Entry<String, JsonElement> entry : lookup.commands.entrySet())
        {
            Command command = lookup.lookupCommand(entry.getKey());
            checkCommand(command);
        }
    }

    @Test
    public void testSourceModDecode() throws IOException
    {
        KnownCommandLookup lookup = new KnownCommandLookup(
                context, KnownCommandLookup.MOD_IDENTIFIER_SOURCEMOD);

        for (Map.Entry<String, JsonElement> entry : lookup.commands.entrySet())
            lookup.lookupCommand(entry.getKey());
    }

    @Test
    public void testSourceModResources() throws IOException
    {
        KnownCommandLookup lookup = new KnownCommandLookup(
                context, KnownCommandLookup.MOD_IDENTIFIER_SOURCEMOD);

        for (Map.Entry<String, JsonElement> entry : lookup.commands.entrySet())
        {
            Command command = lookup.lookupCommand(entry.getKey());
            checkCommand(command);
        }
    }

    private void checkCommand(Command command)
    {
        Assert.assertTrue("Command Name: " + command.command, CommandResourceHelper.getCommandName(context, command) > 0);
        Assert.assertTrue("Command Description: " + command.command, CommandResourceHelper.getCommandDescription(context, command) > 0);

        if (command.hasPlayerArgument())
            Assert.assertNotNull("Command Icon: " + command.command, command.iconName);

        if (command.iconName != null)
            Assert.assertTrue("Icon Resource: " + command.iconName, command.iconResourceID > 0);

        if (command.arguments != null)
            for (CommandArgument argument : command.arguments)
                Assert.assertTrue("Argument Name: " + argument.id, CommandResourceHelper.getArgumentName(context, argument.id) > 0);
    }
}
